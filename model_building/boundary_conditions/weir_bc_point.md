---
title:  Weir BC
description: 
published: true
date: 2021-02-09T11:03:30.046Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:12.029Z
---




## Définition

Prélèvement d’un débit selon une loi de seuil dénoyé.

## Données de l'élément

<img src="/model_building/boundary_conditions/weir_bc_ui.png" width="400px"/>

## Commentaires

L’objet *Weir BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

