---
title:  Froude BC
description: 
published: true
date: 2021-11-26T15:08:20.547Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:42:08.148Z
---




## Définition

Condition à la limite aval  de type écoulement critique : Fr=1.

## Données de l'élément

<img src="/model_building/boundary_conditions/froude_ui.png" width="250px"/>

Aucune donnée attributaire.

## Equations

$\frac{Q_c/S}{\sqrt{g*\frac{S}{L}}}=0.9$.


Avec :

- $Q_c$ = débit
- $S$ = section mouillée de la section portant la condition limite
- $L$ = largeur au miroir de la section portant la condition limite

>Pour des raisons de stabilité numérique les calculs sont faits avec nombre de Froude = 0,9 au lieu de 1.

## Commentaires

L’objet *Froude BC* est une **liaison uninodale**. Il est en général utilisé comme condition limite aval d’une branche filaire: déversement libre

Il ne peut être posé que sur un containeur de type [manhole](/model_building/network/manhole_point) ou [river node](/model_building/river_free_surface_flow/river_node).


