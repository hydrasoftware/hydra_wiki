---
title:  Q(Z) BC
description: 
published: true
date: 2021-02-09T11:03:24.952Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:04.251Z
---




## Définition

Prélèvement d’un débit en fonction de la cote d’eau du containeur.

## Données de l'élément

<img src="/model_building/boundary_conditions/zq_bc_ui.png" width="450px"/>

La courbe q(z) doit être strictement croissante et il faut imposer Q(1)=0. 10 points au maximum.

## Commentaires

L’objet *Q(Z) BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Si le container est un nœud de branche filaire cet objet est généralement posé sur le nœud aval de la branche.

Cet objet peut être utilisé de façon particulière pour imposer une cote initiale le long d'un branche de collecteur ou d'un bief de rivière : Il faut pour cela poser cet objet sur le noeud aval de la branche et :

-définir une seule ligne dans le tableau Q(z),

-imposer  Q(1)=0. et Z(1)=Zini

Le programme imposera alors cette cote sur tous les noeuds de la branche en début de calcul, ce qui aura pour effet d'accélérer l'établissement du régime établi. L'objet est ignoré ensuite dans les calculs.



