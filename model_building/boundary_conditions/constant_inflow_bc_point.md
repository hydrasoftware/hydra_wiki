---
title:  Constant inflow
description: 
published: true
date: 2021-02-09T11:03:32.604Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:15.969Z
---




## Définition 

Injection d’un débit constant.

## Données de l'élément

<img src="/model_building/boundary_conditions/constant_inflow_ui.png" width="250px"/>

## Commentaires

L’objet *constant inflow* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Son utilisation reste assez limitée en pratique car ses fonctionnalités sont englobées dans celles de l’objet [hydrograph](/model_building/boundary_conditions/hydrograph), beaucoup plus riches.
