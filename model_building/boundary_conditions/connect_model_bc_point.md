---
title:  Modèle connexion
description: 
published: true
date: 2023-03-16T21:14:20.067Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:52.650Z
---




## Définition

Objet de raccordement hydraulique entre deux modèles d’un même projet.

## Données de l'élément

<img src="/model_building/boundary_conditions/connect_model_ui.png" width="450px"/>

Deux options sont possibles dans le mode de raccordement en cascade (c’est dire quand les modèles sont exécutés les uns après les autres) 

1.    Downstream conditionz(q)

L’objet se trouve sur un nœud du modèle amont. Il faut dans ce cas définir une condition aval de type z(q), comme illustré ci-dessus.

2.    Hydrograph

L’objet se trouve sur un nœud du modèle aval. Aucune donnée attributaire n’est nécessaire dans ce cas. Cet objet est transformé en objet [hydrograph](/model_building/boundary_conditions/hydrograph) lors de la phase exécution des calculs, il reçoit alors l’hydrogramme stocké dans l’objet *modèle connexion* du modèle amont.

>:warning:**Les objets des modèles amont et aval associés doivent porter le même nom.**

## Commentaires

L’objet *modèle connexion* est traité comme une liaison uninodale lorsque l’on est en mode de calcul *cascade*. 

