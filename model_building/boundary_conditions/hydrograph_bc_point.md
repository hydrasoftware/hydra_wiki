---
title:  Hydrograph
description: 
published: true
date: 2021-02-09T11:03:14.596Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:48.624Z
---




## Définition

Point d’injection hydrologique dans le réseau hydraulique.

## Données de l'élément

<img src="/model_building/boundary_conditions/hydrograph_ui.png" width="450px"/>

Trois groupes de données :

- *Storage area* 

Ce paramètre permet de prendre en compte la capacité d’amortissement du réseau hydrologique connecté à l’objet hydrograph, lorsque les apports dépassent la capacité d’écoulement du réseau hydraulique. 

Il est estimé comme suit : 

<img src="/model_building/boundary_conditions/hydrograph_eq1.png" width="150px"/>

où : 

S hydrol = section d’écoulement du collecteur  du le réseau hydrologique connecté au réseau hydraulique

m = pente moyenne du collecteur du réseau hydrologique.

- *Hydrogramme ruisselé*

Cet hydrogramme peut être défini dans l’élément ( comme ci-dessus) ou dans un fichier externe décarré dans le fenêtre de paramétrage du scénario.

- *Hydrogramme de temps sec*

Il peut être exprimé comme un simple débit de base constant ou calculé à partir des données de paramétrage d’un secteur d’apport. Le secteur d’apport est  délimité par une ligne polygonale associée à l’objet « dry inflow sector ». l’IHM sélectionne le secteur d’apport dont la ligne polygonale entoure  l’objet hydrograph . 
La mécanique de calcul de l’hydrogramme d’apport de temps sec est précisé à *« gestion des données hydrologiques »*.

L’hydrogramme injecté dans le réseau hydraulique via l’objet *hydrograph* résulte dans le cas le plus général de la superposition de trois hydrogrammes distincts :

- L’apport en provenance du réseau hydrologique connecté à l’objet *hydrograph*,
- L’hydrogramme ruisselé défini dans l’objet *hydrograph*
- L’hydrogramme de temps sec calculé à partir des paramètres définies dans l’objet *dry inflow sector*.
_Pollution
Deux boutons supplémentaires permettent de préciser les paramètres de pollution et de qualité à affecter aux hydrogrammes pour le calcul des pollutogrammes. [( cf chap 8 : pollution et qualité)](/pollution_qualite)

## Commentaires

L’objet *hydrograph* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).


