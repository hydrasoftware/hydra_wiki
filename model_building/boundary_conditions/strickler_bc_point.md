---
title:  Strickler BC
description: 
published: true
date: 2022-06-21T07:22:49.603Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:44.708Z
---




## Définition

Condition à la limite aval  de type écoulement en régime uniforme.

## Données de l'élément

<img src="/model_building/boundary_conditions/strickler_bc_ui.png" width="300px"/>

## Equations

<img src="/model_building/boundary_conditions/strickler_bc_eq1.png" width="200px"/>


S : surface mouillée = bxH (avec b largeur et H hauteur d'eau)

R : rayon hydraulique = H (avec H hauteur d'eau; il est considéré que b>>H)

m : pente

H : hauteur d’eau . Z - Zfond


## Commentaires

L’objet *Strickler BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Si le container est un nœud de branche filaire cet objet est généralement posé sur le nœud aval de la branche.

