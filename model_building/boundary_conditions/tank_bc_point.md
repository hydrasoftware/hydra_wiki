---
title:  Tank BC
description: 
published: true
date: 2021-02-09T11:03:22.379Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:00.373Z
---




## Définition

Réservoir

## Données de l'élément

<img src="/model_building/boundary_conditions/tank_bc_ui.png" width="450px"/>

La courbe S(z) est définie sous forme tabulée. 

Nombre Np de points =  20 au maximum.

On peut spécifier une cote initiale de remplissage du bassin.

A noter que cet élément ne contrôle pas les dépassements de cote d’eau :

- Si z > z(np)  S = S(np)
- Si z < z(1) :  S = S(1).

Le  bassin peut ainsi se vider en-deçà de la cote de fond. C’est à l’utilisateur de correctement définir des cotes de seuil de liaisons connectés au bassin pour garantir que la cote dans le bassin reste toujours supérieure à z(1).


## Commentaires

L’objet *Tank BC* est une **liaison uninodale**. Il peut être posé sur n’importe quel container, à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Il est le plus souvent utilisé pour modéliser les bassins de retention à l’intérieur d’une [station de gestion](/model_building/station/station_polygon).

Lorsque l'option pollution est activé dans le gestionnaire de scénario le bouton pollution de l'objet *tank* offre la possibilité de calculer un abattement de pollution de l'effluent stocké selon deux modes :

- taux d'abattement
- concentration résiduelle.

<img src="/model_building/boundary_conditions/tank_bc_pollution.png" width="450px"/>
