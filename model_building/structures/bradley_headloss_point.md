---
title:  Perte de charge Bradley (*Bradley headloss*)
description: 
published: true
date: 2021-02-09T11:04:42.032Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:58.918Z
---



## Définition

Perte de charge calculée selon la formule de Bradley au passage d’un pont.

## Données de l'élément

<img src="/model_building/structures/bradley_headloss.png" width="520px"/>

<img src="/model_building/structures/bradley_headloss_schema.png" width="320px"/>

## Equations

Les équations régissant la perte de charge au franchissement du pont sont basées sur la formulation de Bradley : *Hydraulics of bridge Waterways* 

<img src="/model_building/equations/bradley/eq_1.png" width="260px"/>

<img src="/model_building/equations/bradley/eq_2.png" width="260px"/>

E1 et E1 sont les charges hydrauliques de part et d’autre du pont. Les termes de vitesse sont pondérés par des coefficients cinétiques pour prendre en compte les vitesses d'écoulement différentes dans le lit mineur et le lit majeur en cas de débordement. 

U est la vitesse moyenne d'écoulement occupée la section libre du pont en supposant l'écoulement non pertubé.

Lorsque la cote d’eau amont dépasse Zv (*Z ceiling*), les équations de Bradley sont conservée avec une section de passage constante au droit du pont.

La distance Dg/Dd est négative si la culée rentre dans le lit mineur.

## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un container de type **noeud de rivière** (<img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point)).

