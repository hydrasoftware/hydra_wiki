---
title:  Perte de charge Borda (*Borda headloss*)
description: 
published: true
date: 2021-02-09T11:04:28.898Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:39.193Z
---



## Définition

Perte de charge de type Borda.

Cet objet propose un panel de 13 lois différentes de pertes de charge selon la géométrie de la singularité :

<img src="/model_building/structures/borda_headloss.png" width="750px"/>

La perte de charge est exprimée pour chaque loi sous la forme générale :

<img src="/model_building/equations/borda/definition/borda_headloss_eq1.png" width="100px"/>

où :

<img src="/model_building/equations/borda/definition/borda_headloss_eq2.png" width="350px"/>

dE étant la **perte de charge** de part et d’autre de la singularité

et u la **vitesse de référence** (en général vitesse dans le section amont ou aval de la singularité selon le type de loi sélectionnée)


Le champ *discharged for headloss computation* ne concerne que les objets posés sur un noeud de rivière :

- si l'option *full section* est activée la perte de charge est calculée par rapport au débit total au noeud amont.


- si l'option *river only* est activé la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.


L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement :  


La singularité Borda est obligatoirement posée sur un [regard](/model_building/network/manhole_point) ou un [noeud de bief filaire](/model_building/river_free_surface_flow/river_node_point), et donc à la limite entre les sections amont et aval de collecteurs de géométries différentes. 

Les termes E1 et E2 intègrent donc bien les vitesses d’écoulement qui peuvent être différentes de part et d’autre de la singularité :


## Données et équations spécifiques à chaque loi

### Loi 1 : dE=kQ² 

<img src="/model_building/equations/borda/loi_1/borda_headloss_eq3.png" width="370px"/>

Pour cette loi uniquement, la loi de perte de charge est exprimée en négligeant les termes cinétiques attachés aux sections amont et aval.

Le coefficient K est rentré dans l’unité : m/(m3/s)²

### Loi 2 : dE=(K/2g)Q²/S²

<img src="/model_building/equations/borda/loi_2/borda_headloss_eq4.png" width="370px"/>

Il faut définir :

- La référence à la section S : tronçon amont ou aval,
- Le coefficient k associé à chaque sens d’écoulement ( sans dimension)


### Loi 9 : Sharp geometry transition

<img src="/model_building/equations/borda/loi_9/borda_headloss_eq5.png" width="370px"/>

<img src="/model_building/equations/borda/loi_9/borda_headloss_eq6.png" width="530px"/>

### Loi 10 : Contraction with transition

<img src="/model_building/equations/borda/loi_10/borda_headloss_eq7.png" width="450px"/>

<img src="/model_building/equations/borda/loi_10/borda_headloss_eq8.png" width="530px"/>


### Loi 11 : Enlargment with transition

<img src="/model_building/equations/borda/loi_11/borda_headloss_eq9.png" width="450px"/>

<img src="/model_building/equations/borda/loi_11/borda_headloss_eq10.png" width="530px"/>


### Loi 12 : Circular bend

<img src="/model_building/equations/borda/loi_12/borda_headloss_eq11.png" width="450px"/>

<img src="/model_building/equations/borda/loi_12/borda_headloss_eq12.png" width="530px"/>

Note :  les pertes de charge ci-dessus sont calculées pour une conduite circulaire

### Loi 13 : Sharp angle bend

<img src="/model_building/equations/borda/loi_13/borda_headloss_eq13.png" width="450px"/>

<img src="/model_building/equations/borda/loi_13/borda_headloss_eq14.png" width="530px"/>


### Loi 14 : Screen normal to flow

<img src="/model_building/equations/borda/loi_14/borda_headloss_eq15.png" width="450px"/>

<img src="/model_building/equations/borda/loi_14/borda_headloss_eq16.png" width="530px"/>

Coefficient de blogage = taux d’obstruction = 1. – porosité = 1.- (a/s).

### Loi 15 : Screen oblique to flow

<img src="/model_building/equations/borda/loi_15/borda_headloss_eq17.png" width="450px"/>

<img src="/model_building/equations/borda/loi_15/borda_headloss_eq18.png" width="530px"/>

### Loi 16 : Friction

<img src="/model_building/equations/borda/loi_16/borda_headloss_eq19.png" width="450px"/>

<img src="/model_building/equations/borda/loi_16/borda_headloss_eq20.png" width="530px"/>

### Loi 17 : Parametric headloss

<img src="/model_building/equations/borda/loi_17/borda_headloss_eq21.png" width="450px"/>

<img src="/model_building/equations/borda/loi_17/borda_headloss_eq22.png" width="530px"/>

### Loi 18 : Sharp bend rectangular

<img src="/model_building/equations/borda/loi_18/borda_headloss_eq23.png" width="450px"/>

<img src="/model_building/equations/borda/loi_18/borda_headloss_eq24.png" width="530px"/>

<img src="/model_building/equations/borda/loi_18/borda_headloss_eq25.png" width="530px"/>

<img src="/model_building/equations/borda/loi_18/borda_headloss_eq27.png" width="530px"/>

