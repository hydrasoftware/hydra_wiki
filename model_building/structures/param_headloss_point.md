---
title:  Perte de charge paramétrique (*Parametric headloss*)
description: 
published: true
date: 2021-02-09T11:04:26.175Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:35.328Z
---



## Définition

Loi paramétrique de variation de cote d’eau en fonction du débit.

## Données de l'élément

<img src="/model_building/structures/param_headloss.png" width="470px"/>

Conditions à respecter :

- Le nombre de points doit être au plus égal à 10.
- La courbe dh(Q) doit être monotone et strictement croissante.
- dz(1)=Q(1)=0 obligatoirement.

Le champ *discharged for headloss computation* ne concerne que les objets posés sur un noeud de rivière :

- si l'option *full section* est activée la perte de charge est calculée par rapport au débit total au noeud amont.


- si l'option *river only* est activé la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.


L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement :  



## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- [Manhole](/model_building/network/manhole_point)

- [River node](/model_building/river_free_surface_flow/river_node_point)


