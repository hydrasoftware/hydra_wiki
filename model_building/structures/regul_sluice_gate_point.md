---
title:  Vanne régulée (*Regulated sluice gate*)
description: 
published: true
date: 2024-02-13T13:37:12.520Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:07.088Z
---



## Définition

Vanne régulée en cote ou en débit

## Données et schéma de définition

### 1. Données générales

<img src="/model_building/structures/regul_sluice_gate.png" width="470px"/>

La vanne est définie par :

- la **géométrie** du cadre (*Z ceiling*, *Z invert*, *Width*) 
- la position de la vanne (*Z gate*) qui détermine la **section d’ouverture**,
	- des butées hautes et basses peuvent être définies pour l'ouvertre de la vanne : *z ceiling stop* et *z invert stop*
-  un **coefficient de vanne en régime dénoyé** (*Gate coefficient*).

La vanne peut être déplacée selon deux modes :

- déplacement **vers le haut** (*Upward opening*) : la section d’ouverture augmente quand on lève la vanne,
- déplacement **vers le bas** (*Downward opening*) : la section d’ouverture augmente quand on abaisse la vanne.

La position de la vanne (*Z gate*) peut être modifiée durant une simulation via le module externe de contrôle et régulation. Le paramètre  *V max* règle dans ce cas la vitesse maximum de déplacement du seuil de la vanne.

Le paramètre *gate coefficient* désigne le coefficient  Cd de vanne en régime dénoyé. 

Si l’option *submerged coef.* n’est pas cochée le programme gère le calcul du coefficient de vanne Cn en régime noyé qui varie en fonction du rapport de section entre le point aval et la section mouillée au droit de la vanne.

Si l’option *submerged coef.* est cochée  l’utilisateur doit renter une valeur fixe pour le coefficient Cn. Par défaut ce coefficient est égal à 1.0

Le champ *discharged for headloss computation* ne concerne que les objets posés sur un noeud de rivière :


- si l'option *full section* est activée la perte de charge est calculée par rapport au débit total au noeud amont.


- si l'option *river only* est activé la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.


L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement :  


### 2. Données de régulation

3 modes de régulation sont proposés :




#### a. Régulation en cote

<img src="/model_building/structures/regul_sluice_gate_elevation.png" width="490px"/>

Il faut sélectionner le nœud de contrôle pour lequel la régulation en côte est appliquée en cliquant sur le bouton *Select node* puis sur le nœud souhaité dans le modèle.

**Algorithme de régulation**

La variation du débit de consigne à travers la vanne est donnée par :

$$
\Delta q = k_1 e + k_2 \int e dt + k_3 \frac{de}{dt}
$$


où $e$ est l'écart entre la cote au point de consigne et la cote de consigne

Le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (z1, z2) figés.

La consigne est rafraîchie au pas de temps donné par `dt_reg`.

En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.


**Ajustement des paramètres de régulation**

Les valeurs adoptées en première approximation sont fournies ci-après pour les quelques configurations types les plus souvent rencontrées en pratique. 

Sur les schémas qui suivent, le point de consigne correspond au point c.

*Configuration 1* : Point de consigne localisé à l’aval du régulateur (vanne au point R)

<img src="/model_building/structures/regul_sluice_gate_z1.png" width="470px"/>

Dans cette configuration, une augmentation du débit q produit une augmentation de la cote en C. On suppose par ailleurs que la cote en C est fortement dépendante du débit en ce point.

On peut choisir les paramètres comme suit :

$
k_1 \approx - \left(\alpha +\frac{S}{\Delta t} \right)
$
$
k_2 \approx - S
$
$
k_3 \approx - \frac{S}{\Delta t ^2}
$


Avec $S$ surface au miroir traduisant la capacité d’emmagasinement du réseau entre les points R et C et $
\alpha \approx \frac{dq}{dz}
$ au point C

Exemple pour un collecteur, lorsque le point C est situé à proximité immédiate de R  :

- $k_2 = k_3 = 0$
- $k1 = - LV$

Où $L$ est la largeur au miroir et $V$ la vitesse moyenne de l’écoulement

*Configuration 2* : Point de consigne situé à l’amont du régulateur (vanne au point R)

<img src="/model_building/structures/regul_sluice_gate_z2.png" width="470px"/>

On suppose dans ce cas que le débit q est directement influencé par la cote en C.

$
k_1 \approx \frac{S}{ \Delta t}
$

$
k_2 \approx S
$

$
k_3 = \frac{S}{\Delta t^2}
$

*Configuration 3* : Actionneur situé en dérivation du point de consigne

<img src="/model_building/structures/regul_sluice_gate_z3.png" width="300px"/>

Les lignes du coefficient sont les mêmes que pour la configuration 2.

S’il existe une forte dépendance entre le débit $q_c$ et la cote $z_c$, il faut modifier l’expression pour $k_1$.

$$
k_1 \approx \left( \alpha + \frac{S}{\Delta t}\right)
~\mathrm{avec}~
\alpha = \frac{\Delta q_c}{\Delta z_c}
$$

Les expressions définies ci-dessus ne donnent que des ordres de grandeur. 

Il faut ensuite affiner les valeurs empiriquement par essais successifs afin d’obtenir la qualité de régulation désirée.

*Remarques* : 

- Les temps de la courbe consigne doivent être strictement croissant.

- En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.

#### b. Régulation en débit

<img src="/model_building/structures/regul_sluice_gate_discharge.png" width="490px"/>

  **Algorithme de régulation :**

   La courbe de consigne est le débit de consigne à travers la vanne ; cette consigne peut être réactualisée à chaque pas de temps :

<img src="/model_building/structures/regul_sluice_gate_q2.png" width="490px"/>

   Le point de consigne est fixé au droit de la vanne et ne peut être modifié par l’utilisateur.

   La cote critique est la cote d’eau que l’on s’interdit de dépasser en amont de la vanne. Lorsque cette cote est atteinte la consigne de débit ne peut plus être respectée et la vanne est progressivement ouverte.

   La régulation est rafraîchie tous les pas de temps ; le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (Z1, Z2) figés.

   En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.

#### c. Pas de régulation

<img src="/model_building/structures/regul_sluice_gate_no_regul.png" width="470px"/>

La sélection de  ce mode équivaut à sélectionner l’objet [S_gate](/model_building/structures/sluice_gate). 

Les fonctionnalité de l’objet *s_regulated_gate* couvrent donc celles de l’objet *s_gate*. Cependant l’objet *s_gate* est conservé pour des raisons de compatibilité avec le code de calcul.


#### d. Vanne de chasse et seuil basculants

<img src="/model_building/structures/regul_sluice_gate_relief.png" width="470px"/>

Afin de simuler le comportement des vannes de chasses et seuils busculants, le mode de régulation `Relief` permet de définir un seuil haut d'ouverture rapide (i.e. à la vitesse $V_{max}
$ et un seuil bas de fermeture rapide.

Si la charge amont (en m) dépasse le seuil haut, la vanne s'ouvre jusqu'à ce que la charge amont passe en dessous du seuil bas.

#### e. Vanne de stockage en réseau


<img src="/model_building/structures/regul_sluice_gate_head.png" width="470px"/>


<img src="/model_building/structures/regul_sluice_gate_qh.png" width="470px"/>

Les modes `Head` et `Q(h)` visent à reproduire le comportement de vannes de stockage en réseau.

Le mode `Head` définit une régulation sur de différentiel de côte entre l'amont et l'aval de la vanne.

Le mode `Q(h)` reproduit le fonctionnement d'une vanne définie par sa courbe de tarrage. 

**Remarque**

- La définition des cofficients de régulation $k_1$, $k_2$ et $k_3$ suit la même logique que pour la régulation en cote .

- Pour le mode `Q(h)` la courbe est inversée et un algorythme de régulation en cote `h(Q)` est utilisé.



#### Régles de passage d'un mode de régulation à l'autre

Un mode de régulation défini dans l'objet peut être modifié dynamiquement en cours de calcul via un fichier de contrôle déclaré dans le gestionnaire de scénario (cf [chapitre 10.5 Menu Régulation](/scenarios)).

Les règles de passage d'un mode à l'autre sont précisées dans le tableau suivant :


<img src="/model_building/equations/modes_regulation.png" width="550px"/>


## Equations


On considère la configuration générale suivante : 

<img src="/model_building/equations/schema_principe_gate.png" width="600px"/>

La vanne est schématisée par une géométrie rectangulaire de largeur b et de hauteur d’ouverture (zv-zs). La largeur de la vanne b peut être différente de la largeur du canal. 

### Les régimes d’écoulements

Les différents régimes d’écoulement sont délimités par le diagramme suivant :

<img src="/model_building/equations/regimes_ecoulement.png" width="490px"/>


les paramètres zs et zv sont définis comme suit selon le mode d'action de la vanne :

<img src="/model_building/equations/config.png" width="450px"/>


l'énergie spécifique en amont et en aval de la vanne est définie comme suit :

<img src="/model_building/equations/energie.png" width="170px"/>


### Equations appliquées pour chaque régime lorsque E1 >E2


*Loi de déversoir dénoyé DD*

<img src="/model_building/equations/eq_qdd.png" width="200px"/>

*Loi de déversoir noyé DN*

<img src="/model_building/equations/eq_qdn.png" width="500px"/>


*Loi de vanne dénoyée VD*

<img src="/model_building/equations/eq_qvd.png" width="350px"/>


*Loi de vanne noyée-dénoyée VND*

<img src="/model_building/equations/eq_qvnd.png" width="500px"/>


*Loi de vanne noyée VN*

<img src="/model_building/equations/eq_qvn.png" width="300px"/>

### Equations appliquées pour chaque régime lorsque E2 >E1     


*Loi de déversoir dénoyé DD*

<img src="/model_building/equations/eq_qdd_2.png" width="200px"/>


Loi de déversoir noyé DN*

<img src="/model_building/equations/eq_qdn_2.png" width="500px"/>


*Loi de vanne dénoyée VD*

<img src="/model_building/equations/eq_qvd_2.png" width="350px"/>


*Loi de vanne noyée-dénoyée VND*

<img src="/model_building/equations/eq_qvnd_2.png" width="500px"/>


*Loi de vanne noyée VN*

<img src="/model_building/equations/eq_qvn_2.png" width="300px"/>


## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- [Manhole](/model_building/network/manhole_point)

- [River node](/model_building/river_free_surface_flow/river_node_point)


Les équations ci-dessus respectent la continuité des débits à la frontière entre deux régimes d’écoulement. Elles sont dérivables analytiquement à partir des lois générales de l’hydraulique pour les régimes : DD, VD et VN. Les formes des équations mises en œuvre pour les autres régimes sont définies de façon à satisfaire la continuité des débits entre deux régimes.

Le coefficient Cd est le coefficient de seuil déversant en régime dénoyé. Il est défini par l’utilisateur, il est généralement égal à sa valeur théorique : 0.6. L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque b/b0<1, où b0 est la largeur du canal ou du collecteur en amont de la vanne. On recommande d’appliquer le coefficient.de correction « alp » suivant sur CD :

<img src="/model_building/equations/tableau1.png" width="200px"/>

Le coefficient CN est le coefficient de seuil déversant ou de vanne en régime noyé. Il est par défaut sélectionné automatiquement par le programme qui tient compte du rapport de section s/S, où :
- s est la section d’ouverture de la vanne,
- S est la section mouillée dans le collecteur.

<img src="/model_building/equations/tableau2.png" width="200px"/>

Ce tableau résulte d’une analyse spécifique hydraulique basée sur la formulation de perte de charge à la Borda. 
L’utilisateur peut également imposer une valeur de son choix pour le coefficient Cn.


