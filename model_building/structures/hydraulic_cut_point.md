---
title:  Coupure hydraulique (*Hydraulic cut*)
description: 
published: true
date: 2022-07-08T13:16:56.817Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:47.249Z
---



## Définition

Coupure hydraulique 

## Données de l'élément

<img src="/model_building/structures/hydraulic_cut.png" width="440px"/>

Conditions à respecter :

- Le nombre de points de la courbe Z(Q) doit être au plus égal à 10.
- La courbe z(Q) doit être monotone et strictement croissante.

## Equations

<img src="/model_building/structures/hydraulic_cut_eq1.png" width="240px"/>

Za= Z(Q) si Zc < Z(Q)
Za=Zc si Zc>Z(Q)

## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- **regard** (<img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point))

- **noeud de rivière** (<img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point)).


En pratique, l’utilisation de cet objet reste très limitée car des coupures automatiques sont générées par défaut par le programme de calcul à chaque point de discontinuité de géométrie. 

Ces coupures permettent de gérer les écoulements dénoyés en cas de rupture de cote altimétrique entre deux tronçons.
