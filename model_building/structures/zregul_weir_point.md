---
title:  Déversoir frontal (Weir)
description: 
published: true
date: 2022-01-26T10:26:49.973Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:43.188Z
---



## Définition

Seuil frontal régulé.


Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- [Manhole](/model_building/network/manhole_point)

- [River node](/model_building/river_free_surface_flow/river_node_point)


## Données et schéma de définition

![weir_ui.svg](/model_building/equations/weir/weir_ui.svg)

### Géométrie
Le seuil déversant est défini par :

- la cote **Z regul** : cote de régulation égale à la cote du  seuil mobile lorsque le débit est nul (ou négatif).
- la cote **Z invert** : cote basse de seuil fixe
- **Width** : largeur (m)

Pour représenter un *seuil fixe*, définir $Z regul = Z invert$

### Coefficient de seuil
**Weir coefficient** correspond au coefficient de seuil dénoyé.


### Régulation
Lorsque $Z regul > Z invert$, et lorsque le débit est positif (sens amont vers aval), le seuil mobile se règle à chaque pas de temps pour maintenir la cote d’eau à la valeur *Z regul* en amont immédiat du seuil.

L'option **Regulation mode** permet de choisir entre deux modes de fontionnement du seuil mobile:
- Mode **Elevation** : regulation par abaissement du seuil (clapet) : le niveau d’eau est maintenu tant que le seuil mobile reste supérieur à la cote *Z invert*. Pour les forts débits, le seuil mobile est complètement couché et la loi de seuil est réglée par rapport à la cote *Z invert*.
- Mode **Width** : regulation  par ajustement de la largeur du seuil pour maintenir la cote d’eau *Z regul*. La largeur du seuil est dans ce cas variable et varie entre 0 et la valeur *Witdth*.

La cote de régulation *Z regul* peut être modifiée durant une simulation via le **module externe de contrôle et régulation**. On peut également figer le seuil mobile à une cote donnée.

### Mode de calcul de la perte de charge (rivière)
Le champ **Discharged for headloss computation** ne concerne que les objets posés sur un noeud de rivière :
- si l'option **full section** est activée la perte de charge est calculée par rapport au débit total au noeud amont.
- si l'option **river only** est activée la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.


L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement.  


### Qualité
Le bouton **quality** permet de définir les paramètres de réoxygénation de la chute d’eau créée par le seuil lorsque l’option de calcul *physico-chemical* est sélectionnée dans le gestionnaire de scénarios.





## Equations

Les équations font référence aux points 1 amont de l'écoulement et 2 aval de l'écoulement, avec :
- $V_1$ et $V_2$ vitesses de l'écoulement dans l'axe de la liaison (m/s)
- $Z_1$ et $Z_2$ ligne d'eau (m)
- $E_1$ et $E_2$ charge (m)
- $g$ accélération de la pesanteur $9.81m/s^-2$.

Les équations supposent $E_1 > E_2$ (sinon l'écoulement se fait en sens inverse) et $E_1 > z_s$, sinon il n'y pas d'écoulement.

Les différents régimes d'écoulement (dans les deux sens) sont définis sur le ldiagrame ci-dessous.

![weir_equations.svg](/model_building/equations/weir/weir_equations.svg)


**Expression de la charge :**

- $E_1 = z_1 + \frac{v_1^2}{2 g}$.
- $E_2 = z_2 + \frac{v_2^2}{2 g}$.


**Condition seuil noyé:** $(E_2-z_s) > \frac{2}{3} (E_1-z_s)$

<br/>

| Régime de fonctionnement   |     Equation                                     |
|:---------------------------|:-------------------------------------------------|
| Déversoir dénoyé | $QD_d = \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-z_s)^\frac{3}{2}$.  |
| Déversoir noyé | $QD_n = C_n b_s \sqrt{2 g} (E_2-z_s)(E_1-E_2)^\frac{1}{2}$.  |

>La **stabilité numérique** impose certaines hypothèses dans le calcul des charges amont et aval :
>
>- La vitesse n'est pas prise en compte dans le calcul de la charge aval pour la loi de seuil noyé : $E_2=z_2$.
>
>- L'expression de la vitesse pour la charge amont passe par le nombre de Froude qui est plafonné à 0.7 : 
> 
>    $v=fr*\sqrt(g*b/s)$ avec 
>    - $b$ : largeur du lit mineur au miroir 
>    - $s$ : section mouillée du lit mineur
>    - $fr$ : nombre de Froude tel que $fr=max(0.7; q/s/\sqrt(g*b/s))$
>
>        avec $q$ : débit transitant dans l'ouvrage
>        *pour le sections de type Valley: débit du lit mineur pour l'option river bed only et débit total pour l'option full section*

<br/>

## Paramètres

Le **coefficient de seuil déversant en régime dénoyé** $Cd$ (*Weir coefficient* de l'interface) est défini par l’utilisateur, il est généralement égal à sa valeur théorique: 0.58.
L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque $\frac{b}{b0}<1$, où $b0$ est la largeur du canal, du collecteur ou du lit mineur de rivière pour la hauteur d'eau considérée en amont de la vanne. On recommande d’appliquer le coefficient de correction « alp » suivant sur $Cd$:

| b/b0   |     alp = Cd/0.6         |
|:-------|:-------------------------|
| 1      |   1                      |
| 0.8    |   0.8                    |
| 0.6    |   0.6                    |
| 0      |   0.6                    |


Le **coefficient de seuil déversant ou de vanne en régime noyé** $Cn$ est égal à $1.73*alp*Cd$; *alp* est un facteur de correction qui tient compte du rapport de section s/S où :
- s est la section sur le seuil calculée sous la cote d'eau aval,
- S est la section mouillée dans le collecteur aval de la liaison.

| s/S   |     alp                    |
|:-------|:-------------------------|
| 0      |   1.0                    |
| 0.3    |   1.0                    |
| 0.6    |   1.0                    |
| 0.8    |   1.87                   |
| 0.9    |   3.12                   |
| 0.95   |   5                      |

*Sans correction, $Cn ~ 1$ dans le cas où $Cd=0.58$*


>**Note**
>La formulation retenue pour le déversoir dénoyé est la même que pour celle de la vanne *Gate* en régime *Déversoir dénoyé*.
>
>Toutefois le critère de passage du régime dénoyé en régime noyé et la définition du coefficient Cn sont différents:
>- Condition de passage en régime noyé :
>    - Pour les Weir : $(E_2-z_s) > \frac{2}{3} (E_1-z_s)$
>    - Pour les Gate : $E_2 \geq z_s$
>- Le coefficient Cn:
>    - Pour les Weir : $Cn = 1.73*Cd*alp$ avec alp fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps),
>    - Pour les Gate : formulation indépendante de $Cd$ mais fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps). L'utilisateur peut cependant imposer une valeur fixe.
>
>La simplification faite pour les Gate est rendue nécessaire pour satisfaire la continuité de débit entre tous les régimes qui sont beaucoup plus nombreux que dans le cas du Weir. Le critère de transition entre l’écoulement noyé et l’écoulement dénoyé est considéré comme physiquement plus précis dans la formulation Weir.



