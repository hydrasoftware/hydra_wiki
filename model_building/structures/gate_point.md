---
title: Vanne manuelle (Gate)
description: 
published: true
date: 2024-08-19T14:20:48.032Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:02.907Z
---



## Définition

Vanne manuelle.

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- [Manhole](/model_building/network/manhole_point)

- [River node](/model_building/river_free_surface_flow/river_node_point)



## Données et schéma de définition

<img src="/model_building/structures/s_gate.png" width="490px"/>

### Géométrie
La vanne est définie par :
- la **géométrie du cadre** (*Z ceiling*, *Z invert*, *Width* exprimée en m) 
- la position de la vanne mobile (*Z gate*) qui détermine la **section d’ouverture**

<br/>

#### Type de vanne et régulation
**Action mode** définit le mode de déplacement de la vanne :
- Déplacement **vers le haut** (*Upward opening*) : la section d’ouverture augmente quand on lève la vanne,
- Déplacement **vers le bas** (*Downward opening*) : la section d’ouverture augmente quand on abaisse la vanne.

La position de la vanne (*Z gate*) peut être modifiée durant une simulation via le module externe de contrôle et régulation. Le paramètre  *V max* règle dans ce cas la vitesse maximum de déplacement du seuil de la vanne.

**Mode valve type** permet de munir la vanne d'un clapet :
- *No valve* : pas de clapet
- *One way valve (toward downstream)* : clapet ouvert dans le sens amont - aval
- *One way valve (toward upstream)* : clapet ouvert dans le sens aval - amont

<br/>

### Coefficients de vanne
Les coeffciients suivants interviennent dans les équations associées à la vanne:
- Un **coefficient de vanne en régime dénoyé** (*Gate coefficient* Cd)
- Un **coefficient de vanne en régime dénoyé** (*submerged coefficient* Cn) 
    - Si l’option *submerged coef.* n’est pas cochée le programme gère le calcul du coefficient de vanne Cn en régime noyé qui varie en fonction du rapport de section entre le point aval et la section mouillée au droit de la vanne.
    - si l’option *submerged coef.* est cochée, l’utilisateur doit rentrer une valeur fixe pour le coefficient Cn. 
    

### Mode de calcul de la perte de charge (rivière)
Le champ **Discharged for headloss computation** ne concerne que les objets posés sur un noeud de rivière :
- si l'option **full section** est activée la perte de charge est calculée par rapport au débit total au noeud amont.
- si l'option **river only** est activée la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.


L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement.  


<br/>

## Equations

Les équations font référence aux noeuds 1 et 2 que relie la liaison, avec:
- $V_1$ et $V_2$ vitesses de l'écoulement dans l'axe de la liaison (m/s)
- $Z_1$ et $Z_2$ ligne d'eau (m)
- $E_1$ et $E_2$ charge (m)
- g accélération de la pesanteur $9.81m/s^-2$

Les équations supposent $E_1 > E_2$, sinon l'écoulement se fait en sens inverse et $E_1 > z_s$, sinon il n'y pas d'écoulement.

Les différents régimes d'écoulement (dans les deux sens) sont définis sur le ldiagrame ci-dessous.

![gate_link_equations.svg](/model_building/equations/gate/gate_link_equations.svg)


**Expression de la charge :**

- $E_1 = z_1 + \frac{v_1^2}{2 g}$.
- $E_2 = z_2 + \frac{v_2^2}{2 g}$.

La **vitesse prise en compte** pour le calcul de la charge dépend du type d'élément et de la position de la liaison:
- Casier, carrefour, noeud 1D hors extrémité de *branch* ou de *reach* (maillage latéral) : v = 0
- Noeud 1D en extrémité de *branch* ou de *reach* : vitesse sur le noeud
- Maille 2D : composante de la vitesse projetée sur l'axe de la liaison

**Condition seuil noyé:** $E_2 \geq z_s$

**Condition de passage seuil -> vanne**  $E_1 \geq z_v$ :
- vanne dénoyée $E_2 < z_s$
- vanne noyée-dénoyée $z_s < E_2 < z_v$
- vanne noyée $E_2 > z_v$

<br/>
<br/>


| Régime de fonctionnement *(1 > 2)*  |     Equation                                     |
|:---------------------------|:-------------------------------------------------|
| Déversoir dénoyé | $QD_d = \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-z_s)^\frac{3}{2}$.|
| Déversoir noyé | $QD_n =  \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-E_2)^\frac{3}{2} + C_n b_s \sqrt{2 g} (E_2 - z_s)(E_1 - E_2)^\frac{1}{2}$.  |
| Vanne dénoyée | $QV_d = \frac{2}{3} C_d b_s \sqrt{2 g}  \left[ (E_1-z_s)^\frac{3}{2} - (E_1 - z_v)^\frac{3}{2} \right]$.  |
| Vanne noyée-dénoyée | $QV_{nd} = \frac{2}{3} C_d b_s \sqrt{2 g} \left[ (E_1-E_2)^\frac{3}{2} - (E_1 - z_v)^\frac{3}{2} \right] + C_n b_s \sqrt{2 g} (E_2 - z_s)(E_1 -E_2)^\frac{1}{2}$.  |
| Vanne noyée | $QV_n = C_n b_s \sqrt{2 g} (z_v - z_s)(E_1 - E_2)^\frac{1}{2}$.  |

<br/>

Les équations ci-dessus respectent la continuité des débits à la frontière entre deux régimes d’écoulement. Elles sont dérivables analytiquement à partir des lois générales de l’hydraulique pour les régimes: DD, VD et VN. Les formes des équations mises en œuvre pour les autres régimes sont définies de façon à satisfaire la continuité des débits entre deux régimes.

<br/>

### **Paramètres**

Le coefficient **Cd** est le **coefficient de seuil déversant en régime dénoyé**. Il est défini par l’utilisateur, il est généralement égal à sa valeur théorique: 0.6. L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque b/b0<1, où b0 est la largeur du canal ou du collecteur en amont de la vanne. On recommande d’appliquer le coefficient de correction « alp » suivant sur CD:

| b/b0   |     alp = Cd/0.6         |
|:-------|:-------------------------|
| 1      |   1                      |
| 0.8    |   0.8                    |
| 0.6    |   0.6                    |
| 0      |   0.6                    |

Le coefficient **Cn** est le **coefficient de seuil déversant ou de vanne en régime noyé**. Il est par défaut égal à 0.6, sauf dans le cas où la liaison relie deux extrémités de reach ou de branch où ce dernier est alors sélectionné automatiquement par le programme qui tient compte du rapport de section s/S (s'il n'est pas forcé par l'utilisateur via l'option *Submerged coef*) où :
- s est la section d’ouverture de la vanne,
- S est la section mouillée dans le collecteur aval de la liaison.

| s/S   |     Cn                    |
|:-------|:-------------------------|
| 0      |   0.6                    |
| 0.3    |   0.73                   |
| 0.6    |   1                      |
| 0.8    |   1.87                   |
| 0.9    |   3.12                   |
| 0.95   |   5                      |

Ce tableau résulte d’une analyse hydraulique spécifique basée sur la formulation de perte de charge à la Borda. L’utilisateur peut également imposer une Valeur de son choix pour le coefficient Cn.



>**Note**
>La formulation retenue pour le déversoir dénoyé est la même que pour celle du seuil *Weir* en régime *Déversoir dénoyé*.
>
>Toutefois le critère de passage du régime dénoyé en régime noyé et la définition du coefficient Cn sont différents:
>- Condition de passage en régime noyé :
>    - Pour les Weir : $(E_2-z_s) > \frac{2}{3} (E_1-z_s)$
>    - Pour les Gate : $E_2 \geq z_s$
>- Le coefficient Cn:
>    - Pour les Weir : $Cn = 1.73*Cd*alp$ avec alp fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps),
>    - Pour les Gate : formulation indépendante de $Cd$ mais fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps). L'utilisateur peut cependant imposer une valeur fixe.
>
>La simplification faite pour les Gate est rendue nécessaire pour satisfaire la continuité de débit entre tous les régimes qui sont beaucoup plus nombreux que dans le cas du Weir. Le critère de transition entre l’écoulement noyé et l’écoulement dénoyé est considéré comme physiquement plus précis dans la formulation Weir.




