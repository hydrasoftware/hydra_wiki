---
title:  Marqueur de noeud (*Point marker*)
description: 
published: true
date: 2021-02-09T11:04:36.822Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:51.122Z
---



## Définition

Marqueur de noeud

## Données de l'élément

<img src="/model_building/structures/marker_point.png" width="240px"/>

Cet objet n’a pas de données attributaires.

## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un des deux containers suivants :

- [Manhole](/model_building/network/manhole_point)

- [River node](/model_building/river_free_surface_flow/river_node_point)

Il permet de "tagger" visuellement des nœuds dans un modèle, il est également utilisé dans certaines options de calculs définies dans le paramétrage d’un scénario, telle que l’imposition d’un hydrogramme le long d’un tronçon courant, ou la définition de sortie de résultats en un point courant avec la sortie  [SOR1_computation_options](/scenarios) .                

