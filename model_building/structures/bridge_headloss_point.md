---
title:  Bridge headloss
description: 
published: true
date: 2024-02-26T09:37:12.973Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:55.013Z
---



## Définition

Perte de charge de type **Borda** au franchissement d’un pont; ne peut êter posée que sur un nœud de rivière.

## Données de l'élément

<img src="/model_building/structures/bridge_headloss.png" width="650px"/>

La section d’ouverture du pont est donnée par une courbe tabule b(Z).

## Equations

<img src="/model_building/equations/bridge/eq_1.png" width="210px"/>


<img src="/model_building/equations/bridge/eq_2.png" width="320px"/>

E1 et E2 sont les charges hydrauliques de part et d’autre du pont.

C est un coefficient de contraction qui est fonction du rapport SB/S2. Il varie entre 0.6 (si SB/S2<0.6) et 1 (si SB/S2=1.)

Les sections Sb et S2 sont recalculées à chaque pas de temps en fonction des cotes d’eau Z1 et Z2.

Si z1 > Z road,  le programme active une loi de déversement au-dessus de la route.

Cette formulation est plus générale que la formulation [Bradley](/model_building/structures/bradley_headloss_point) car elle prend en compte des sections d’ouverture de géométrie quelconque.

Si z2 < Zf (radier) : l’écoulement est dénoyé et E2 est remplacé par Zf dans l’équation ci-dessus.

Le champ *discharged for headloss computation* permet de considérer un contournement potentiel de l'ouvrage par le lit majeur sans obstacle :

- si l'option *full section* est activée la perte de charge est calculée par rapport au débit total au noeud amont.

- si l'option *river only* est activé la perte de charge est calculée par rapport au débit transitant dans le lit mineur seul.

L'option *full section* est activée par défaut. L'option *river only* ne doit être utilisé que dans le cas d'un lit majeur ne présentant aucun obstacle et sans aucun ouvrage de franchissement :  



## Commentaires

Cet objet est de type *singularité*. Il ne peut être posé que sur un container de type **noeud de rivière** (<img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point)).



