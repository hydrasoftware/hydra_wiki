---
title: L’ÉLEMENT DÉVERSOIR (WEIR)
description: 
published: true
date: 2021-02-09T11:03:35.268Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:19.781Z
---


Cette formulation s’applique aux objets de modélisation : «S-Weir »,«L-Weir » et « Overflow ».

### Schéma de définition

<img src="/model_building/equations/weir/schema_definition_weir.png"/>

### Les régimes d’écoulement

<img src="/model_building/equations/weir/regimes_ecoulement_weir.png"/>

### Equations appliquées pour chaque régime lorsque z1 >z2

Les différents termes sont définis comme suit :

<img src="/model_building/equations/weir/eq_z1.png"/>

<img src="/model_building/equations/weir/eq_z2.png"/>

* Déversoir dénoyé:

<img src="/model_building/equations/weir/eq_qdd.png"/>

* Déversoir noyé:

<img src="/model_building/equations/weir/eq_qdn.png"/>


### Equations appliquées pour chaque régime lorsque z2 >z1

Les différents termes sont définis comme suit :

<img src="/model_building/equations/weir/eq_z1_2.png"/>

<img src="/model_building/equations/weir/eq_z2_2.png"/>

* Déversoir dénoyé:

<img src="/model_building/equations/weir/eq_qdd_2.png"/>

* Déversoir noyé:

<img src="/model_building/equations/weir/eq_qdn_2.png"/>



## Commentaires

* La formulation retenue pour le déversoir dénoyé est la même que pour celle de la vanne ci-dessus en régime DD. Toutefois le critère de passage du régime dénoyé en régime noyé est différent :

Le régime est considéré comme dénoyé si ( dans le cas où z1>z2) : 

<img src="/model_building/equations/weir/eq_e2.png"/>

* Le coefficient Cd est défini par l’utilisateur. Il est généralement égal à 0.58 ( sa valeur théorique) pour un seuil doit. Il peut être diminué pour tenir compte de l’effet de contraction latérale de l’écoulement si la largeur du seuil b est inférieure à la largeur de la section d’écoulement amont. Le facteur de correction à appliquer est donné par le tableau 4.1

* Le coefficient Cn est automatiquement calculé par le programme pour satisfaire la continuité du débit au changement de régime d’écoulement.
La relation appliquée est : Cn = 1.73 x Cd

* La raison pour laquelle le traitement des équations pour l’élément « Weir » est différent de celui pour l’élément « Gate » tient au fait que le critère de transition entre l’écoulement dénoyé et l’écoulement dénoyé est considéré comme physiquement plus précis dans la formulation « Weir ». Il est difficile dans l’élément « Gate » d’adopter la même formulation en raison de la nécessité de satisfaire la continuité de débit entre tous les régimes qui sont beaucoup plus nombreux que dans le cas du « Weir ». 

* Le coefficient Cn augmente lorsque le rapport s/S devient supérieur à 0.6. Dans le cas du « Weir » le rapport s/S s’écrit : b(z2-zs)/(z2-zf2). Le coefficient Cn modifié s’écrit :

Cn’= alp x Cn. On admet que le coefficient alp varie comme suit :

<img src="/model_building/equations/weir/tableau1"/>



