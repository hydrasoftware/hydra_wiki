---
title: Profil en travers (*Cross section*)
description: 
published: true
date: 2023-01-11T10:22:40.189Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:23.186Z
---


## Définition

Les objets <img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point), posés sur des noeuds de rivière <img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point), permettent de définir la géométrie d'une branche filaire. Pour chacun de ces objets on peut définir une section amont et une section aval, afin de prendre en compte les **discontinuités de géométries**.


## Données de l'élément


L'interface de l'objet *cross section* permet :
- d'affecter les géométries de sections, avec la possibilité :
	- de sélectionner une **géométrie de section déjà existante**,
  - de créer et d'ajuster les **géométries de section** 
- de paramétrer l'objet cross section en lien avec la géométrie de section sélectionnée

![_cross_section_ui.svg](/model_building/river_free_surface_flow/_cross_section_ui.svg)

### Selection d'un type de section et d'une géométrie de section
Les sélectionneurs en haut de l'interface permettent de se positionner sur **l'amont** *(upstream)* ou **l'aval** *(downstream)* de l'objet cross section, et de définir le **type de section** affectée (via le menu déroulant).

![_cross_section_navigation_valley_ui.png](/model_building/river_free_surface_flow/cross_section/_cross_section_navigation_valley_ui.png)

**Après séletion du type de section, une nouvelle section est créée automatiquement**. Il est alors possible :
- de renommer la section : **Rename**
- d'**appeler une section existante** dans la** bibliothèque de sections**, via la liste déroulante *Geometry*
- de créer une nouvelle section : **New**

> Toute section créée est automatiquement ajoutée à la bibliothèque de sections et peut être appelée par une autre *Cross section*.

Chaque type de géométrie de section dispose de sa propre interface de paramétrage. **Cinq types différents de sections géométriques** sont disponibles :
- les **4 sections de collecteurs** définies pour les branches d’assainissement (pour plus de précisions, se reporter à  <img src="/model_building/network/pipe_line.png"/> [tronçons de collecteur](/model_building/network/pipe_line)  )
  - **Circular** : collecteur circulaire, caractérisé par son diamètre
  - **Ovoïd** : collecteur ovoïde, caratérisé par ses diamètres bas et haut et par sa hauteur
  - **Sections paramétriques**, définies par une courbe tabulée hauteur / largeur :
    - **Pipe** : collecteur paramétrique fermé
    - **Channel** : canal paramétrique ouvert   
Une **cote radier** (*Invert altitude*) et un **coefficient de strickler** (*strickler coefficient*) sont affectés à chaque section.
    

- la section **valley** à lits composés. Elle peut être construite manuellement, ou **générée à partir du MNT et/ou de semis de points bathymétriques** (pour plus de précisions, se reporter à [Géométries de vallées](/model_building/river_free_surface_flow/cross_section/valley_cross_section)).
Sont définis :
  - deux **coefficients de strickler** (*strickler coefficient*) : un pour le lit mineur, et un pour le lit majeur (affecté aux rives gauche et droite),
  - un **coefficient de sinuosité** (*Sinuosity*), correspondant au ratio entre la longueur développée du lit mineur et la longueur de la vallée jusqu'à la section suivante pour laquelle un coefficient de sinuosité est défini (compris entre 0 et 1).

	> :bulb: En l'absence de coefficient de Strickler et de coefficient de sinuosité (alors définis à 0), les trois valeurs de la section amont seront automatiquement affectées.
            
> :chart: Une **zone de graphique** spécifique à chaque tpe de géométrie de section est affichée. Le profil en travers du terrain est reporté sur ce gaphique à titre indicatif:
>- pour les géométrie de type *Valley*, au droit de la Flood Plain transect support le cas échéant, 
>- pour les autres sections : sur une ligne orthogonale au reach au droit de l'objet *cross section*

Les géométries de section peuvent être créées via la **bibliothèque de sections**, accessible via le bouton <img src="/tool_bar/geom.png"/> `Geometries library` de la barre d'outils hydra.

Les **flèches < >** permettent de **naviguer le long des cross section** d'un reach. Cet outil permet de créer et d'ajuster les sections d'une même branche sans sortir de l'éditeur (évitant ainsi l'édition systématique de chacun des objets).

La position de la section sur le reach ainsi que le profil en long du fond du lit sont présentés en bas de léditeur. L'encart en bas à droit présente une vue en plan locale au droit de la cross section.
<img src="/model_building/river_free_surface_flow/_cross_section_longitudinal_profile_navigation_ui.png"/>

## Règles d'agencement des géométries

- Une *geometry downstream* doit être nécessairement définie sur le **nœud amont** du bief

- Une *geometry upstream* doit être nécessairement définie sur le **nœud aval** du bief

- Si un seul profil est défini sur un point courant, il peut être posé indifféremment en amont (*upstream*) ou en aval (*downstream*).


Un bief peut ainsi être défini géométriquement comme suit :

<img src="/model_building/river_free_surface_flow/_reach_sections_building_rules.png" style="transform: scale(0.75);"/>





