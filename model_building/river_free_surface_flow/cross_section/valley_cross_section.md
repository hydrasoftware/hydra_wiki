---
title: Géométries de section de type vallée
description: 
published: true
date: 2023-01-11T10:41:01.714Z
tags: 
editor: markdown
dateCreated: 2023-01-07T18:33:34.170Z
---

# Introduction

L'interface de l'objet *cross section* permet de:
- créer et ajuster la **géométrie de section** 
- paramétrer l'objet cross section auquel est affectée la géométrie

> La présente section présente les méthodes de création des géométries de section de type *Valley*, puis à la fin les paramètres de l'objet cross ssection associés aux géométries Valley.

La géométrie de section de type **valley** en bleu ci-dessous et dénommée par la suite **section hydra**, est définie par un nombre de points limités comme suit :
- entre 1 et 6 couples cote / largeur pour le lit mineur,
- entre 0 et 8 points pour le lit majeur rive gauche,
- entre 0 et 8 points pour le lit majeur rive gauche.

![valley_data_section_ui.svg](/model_building/river_free_surface_flow/cross_section/valley_data_section_ui.svg)

Ces sections peuvent être générées par deux méthodes présentées successivement:
- à partir des données topographiques matérialisées par le MNT (raster) et/ou des données vectorielles (semis de points ou polylignes 3D), via les lignes de contrainte **Flood Plain Transect** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> matérialisant la position du profil; une fonction d'**autofit** permet d'ajuster la section hydra aux données topographiques brutes, et si besoin de corriger ces donénes brutes;
- manuellement.


> :warning: **Lors de la mise à jour de modèles créés avec des versions antérieures à la version 3.5.1 d'hydra, la fonction d'Autofit est désactivée par défaut.**
> Cette option permet de conserver les sections effectivement prises en compte pour al modélisaition, dans la mesure ces versions d'hydra antérieures ne conservaient pas un lien dynamique avec les lignes de contrainte support (Flodd plain transect).

# Coss section : choix de la section, renommage
Les sélectionneurs en haut de l'interface permettent de se positionner sur **l'amont** *(upstream)* ou **l'aval** *(downstream)* de l'objet cross section, et de définir le **type de section** affectée (via le menu déroulant).

![_cross_section_navigation_valley_ui.png](/model_building/river_free_surface_flow/cross_section/_cross_section_navigation_valley_ui.png)

Après séletion du type de section *Valley*, une nouvelle section est créée automatiquement. Il est alors possible :
- de renommer la section : **Rename**
- d'appeler une section existante dans la bibliothèque de sections, via la liste déroulante *Geometry*
- de créer une nouvelle section : **New**


# Création et manipulation de géométries de sections générées à partir de données topographiques MNT et points bathymétriques
## Présentation générale : lignes support, sources de données et section modélisée
Les sections de vallées sont générées à partir des données topographiques matérialisées par le MNT (raster) et/ou des données vectorielles (semis de points ou polylignes 3D), via les lignes de contrainte **Flood Plain Transect** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> matérialisant la position du profil. Cette ligne de contrainte doit intersecter le bief et les sommets de cette polyligne matérialisent les limites des différents lits :
- sommets encadrant le point d'intersection avec le bief : limites du lit mineur,
- sommets extrémité : limites du lit majeur rives gauche et droite si au moins deux sommets respectivement à gauche et à droite du point d'intersection avec le bief; dans le cas contraire, il est considéré que seul le lit mineur est représenté par la section. 

<img src="/model_building/river_free_surface_flow/cross_section/_flood_plain_transect_vertice_section.png" width=450/>

Après création de l'objet  <img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point), **l'éditeur** de création et de paramétrage des **géométries de section** s'ouvre.


**L'interface permet d'ajuster les sections, en lien avec la *Flood Plain Transect* support. Les principales fonctionnalités sont décrites ci-dessous.**

En choisissant la géométrie **valley**, Hydra recherche automatiquement la ligne de contrainte de type *Flood plain transect* ou *Non topological* la plus proche et génère une section hydra optimisée, affichée en bleu sur le graphique. La ligne de contrainte peut également être choisie manuellement dans le menu déroulant *Transect* **(3)**

![valley_ui.svg](/model_building/river_free_surface_flow/cross_section/valley_ui.svg)



## Ajustement des sections
Les fonctions ci-dessous s'effectuent principalement via la **zone de graphique**, qui propose des **fonctions de navigation et de zoom**  :
- **déplacement** du graphique : ciquer dans l'espace de graphique, maintenir le clic et déplacer la souris,
- **zoom** via la molette de la souris: 
	- zoom coorodnné x / z : positionner le curseur dans l'espace de graphique
  - zoom X : positionner le curseur sur les coordonnées x
  - zoom Z : positionner le curseur sur les coordonnées Z

### Ajustement de la *section hydra* aux donées topographiques source
La fonction **Autofit (2)** cochée par défaut permet d’ajuster dynamiquement la section hydra à la section topographique brute lorsque sont modifiées les limites de berges du lit mieur ou les extrémités latérales de la section (cf. ci-dessous).


### Ajustement de l’emprise latérale de la section et des limites lit mineur / lit majeur
-	Les **lignes rouge (4)** et **verte (5)** verticales représentent respectivement la limite gauche et droite du lit mineur (ou les extrémités de la section en l’absence de lit majeur, ce qui est le cas ici sur la rive gauche).
-	Pour ajouter un lit majeur, sélectionner la ligne bleue centrale et glisser en maintenant le clic respectivement vers la gauche ou vers la droite

**Si Autofit est activé**, la section hydra est regénérée en dynamique.
>**note**
>La fonction reset ne modifie pas l’ajustement fait sur les limites lit mineur / lit majeur ni sur les extrémités de la section.

Comme indiqué précédemment, la localisation des limites du lit mieur est matérialisée sur la ligne de contrainte portant la section par les sommets qui encadrent le point d’intersection avec le reach. L’ajout d’un lit majeur entraine la création d’un nouveau sommet, et l’ajustement de sa position ou de l’extrémité de la section un déplacement des sommets correspondant.
L’encart **7** en bas à droite permet de visualiser l’évolution de la ligne de contrainte considérée au fur et à mesure de l’ajustement de la section.


### Correction / simplification des données source
Il est possible de modifier les points du profil topographique rouge, notamment pour supprimer des points aberrants (erreur de traitement ou approximation du MNT notamment) :
- sélectionner un point dans le graphique,
- puis le déplacer ou le supprimer (Suppr).
Le click droit permet d’éditer ses coordonnées et de les modifier ou de le supprimer.


**Reset** permet de revenir à la section non modifiée.
**Simplfy** permet de simplifier la section rouge ; utile lorsque la section brute nécessite d’importantes corrections. Il est toujours possible de revenir à la section initiale avec Reset.


### Correction de la section hydra
Le mode **Advanced** permet de visualiser le tableau de données de la section hydra tracée en bleu sur le graphique. Tant que le mode *Autofit* est actvé, ce tableau n'est ccessible en consultation.
En désactivant le mode *Autofit*, ce tableau devient éditable, et il est possible de modifier la section hydra :
- via le **tableau** (utiliser + et - pour respectivement ajouter ou supprimer une ligne; voir paragraphe suivant pour plus de précisions) :
	- tableau de gauche :  lit majeur rive gauche (entre 1 et 8 couples cote/largeur calculée à partir de la limite de la berge)
  	- tableau central :  lit mineur (entre 1 et 6 couples cote/largeur)
    - tableau de droite :  lit majeur rive droite (entre 1 et 8 couples cote/largeur calculée à partir de la limite de la berge)
- via le **graphique** en sélectionnant les sommets de la courbe (ou clic droit et edit)

![valley_data_section_advanced_ui.svg](/model_building/river_free_surface_flow/cross_section/valley_data_section_advanced_ui.svg)

>:warning: Si la possibilité de modifier mauellement les sections hydra est possible, il est préférable de conserver l'autofit activé et de modifier les données brutes (cf. § précédent).

# Création de géométries de sections sans données topographiques support
La création de sections sans données topographiques source est possible. Nous présentons ici la création d'une section depuis la bibliothèque de sections.

- Créer une nouvelle section à l'aide du bouton <img src="/tool_bar/geom.png"/> `Geometries library`, sélectionner l'onglet *Valley*
- A l'ouverture de l'interface, la section est vide
- Il est possible de renmmer la section en double-cliquant sur le champ *Name* de la liste en bas à gauche
- Activer le mode **Advanced** sur le panneau de droite
- Le tableau de section s'affiche en dessous du graphique, et peut être renseigné :
	- entre 2 et 6 couples cotes / largeur pour le lit mineur
  - entre 1 et 8 couples cote largeur pour chacun des deux lits majeur rives gauche et droite :
  	> :bulb: **Précisions sur le paramétrage des sections**
    > - Les largeurs sont calculées par rapport à la limite de la berge
  	> - 1er point d'altitude supérieure ou égale à celle du dernier point du lit mineur
    > - si le 1er point du lit majeur a une largeur non nulle, la première largeur est intégrée au lit mineur.
- Le graphique se met à jour dynamiquement. En cas d'incohérence dans les coordonnées des points, le fond du graphique se colore en rouge.

![valley_data_section_manual_ui.svg](/model_building/river_free_surface_flow/cross_section/valley_data_section_manual_ui.svg)

>:warning: **Les altitudes et les largeurs doivent être croissantes**.

>:warning: Les **points peuvent également être ajoutés et modifiés sur le graphique**. Nous déconsieillons ce mode de fonctionnement dans ce mode sans donnée topographiques source. Les échelles n'étant pas ajustées, se déplacer dans le graphique comme mentionné précédemment.


>:bulb: La section hydra peut être préparée en amont à partir de sections topographiques brutes non géoréférencées, puis coiée collée dans l'interface (par section de lit, en commençant de préférence par le lit mineur).

La création d'une section via l'interface *Cross section* est identique.



# Paramétrages complémentaires de l'objet cross section
- Il est possible d'ajouter des **murettes** le long des berges : **Levee altitudes (m)**
	- Tant que la ligne d'eau ne dépasse pas la cote de murette, le lit majeur n'est pas sollicité,
  - Après surverse, le lit majeur est mis en eau, et la murette est considérée effacée.
- L’option **Invert altitude (8)** permet de translater verticalement la section.
- Définir les paramètres de **rugosité** et de **sinuosité (9)**.
  - **coefficients de strickler** : un pour le lit mineur, et un pour le lit majeur (affecté aux rives gauche et droite),
  - **coefficient de sinuosité** : ratio entre la longueur développée du lit mineur et la longueur de la vallée jusqu'à la section suivante pour laquelle un coefficient de sinuosité est défini (toujours supérieur à 1).
  >  :bulb: En l'absence de coefficient de Strickler et de coefficient de sinuosité (alors définis à 0), les trois valeurs de la section amont seront automatiquement affectées.
**Enregistrer pour que soient prises en compte les modifications apportées.**

Les **flèches (10)** permettent de naviguer sur les sections le long du reach. La section courante est localisée sur le **profil en long** en bas de la fenêtre **(11)**.


