---
title: Géométries de section de type vallée v0
description: 
published: true
date: 2023-01-11T10:27:26.143Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:31.329Z
---

#### Description

Un profil en travers de valley est découpé en 3 lits : lit majeur rive gauche, lit mineur et lit majeur rive droite :

<img src="/model_building/river_free_surface_flow/cross_section/valley_cross_section_scheme.png"/>

Les sections peuvent être renseignées via l'éditeur de l'objet *Cross sections* ou via la bibliothèque de sections <img src="/tool_bar/geom.png"/> .

<img src="/model_building/river_free_surface_flow/cross_section/cross_section_geometry.png" style="transform: scale(0.75);"/>


**Les sections vallée sont décrites par 14 couples cote / largeur :**
- 6 pour le lit mineur,
- 4 pour le lit majeur rive gauche,
- 4 pour le lit majeur rive gauche,

<img src="/model_building/river_free_surface_flow/cross_section/_valley_cross_section_1.png" style="transform: scale(0.75);"/>


> **warning**
>
> - Les cotes et les largeurs de sections doivent être rentrées dans un ordre strictement croissant de valeurs. Entre 2 cotes, les largeurs sont interpolées linéairement.
> - Il faut respecter : :math:`Z_{MAJ}-RG(1) >= Z_{MIN}(N1)` et :math:`Z_{MAJ}-RD(1) >= Z_{MIN}(N1)`
> 
> 

_____

Des **murettes** peuvent être ajoutées sur les berges rives gauche et droite, caractérisées par leur cote d'arase (Z levee); elles sont matérialisées en rouge sur la section.Elles sont automatiquement positionnées au droit du premier point du lit majeur.


> **note**
>
> - Si :math:`ZMG < Z_{MAJ}-RG(1)`, la protection est ignorée en rive gauche
> - Si :math:`ZMD < Z_{MAJ}-RD(1)`, la protection est ignorée en rive droite
> - Si :math:`B_{MAJ}(1) ≠ 0` (en rive gauche ou en rive droite) la murette est en retrait. On considère alors que le lit mineur comprend la largeur :math:`B_{MAJ}-RG(1) + B_{MAJ}-RD(1)`.
> 

_____

L'utilisateur peut ajuster la **largeur de lit majeur actif** (active floof plain ratio, compris entre 0 et 1), ainsi que les **coefficients d'échange µ1** entre le lit majeur actif et la zone de stockage en montée de crue et à la décrue :
-  lit majeur actif / zone de stockage (exchange coefficient : active flood plain --> storage area)
-  zone de stockage / lit majeur actif (exchange coefficient : storage area --> active flood plain)


> **warning**
>
>  Si µ1 < 0, le programme considère que l’échange latéral de débit entre le lit majeur actif et le lit d’expansion se fait instantanément. Autrement dit, les cotes d’eau dans le lit (mineur + majeur actif) et dans le lit d’expansion sont maintenues à tout instant.
> 
> Le long d’une même branche, les paramètres µ1 doivent avoir le même signe.
> 

_____


> **note**
>
>  La section est tracée sur le graphique associé. Des fonctions de **zoom** sont disponibles via la molette de la souris : zoom sur l'abscisse lorsque la souris pointe sur l'espace de tracé du graphique, zoom sur l'ordonnée lorsque la souris pointe sur l'échelle verticale. Le clic gauche de la souris permet de **déplacer** la courbe. 
> 
> 

#### Génération des sections de vallée à partir du MNT et de semis de points

La géométrie d’une section de vallée peut être renseignée manuellement, mais il est beaucoup plus rapide et efficace de la calculer à partir d’un MNT et/ou d’un semis de points bathymétriques.

La génération des sections se fait via une **ligne de contrainte** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> [Constrain](/model_building/river_free_surface_flow/constrain_line) de type **flood plain transect** matérialisant la position du profil. Cette ligne de contrainte doit intersecter le bief; les sommets de cette polyligne matérialisent les limites des différents lits :
- sommets encadrant le point d'intersection avec le bief : limites du lit mineur,
- sommets extrémité : limites du lit majeur rives gauche et droite si au moins deux sommets respectivement à gauche et à droite du point d'intersection avec le bief; dans le cas contraire, il est considéré que seul le lit mineur est représenté par la section. 


<img src="/model_building/river_free_surface_flow/cross_section/_flood_plain_transect_vertice_section.png" style="transform: scale(0.5);"/>

Le schéma ci-dessous précise la méthode de restitution du profil en travers topographique à partir d'un MNT et d'un semis de point, ce dernier étant pris en priorité :
- recherche des points situés autour de la ligne de contrainte sur laquelle doit être restituée le profil, dans un buffer de distance dx spécifiée par l'utilisateur,
- projection de ces points sur la ligne de contrainte pour reconstituer le profil en travers issu des points topographiques,
- création du profil en travers à partir du raster sur les linéaires de la polyligne non couverts par le semis de points,
- assemblage des deux sources de données pour création du profil complet,
- génération de la section simplifiée au format hydra.

<img src="/model_building/river_free_surface_flow/cross_section/_valley_cross_section_generation.png" style="transform: scale(0.5);"/>

Dans le menu de paramétrage de la section :
- sélectionner la ligne de contrainte à prendre en compte,
- cocher l'option ignore points si les points ne doivent pas être pris en compte pour la génération de la section,
- préciser :
  - la distance de recherche de points autour de la ligne de contraintes (*Point distance*)
  - le pas de discrétisation de la section brute (nombre de points, *Discretisation*)
- cliquer sur le bouton <img src="/model_building/river_free_surface_flow/cross_section/_generate_geom_from_button_ui.png"/>

Les **limites du lit mineur**, définies par la position des sommets de la ligne de contrainte utilisée, sont matérialisées sur le graphique par deux lignes verticales vertes. Elles peuvent être ajustées : cliquer sur une des limites puis cliquer sur la nouvelle position et regénérer la section.

<img src="/model_building/river_free_surface_flow/cross_section/_valley_cross_section_2_graph.png" style="transform: scale(1.0);"/>



