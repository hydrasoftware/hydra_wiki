---
title: Sous-domaine 2D (*2D domain*)
description: 
published: true
date: 2021-02-09T11:04:07.562Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:07.572Z
---


Il est utile dans certaines applications d’affecter un numéro de domaine commun à un ensemble de mailles 2D. 

On affecte à ce numéro de domaine des propriétés particulières associées à des options de calculs via le gestionnaire de scénarios.

On dispose pour cela de deux outils :

- le bouton <img src="/model_building/river_free_surface_flow/manage_2d_point.png"/> (*manage 2D domain*) qui permet de créer un numéro de domaine 2D.


<img src="/model_building/river_free_surface_flow/manage_2d.png" width="390px"/>

- le bouton <img src="/model_building/river_free_surface_flow/2d_domain_marker_point.png"/> (*2D domain marker*) qui permet d’affecter un numéro de domaine 2D à un ensemble de mailles 2D générées via un coverage 2D.

<img src="/model_building/river_free_surface_flow/2d_domain_marker.png" width="250px"/>

Ce marker est placé à l’intérieur du coverage 2D avant maillage. Après maillage du coverage, chaque maille 2D reçoit comme attribut le numéro du sous-domaine inscrit dans le marker :

<img src="/model_building/river_free_surface_flow/2d_domain_marker-fonct.png"/>




