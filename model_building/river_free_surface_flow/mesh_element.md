---
title:  Maille 2D (Mesh element)
description: 
published: true
date: 2022-01-24T09:16:57.718Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:43:03.697Z
---



## Définition

La maille 2D (*mesh element*) peut en première approche être assimilée à un casier de forme particulière. En réalité c’est une maille d’un domaine bidimensionnel, connectée aux mailles adjacentes par l’intermédiaire de liaisons spécifiques : les liaisons de type *mesh link*. 

Le couple (mesh element, mesh link) interagit pour résoudre les équations de St Venant à deux dimensions. 

## Données de l'élément

<img src="/model_building/river_free_surface_flow/mesh_element.png" width="270px"/>

La maille 2D est un container défini géométriquement par un contour polygonal de 3 ou 4 sommets.

Les données attributaires sont :

- *Z bottom* = la cote de fond moyenne
- *Strickler coefficient* = le coefficient de frottement.
- *Area* = la surface (calculée) en m²

>*Le calcul applique une taille minimale des mailles 2D de 4 m² par défaut. Si un nombre significatif de mailles générées ont une surface inférieure, la superficie inondée prise en compte dans les calculs se trouve alors artificiellement augmentée, ce qui peut conduire à des niveaux d'eau atteints sous-estimés.
L'option NOLIM_INF_SMIN2D peut être appelée dans le [paramétrage de scénario](../../scenarios#scenario_computation_runoff) pour lever cette limite (qui est en fait abaissée à 0.5 m²)*

<a name="scenario_computation_runoff" ></a>

## Méthode de construction

Cet objet est généré automatiquement via le bouton de [ligne de contrainte](/model_building/river_free_surface_flow/constrain_line) <img src="/model_building/river_free_surface_flow/constrain_line.png"/>.


La fermeture d’un contour polygonal crée par défaut un « coverage » de type 2D :

<img src="/model_building/river_free_surface_flow/mesh_element_image.png" width="320px"/>

La sélection du bouton *create mesh* et un clic souris à l’intérieur du coverage génère le maillage suivant, dont la densité est contrôlée par les attributs des **lignes de contraintes** :

<img src="/model_building/river_free_surface_flow/mesh_element_image2.png" width="320px"/>

Cette opération permet de générer non seulement **les mailles 2D** et leurs attributs mais également les liaisons [*mesh link*](/model_building/links/mesh_2d_line)  connectant les mailles entre elles, ainsi que leurs **attributs géométriques et altimétriques**.

Les **attributs altimétriques** ne sont toutefois générés que si la **couche MNT** sous-jacente est présente dans le modèle.


