---
title:  Casier (*Storage*)
description: 
published: true
date: 2021-02-09T11:04:20.756Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:27.160Z
---



## Définition

Le casier (storage) est une zone d’expansion du lit majeur caractérisée par des vitesses d’écoulement généralement faibles et dont le contour s’appuie sur la topographie naturelle ou sur des obstacles artificiels à l’écoulement des eaux. 

Le casier est délimité par un contour polygonal de forme quelconque.

## Méthode de construction

La méthode de construction d'un casier est la suivante :

- créer un contour polygonal à l’aide de l’objet [ligne de contrainte](/model_building/river_free_surface_flow/constrain_line) <img src="/model_building/river_free_surface_flow/constrain_line.png"/>

- insérer l’objet <img src="/model_building/river_free_surface_flow/storage_point.png"/> à l’intérieur du contour polygonal.


L’objet casier est alors crée avec sa géométrie :

<img src="/model_building/river_free_surface_flow/storage_image.png" width="350px"/>

Cette manipulation particulière est motivée par le fait que l’objet *storage* est également un objet de type *coverage* exploité pour les applications cartographiques des résultats.

## Données de l'élément

<img src="/model_building/river_free_surface_flow/storage.png" width="450px"/>

La **courbe de remplissage** se compose de couples (cote, surface). Elle est limitée à 10 couples maximum. Cette courbe doit être monotone et strictement croissante.

On peut définir une **cote initiale de remplissage** (*Z init*).

La courbe de remplissage du casier peut également être générée automatiquement à l’ide de la couche MNT sous-jacente si elle existe. Il faut dans ce cas cliquer sur le bouton *Generate filling curve from terrain*.

Le champ **porosity coefficient** permet de préciser le taux d’obstruction des surfaces opaques et donc soustraites à l’inondation dans l’objet « Storage », pour prendre en compte des bâtis hors d'eau non visibles sur le MNT pa exemple.

- La surfe S’ prise en compte est égale à : S’= porosité x S  
- Le champ « porosité » est compris entre 0. Et 1.


## Equations

<img src="/model_building/river_free_surface_flow/storage_eq1.png" width="150px"/>

Avec :

- S = surface au miroir du casier, définie sous forme tabulaire par la grille ci-dessus,
- Zc : cote d’eau (supposée uniforme dans le casier)
- qli : débit échangé avec un autre container via une liaison latérale. 
- Zi : cote d’eau des autres containers.



