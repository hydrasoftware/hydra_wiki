---
title: Rues (*street*) et carrefours (*crossroad*)
description: 
published: true
date: 2021-02-09T11:04:10.347Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:11.423Z
---


## Définition

Les  objets *crossroad* et *street* sont conçus pour modéliser les écoulements préférentiels le long des rues et à travers les carrefours en cas de débordement en zone urbanisée.

## Données des éléments

Le **crossroad** est un nœud défini par :

<img src="/model_building/river_free_surface_flow/crossroad_ui.png" style="transform: scale(1.0);"/>

- *Ground elevation* = cote du terrain naturel, calculée par l'application s’il existe un MNT sous-jacent
- *Area* = surface (par défaut à 1m²).
- *h* = décaissé h du carrefour par rapport au terrain naturel (par défaut à 0m).

L'objet **street** est un objet de construction (polyligne), à partir duquel seront générés automatiquement des objets **Street segment** de tpe *link*. Il est défini par:

<img src="/model_building/river_free_surface_flow/street_ui.png" style="transform: scale(1.0);"/>

- *Width* = largeur
- *Width invert* = largeur basse
- *Strickler coefficient* = coefficient de frottement
- *Mesh element length* = longueur de discrétisation du maillage autour de la rue (affectée aux lignes de contrainte automatiquement générées autour de la rue)


> **note**
>
>  le champ *Width invert* combiné au champ *h* du crossroad permettent d'utiliser l'objet **street** pour modéliser des **fossés de drainage** immergés dans les domaines 2D. Si ces éléments peuvent également être modélisés avec des reach et des sections de type *open channel*, l’objet « Street » constitue une option plus simple à mettre en œuvre lorsque les géométries varient peu.    
> 
> :scale: 50%    
> 
> <img src="/model_building/river_free_surface_flow/street_section.png"/>
> 
> Des objets **crossroad** sont générés automatiquement à chaque sommet de la polyligne *street*.
> 
> 
> Des objets **Street segment** de tpe *link* sont automatiquement générés par l'application lors de la construction du modèle, correspondant aux rues élémentaires reliant les crossroad entre eux, auxquels sont affectés les paramètre de l'objet street qui es porte. Ces objets ne peuvent pas être modifiés.
> 

## Equations

Les **carrefours** sont gérés par les mêmes équations que les [casiers](/model_building/river_free_surface_flow/storage).

Les **segments de rue** sont quant à eux gérés par les même équations que les [tronçons de collecteur](/model_building/network/pipe_line) avec une section géométrique **trapézoïdales**.

## Méthode de construction

On procède comme suit :

**ETAPE 1 : Création des rues à l’aide de l’outil** <img src="/model_building/river_free_surface_flow/street_line.png"/>


Cet outil permet de tracer une polyligne représentant l’axe d’une rue. Des lignes de contrainte ont automatiquement créées autour de la *street* si on affecte une largeur non nulle dans le champ *Width* de l'écran de saisie, formant alors un coverage de type *street*.

<img src="/model_building/river_free_surface_flow/street_const1.png" width="700px"/>

Cette opération génère des carrefours à chaque extrémité de rue et à chaque sommet de la polyligne *street*.


**ETAPE 2 : Densification du réseau de rues**

Il est possible de densifier le réseau de rues en venant appuyer un sommet de la polyligne définissant la nouvelle rue sur un sommet d’une rue existante.

<img src="/model_building/river_free_surface_flow/street_const2.png" width="550px"/>

**ETAPE 3 : Création d'îlots entre les rues**

La création des lignes de contrainte autour des *street* entraîne la création d'ilots entre ces dernières, correspondant par défaut à des *coverage* de type *mesh*. Ces coverage peuvent être :

- Maillés. Les liaisons entre les rues et les mailles 2d de l’îlot sont crées lors du maillage, ainsi que les *crossroad* intermédiaires nécessaires pour accrocher les liaisons.

- Convertis en casiers si le terrain est relativement plat et si l'on ne cherche pas à définir une dynamique précise des écoulements dans l'ilot. 


<img src="/model_building/river_free_surface_flow/street_const3.png" width="550px"/>


