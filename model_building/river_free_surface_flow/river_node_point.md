---
title:  Noeud de rivière (*River node*)
description: 
published: true
date: 2021-02-09T11:04:15.469Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:43:19.252Z
---



## Définition

Nœud utilisateur de bief filaire.

## Données de l'élément

<img src="/model_building/river_free_surface_flow/river_node.png" width="230px"/>

Le nœud utilisateur doit être posé le long de l’axe du bief. 

Contrairement au regard de collecteur il ne découpe  pas de tronçon élémentaire de cours d’eau au niveau de l’IHM, il sert essentiellement à accueillir des singularités ponctuelles.

Les données attributaires du *river node* sont :

- la section (*Area*) = 1 m² par défaut 

- son PK ( généré par le programme)

- la cote radier (générée par le programme)

## Commentaires

Deux nœuds sont automatiquement générés par l’IHM aux extrémités de bief lors de sa création.


