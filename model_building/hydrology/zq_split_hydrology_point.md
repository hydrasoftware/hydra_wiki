---
title:  Geometric split singularity (*split_ZQ*)
description: 
published: true
date: 2021-02-09T11:03:48.504Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:40.316Z
---



## Définition

Calcul de répartition de débit entre une branche aval et une ou deux branches dérivées selon une loi « split QZ »

<img src="/model_building/hydrology/splitqz-fonct.png" width="300px" align="center"/>

## Données de l'élément

<img src="/model_building/hydrology/splitqz.png" width="460px" align="center"/>

Pour sélectionner la branche aval ou  une branche dérivée :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

Le débit dans la branche aval et dans chaque branche dérivée est calculé à partir de la sélection d’un des 4 types de lois suivantes :

**1. Loi de seuil dénoyé** (pas d'influence aval)

<img src="/model_building/equations/zq_split_hydrology/schema_seuil.png" width="300px"/>


<img src="/model_building/equations/zq_split_hydrology/eq_seuil.png" width="300px"/>

**2. Loi d'orifice dénoyé** (pas d'influence aval)  

<img src="/model_building/equations/zq_split_hydrology/schema_orifice.png" width="300px"/>


<img src="/model_building/equations/zq_split_hydrology/eq_orifice.png" width="300px"/>

**3.Loi de strickler** (pas d'influence aval)

<img src="/model_building/equations/zq_split_hydrology/schema_strickler.png" width="300px"/>


<img src="/model_building/equations/zq_split_hydrology/eq_strickler.png" width="300px"/>


**4. Loi Q(z) définie paramétriquement**  


La relation entre le débit amont et le débit aval dans chaque branche est calculée géométriquement comme suit :

<img src="/model_building/hydrology/splitqz-lois2.png" width="460px"/>


## Commentaires

L’objet *geometric split singularity* doit être posé obligatoirement sur un nœud hydrologique.


