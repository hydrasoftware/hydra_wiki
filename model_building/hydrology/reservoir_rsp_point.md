---
title:  Bassin de stockage paramétrique (*Parametric reservoir*)
description: 
published: true
date: 2021-02-09T11:03:59.348Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:55.876Z
---



## Définition

Bassin de retenue équipé d’une surverse S et d’un ouvrage de vidange F à **débit variable** en fonction du niveau d’eau dans le bassin.

<img src="/model_building/hydrology/reservoir_rsp_fonct.png" width="300px" align="center"/>


## Données de l'élément

<img src="/model_building/hydrology/reservoir_rsp.png" width="550px" align="center"/>


Pour sélectionner la branche connectée à la vidange ou la branche connectée à la surverse :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

La géométrie du réservoir est définie par une courbe tabulée S(z).

Il faut de plus définir :
  * la cote d'eau initiale dans le bassin (*Z initial*)
  * la courbe tabulée Qf(z) du débit de vidange (*drainage discharge*)
  * la courbe tabulée Qs(z) du débit de surverse (*overflow discharge*) 

Si l’option de calcul **pollution** est activée dans la fenêtre de paramétrage du scénario, le bouton *pollution* sert à définir les taux d’abattement ou les concentrations résiduelles des différents paramètres de pollution dans le bassin.

## Equations

La variation du niveau d'eau h dans le bassin est calculée au moyen de l'équation de continuité liant la surface au miroir et les débits entrant et sortant.

<img src="/model_building/equations/reservoir_rsp/reservoir_rsp_equations.png" width="260px"/>

## Commentaires

L’objet *reservoir* doit être posé obligatoirement sur un nœud hydrologique.

Lorque l'option pollution est activé dans le gestionnaire de scénario le bouton pollution de l'objet *tank* offre la possibilité de calculer un abattement de pollution de l'effluent stocké selon deux modes :

- taux d'abattement
- concentration résiduelle.

<img src="/model_building/hydrology/tank_bc_pollution.png" width="450px"/>
