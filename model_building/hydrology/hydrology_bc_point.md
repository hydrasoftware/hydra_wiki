---
title:  Condition limite hydrologique (*Hydrology boundary condition*)
description: 
published: true
date: 2021-02-09T11:03:51.254Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:44.189Z
---



## Définition

Exutoire aval d’une arborescence hydrologique.

## Données de l'élément

<img src="/model_building/hydrology/hydrology_bc.png" width="250px"/>

Cet objet  ne contient aucune donnée attributaire.

## Commentaires

Cet objet est de type **singularité**. Il est posé obligatoirement sur le nœud hydrologique aval d’une arborescence hydrologique. 

Le débit sortant est perdu pour le réseau.
