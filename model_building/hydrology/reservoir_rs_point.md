---
title:  Bassin de stockage (*Reservoir*)
description: 
published: true
date: 2024-03-18T21:51:17.651Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:42:27.652Z
---



## Définition

Bassin de retenue équipé d’une surverse S et d’un ouvrage de vidange F à **débit constant**.

![reservoir_rs_fonct.svg](/model_building/hydrology/reservoir_rs_fonct.svg)

## Données de l'élément

<img src="/model_building/hydrology/reservoir_rs.png" width="400px" align="center"/>


Pour sélectionner la branche connectée à la vidange ou la branche connectée à la surverse :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

La géométrie du réservoir est définie par une courbe tabulée S(z).

Il faut de plus définir :
  * *Z initial* : la cote d'eau initiale dans le bassin
  * *drainage discharge* : le débit de fuite en $m^3/s$ ($Q_F$)

Si l’option de calcul **pollution** est activée dans la fenêtre de paramétrage du scénario, le bouton *pollution* sert à définir les taux d’abattement ou les concentrations résiduelles des différents paramètres de pollution dans le bassin.

## Equations

La variation du niveau d'eau h dans le bassin est calculée au moyen de l'équation de continuité liant la surface au miroir et les débits entrant et sortant.


$S.dh/dt=Q_A-Q_F-Q_S$

$Q_C=Q_A$ si $Q_A<Q_F$ 
$Q_C=Q_F$ si $Q_A>Q_F$ 

$Q_F=Q_F$ si $h<h_d$ 
$Q_F=\mu.L.\sqrt{2.g}.(h-h_d)^{3/2}$  si $h>h_d$ 


Avec :
  * $S$  = surface au miroir correspondant au niveau d'eau h dans le bassin,
  * $h_d$ = hauteur de la surverse
  * $h$ = hauteur d'eau dans le bassin
  * $L$ = 10 m (valeur par défaut)

## Commentaires

L’objet *reservoir* doit être posé obligatoirement sur un nœud hydrologique.

Lorsque l'option pollution est activé dans le gestionnaire de scénario le bouton pollution de l'objet *tank* offre la possibilité de calculer un abattement de pollution de l'effluent stocké selon deux modes :

- taux d'abattement
- concentration résiduelle.

<img src="/model_building/hydrology/tank_bc_pollution.png" width="450px"/>
