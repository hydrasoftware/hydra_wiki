---
title:  Bassin versant (*Catchment*)
description: 
published: true
date: 2023-04-20T08:53:13.076Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:42:31.565Z
---


## Définition

Délimitation d’un  bassin versant et calcul d’un hydrogramme de ruissellement à partir de données pluviométriques.

Le **contour de bassin versant** <img src="/model_building/hydrology/catchment_contour_polygon.png"/> `catchment contour` est une entité polygonale, elle  ne contient aucune donnée attributaire, c’est une donnée facultative.

Le **bassin versant** est représenté par le symbole <img src="/model_building/hydrology/catchment_node_point.png"/>. Il contient toutes les données attributaires.



## Données de l'élément
![catchement_node_form.png](/model_building/hydrology/catchement_node_form.png)

Il faut renseigner 4 types de données :

**1. Données générales du bassin versant**

- surface du BV $S$ (ha),
- longueur $L$ (m)
- pente moyenne $P$ (&nbsp;),
- coefficient imperméabilisation $C$ (&nbsp;)
- coefficient de ruisselement constant $C_r$ (&nbsp;)

Le nom du contour de BV entourant l’objet BV est indiqué s’il existe.

Elles peuvent être calculées à partir du contour de BV en cliquant sur le bouton Auto-fill (voir ci-dessous).

**2. Fonction de production de la pluie nette** (*Net flow production function*)  

Six modes de calculs de la pluie nette sont proposés :
* Coefficient de ruissellement constant
* Méthode d’**Horner**
* Méthode **Holtan**
* Méthode **SCS**
* Méthode **hydra** (SCS améliorée)
* Méthode **GR4J**


Les deux premiers modes de calculs s’appliquent à des bassins versants urbains ou à dominante imperméable, les quatre autres s’appliquent à des sols perméables caractérisés par une forte infiltration dans le sous-sol.

**3. Fonction de production du ruissellement à partir du calcul de la pluie nette** (*Runoff production function*)

Deux modes de calculs sont proposés :

* La méthode du **réservoir linéaire**, adaptée aux bassins versants urbains et basée sur l’application d’une formule de temps de concentration $K$ qui peut être spécifié par l'utilisateur (option Define K) ou déterminé par les loies: 

	- **Desbordes 1 Cr**: $
  K = 5.07 A^{0.18} (100 P)^{-0.36} (1 + C_r)^{-1.9}  L^{0.15} 30^{0.21} 10^{-0.07} 
  $ 
  valide quand 0.4ha < $S$ < 5000ha, 0.2 < $C$ < 1, 100m < $L$ < 18000m, 0.2% < $P$ < 15%
  
	- **Desbordes 1 Cimp**: $
  K = 5.07 A^{0.18} (100 P)^{-0.36} (1 + C)^{-1.9}  L^{0.15} 30^{0.21} 10^{-0.07} 
  $ 
  valide quand 0.4ha < $S$ < 5000ha, 0.2 < $C$ < 1, 100m < $L$ < 18000m, 0.2% < $P$ < 15
  
  - **Desbordes 2 Cimp**: $
  K = 5.3 A^{0.3} (100 P)^{-0.38} C^{-0.45}
  $
  valide quand 0.4ha < $S$ < 5000ha, 0.2 < $C$ < 1, 100m < $L$ < 18000m, 0.2% < $P$ < 15
  
  - **Krajewski**: $
  K =2.436 A^{0.455} C^{-0.57} (100 P)^{-0.127}
  $
  valide quand 0.4ha < $S$ < 5000ha, 0.2 < $C$ < 1, 100m < $L$ < 18000m, 0.2% < $P$ < 15
  
* La méthode de **l’hydrogramme unitaire** de type **Socose**. Il faut dans ce cas renseigner le temps de concentration $T_c$ et un coefficient de forme pour l’hydrogramme unitaire. Le temps de conscentration peut aussi être calculé avec les loies:

  - **Passini**: $
  T_c = 0.14 \frac{(A L)^{\frac{1}{3}}}{\sqrt{P}}
  $ 
  valide quand 4000ha < $S$ < 7000ha

  - **Giandotti**: $
  T_c = 60 \frac{0.4\sqrt{A} + 0.0015 L}{0.8\sqrt{P L}}
  $ 
  valide quand $S$ > 4000ha

  - **Turraza**: $
  T_c = 6.51\sqrt{A}
  $
  domaine de validité inconnu
  
  - **Ventura**: $
  T_c = 0.762 \sqrt{\frac{A}{P}}
  $ 
  valide quand $S$ > 1000ha
  
  - **Kirpich**: $
  T_c = 0.0195 \left(\frac{L}{\sqrt{P}}\right)^{0.77}
  $ 
  valide quand 4ha < $S$ < 80ha, 3% < $P$ < 10%, sol argileux
  
  - **Cemagref**: $
  T_c = {\rm e}^{0.375 \ln(A/100) + 3.729}
  $ 
  valide pour de petits bassins ruraux
  
  - **Mockus**: $
  T_c = 60 \frac{L^{0.8} (\frac{1000}{CN} - 9)^{1.67}}{2083 \sqrt{100 P}}
  $ 
  où le numéro de courbe $CN$ est approximé par $CN=\frac{1000}{10 + \frac{RFU}{25.4}}$ 
  valide quand 4ha < S < 1000ha, pente<1%, sols limoneux/argileux, la loi de production nette doit être de type SCS ou hydra (pour que la RFU soit définie)
  
 
**4. Données annexes**

* Débit de temps sec permanent à l’exutoire du BV (=0 par défaut)
* Ecrêtement du débit de pointe (pas d’écrètement par défaut)

<a name="auto_fill_hydrologic_parameters"></a>

## Calcul automatique des paramètres physiques des bassins versant et de la fonction de production de pluie nette

Il suffit de cliquer sur le bouton **Auto fill** du formulaire d’édition de l’objet catchment. HYDRA exécute les opérations suivantes :

- Calcul de la **surface** du BV et renseignement du champ area,

- Calcul de la **longueur** du thalweg, basée sur la longueur développée de l’objet hydrology_routing,

<img src="/model_building/hydrology/length_computation.png" style="transform: scale(0.6);"/>

- Calcul de la **pente** (uniquement si le MNT sous-jacent est présent) :
Pente    = (ZtnA – ZtnB) / L

- Calcul du **coefficient d’imperméabilisation**. Ce calcul est effectué uniquement si la table land_occupation est renseignée.
Le coefficient d'imperméabilisation est calculé par pondération surfacique des coefficients des polygones (définis via la table land_types) intersectés par le contour de BV <img src="/model_building/hydrology/catchment_contour_polygon.png"/> `catchment contour` : 

<img src="/model_building/hydrology/c_imp_computation.png" style="transform: scale(1.0);"/>

Tous ces paramètres peuvent être modifiés manuellement au besoin.

> **note**
>
> Une pente minimum = 0.005 est imposée.
> 
> Le champ Cr est défini à partir du champ Cimp comme suit : Cr= 0.7 x Cimp.
> 
> 

Le calcul du **coefficient d’imperméabilisation** s'appuie sur la table *land_occupation* qui doit être préalablement importée, ainsi que sur la table *land_type* qui définit le coefficient d'imperméabilisation associé à chaque type d'occupation du sol. La démarche est décrite ci-après.

**Etape 1** : Importer une table d'occupation des sols via l’outil external_table_manager dans le menu <img src="/0_hydra_icone.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> **Advanced tools / Manage external tables** (cf. [Import_shape](/advanced/import_shape)).


<img src="/advanced/import_shape/import_shape_menu.png" style="transform: scale(0.8);"/>

Importer la table .shp d’occupation des sols dans le schéma work à l’aide du bouton <img src="/0_hydra_+.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/>



<img src="/model_building/hydrology/import_shape_land_occupation_ui.png" style="transform: scale(0.8);"/>

La table mos_paris de l'exemple ci-dessus est stockée dans le schéma work de la base de données.  

**Etape 2** : importer cette table dans la table *land_occupation* du projet

- Affecter le c_type de la table source au champ Land_type de la table Hydra.
*Le champs C_imp de la table Hydra n’est utile que si la table d’origine fournit pour chaque enregistrement le nom du type d’occupation des sols et le coefficient d’imperméabilisation correspondant. Ce n’est généralement pas le cas. Ces champs peuvent donc être ignorés dans la plupart des cas.*

- Importer dans le projet HYDRA à l’aide du bouton import à droite du formulaire.

L’opération import provoque deux actions :
1.    Création de la table land occupation dans le répertoire Projet d’HYDRA 
2.    Remplissage de la table land type Cette table est accessible via le menu hydrology du plug_in. 

Le champ itype_id de la vue projet\ land_occupation contient deux champs en plus de la clé primaire :
1.    Le champ land_type renseigné dans le fichier MOS.shp ou la table importée dans le schéma Work
2.    Le champ Ci : coefficient d’imperméabilisation.

La logique de remplissage de la table land_types est présentée ci-après.

<img src="/model_building/hydrology/import_shape_land_occupation_layer.png" style="transform: scale(0.8);"/>

**Etape 3** : renseigner la table *land_type* du projet

L'affectation du coefficient d'imperméabilisation et des paramètres des lois de production associés aux différents types d'occupation du sol se fait via la table *land_types* du projet.

L’activation du bouton import a pour effet de créer autant enregistrements que de types d'occupation du sol différents sont recensés dans la table land_aoocupation, et de renseigner les deux champs land_type et CI de la table land_types.
- Le champ land_types est défini à partir des données de types différents répertoriés dans le fichier MOS.
- Le champ CI est égal=0.5 par défaut, il peut être modifié par l’utilisateur pour chaque enregistrement.

Ces éléments peuvent être édités via le menu <img src="/0_hydra_icone.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> **Hydrology / Land occupation**.
<img src="/model_building/hydrology/land_type_menu.png" style="transform: scale(1.0);"/>


<img src="/model_building/hydrology/land_types_ui.png" style="transform: scale(0.75);"/>


Le formulaire ci-dessus contient des champs complémentaires, à renseigner pour des applications spécifiques comme la modélisation du ruissellement via un maillage 2D (cf. [Runoff](/model_building/runoff)).


> **note**
>
> Les données renseignées ci-dessus peuvent être sauvegardées dans un fichier Excel via l’outil Copy/Paste.
> 
> De même les données créées ou sauvegardées dans un fichier Excel peuvent être copiées dans le formulaire ci-dessus. Les champ « land_type » doivent dans ce cas être cohérents avec ceux de la table land_occupation. 
> 


> **note**
>
> Le bouton **Load preset land type** permet d’importer des profils courants pré-enregistrés dans la base d’HYDRA (MOS Ile de France et COrine Land Cover).
> 
> 
> 

## Commentaires

Les différentes formulations et les règles de sélection des différents paramètres sont précisées dans le manuel d’analyse modélisation hydrologique. 

Concernant la méthode GR4J il convient de noter que tous les paramètres sont définis dans l'onglet fonction de production, contrairement aux autres lois pour lesquelles on distingue la fonction de production du ruissellement de la fonction de production de la pluie nette. 
Cette mthode désigne indifféremment la loi GR4J (pas de temps de 24h) ou GR4H (pas de temps 1h. les seuls  pas de temps de calculs hydrologiques autorisés sont 24h et 1h pour cette méthode. La loi est sélectionnée dans les calculs en fonction du pas de temps déclaré dans le gestionnaure de scénario.

L’objet *contour de BV* <img src="/model_building/hydrology/catchment_contour_polygon.png"/> `catchment contour` doit être défini uniquement si la pluie sélectionnée nécessite le calcul d’une lame d’eau moyenne attachée au BV : pluie *radar* ou lame d’eau définie à partir de données de pluviographes.

Les données du bouton *pollution* précisent le mode d'occupation des sols et la nature du réseau. Elles doivent être renseignées lorsque l'option *pollution* est activée dans l'onglet type de transport *(transport type) du gestionnaire de scénario*.

