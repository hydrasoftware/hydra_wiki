---
title:  Regard ou noeud hydrologique (*hydrology node*)
description: 
published: true
date: 2021-02-09T11:03:53.928Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:48.057Z
---


## Définition

Regard d’une arborescence hydrologique



## Données de l'élément

<img src="/model_building/hydrology/hydrology-node.png"/>

Les données attributaires sont :
    * Ground elevation : la cote du tampon
    * Area (m²) : la section du regard exprimée en m²


## Commentaires

Cet objet ne peut être connecté qu’aux links suivants :
    * <img src="/model_building/hydrology/connector_hydrology_line.png"/> [connecteur hydrologique](/model_building/hydrology/connector_hydrology_line) (*hydrology connector*) 
    * <img src="/model_building/hydrology/routing_line.png"/> [routage hydrologique](/model_building/hydrology/routing_line) (*hydrology routing*)
    * des pipes

Il ne peut être occupé que par une des singularités suivantes :
    * <img src="/model_building/hydrology/qq_split_hydrology_point.png"/> [dérivation QQ](/model_building/hydrology/qq_split_hydrology_point) (*split QQ*)
    * <img src="/model_building/hydrology/zq_split_hydrology_point.png"/> [dérivation ZQ](/model_building/hydrology/zq_split_hydrology_point) (*split ZQ*)
    * <img src="/model_building/hydrology/reservoir_rs_point.png"/> [bassin de stockage](/model_building/hydrology/reservoir_rs_point) (*reservoir*)
    * <img src="/model_building/hydrology/reservoir_rsp_point.png"/> [bassin de stockage paramétrique](/model_building/hydrology/reservoir_rsp_point) (*parametric reservoir*)


