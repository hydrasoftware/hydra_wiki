---
title:  Discharge split singularity (*split_QQ*)
description: 
published: true
date: 2021-02-09T11:03:56.563Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:52.075Z
---



## Définition

Calcul de répartition de débit entre une branche aval et une ou deux branches dérivées selon une loi « split QQ » :

<img src="/model_building/hydrology/splitqq-fonct.png" width="300px" align="center"/>


## Données de l'élément

<img src="/model_building/hydrology/splitqq.png" width="430px" align="center"/>

Pour sélectionner la branche aval ou  une branche dérivée :
  * Cliquez sur le bouton *selection*
  * Puis cliquez dans le réseau sur le tronçon à sélectionner

Les lois de dérivation sont définies sous forme de courbes tabulées Qd1(Qam) et Qd2(Qam). L’hydrogramme aval est déduit par différence : Qav= Qam-Qd1-Qd2.

Chaque courbe de débit doit être strictement croissante.

## Commentaires

L’objet *discharge split singularity* doit être posé obligatoirement sur un nœud hydrologique.

