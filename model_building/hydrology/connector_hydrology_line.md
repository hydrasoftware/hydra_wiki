---
title:  Connecteur hydrologique (*hydrology connector*)
description: 
published: true
date: 2021-02-09T11:03:45.820Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:36.493Z
---




## Définition

Le connecteur hydrologique sert à **propager un hydrogramme** entre deux noeuds de calcul **sans déformation** ni **déphasage**.


## Données de l'élément

<img src="/model_building/hydrology/hydrology-connector.png"/>

Cet objet  ne contient aucune donnée attributaire.

## Commentaires

Cet objet est de type *link*.

Son extrémité aval doit être connectée à un des deux objets suivants :
  * <img src="/model_building/hydrology/hydrology_node_point.png"/> [un regard](/model_building/hydrology/hydrology_node_point) (*hydrology node*),
  * un containeur hydraulique. Il est obligatoire dans ce cas que le container soit occupé par la singularité « hydrograph », faute de quoi l’objet connecteur ne peut pas être créé

