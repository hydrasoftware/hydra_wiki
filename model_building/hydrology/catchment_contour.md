---
title: contour de bassin versant (*Catchment contour*)
description: 
published: true
date: 2021-02-09T11:03:37.810Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:42:23.749Z
---


## Définition

Délimitation d’un  bassin versant et calcul d’un hydrogramme de ruissellement à partir de données pluviométriques issues d'images radar.

Le **contour de bassin versant** <img src="/model_building/hydrology/catchment_contour_polygon.png"/> `catchment contour` est une entité polygonale, elle  ne contient aucune donnée attributaire, c’est une donnée facultative.

Les caractéristiques du **bassin versant** sont représentée par l'objet <img src="/model_building/hydrology/catchment_node_point.png"/>.



## Données de l'élément

<img src="/model_building/hydrology/catchment_contour_ui.png"/>

Le contour de bassin versant est identifié par un nom.

Lorsqu'une pluie radar est présente dans la bibliothèque de pluies (et les cumuls de pluie par bassin versant calculés, cf. [Pluies radar](/hydrology_data_management/radar_rainfall)), les hyétogrammes affectés à chaque contour de bassin versant peuvent être visualisés via le bouton **Hyetogram**.

<img src="/model_building/hydrology/catchment_contour_hyetograph.png" style="transform: scale(0.75);"/>


## Commentaires



