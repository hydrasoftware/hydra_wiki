---
title:  Routage hydrologique (*Hydrology routing*)
description: 
published: true
date: 2023-03-16T23:04:59.837Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:42:59.798Z
---


## Définition

Cet élément permet de définir un décalage temporel entre un hydrogramme ruisselé calculé à l’exutoire d’un BV et le point d’injection dans le réseau.



## Données de l'élément


<img src="/model_building/hydrology/routing.png"/>

En général le décalage est nul, ce qui est reflété par les valeurs ci-dessus renseignées par défaut.


## Commentaires

Cet objet est de type « link ». Il sert de lien entre un **bassin versant hydrologique** et le **réseau hydraulique**.

Son extrémité amont est obligatoirement connectée à un objet <img src="/model_building/hydrology/catchment_node_point.png"/> [bassin versant](/model_building/hydrology/catchment_node_point) (*catchment*).

Son extrémité aval doit être connectée à un des deux objets suivants :
   * un <img src="/model_building/hydrology/hydrology_node_point.png"/> [regard](/model_building/hydrology/hydrology_node_point) (*hydrology node*)
   * un containeur hydraulique qui peut être de type quelconque (`manhole`, `river node`, `station node`, `storage`, `element 2D`, `crossroad`) mais qui doit impérativement porter un objet `hydrograph` faute de quoi l’objet routage ne peut pas être créé.

Le paramètres **Split coefficient** permet de réaprtir les apports d'un `catchment` vers plusieurs noeuds du modèle hydraulique. 
Plusieurs liaisons `routage` peuvent ainsi être reliées à un `catchment`, afin de réaprtir les apports calculés par la modélisation pluie - débit vers différents points d'apport dans le modèle hydraulique. 
- Dans l'exemple ci-dessus, 100% des apports du catchment_1 sont dirigés vers le NOD_2314 (Split coefficient = 1).
- Dans l'exemple ci-dessous, 40% sont dirigés vers le NOD_1544 (les 60% restants sont dirigés vers un ou plusieurs autes noeuds).


    ![routing_2_catchment.png](/model_building/hydrology/routing_2_catchment.png)

> :high_brightness: Le paramète *Split weight on catchment 1* permet de contrôler que tous les apports du catchment 1 sont bien affectés à un noeud; ce paramètre doit in fine être égal à *Split coefficient*.


