---
title:  Marqueur de branche (*Branch marker*)
description: 
published: true
date: 2021-02-09T11:04:58.669Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:23.904Z
---



## Définition

Marqueur de branche. 

La branche est une succession de **tronçons de collecteurs** (<img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line)) délimités à ses extrémités par une confluence, une défluence, un début ou une fin de tronçon.



## Données de l'élément

<img src="/model_building/network/marker_branch.png" width="230px"/>

- pk0 = pk du début de la branche

- dx = pas d’espace entre deux nœuds de calcul (50 m par défaut)

## Commentaires

Les branches de collecteurs sont découpées automatiquement et de façon dynamique au fur et à mesure de l’enrichissement du modèle. Les branches ainsi générées et leur délimitation peuvent être consultées dans la couche « branch » du gestionnaire de couches.

Il n’est donc pas nécessaire de marquer une branche. Toutefois la singularité branch <img src="/model_building/network/marker_branch_point.png"/> [Marker](/model_building/network/marker_branch_point) permet à l’utilisateur de donner à une branche le nom de son choix et de **redéfinir le pas de discrétisation des calculs**; par défaut ce pas est égal à 50m.


Pour imposer un nom de branche utilisateur il suffit de poser le **marqueur de branche** <img src="/model_building/network/marker_branch_point.png"/> sur n’importe quel **regard** ( <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point)) faisant partie de la branche.


