---
title:  Regard (*Manhole*)
description: 
published: true
date: 2021-06-24T09:28:06.236Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:19.914Z
---



## Définition

Regard d’un réseau d’assainissement

## Données de l'élément

<img src="/model_building/network/manhole.png" width="470px" align="center"/>

Les données attributaires sont :
  * la cote du tampon (*Ground elevation*)
  * la section du regard en m² (*Area*)
  * Le type de connexion du haut du regard avec la chaussée et sa géométrie; deux types d’ouvertures sont disponibles :
        * Tampon *(manhole cover)*: diamètre d'ouverture du tampon et pression de rupture
        * Bouche avaloir *(drainage inlet)*: largeur et hauteur

La connexion entre le sommet du manhole et le terrain naturel se fait lorsque des liaisons de type <img src="/model_building/links/network_overflow_line.png"/> ([Network overflow link](/model_building/links/network_overflow_line)) ou <img src="/model_building/links/interlink_line.png"/> ([Interlink](/model_building/links/interlink_line>)) sont générées.



## Equations

<img src="/model_building/network/manhole_equations.png" width="150px"/>

Avec :
  * qi  = débit du tronçon connecté au regard,
  * qd = débit de débordement si z > zground. Ce débit est exprimé par une loi d’orifice si l'ouvrage de connexion est un tampon, et par une loi de seuil si c'est une bouche avaloir.

Le débit débordé est considéré comme perdu par le modèle, sauf si l’option *NOVL_STORAGE est activée. Dans ce cas le volume excédentaire est stocké provisoirement en haut du regard, puis restitué dans le réseau dès en phase de décrue. 

Si le type de connexion est « tampon » et que la pression de rupture contre le tampon est atteinte en cas de débordement, celui-ci saute et le programme sélectionne une section d’orifice égale à la section du regard.

Si le regard est connecté à un réseau de surface via une liaison binodale de type **surverse depuis un réseau** (<img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line)) le débit déversé n’est pas perdu mais échangé avec le réseau de surface.



## Commentaires

Les **tronçons de collecteur** ( <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line)) ne peuvent être connectés qu’à cet objet.

Le regard ne peut être occupé que par **une singularité** de type *structures ans headlosses*

Pour plus de précisions sur les modalités de calcul des échanges et déversement vers la surface, se reporter à <img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line).

