---
title:  Tronçon de collecteur (Pipe)
description: 
published: true
date: 2022-01-28T08:06:18.593Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:27.830Z
---



## Définition

Tronçon de collecteur d’assainissement

## Données de l'élément

### Données communes à tous les types de tronçon

<img src="/model_building/network/pipe.png" width="430px"/>

Les données attributaires sont :
  * la cote amont (*Upstream elevation*)
  * la cote aval (*Downtream elevation*)
  * la hauteur moyenne d'ensablement (*Silting height*) supposée constante au cours d’une simulation
  * la longueur du collecteur en m (*Length*)

La **géométrie** d’un collecteur peut être de 4 types différents :

<img src="/model_building/network/_network_sections.png" style="transform: scale(0.75);"/>

La **longueur** du collecteur est par défaut déduite des coordonnées amont et aval des regards connectés au tronçon. L’utilisateur peut forcer cette longueur à une valeur de son choix pour la simulation. Cette modification entraînera cependant un décalage pour le dessin des profils en longs dans la phase d’exploitation des résultats.

>La zone de texte **comment** permet d'ajouter un commentaire. Elle permet notamment de préciser le type de réseau associé à ce collecteur, via des mots clés qui seront ensuite traduits par un style spécifique dans le gestionnaire de couches:
>
>- *EU* : trait épais rouge
>- *UN* : trait épais violet
>- *EP* : trait épais vert

L'option **Exclude from branch** transforme l’objet pipe en une simple liaison bi-nodale sans ajout de nœuds de calcul intermédiaires dans la phase d’exécution des calculs. Aucune branche n’est générée à travers le tronçon, ce qui allège d’autant les calculs. Cette option est utile dans les applications mettant en œuvre des modèles de taille importante et comportant des tronçons jouant un simple rôle de convecteurs entre deux nœuds.


### Données spécifiques à chaque type de collecteur

- **Section circulaire** `circular`

<img src="/model_building/network/pipe_circ.png" width="410px"/>

- **Section ovoïde** `ovoïd`

<img src="/model_building/network/pipe_ov.png" width="410px"/>

<br/>

- **Section paramétrique fermée** `pipe`

<img src="/model_building/network/pipe_param.png" width="430px"/>

- Les sections géométriques sont définies sous forme tabulée de couples de valeurs hauteur-largeur. Le nombre de points est limité à 20.

- Ces sections  sont stockées dans une bibliothèque et peuvent être appelées par autant de tronçons qu’on le désire.

<br/>

- **Section paramétrique ouverte** `channel`

<img src="/model_building/network/pipe_param_channel.png" width="430px"/>

- Les sections géométriques sont définies sous forme tabulée de couples de valeurs hauteur-largeur. Le nombre de points est limité à 20.

- Ces sections  sont stockées dans une bibliothèque et peuvent être appelées par autant de tronçons qu’on le désire.

<br/>

>*Par défaut les sections fermées de conduite (`circular`, `ovoid` et `pipe`) sont surmontées d’une fente de 5cm de large, afin de 
temporiser l’élévation du niveau d’eau et maintenir un pas de temps raisonnable. La cote d’eau calculée est alors assimilée à une cote piézométrique.*
>![fente.png](/model_building/network/fente.png)
>*Dans certaines applications, cet artifice peut fausser les bilans de volume. Cet inconvénient peut 
être corrigé en réduisant cette largeur à 1mm via l'option **NO_FENTE** qui peut être appelée dans le [paramétrage de scénario](../../scenarios#scenario_computation_runoff).*


>Pour les **collecteurs ouverts** (`channel`) le canal est prolongé par des parois verticales fictives prolongeant la dernière largeur de la section.


## Pipes sur réseaux hydrologique ou hydraulique

L’objet **pipe** est de type *link*, il est connecté à ses deux extrémités :
  * soit avec deux nœuds hydrologiques,
  * soit avec deux regards de collecteur.

Cet objet est donc utilisé à la fois pour les **arborescences hydrologiques** et pour le **réseau hydraulique d’assainissement**. 

Les équations mises en œuvre dans  chaque domaine sont néanmoins très différentes :
  * dans le cas de l’**arborescence hydrologique**, les écoulements sont décrits par de simples lois de routage : les conditions de remous aval sont négligées et les mises en charge sont décrites localement. Les équations correspondantes sont décrites dans le manuel d’analyse : « Modélisation hydrologique ».
  * dans le cas du **réseau hydraulique d’assainissement**, les écoulements le long de chaque tronçon sont régis par les équations de St Venant intégrées sur la section du collecteur. Les équations mises en œuvre sont décrites dans le manuel d’analyse : « Modélisation hydraulique ».

<a name="pipe_link"></a>

## Conversion un tronçon de collecteur en simple liaison de type link

Très souvent les modèles de réseaux d’assainissement intègrent des tronçons de collecteurs très courts qui représentent en fait des galeries de liaison. Afin d’alléger les modèles et limiter le nombre de branches, hydra offre la possibilité de **convertir un tronçon en simple liaison de type link** (raccordée à un nœud de type mandole à chaque extrémité) via l'option *Exclude_from_branches* de l'éditeur de collecteurs <img src="/model_building/network/pipe_line.png"/>.

Il suffit de cocher la case **Exclude from branches** dans l’éditeur du tronçon :

<img src="/model_building/network/pipe_to_link-option.png" style="transform: scale(0.75);"/>

Dans l’exemple ci-dessus deux tronçons sont transformés en objets link-pipe : le nombre de branches est limité à 4, il est égal à 8 si on l’option « excluded » est désactivé. 

Par ailleurs aucun nœud de calcul intermédiaire n’est généré le long des tronçons convertis en links. Cette simplification entraîne bien sûr une précision moindre dans les calculs : elle doit être appliquée à bon escient, en fonction de l’objectif de modélisation visé.

