---
title: Station de gestion
description: 
published: true
date: 2021-02-09T11:04:53.478Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:15.971Z
---


## Définition

Contour polygonal représentant l’emprise d’une usine ou d’un ouvrage hydraulique complexe.

## Données de l'élément

<img src="/model_building/station/station_ui.png" width="260px"/>

Aucune donnée attributaire.

## Commentaires

La station de gestion n’est pas à proprement parler un objet de modélisation, c’est une unité fonctionnelle qui sert à regrouper visuellement un certains nombres d’objets de modélisation : [Station node](/model_building/station/station_node) et *links* qui représentent généralement des équipements électro mécaniques.

Une station de gestion n’est pas  soumise à des contraintes de positionnement géographique. Elle peut se superposer à n’importe quel autre objet, notamment les coverage et n’interfère aucunement avec ces entités.

Le contour d’une station de gestion ne peut pas être traversé par une branche de collecteur ou un bief filaire : la connexion avec les nœuds de gestion internes à la station ne peut être assurée que par les objets *links*.

<img src="/model_building/station/_station_example.png" style="transform: scale(0.75);"/>


