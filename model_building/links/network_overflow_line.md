---
title:  Surverse depuis un réseau (*Network overflow link*)
description: 
published: true
date: 2021-02-09T11:03:00.600Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:27.855Z
---



## Définition

Liaison de surverse entre un regard de collecteur et un conteneur de surface : carrefour ou maille 2D.

## Données de l'élément

<img src="/model_building/links/network_overflow_ui.png" width="260px"/>

Les deux paramètres *Zoverflow* et *Area*  définis dans le feêtre ci dessus sont ignorés en pratique car ils sont déjà contenus dans les objets suivants :

- *Zoverflow* est sélectionné dans les calculs comme la cote de terrain naturel du carrefour ou de la maile 2D connectée à la liaison.

- *Area* est la section  du regrd *manhole* connecté à la liaison. 


## Equations et configurations d'écoulement

Les équations d’échanges sont gérées par une loi symétrique de type  [*gate*](/model_building/links/gate_line) selon les niveaux d’eau respectifs de part et d’autre de la bouche d’égout. 

Les dimensions de la fenêtre de décersement sont définis par l'objet [*manhole*](/model_building/network/manhole_point).


Les 4 régimes possibles d’écoulement sont récapitulés ci-dessous :

<img src="/model_building/links/network_overflow_sch1.png" width="550px"/>

## Commentaires

Cet objet est de type *link*.

Son extrémité amont est nécessairement connecté à un regard ( <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point)).


Son extrémité aval est en général connectée à un des deux containeurs suivants :

- une maille 2D,
- un carrefour.

En pratique les liaisons *network overflow* sont générées automatiquement via le bouton <img src="/model_building/network/network_overflow_button.png"/>.



Cet outil  scanne l’ensemble des objets *manhole node* et scrute la présence d’un *coverage 2D* ou d’un coverage *street* au-dessus du node. Si c’est le cas il génère la liaison *network overflow*. 

Dans le cas du coverage *street*, il ajoute au besoin des carrefours pour connecter au plus près le *manhole node* à la surface.


