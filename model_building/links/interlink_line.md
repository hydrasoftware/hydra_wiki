---
title:  Liaisons de connexion d'un modèle de réseau à un modèle de surface (*Inter link*)
description: 
published: true
date: 2021-06-24T09:25:02.648Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:40:55.289Z
---



## Définition

Liaison de connexion entre les éléments d'un modèle de réseau (Manhole <img src="/model_building/network/manhole_point.png"/>) et les éléments d'un modèle de surface.


## Données de l'élément

Les règles de génération et les caractéristiques des liaisons sont les mêmes que dans le cas d’un seul réseau, où le couplage réseau / surface se fait via des liaisons <img src="/model_building/links/network_overflow_line.png"/> ([Network overflow link](/model_building/links/network_overflow_line)). Elles sont applicables à des coverage 2D, des coverage streets et des coverage storage.


Ces liaisons sont générées en lot, cf. [Interlink](/model_building/#interlink).

## Commentaires

Cet objet est de type *link*; il est stocké dans le schéma **Projet**.




