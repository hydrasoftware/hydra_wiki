---
title:  Connecteur (*Connector link*)
description: 
published: true
date: 2021-02-09T12:39:02.624Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:07.272Z
---




## Définition

Connection simple entre deux containeurs.

## Données d'entrée

<img src="/model_building/links/connector.png" width="250px"/>

Aucune donnée attributaire pour cet objet

## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

L’objet *connector* est d’une utilisation très large. Il sert le plus souvent à raccorder des domaines entre eux, en l’absence de singularités physiques.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : <img src="/model_building/river_free_surface_flow/mesh_button.png"/> <img src="/model_building/river_free_surface_flow/mesh_button_2.png"/>



Ce mode de génération est utilisé notamment pour définir une condition à la limite le long d’un frontière d'un domaine 2D.

## Equations

On suppose Q>0 . zc est la cote critique calculé pour le débit Q et la section 1.

* Si z2 > zc : on impose : z1 = z2 
* Si z2 < zc : on impose z1 = zc

Si le régime est torrentiel en amont zc est remplacé par la cote normale zn.

Si Q <0 : les conditions d’écoulement sont symétriques.

