---
title:  Seuil déversant (Weir link)
description: 
published: true
date: 2021-11-05T17:15:43.568Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:40.767Z
---




## Définition

Seuil latéral

Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

## Données de l'élément

![weir_line_ui.png](/model_building/links/weir_line_ui.png)

Le seuil déversant est défini par :

- une cote radier (*Z invert*), 
- une cote de seuil fixe (*Z weir*),
- une largeur de seuil (*Width*),
- un coefficient de seuil,

La position du seuil (*Z weir*) peut être modifiée durant une simulation via le module externe de contrôle et régulation. Le paramètre V max règle dans ce cas la vitesse maximum de déplacement du seuil de la vanne.


## Equations

Les équations font référence aux points 1 amont de l'écoulement et 2 aval de l'écoulement, avec :
- $V_1$ et $V_2$ vitesses de l'écoulement dans l'axe de la liaison (m/s)
- $Z_1$ et $Z_2$ ligne d'eau (m)
- $E_1$ et $E_2$ charge (m)
- g accélération de la pesanteur $9.81m/s^-2$.

Les équations supposent $E_1 > E_2$, sinon l'écoulement se fait en sens inverse et $E_1$ ou $E_2 > z_s$, sinon il n'y pas d'écoulement.

Les différents régimes d'écoulement (dans les deux sens) sont définis sur le ldiagrame ci-dessous.

![weir_link_equations.svg](/model_building/equations/weir/weir_link_equations.svg)

**Expression de la charge :**

- $E_1 = z_1 + \frac{v_1^2}{2 g}$.
- $E_2 = z_2 + \frac{v_2^2}{2 g}$.

La **vitesse prise en compte** pour le calcul de la charge dépend du type d'élément et de la position de la liaison:
- Casier, carrefour, noeud 1D hors extrémité de *branch* ou de *reach* (maillage latéral) : v = 0
- Noeud 1D en extrémité de *branch* ou de *reach* : vitesse sur le noeud
- Maille 2D : composante de la vitesse projetée sur l'axe de la liaison

**Condition seuil noyé:** $(E_2-z_s) > \frac{2}{3} (E_1-z_s)$

<br/>

| Régime de fonctionnement *(1>2)*  |     Equation    |
|:---------------------------|:-------------------------------------------------|
| Déversoir dénoyé | $QD_d = \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-z_s)^\frac{3}{2}$.  |
| Déversoir noyé | $QD_n = C_n b_s \sqrt{2 g} (E_2-z_s)(E_1-E_2)^\frac{1}{2}$.  |

<br/>

## Paramètres

Le coefficient **Cd** est le **coefficient de seuil déversant en régime dénoyé**. Il est défini par l’utilisateur, il est généralement égal à sa valeur théorique: 0.58.
L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque $\frac{b}{b0}<1$, où $b0$ est la largeur du canal ou du collecteur en amont de la vanne. On recommande d’appliquer le coefficient de correction « alp » suivant sur CD:

| b/b0   |     alp = Cd/0.6         |
|:-------|:-------------------------|
| 1      |   1                      |
| 0.8    |   0.8                    |
| 0.6    |   0.6                    |
| 0      |   0.6                    |


Le coefficient **Cn** est le **coefficient de seuil déversant ou de vanne en régime noyé**. Il est par défaut égal à $1.73*Cd$ (soit ~1 dans le cas où $Cd=0.58$), sauf dans le cas où la liaison relie deux extrémités de reach ou de branch où ce dernier est alors corrigé automatiquement par un facteur de correction *alp* qui tient compte du rapport de section s/S où :
- s est la section sur le seuil sous la hauteur d'eau $z_2$,
- S est la section mouillée dans le collecteur aval de la liaison.

| s/S   |     alp                    |
|:-------|:-------------------------|
| 0      |   1.0                    |
| 0.3    |   1.0                    |
| 0.6    |   1.0                    |
| 0.8    |   1.87                   |
| 0.9    |   3.12                   |
| 0.95   |   5                      |



>**Note**
>La formulation retenue pour le déversoir dénoyé est la même que pour celle de la vanne *Gate* en régime *Déversoir dénoyé*.
>
>Toutefois le critère de passage du régime dénoyé en régime noyé et la définition du coefficient Cn sont différents:
>- Condition de passage en régime noyé :
>    - Pour les Weir : $(E_2-z_s) > \frac{2}{3} (E_1-z_s)$
>    - Pour les Gate : $E_2 \geq z_s$
>- Le coefficient Cn:
>    - Pour les Weir : $Cn = 1.73*Cd*alp$ avec alp fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps),
>    - Pour les Gate : formulation indépendante de $Cd$ mais fonction du rétrécissement que crée l'ouvrage par rapport au lit mineur ou au collecteur (re-calculé à chaque pas de temps). L'utilisateur peut cependant imposer une valeur fixe.
>
>La simplification faite pour les Gate est rendue nécessaire pour satisfaire la continuité de débit entre tous les régimes qui sont beaucoup plus nombreux que dans le cas du Weir. Le critère de transition entre l’écoulement noyé et l’écoulement dénoyé est considéré comme physiquement plus précis dans la formulation Weir.  

  