---
title:  Liaison poreuse (*Porous link*)
description: 
published: true
date: 2021-02-09T11:03:03.160Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:31.898Z
---



## Définition

Fuites à travers des digues perméables.

## Données de l'élément

<img src="/model_building/links/porous_line_ui.png" width="450px"/>

## Equations

<img src="/model_building/links/porous_line_eq1.png" width="420px"/>


<img src="/model_building/links/porous_line_eq2.png" width="130px"/>


T est la transmittivity (en m2/s)

b est la largeur (m).

## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Il est principalement utilisé pour raccorder latéralement des biefs 1D parallèles de vallée.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : <img src="/model_building/river_free_surface_flow/mesh_button.png"/> <img src="/model_building/river_free_surface_flow/mesh_button_2.png"/>




