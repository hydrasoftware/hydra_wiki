---
title:  Liaison 2D (*Mesh 2D link*)
description: 
published: true
date: 2021-02-09T11:02:49.534Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:11.581Z
---




## Définition

Liaisons internes entre mailles d’un domaine 2D

## Données de l'élément

<img src="/model_building/links/mesh_link_ui.png" width="260px"/>

Le coefficient de contraction latérale est le plus souvent égal à 1. Il a été introduit pour modéliser les écoulements dans les îlots urbains avec la prise en compte  des effets de blocage partiels des bâtiments.

*Z invert*  est la cote la plus basse calculée le long de l’arête AB :

<img src="/model_building/links/mesh_link_sch1.png" width="250px"/>

La définition de cette cote basse est motivée par la raison suivante :

Chaque pavé est caractérisé par une cote moyenne de fond zfi renseignée par l’utilisateur. La connaissance de cette seule cote ne suffit pas pour décrire correctement les seuils d’inondation. 

En effet, le fond d’un pavé n’est pas nécessairement plat : il peut avoir une pente et être alimenté par une liaison à une cote inférieure à la cote moyenne zfi. 

Cette configuration est illustrée par le schéma suivant :

<img src="/model_building/links/mesh_link_sch2.png" width="550px"/>

La liaison interne entre deux mailles 2D est en définitive définie par deux côtes :

- la cote générique Zbij. La liaison contrôlée par cette cote est gouvernée par une loi d’écoulement de type « poreux », à faible débitance : le débit de transfert ne joue aucun rôle dans les échanges de quantité de mouvement entre mailles mais il permet de décrire le remplissage progressif d’une maille à partir d’un point bas le long d’une de ses arêtes. 
- la cote induite Zsij = max ( Zfi, Zfj). La liaison contrôlée par cette cote est gouvernée par les équations de barré de St Venant.

En pratique les écoulements à travers la liaison basse ne sont pas modélisés hydrauliquement mas la cote basse est exploitée après chaque pas de temps de calcul pour « corriger » la pénétration de l’eau d’une maille active vers les points bas d’un maille adjacentes. Cette correction reste locale mais elle fournit une cartographie  plus réaliste des mailles inondées  dans la cas d’une topographie accidentée.


## Equations

Les écoulements dans  un domaine 2D sont régies par les équation de St Venant. 

La discrétisation de ce domaine en mailles 2D et liaisons internes entre mailles est une commodité de représentation, chaque objet contribuant aux termes volumiques et aux termes de flux intervenant dans les équations de St Venant.

Ces équations sont décrites dans le manuel d’analyse : « Modélisation hydraulique ».


## Commentaires

L'objet *mesh link* est en pratique généré automatiquement à l'aide de l'outil de maillage : <img src="/model_building/river_free_surface_flow/mesh_button.png"/>




