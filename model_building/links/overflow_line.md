---
title:  Surverse (*Overflow link*)
description: 
published: true
date: 2023-03-16T21:26:58.744Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:03.271Z
---



## Définition

Franchissement d’un obstacle linéaire dans ou le long du lit majeur : berge, talus, remblais.

## Données de l'élément

<img src="/model_building/links/overflow_line_ui.png" width="450px"/>

Cet objet est défini par un assemblage de **deux seuils déversants** parallèles dont les caractéristiques (largeur *Width*, cote de seuil *Z crest*) sont déduites de la géométrie de l’obstacle.

Le paramètre *weir coefficient* est le **coefficient de seuil** classique,

Le paramètre *latéral contraction coefficient* contrôle la **largeur effective de la lame déversante**. Il est fonction de la nature physique de la liaison.

**Paramètres de rupture de berge**

Il est possible de simuler une rupture de berge avec cet objet en activant l’option *break mode*. 

2 modes de rupture sont possibles :

- Début de rupture commandée par le dépassement d’un niveau d’eau :

<img src="/model_building/links/overflow_line_rupt1.png" width="470px"/>

- Début de rupture commandé par un temps :

<img src="/model_building/links/overflow_line_rupt2.png" width="470px"/>

Le paramètre *Group number* détermine le groupe de brèches qui se forment simultanément, selon la logique suivante : dès qu’une brèche est détectée sur une liaison d’un groupe donné, les brèches sont déclenchées simultanément sur toutes le liaisons du groupe.

Les autre paramètres de rupture sont :

- *Zinvert* : cote d'arase de la rupture. Cete cote est généralement inférieure à  *Min Zcrest_up, Zcrest_down).
- La largeur totale de la brèche en fin de rupture (qui doit rester inférieure à la largeur de la liaison)
- La courbe temporelle d’élargissement de la brèche à partir de l’instant de la rupture.

Après rupture la séquence temporelle d'élargissment de la brèche est la suivante :

<img src="/model_building/links/overflow_break.png" width="600px"/>


## Equations

Le programme gère deux seuils déversants en situation normale et trois seuils lors d'une rupture de brèche. Le débit total déversé est la somme des débits déversés par chaque seuil.

Chaque seuil déversant est régie par les équations des liaisons de type *weir* . 

Les termes d'énergie spécifique de part et d'autre du seuil sont définis comme suit :


<img src="/model_building/equations/weir/energie.png"/>


### Equations appliquées pour chaque régime lorsque z1 >z2

*Loi DD : Déversoir dénoyé:*

<img src="/model_building/equations/weir/eq_qdd.png"/>


*Loi DN : Déversoir noyé:*

<img src="/model_building/equations/weir/eq_qdn.png"/>


### Equations appliquées pour chaque régime lorsque z2 >z1

*Loi DD : Déversoir dénoyé:*

<img src="/model_building/equations/weir/eq_qdd_2.png"/>


*Loi DN : Déversoir noyé:*

<img src="/model_building/equations/weir/eq_qdn_2.png"/>


La formulation retenue pour le déversoir dénoyé est la même que pour celle de la vanne ci-dessus en régime DD. Toutefois le critère de passage du régime dénoyé en régime noyé est différent :

Le régime est considéré comme dénoyé si ( dans le cas où z1>z2) : 

<img src="/model_building/equations/weir/eq_e2.png"/>

Le coefficient Cd est défini par l’utilisateur. Il est généralement égal à 0.58 ( sa valeur théorique) pour un seuil doit. Il peut être diminué pour tenir compte de l’effet de contraction latérale de l’écoulement si la largeur du seuil b est inférieure à la largeur de la section d’écoulement amont. Le facteur de correction à appliquer est donné par le tableau 4.1

Le coefficient Cn est automatiquement calculé par le programme pour satisfaire la continuité du débit au changement de régime d’écoulement.
La relation appliquée est : Cn = 1.73 x Cd

La raison pour laquelle le traitement des équations pour l’élément « Weir » est différent de celui pour l’élément « Gate » tient au fait que le critère de transition entre l’écoulement dénoyé et l’écoulement dénoyé est considéré comme physiquement plus précis dans la formulation « Weir ». Il est difficile dans l’élément « Gate » d’adopter la même formulation en raison de la nécessité de satisfaire la continuité de débit entre tous les régimes qui sont beaucoup plus nombreux que dans le cas du « Weir ». 

Le coefficient Cn augmente lorsque le rapport s/S devient supérieur à 0.6. Dans le cas du « Weir » le rapport s/S s’écrit : b(z2-zs)/(z2-zf2). Le coefficient Cn modifié s’écrit :

Cn’= alp x Cn. On admet que le coefficient alp varie comme suit :  .. image:: ..\\equations\\weir\\tableau1.PNG


## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Son utilisation couvre la modélisation d’un grande variété d’obstacles linéaires, en particulier :

- Le raccordement entre berges d’un lit mineur de cours d’eau et un lit majeur modélisé comme un domaine 2D. Le coefficient de contraction latérale est dans ce cas généralement limité à la valeur .2 pour tenir compte de la nature physique des mécanisme d’échange de débit latéral entre le cours d’eau et le domaine 2D en cas de débordement.
- Le franchissement d’un talus dans un lit majeur,
- Le raccordement entre une rue et l’îlot adjacent en tenant compte de l’effet de blocage des habitations riveraines en bordure de rue.

Cette liaison est généralement générée automatiquement le long d’une ligne de contrainte via les outils de maillage : <img src="/model_building/river_free_surface_flow/mesh_button.png"/> <img src="/model_building/river_free_surface_flow/mesh_button_2.png"/>



