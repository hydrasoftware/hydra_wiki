---
title:  Vanne régulée (*Regulated gate link*)
description: 
published: true
date: 2024-02-13T13:37:00.995Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:35.915Z
---





## Données et schémas de définition

### 1. Données générales

<img src="/model_building/links/regul_gate_line_ui.png" width="490px"/>

La vanne est définie par :

- la **géométrie** du cadre (*Z ceiling*, *Z invert*, *Width*) 
- la position de la vanne (*Z gate*) qui détermine la **section d’ouverture**,
-  un **coefficient de vanne en régime dénoyé** (*Gate coefficient*).

La vanne peut être déplacée selon deux modes :

- déplacement **vers le haut** (*Upward opening*) : la section d’ouverture augmente quand on lève la vanne,
- déplacement **vers le bas** (*Downward opening*) : la section d’ouverture augmente quand on abaisse la vanne.

La position de la vanne (*Z gate*) peut être modifiée durant une simulation via le module externe de contrôle et régulation. Le paramètre  *V max* règle dans ce cas la vitesse maximum de déplacement du seuil de la vanne.

Le paramètre *gate coefficient* désigne le coefficient  Cd de vanne en régime dénoyé. 

Si l’option *submerged coef.* n’est pas cochée le programme gère le calcul du coefficient de vanne Cn en régime noyé qui varie en fonction du rapport de section entre le point aval et la section mouillée au droit de la vanne.

Si l’option *submerged coef.* est cochée  l’utilisateur doit renter une valeur fixe pour le coefficient Cn. Par défaut ce coefficient est égal à 1.0

Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

### 2. Données de régulation

3 modes de régulation sont proposés :

#### a. Régulation en cote

<img src="/model_building/structures/regul_sluice_gate_z.png" width="490px"/>

Il faut sélectionner le nœud de contrôle pour lequel la régulation en côte est appliquée en cliquant sur le bouton *Select node* puis sur le nœud souhaité dans le modèle.

   **Algorithme de régulation :**

   La variation du débit de consigne à travers la vanne est donnée par :

<img src="/model_building/structures/regul_sluice_gate_eq1.png" width="270px"/>

   où e = écart entre la cote au point de consigne et la cote de consigne

   Le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (z1, z2) figés.

   La consigne est rafraîchie au pas de temps donné par dt-reg.

   En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.


   **Ajustement des paramètres de régulation**

   Les valeurs adoptées en première approximation sont fournies ci-après pour les quelques configurations types les plus souvent rencontrées en pratique. 

   Sur les schémas qui suivent, le point de consigne correspond au point c.

   *Configuration 1* : Point de consigne localisé à l’aval du régulateur (vanne au point R)

<img src="/model_building/structures/regul_sluice_gate_z1.png" width="470px"/>

     Dans cette configuration, une augmentation du débit q produit une augmentation de la cote en C. On suppose par ailleurs que la cote en C est fortement dépendante du débit en ce point.

     On peut choisir les paramètres comme suit :

<img src="/model_building/structures/regul_sluice_gate_eq2.png" width="185px"/>

     Avec : 

     S = surface au miroir traduisant la capacité d’emmagasinement du réseau entre les points R et C

<img src="/model_building/structures/regul_sluice_gate_eq3.png" width="155px"/>

     Exemple pour un collecteur, lorsque le point C est situé à proximité immédiate de R  :

- K2 = K3 = 0
- K1 = - LV

Où L est la largeur au miroir et V la vitesse moyenne de l’écoulement

*Configuration 2* : Point de consigne situé à l’amont du régulateur (vanne au point R)

<img src="/model_building/structures/regul_sluice_gate_z2.png" width="470px"/>

On suppose dans ce cas que le débit q est directement influencé par la cote en C.

<img src="/model_building/structures/regul_sluice_gate_eq4.png" width="150px"/>

*Configuration 3* : Actionneur situé en dérivation du point de consigne

<img src="/model_building/structures/regul_sluice_gate_z3.png" width="300px"/>

Les lignes du coefficient sont les mêmes que pour la configuration 2.

S’il existe une forte dépendance entre le débit Qc et la cote Zc, il faut modifier l’expression pour K1.

<img src="/model_building/structures/regul_sluice_gate_eq5.png" width="330px"/>

Les expressions définies ci-dessus ne donnent que des ordres de grandeur. 

Il faut ensuite affiner les valeurs empiriquement par essais successifs afin d’obtenir la qualité de régulation désirée.

*Remarques* : 

Les temps de la courbe consigne doivent être strictement croissant.

En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.

#### b. Régulation en débit

<img src="/model_building/structures/regul_sluice_gate_q.png" width="490px"/>

  **Algorithme de régulation :**

   La courbe de consigne est le débit de consigne à travers la vanne ; cette consigne peut être réactualisée à chaque pas de temps :

<img src="/model_building/structures/regul_sluice_gate_q2.png" width="490px"/>

   Le point de consigne est fixé au droit de la vanne et ne peut être modifié par l’utilisateur.

   La cote critique est la cote d’eau que l’on s’interdit de dépasser en amont de la vanne. Lorsque cette cote est atteinte la consigne de débit ne peut plus être respectée et la vanne est progressivement ouverte.

   La régulation est rafraîchie tous les pas de temps ; le débit de consigne à travers la vanne est transformé en cote d’ouverture de vanne en appliquant les équations du module QMV avec (Z1, Z2) figés.

   En cas d’inversion de débit, la régulation est interrompue et la vanne se bloque sur sa butée en position fermée.

#### c. Pas de régulation

<img src="/model_building/structures/regul_sluice_gate_noregul.png" width="470px"/>

    La sélection de  ce mode équivaut à sélectionner l’objet [S_gate](/model_building/links/sluice_gate). 

    Les fonctionnalité de l’objet *regulated_gate* couvrent donc celles de l’objet *gate*. Cependant l’objet *gate* est conservé pour des raisons de compatibilité avec le code de calcul.

#### Régles de passage d'un mode de régulation à l'autre

Un mode de régulation défini dans l'objet peut être modifié dynamiquement en cours de calcul via un fichier de contrôle déclaré dans le gestionnaire de scénario (cf [chapitre 10.5 Menu Régulation](/scenarios)).

Les règles de passage d'un mode à l'autre sont précisées dans le tableau suivant :


<img src="/model_building/equations/gate/modes_regulation.png" width="550px"/>



## Equations


On considère la configuration générale suivante : 

<img src="/model_building/equations/schema_principe_gate.png" width="600px"/>

La vanne est schématisée par une géométrie rectangulaire de largeur b et de hauteur d’ouverture (zv-zs). La largeur de la vanne b peut être différente de la largeur du canal. 

### Les régimes d’écoulements

Les différents régimes d’écoulement sont délimités par le diagramme suivant :

<img src="/model_building/equations/regimes_ecoulement.png" width="490px"/>


les paramètres zs et zv sont définis comme suit selon le mode d'action de la vanne :

<img src="/model_building/equations/config.png" width="450px"/>


l'énergie spécifique en amont et en aval de la vanne est définie comme suit :

<img src="/model_building/equations/energie.png" width="170px"/>

### Equations appliquées pour chaque régime lorsque E1 >E2


*Loi de déversoir dénoyé DD*

<img src="/model_building/equations/eq_qdd.png" width="200px"/>


*Loi de déversoir noyé DN*

<img src="/model_building/equations/eq_qdn.png" width="500px"/>


*Loi de vanne dénoyée VD*

<img src="/model_building/equations/eq_qvd.png" width="350px"/>


*Loi de vanne noyée-dénoyée DND*

<img src="/model_building/equations/eq_qvnd.png" width="500px"/>


*Loi de vanne noyée VN*

<img src="/model_building/equations/eq_qvn.png" width="300px"/>

### Equations appliquées pour chaque régime lorsque E2 >E1     


*Loi de déversoir dénoyé DD*

<img src="/model_building/equations/eq_qdd_2.png" width="200px"/>


Loi de déversoir noyé DN*

<img src="/model_building/equations/eq_qdn_2.png" width="500px"/>


*Loi de vanne dénoyée VD*

<img src="/model_building/equations/eq_qvd_2.png" width="350px"/>


*Loi de vanne noyée-dénoyée VND*

<img src="/model_building/equations/eq_qvnd_2.png" width="500px"/>


*Loi de vanne noyée VN*

<img src="/model_building/equations/eq_qvn_2.png" width="300px"/>




## Commentaires

Les équations ci-dessus respectent la continuité des débits à la frontière entre deux régimes d’écoulement. Elles sont dérivables analytiquement à partir des lois générales de l’hydraulique pour les régimes : DD, VD et VN. Les formes des équations mises en œuvre pour les autres régimes sont définies de façon à satisfaire la continuité des débits entre deux régimes.

Le coefficient Cd est le coefficient de seuil déversant en régime dénoyé. Il est défini par l’utilisateur, il est généralement égal à sa valeur théorique : 0.6. L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque b/b0<1, où b0 est la largeur du canal ou du collecteur en amont de la vanne. On recommande d’appliquer le coefficient.de correction « alp » suivant sur CD :

<img src="/model_building/equations/tableau1.png" width="250px"/>

Le coefficient CN est le coefficient de seuil déversant ou de vanne en régime noyé. Il est par défaut sélectionné automatiquement par le programme qui tient compte du rapport de section s/S, où :
- s est la section d’ouverture de la vanne,
- S est la section mouillée dans le collecteur.

<img src="/model_building/equations/tableau2.png" width="230px"/>

Ce tableau résulte d’une analyse spécifique hydraulique basée sur la formulation de perte de charge à la Borda. 
L’utilisateur peut également imposer une Valeur de son choix pour le coefficient Cn.

