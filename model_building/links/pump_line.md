---
title:  Maillage par pompage (*Pump link*)
description: 
published: true
date: 2021-02-09T11:02:58.021Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:23.980Z
---



## Définition

Station de pompage


## Données de l'élément

<img src="/model_building/links/pump_line_ui.png" width="540px"/>

La station de pompage peut contenir jusqu’à 10 pompes en parallèle.

Les séquences de marche / arrêt de chaque pompe sont réglées par un niveau d’arrêt et un niveau bas de redémarrage. 

Une courbe caractéristique Q(H) est définie pour chaque pompe (10 points maximum)

H désigne la charge hydraulique aux bornes de chaque pompe telle quedéfie par le fournisseur. Les pertes de chages dans la bache ou dans le circuit de refoulement doivent être ajoutées dans le modèle.


## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

il faut satisfaire les conditions suivantes pour chaque courbe caractéristique : Q(1)= H(n)=0.

En cas de fonctionnement en dehors des limites de la courbe caractéristique d'un pompe un message de "warning" est écrit dans le fichier de suivi des calculs. Les calculs restent dans les limites décrites par la courbe caractéristique. En particulier aucun débit de reour n'est modélisé.

Il faut veiller pour cet objet à définir au point amont une chambre de surface suffisante pour éviter les démarrage-arrêts des pompes trop fréquentes. Les régles de bonne pratiques limitent le onmbes de cycles de démarrage-arrêts à 6 par heure.
La chambre de pompage peut être modélisée en ajustant la section du noeud extrémité amont de la pompe ou en posant sur ce noeud un objet [tank](/model_building/boundary_conditions/tank_bc_point) .
