---
title:  Liaison frottement (*Strickler link*)
description: 
published: true
date: 2021-11-05T10:12:02.103Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:41:15.434Z
---




## Définition

Liaison par frottement sur le fond

## Données d'entrée

<img src="/model_building/links/strickler_ui.png" width="480px"/>


## Equations

Le débit d’échange entre les éléments amont et aval est donné par les équations :

$$
Q = K b h^\frac{5}{3} (z_1 - z_2)^\frac{1}{2}
$$

$b$ est la largeur correspondant à la côte amont $z_1$:

$$
b = b_s + (b_u - b_d)\frac{z_1 - z_d}{z_u - z_d}
$$

$$
h = \frac{z_1 - z_d}{2} + \frac{z_2 - z_d}{2}
$$

![strickler_link_equation.svg](/model_building/links/strickler_link_equation.svg)


$z_d$ est la cote de fond : *Zcrest_down*


$z_u$ est la cote de fond : *Zcrest_up*

$b$ est la largeur correspondant à la cote z1. Elle est interpolée entre les cotes *Zcrest_down* et *Zcrest_up*.

$Rk$ est le coefficient de strickler.

## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

Il est principalement utilisé pour raccorder latéralement des biefs 1D parallèles de vallée.

Cette liaison peut être générée automatiquement le long d’une ligne de contrainte via les outils de maillage : <img src="/model_building/river_free_surface_flow/mesh_button.png"/> <img src="/model_building/river_free_surface_flow/mesh_button_2.png"/>




