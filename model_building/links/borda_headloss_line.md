---
title:  Perte de charge type Borda (*Borda headloss*)
description: 
published: true
date: 2021-02-09T11:02:55.354Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:41:19.317Z
---



## Définition

Perte de charge de type Borda.

Cet objet propose un panel de 20 lois différentes de pertes de charge selon la géométrie de la singularité.

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).

La perte de charge est exprimée pour chaque loi sous la forme générale :

<img src="/model_building/equations/borda/definition/borda_headloss_eq1.png" width="100px"/>

où :

<img src="/model_building/equations/borda/definition/borda_headloss_eq2.png" width="350px"/>

dE étant la **perte de charge** de part et d’autre de la singularité

et u la **vitesse de référence** (en général vitesse dans le section amont ou aval de la singularité selon le type de loi sélectionnée)

L’objet *Borda link* partage des lois communes avec l’objet [Borda singularity](/model_building/structures/borda_headloss_point). Il propose des lois supplémentaires en raison de la plus grande diversité de configurations d’accroche des extrémités de la liaison.

Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).


## Données générales à toutes les lois

<img src="/model_building/links/borda_headloss_line_schema.png" width="800px"/>

Il faut préciser la nature de l’objet sur laquelle s’accroche chaque extrémité, car cela conditionne le calcul des termes cinétiques dans l’expression de la charge hydraulique.

Trois types de  choix sont offerts :

1. *Computed* : le nœud extrémité est un container pour laquelle une section géométrique est définie. Le terme cinétique est donné par la vitesse calculée le long du tronçon amont du nœud (pour l’extrémité amont du link) ou aval du nœud (pour l’extrémité aval du link).

2. *Circulaire* : le nœud extrémité est connecté à un tronçon de section circulaire supposé en charge. Il faut alors fournir la section de la canalisation.

3. *Canal* : le nœud extrémité est connecté à un canal de section rectangulaire. Il faut dans ce cas définir la cote radier du canal et sa largeur.

## Données et équations spécifiques à chaque loi

### Loi 1 : dE=kQ² 

<img src="/model_building/equations/borda_link/loi_1/borda_headloss_line_eq1.png" width="370px"/>

Pour cette loi uniquement, la loi de perte de charge est exprimée en négligeant les termes cinétiques attachés aux sections amont et aval.

Le coefficient K est rentré dans l’unité : m/(m3/s)²

### Loi 2 : dE=(K/2g)Q²/S²

<img src="/model_building/equations/borda_link/loi_2/borda_headloss_line_eq2.png" width="370px"/>

Il faut définir :

•    La référence à la section S : tronçon amont ou aval,
•    Le coefficient k associé à chaque sens d’écoulement   ( sans dimension)

### Loi 3 : Sharp edge outlet (piquage sortant dans un réservoir)

<img src="/model_building/equations/borda_link/loi_3/borda_headloss_line_eq3.png" width="520px"/>


### Loi 4 : Sharp edge inlet (piquage entrant depuis un réservoir)

<img src="/model_building/equations/borda_link/loi_4/borda_headloss_line_eq4.png" width="530px"/>

Commentaires :      
    * Cette loi est utilisée dans le cas particulier d’une canalisation équipée d’ouvertures latérales pour assurer une alimentation latérale de l’écoulement répartie le long de la canalisation, comme dans le cas d’un émissaire marin. Le nœud aval 1 est généralement disposé dans une station de gestion avec une condition à la limite de type niveau imposé.

    * La liaison doit être définie dans le sens 1 vers 3 comme indiqué sur le schéma ci-dessus.

    * La perte de charge le long de la canalisation rectiligne est négligée au droit du piquage.

Le coefficient Cd est calculé comme suit :


### Loi 5 : Bifurcation – séparation de courants

<img src="/model_building/equations/borda_link/loi_5/borda_headloss_line_eq5.png" width="530px"/>

Commentaire :
    * Cette formulation est valable uniquement pour un angle compris entre 0 et 90° et une section rectiligne constante.

### Loi 6 : Jonction – réunion de courants

<img src="/model_building/equations/borda_link/loi_6/borda_headloss_line_eq6.png" width="530px"/>

Commentaire :
    * Cette formulation est valable uniquement pour un angle compris entre 0 et 90° et une section rectiligne constante.

### Loi 9 : Sharp geometry transition

<img src="/model_building/equations/borda_link/loi_9/borda_headloss_line_eq7.png" width="350px"/>

<img src="/model_building/equations/borda_link/loi_9/borda_headloss_line_eq8.png" width="540px"/>

### Loi 10 : Contraction with transition

<img src="/model_building/equations/borda_link/loi_10/borda_headloss_line_eq9.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_10/borda_headloss_line_eq10.png" width="540px"/>

Commentaire :
Le paramètre « transition option n’est pas utilisé en pratique : le tableau d’Idel’cik ne distingue pas les différentes formes géométrique de transition.

### Loi 11 : Enlargment with transition

<img src="/model_building/equations/borda_link/loi_11/borda_headloss_line_eq11.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_11/borda_headloss_line_eq12.png" width="540px"/>

### Loi 12 : Circular bend

<img src="/model_building/equations/borda_link/loi_12/borda_headloss_line_eq13.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_12/borda_headloss_line_eq14.png" width="540px"/>

Note :  les pertes de charge ci-dessus sont calculées pour une conduite circulaire

### Loi 13 : Sharp angle bend

<img src="/model_building/equations/borda_link/loi_13/borda_headloss_line_eq15.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_13/borda_headloss_line_eq16.png" width="540px"/>

Commentaires : 
    * Les  abaques ci-dessus s’appliquent ont été établies pour des conduites circulaires.
    * Pour  N>0 le facteur de correction en fonction de l’angle Teta est donné par le  tableau du bas du Borda 12 : « circular bend ».


### Loi 14 : Screen normal to flow

<img src="/model_building/equations/borda_link/loi_14/borda_headloss_line_eq17.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_14/borda_headloss_line_eq18.png" width="580px"/>

Coefficient de blogage = taux d’obstruction = 1. – porosité = 1.- (a/s)

### Loi 15 : Screen oblique to flow

<img src="/model_building/equations/borda_link/loi_15/borda_headloss_line_eq19.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_15/borda_headloss_line_eq20.png" width="580px"/>

Commentaires :
    * Les abaques ci-dessus ont été établies pour un rapport longueur barreau/largeur barreau égal  à 5.
    * Les abaques sont fournies en fonction du coefficient de porosité = 1.- coef-blocage.    


### Loi 16 : Friction

<img src="/model_building/equations/borda_link/loi_16/borda_headloss_line_eq21.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_16/borda_headloss_line_eq22.png" width="560px"/>

Commentaire :
    * Le calcul de rugosité est effectué par rapport à la géométrie de la section amont.

### Loi 17 : Parametric headloss

<img src="/model_building/equations/borda_link/loi_17/borda_headloss_line_eq23.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_17/borda_headloss_line_eq24.png" width="560px"/>

### Loi 18 : Sharp bend rectangular

<img src="/model_building/equations/borda_link/loi_18/borda_headloss_line_eq25.png" width="490px"/>

<img src="/model_building/equations/borda_link/loi_18/borda_headloss_line_eq26.png" width="560px"/>

<img src="/model_building/equations/borda_link/loi_18/borda_headloss_line_eq27.png" width="560px"/>

<img src="/model_building/equations/borda_link/loi_18/borda_headloss_line_eq28.png" width="560px"/>

### Loi 19 : T shaped bifurcation

<img src="/model_building/equations/borda_link/loi_19/borda_headloss_line_eq29.png" width="560px"/>

Les lois ci-dessus ont été établies pour un T normalisé à branches taraudées en fonte malléable.
En pratique on dispose d’une seule liaison 1-3. La formulation est en conséquence simplifiée comme suit :


Commentaire :
Il faut s’assurer après calcul que la configuration de l’écoulement est bien celle définie  ci-dessus pour toute la durée de la simulation.

### Loi 20 : T shaped junction

<img src="/model_building/equations/borda_link/loi_20/borda_headloss_line_eq30.png" width="560px"/>

Les lois ci-dessus ont été établies pour un T normalisé à branches taraudées en fonte malléable.
En pratique on dispose d’une seule liaison 1-3. La formulation est en conséquence simplifiée comme suit :


Commentaire :
Il faut s’assurer après calcul que la configuration de l’écoulement est bien celle définie  ci-dessus pour toute la durée de la simulation.

