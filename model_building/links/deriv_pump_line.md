---
title:  Dérivation par pompage (*Derivated pump link*)
description: 
published: true
date: 2021-02-09T11:02:35.833Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:40:51.409Z
---



## Définition

Ouvrage de dérivation selon une loi q(z)


## Données de l'élément

<img src="/model_building/links/deriv_pump_line_schema.png" width="450px"/>

On doit définir :

- Le débit capable de la dérivation,
- Les courbe tabulée débit dérivé – cote d’eau au nœud amont de la liason.

Le débit dérivé est exprimé en fraction du débit capable, sa valeur est comprise entre 0 et 1.

La courbe de dérivation doit être monotone et croissante. Elle ne doit pas dépasser 10 points.


## Commentaires

Cet objet est de type *link*. Ses extrémités peuvent être accrochées sur tout type de containeur à l’exception des objets [catchment](/model_building/hydrology/catchment_node_point) et [hydrology node](/model_building/hydrology/hydrology_node_point).


