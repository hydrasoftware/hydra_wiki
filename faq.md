---
title: FAQ et astuces
description: 
published: true
date: 2024-10-28T13:57:32.208Z
tags: 
editor: markdown
dateCreated: 2024-10-22T20:31:23.215Z
---

 <a name="processing_run"></a>

# Continuer à travailler pendant que le moteur calcule.
Il y a un algo "simulate" dans la boîte à outils de traitement, testez-le pour lancer un scenario... ou plusieurs 
 
Lancer l'algo "simultate" de la boite à outil traitements c'est comme appuyer sur le bouton run... mais mieux.
 
Normalement, vous devriez trouver l'algo dans le groupe de traitement "hydra" de la boite à outil, ou en tapant "simu" dans la barre de recherche de la boite à outil, ou dans les algos récemment utilisés (pratique, c'est tout en haut, faut juste l'avoir récemment utilisé).

![processing_simulate.png](/computation/processing_simulate.png =250x)
 
Voilà à quoi ça ressemble...

 ![processing_simulate_ui.png](/computation/processing_simulate_ui.png =600x)
 
et là vous ☺ en voyant que vous pouvez choisir un autre projet que le projet courant, et une deuxième fois vous ☺ en voyant qu'une fois le calcul lancé vous pouvez faire autre chose dans QGIS que regarder le calcul défiler... 
 
Vous le faisiez déjà, me direz-vous, en lançant un autre QGIS, mais QGIS est assez gourmant en mémoire, donc c'est mieux comme ça non ? #sobrietenumerique.
 
Et pour le même prix: la boîte à outil vous offre la possibilité de lancer plusieurs calculs (le bouton en bas de la fenêtre "run as batch" ou un truc du genre en français). 

 ![processing_simulate_ui_batch.png](/computation/processing_simulate_ui_batch.png =600x)
 
 
 <a name="slowdown"></a>
 
 # Identifier rapidement où le calcul "gratte".
Il y a un algo "slowdown" dans la boîte à outils de traitement, testez-le sur un scenario qui a tourné.
 
Précision et célérité, 
 
c'est ce que vous promet le pas de temps adaptatif d'hydra. Le moteur fait de grands pas de temps quand la ligne d'eau varie lentement et de petits pas de temps quand la ligne d'eau change rapidement. 
 
La variation que le moteur considère c'est le max sur tous les nœuds du modèle, donc si, hydrauliquement, il se passe un truc quelque part, tout le calcul ralenti pour maintenir la précision des résultats et éviter de diverger, aussi.
 
Le nœud qui limite le temps de calcul, et le pas de temps de calcul sont affichés à chaque pas de temps, c'est ce qu'on voit défiler, donc vous pouvez voir quel nœud est en cause... sauf que c'est pas facile parce-que ça défile vite et que le numéro du noeud indiqué est un numéro interne, donc pas facile à retrouver dans le modèle...
 
C'est pour ça que depuis peu il y a un post-traitement "slowdown", accessible dans la boite à  outils "Traitements" de QGIS. Il traite les sorties du moteur de calcul et fait quelques statistiques sur les nœuds qui ont limité le pas de temps au cours du calcul et génère un couche de points.
 ![processing_simulate.png](/computation/processing_simulate.png =250x)
 
On peut, en ouvrant la table attributaire et en triant (e.g. par le nombre d'occurrences), trouver rapidement le lieu du ralentissement (selection + zoom sur sélection).
 
Parfois il n'y a rien à faire, il se passe réellement des choses intéressantes à cet endroit là, mais, souvent, surtout quand on met au point le modèle, le ralentissement est lié à un problème de modélisation.
 
 # QGIS rame? Quelquespistes d'optimisation
 ## l'accroche objet
**Problème**: je passe ma couche en mode édition et j'attends, j'attends, j'attends... que QGIS indexe toutes les couches du projet pour l'accrochage.
**Solution**: ne garder dans la configuration de l'accrochage que les couches de noeuds sur lesquels on a besoin de s'accrocher.
 
 ## Vous pouvez accélérer le chargement de vos projets QGIS... mais attention !
 Le chargement des projets QGIS peut être très looooooooong, en particulier avec des couches nombreuses et/ou aillant de nombreux éléments. 
 
Tout ça c'est à cause de la magie de QGIS qui détermine tout seul le nombre et le type des colonnes, l'étendue de la couche... bref plein de choses qui vous facilitent la vie (d'ailleurs vous n'avez jamais eu à y penser). Mais tout cela a un prix, QGIS fait, pour chaque couche, un gros paquet de requêtes au moment de l'ouverture du projet (des fois que des choses auraient changé).
 
Il y a deux choses qui coûtent particulièrement cher, et qui ne sont pas vraiment nécessaires: la vérification de l'unicité des clés primaires et la détermination de l'étendue de la couche. Il y a un option de projet pour éviter ça, bien sûr (il y a même une option par couche pour la vérification des clés primaires) !

![qgis_indexation_couches.png](/advanced/outils_qgis/qgis_indexation_couches.png)

Avec ça vous devriez gagner du temps.
 
Attention: si vous avez une couche (e.g. une vue que vous avez créé) dont la clé n'est pas unique, vous risquez le crash de QGIS (ou une mauvaise édition de données si la couche en question est éditable).

# Comment avoir plusieurs QGIS ouvert avec des version d'hydra  différentes dans chacun ?
Il y a une variable d'environnement QGIS_PLUGINPATH qui dit à QGIS où aller chercher les plugins. On peut utiliser ce mécanisme pour ce créer des racourcis pour lancer QGIS+hydra3.6.0 ou QGIS+hydra3.10.8.
 
Dans vos répertoires d'installation de QGIS (pluriel parce-que vous avez plusieurs versions d'installées, je n'en doute pas), chez moi c'est `C:\Program Files\QGIS 3.22.10\bin` il y a un fichier `qgis-ltr.bat` (chez vous c'est peut-être `qgis.bat`). Si vous l'ouvrez, vous avez ça:
 
`@echo off`
`call "%~dp0\o4w_env.bat"`
`if not exist "%OSGEO4W_ROOT%\apps\qgis-ltr\bin\qgisgrass7.dll" goto nograss`
`set savedpath=%PATH%`
`call "%OSGEO4W_ROOT%\apps\grass\grass78\etc\env.bat"`
`path %OSGEO4W_ROOT%\apps\grass\grass78\lib;%OSGEO4W_ROOT%\apps\grass\grass78\bin;%savedpath%`
`:nograss`
`@echo off`
`path %OSGEO4W_ROOT%\apps\qgis-ltr\bin;%PATH%`
`set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis-ltr`
`set GDAL_FILENAME_IS_UTF8=YES`
`rem Set VSI cache to be used as buffer, see #6448`
`set VSI_CACHE=TRUE`
`set VSI_CACHE_SIZE=1000000`
`set QT_PLUGIN_PATH=%OSGEO4W_ROOT%\apps\qgis-ltr\qtplugins;%OSGEO4W_ROOT%\apps\qt5\plugins`
`start "QGIS" /B "%OSGEO4W_ROOT%\bin\qgis-ltr-bin.exe" %*`

On copier/coller ce fichier ailleur (e.g. sur le bureau) et on le nomme QGIS + hydra 3.6.0.bat, puis on l'édite:
- on change la première ligne `call "%~dp0\o4w_env.bat"`en `call C:\Program Files\QGIS 3.22.10\bin\o4w_env.bat"`
1. on ajouter une ligne juste avant la dernière ligne `set QGIS_PLUGINPATH=c:\Users\{user}\hydra-3.6.0`

Dans le répertoire `C:\Users\{user}\hydra-3.6.0` j'ai un répertoire` hydra` qui est mon plugin dézippée dans la version qui va bien.
 
Maintenant en double cliquant sur le .bat...
 
Vous pouvez en créer autant que vous voulez.
 
Attention, ça marche pour plusieurs versions d'hydra 3 (ou d'hydra 2), mais pas pour hydra 3 et hydra 2 en même temps.
 
Attention, QGIS peut être assez gourmant en mémoire vive; il y a un moment ou trop c'est trop: d'abord ça rame, ensuite ça crash...
 
 # Comment sauvegarder vos projets hydra régulièrement ?
Voici un petit script .bat (à adapter) pour sauvegarder un projet hydra.
En définissant une tâche récurrente (Planificateur de tâches) vous pouvez lancer cette sauvegarde tous les jours à la même heure... 

`set projet=mon_projet_hydra`
`set rep=mon_répertoire_de_sauvegarde`
`call "%ProgramFiles%\QGIS 3.22.10\bin\o4w_env.bat"`

`set sql=%rep\%projet%_%date:~6,4%%date:~3,2%%date:~0,2%.sql`
`set PATH=%OSGEO4W_ROOT%\apps\Qt5\bin;%OSGEO4W_ROOT%\apps\qgis-ltr\bin;%PATH%`
`set PYTHONPATH=%userprofile%\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins;%OSGEO4W_ROOT%\apps\qgis-ltr\python;%PYTHONPATH%`
`set PGSERVICEFILE=%userprofile%\.pg_service.conf`
`set QGIS_PREFIX_PATH=%OSGEO4W_ROOT%\apps\qgis`
`python -c "from hydra.versions.dump_restore import dump_project;from hydra.utility.log import LogManager, ConsoleLogger;log=LogManager(ConsoleLogger(), 'hydra');dump_project(log, r'%sql%', '%projet%')"`

 