---
title: Elements de modélisation
description: 
published: true
date: 2021-02-09T11:05:37.200Z
tags: 
editor: undefined
dateCreated: 2021-02-09T10:27:45.184Z
---

**Hydrologie**

<img src="/model_building/hydrology/catchment_node_point.png"/> [Catchment](/model_building/hydrology/catchment_node_point) : **bassin versant**

<img src="/model_building/hydrology/catchment_contour_polygon.png"/> [Catchment contour](/model_building/hydrology/catchment_contour) : *contour de bassin versant**

<img src="/model_building/hydrology/hydrology_node_point.png"/> [Hydrology manhole](/model_building/hydrology/hydrology_node_point) : **regard hydrologique**

<img src="/model_building/hydrology/connector_hydrology_line.png"/> [Connector](/model_building/hydrology/connector_hydrology_line) : **connecteur**

<img src="/model_building/hydrology/routing_line.png"/> [Hydrology routing](/model_building/hydrology/routing_line) : **routage hydrologique**

<img src="/model_building/hydrology/qq_split_hydrology_point.png"/> [Split QQ](/model_building/hydrology/qq_split_hydrology_point) : **dérivation QQ**

<img src="/model_building/hydrology/zq_split_hydrology_point.png"/> [Split ZQ](/model_building/hydrology/zq_split_hydrology_point) : **dérivation ZQ**

<img src="/model_building/hydrology/reservoir_rs_point.png"/> [Reservoir](/model_building/hydrology/reservoir_rs_point) : **bassin de stockage**

<img src="/model_building/hydrology/reservoir_rsp_point.png"/> [Parametric reservoir](/model_building/hydrology/reservoir_rsp_point) : **bassin de stockage paramétrique**

<img src="/model_building/hydrology/hydrology_bc_point.png"/> [Hydrology boundary condition](/model_building/hydrology/hydrology_bc_point) : **condition limite hydrologique**


***Réseaux d'assainissement**

<img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line) : **tronçon de collecteur**

<img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point) : **regard**


***Rivières et écoulements à surface libre**

<img src="/model_building/river_free_surface_flow/reach_line.png"/> [Reach](/model_building/river_free_surface_flow/reach_line) : **bief**

<img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point) : **noeud de rivière**

<img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point) : **profil en travers**

<img src="/model_building/river_free_surface_flow/pave_polygon.png"/> [Mesh element](/model_building/river_free_surface_flow/mesh_element) : **maille 2d**

<img src="/model_building/river_free_surface_flow/storage_marker.png"/> [Storage](/model_building/river_free_surface_flow/storage) : **casier**

<img src="/model_building/river_free_surface_flow/crossroad_point.png"/> [Crossroad](/model_building/river_free_surface_flow/crossroad_street) : **carrefour**

<img src="/model_building/river_free_surface_flow/street_line.png"/> [Street segment](/model_building/river_free_surface_flow/crossroad_street) : **rue**


***Stations de gestion**

<img src="/model_building/station/station_node_point.png"/> [Station node](/model_building/station/station_node_point) : **noeud de station de gestion**

<img src="/model_building/station/station_polygon.png"/> [Station contour](/model_building/station/station_polygon) : **contour de station de gestion**


***Ouvrages**

<img src="/model_building/structures/gate_point.png"/> [Gate](/model_building/structures/gate_point) : **vanne**

<img src="/model_building/structures/zregul_weir_point.png"/> [Weir](/model_building/structures/zregul_weir_point) : **déversoir frontal** (seuil)

<img src="/model_building/structures/borda_headloss_point.png"/> [Borda](/model_building/structures/borda_headloss_point) : **perte de charge de type Borda**

<img src="/model_building/structures/param_headloss_point.png"/> [Parametric headloss](/model_building/structures/param_headloss_point) : **perte de charge paramétrique**

<img src="/model_building/structures/bradley_headloss_point.png"/> [Bradley](/model_building/structures/bradley_headloss_point) : **ouvrage de franchissement**

<img src="/model_building/structures/bridge_headloss_point.png"/> [Bridge](/model_building/structures/bridge_headloss_point) : **ouvrage de franchissement**

<img src="/model_building/structures/regul_sluice_gate_point.png"/> [Regulated gate](/model_building/structures/regul_sluice_gate_point) : **vannage régulé**

<img src="/model_building/structures/hydraulic_cut_point.png"/> [Hydraulic cut](/model_building/structures/hydraulic_cut_point) : **coupure hydraulique**

<img src="/model_building/structures/marker_point_point.png"/> [Point marker](/model_building/structures/marker_point_point) : **marqueur de point**



***Liaisons**

<img src="/model_building/links/gate_line.png"/> [Gate link](/model_building/links/gate_line) : **vanne manuelle**

<img src="/model_building/links/regul_gate_line.png"/> [Regulated gate link](/model_building/links/regul_gate_line) : **vanne régulée**

<img src="/model_building/links/gate_line.png"/> [Weir link](/model_building/links/weir_line) : **seuil déversant**

<img src="/model_building/links/pump_line.png"/> [Pump link](/model_building/links/pump_line) : **maillage par pompage**

<img src="/model_building/links/deriv_pump_line.png"/> [Deriv pump link](/model_building/links/deriv_pump_line) : **dérivation par pompage**

<img src="/model_building/links/borda_headloss_line.png"/> [Borda link](/model_building/links/borda_headloss_line) : **perte de charge de type Borda**

<img src="/model_building/links/connector_line.png"/> [Connector link](/model_building/links/connector_line) : **connecteur**

<img src="/model_building/links/strickler_line.png"/> [Strickler link](/model_building/links/strickler_line) : **liaison frottement**

<img src="/model_building/links/porous_line.png"/> [Porous link](/model_building/links/porous_line) : **liaison poreuse**

<img src="/model_building/links/overflow_line.png"/> [Overflow link](/model_building/links/overflow_line) : **surverse**

<img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line) : **surverse depuis un réseau**

<img src="/model_building/links/interlink_line.png"/> [Interlink](/model_building/links/interlink_line) : **surverse depuis un réseau**, connexion entre deux modèles

<img src="/model_building/links/mesh_2d_line.png"/> [Mesh 2d link](/model_building/links/mesh_2d_line) : **liaison 2d**


***Conditions aux limites**

<img src="/model_building/boundary_conditions/constant_inflow_bc_point.png"/> [Constant inflow](/model_building/boundary_conditions/constant_inflow_bc_point) : **débit constant**

<img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) : **hydrogramme**

<img src="/model_building/boundary_conditions/froude_bc_point.png"/> [Froude](/model_building/boundary_conditions/froude_bc_point) : condition aval de type **chute (Froude)**

<img src="/model_building/boundary_conditions/strickler_bc_point.png"/> [Strickler](/model_building/boundary_conditions/strickler_bc_point) : condition aval de type **strickler**

<img src="/model_building/boundary_conditions/zq_bc_point.png"/> [Z(Q)](/model_building/boundary_conditions/zq_bc_point) : **loi z(q)**

<img src="/model_building/boundary_conditions/tz_bc_point.png"/> [Z(t)](/model_building/boundary_conditions/tz_bc_point) : **loi z(t)**

<img src="/model_building/boundary_conditions/weir_bc_point.png"/> [Weir](/model_building/boundary_conditions/weir_bc_point) : **seuil**

<img src="/model_building/boundary_conditions/tank_bc_point.png"/> [Tank](/model_building/boundary_conditions/tank_bc_point) : **bassin**

<img src="/model_building/boundary_conditions/connect_model_bc_point.png"/> [Model connection](/model_building/boundary_conditions/connect_model_bc_point) : **connexion de modèles**

