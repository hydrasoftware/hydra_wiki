---
title: Pollution et qualité
description: 
published: true
date: 2021-02-11T16:23:45.584Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:41.122Z
---

# Introduction

Cette note vient en complément du chapitre « transport » du document utilisateur. Elle précise les fonctionnalités offertes par HYDRA pour traiter les applications couplant la pollution rejetée par les réseaux et la qualité du milieu récepteur.
Les différents cas de figures impliquant les volets pollution et qualité peuvent être regroupés en trois grandes familles :

## Famille 1 : Calcul des flux polluants rejetés par les réseaux.

Hydra calcule à l’aide d’un modèle d’assainissement les flux polluants rejetés aux exutoires en activant l’option « pollution » dans le gestionnaire de scénarios. Les quantités calculées sont :
-    les volumes rejetés,
-    les masses polluantes rejetées selon les paramètres sélectionnés : MES, DBO5, NTK, DCO. 

Dans la famille 1 on s’intéresse à des bilans de flux rejetés dans le cadre des applications d’autocontrôle des rejets ou de diagnostic permanent des réseaux. Les flux calculés aux exutoires peuvent dans une seconde étape venir nourrir un modèle fluvial en enrichissant au besoin les données de flux rejetés à l’aide de routines de traitement spécifiques externes à HYDRA.

## Famille 2 : Calcul d’impact sur la qualité du milieu récepteur.

Hydra calcule à l’aide d’un modèle fluvial les flux et concentrations de paramètres de qualité à partir de données d’hydrogrammes et de pollutogrammes aux différents points d’injection du modèle. Trois options de calcul sont proposées :
 - **options 1** : qualité physico chimique mettant en œuvre 4 paramètres liés par des réactions physico-chimiques : O2 dissous, DBO5, NH4, NO3,
 - **option 2** : qualité bactériologique  mettant en œuvre entre 1 et 4 paramètres de qualité bactériologique ; ces paramètres sont indépendants,
 - **option3** : transport en suspension et dépôt mettant en œuvre le paramètre MES.

Ces options sont mutuellement exclusives.

La famille 2 nécessite la définition préalable des flux d’apports selon l’option de qualité sélectionnée. Une possibilité consiste à lire en entrée les hydrogrammes rejetés calculés par un modèle de réseau et à les convertir en pollutogrammes via les données de concentration paramétrables dans l’objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [hydrograph](/model_building/boundary_conditions/hydrograph_bc_point)	. Il est également possible de lire les pollutogrammes d’entrée dans des fichiers externes.

## Famille 3 : Calcul couplé pollution-qualité**

Le modèle (ou projet) comprend deux sous modèles couplés : un réseau d’assainissement et un réseau fluvial.


# Pollution

Cette option permet de simuler la génération de pollution par ruissellement et sa propagation dans un réseau d’assainissement. 4 paramètres peuvent être simulés :

<img src="/pollution_qualite/pollution_scenaio.png" width=600px/>

Ces paramètres n’interagissent pas entre eux.

## Génération des flux polluants par ruissellement

L’étape de génération est gérée par le module hydrologique :
- les stocks de pollution entraînés dans les réseaux par ruissellement sont définis dans la table `land pollution accumulation` du menu principal <img src="/0_hydra_icone.png" height="25px"/> *Hydrology*
- les valeurs de stock de pollution sont définis par défaut et peuvent être réglées pour un projet donné. 

<img src="/pollution_qualite/land_pollution_accumulation_ui.png" width=400px/>

Les masses polluantes disponibles sont calculées pour chaque bassin versant et pour chaque paramètre via le bouton d’édition `pollution` accessible dans l’objet <img src="/model_building/hydrology/catchment_node_point.png"/> [Catchment](/model_building/hydrology/catchment_node_point) :

<img src="/pollution_qualite/pollution_catchment.png" width=180px/>



Lors de l’exécution des calculs hydrologiques, le programme calcule à chaque pas de temps le flux polluant arraché par ruissellement via l’équation :

$dF=A*I^B$

où :
- I est l’intensité de la pluie nette en mm/h
- Les coefficient A et B sont les coefficients d’arrachement réglables via les champs de l’écran *pollution land accumulation*.

En l’absence de précipitation, on considère que le stock polluant se reforme via une constante de temps de régénération défini dans l’écran « pollution land accumulation ».

Le flux polluant ainsi généré est propagé le long de chaque arborescence hydrologique, puis stocké dans l’objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/>[Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point). Il est possible de superposer au flux polluant ainsi calculé deux  autre pollutogrammes via le bouton `pollution` de l’objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point)	:
- Un pollutogramme de temps sec qui est calculé en multipliant l’hydrogramme de temps sec par la concentration correspondante,
- Un pollutogramme attaché à l’hydrogramme défini dans l’élément ou via un fichier externe :

<img src="/pollution_qualite/pollution_hydrograph.png" width=650px/>



## Propagation des flux polluants dans le réseau hydraulique

Les  flux polluants sont propagés par convection dans le réseau, ils sont calculés à chaque pas et à tous les nœuds de calcul à la suite de l’étape de calcul hydraulique.
La pollution peut être partiellement éliminée dans  l’objet [tank](/model_building/boundary_conditions/tank_bc_point) via deux mécanismes :
-    Taux d’abattement
-    Calcul d’une concentration résiduelle.


# Qualite

Cette option permet de simuler l'impact de rejets polluants sur la qualité d'un milieu récepteur . Le modèle du milieu récepteur peut être de structure filaire, 2D ou mixte. Trois classes de qualité peuvent être simulées :
- qualité physico-chimique comprenant 4 paramètres intéragissant via des relations physico-chimques : O2, DBO5, NH4 et N03,
- qualité bactériogique : de 1 à 4 paramètres peuvent être simulés, ces parmètres n'intergissent pas entre eux  : E.coli, Ent.I, Ad3, Ad4. Un taux de mortalité différent peut être défini pour chaque paramètre.
- matières en suspension . Le programme calcule le devenir des MES en simulant une vitesse moyenne de chute.

Les équations régissant les  différents mécanismes d'impact sont décrites dans le **manuel d'analyse "qualité"**. 

Les paramètres à définir pour chaque famille sont décrits  dans le **menu Transport** du [gestionnaire de scénarios](/scenarios) 

Les pollutogrammes d'injection sont définis dans l'objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Catchment](/model_building/hydrology/catchment_node_point).

<img src="/pollution_qualite/pollution_hydrograph.png" width=700px/>

 le pollutogramme associé à un paramètre est calculé en affectant à l'hydrogramme d'injection et au débit de temps sec une concentration donnée.

Lorsque l'hydrogramme est renseigné dans un fichier externe il est possible de définir directement les pollutogrammes correspondants dans ce fichier. Le format associé est décrit dans la documentation consultable dans le menu *Hydrology ssettings - External Hydrographs* du gestionnaire de scénarios. 


# Couplage pollution-qualité

### Principes de couplage

## Architecture du modèle

Le modèle est composé de deux réseaux ou deux groupes de réseaux distincts :
-    le réseau d’assainissement : la pollution est générée, propagée et déversée dans le milieu naturel à travers les différents exutoires,
-    le réseau fluvial : il récupère les apports déversés par le réseau d’assainissement et simule leur évolution dans le milieu récepteur.

Les deux réseaux sont connectés par des couples d’objets RACC portant le même nom :
-    l’objet *RACC amont* appartient au réseau d’assainissement et représente un exutoire,
-    l’objet *RACC aval*  appartient au modèle fluvial ; il reçoit le flux polluant du *RACC amont*, complète au besoin les paramètres de flux et constitue un point d’injection dans le modèle fluvial.

Cette architecture de modélisation est illustrée ci-dessous :

<img src="/pollution_qualite/schema_pollution_qualite.png" width=500px/>

## Pilotage des sous modèles

**Modèle constitué de deux réseaux**

Le pilotage doit être paramétré en mode « cascade » : le sous modèle « réseau d’assainissement » est simulé d’abord, le sous modèle « réseau fluvial » est simulé ensuite.

**Modèle constitué de plus de deux réseaux**

Chaque réseau est défini par son type via le mot clé COUPLAGE_PQ ( voir ci-après) : type « assainissement » ou type « fluvial ».

Les réseaux de type assainissement sont simulés d’abord, les réseaux de type « fluvial » sont simulés ensuite. Les deux types doivent appartenir à des groupes différents.

## Paramétrage d'un scénario

La procédure de paramétrage est celle appliquée classiquement dans un modèle constitué de plusieurs sous modèles, en respectant les particularités ci-dessous.

### Option de transport

Il faut sélectionner une des trois options suivantes, elles concernent le réseau fluvial.
-    option qualité 1 : physico-chimie,
-    option qualité 2 : bactériologie,
-    option qualité 3 : transport MES.

Aucune option de transport n’est à cocher pour le réseau d’assainissement, cette option est générée automatiquement par HYDRA dans la phase d’exécution des calculs.

### Option de paramétrage

Il faut déclarer le mot clé *COUPLAGE_QP suivie de lignes de données  contïgues: chaque ligne est composée de deux champs :
-    Le nom du sous modèle,
-    Le paramétrage de transport associé : « P » (pollution) , « Q » (qualité).

La lettre « P » doit être associée au réseau d’assainissement, la lettre « Q » doit être associée au réseau fluvial.

On peut également définir la lettre « N » à la place de la lettre « P ».

## Exécution des calculs

La description ci-après considère deux sous-modèles. Elle est directement applicable à un nombre quelconque de sous-modèles.

**Etape 1 : simulation du réseau assainissement**

Lors de la génération des calculs HYDRA sélectionne les paramètres des pollution qui vont être simulés. Ces paramètres dépendent du choix de l’option de qualité pour le réseau fluvial.
-    Option qualité 1 : physico-chimie :les paramètres de pollution simulés sont DBO5 et NTK.
-    Option qualité 2 :bactériologie :  aucun paramètre des pollution n’est simulé.
-    Option qualité 3 : MES : le paramètre de pollution simulé est MES.

Les paramètres de génération des flux polluants doivent  être renseignés par ailleurs dans le sous modèle de réseau au niveau des objets *catchment* et *hydrograph*. 

Si la lettre « N » est définie pour l’option qualité 1 ou l’option qualité 2, aucun paramètre de pollution n’est simulé.

En fin de calcul les débits et flux polluants calculés à chaque exutoire sont stockés dans le fichier : 

X_R_RACC.hys du répertoire /hydraulique du scénario de calcul.


**Etape 2 : simulation du réseau fluvial**

Lors de la phase de génération des calculs, HYDRA va compléter les flux polluants calculés dans l’étape 1 à l’aide des concentrations définies sous le bouton « qualité » de chaque objet « RACC » appartenant au réseau fluvial.

<img src="/pollution_qualite/ecran_qualite.png" width=500px/>

 Le mode de calcul dépend de l’option qualité sélectionnée :

a.    Option quality 1 : physico-chimie

•    Paramètre DBO5 : le flux polluant stocké dans le fichier X_R_RACC.hys est conservé, sauf si le paramétrage de transport est « N ». Dans ce cas le flux de DBO5 est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

•    Paramètre NH4 : le flux polluant stocké dans le fichier X_R_RACC.hys est multiplié par le coefficient 0.7 pour convertir le flux NTK en flux NH4 sauf si le paramétrage de transport est « N ». Dans ce cas le flux de NH4 est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

•    Paramètre O2 : le flux de DBO5 est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

•    Paramètre NO3 : le flux de NO3 est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

b.    Option quality 2 : bactériologie

•    Pour chaque paramètre sélectionné dans le gestionnaire de scénario : le flux bactériologique est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

c.    Option quality 3 : MES

•    Paramètre MES : le flux de MES stocké dans le fichier X_R_RACC.hys est conservé, sauf si le paramétrage de transport est « N ». Dans ce cas le flux de MES est calculé en multipliant chaque débit stocké dans le fichier X_R_RACC.hys par la concentration définie dans l’objet RACC ci-dessus.

Les flux polluants d’apport ainsi calculés pour chaque objet « RACC » sont stockés dans le fichier X_R.APP du répertoire /Hydrol, puis exploités dans la phase de calcul hydraulique.

### Pilotage d'un série chronologique

Les principes de paramétrage et de calculs ci-dessus sont directement transposables au cas d’une série chronologique. Pour l’utilisateur cette transposition est transparente et ne nécessite aucune adaptation particulière.

La série chronologique est généralement activée pour établir des bilans de flux en un certain nombre de stations. Il est dans ce cas utile d’activer l’option de sortie SOR1 en mode 2 (bilan) dans le menu *Computation option- Ouput option* du gestionnaire de scénario et de définir un pas de temps de sortie sur 24h : l’utilisateur dispose ainsi de la séquence de flux ou de concentration moyennée sur 24h sur toute la durée de la chronique dans un fichier .csv. Il peut ainsi établir les bilans statistiques de son choix pour chaque paramètre à l’aide des outils d’exploitation disponibles sous Excel.



