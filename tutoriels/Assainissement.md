---
title: Tutoriel assainissement
description: 
published: true
date: 2023-03-16T22:36:20.521Z
tags: 
editor: markdown
dateCreated: 2022-01-26T18:14:36.632Z
---

# Introduction
Pour l'installation d'hydra, se reporter à la section dédiée : [installation hyydra](/fr/installation_v3)

Les actions présentées ci-après permettent de créer un petit modèle réseau unitaire de surface dont le point aval est partagé entre :
- Une surverse vers le milieu récepteur
- Un pompage vers une station d’épuration

Ce tutoriel s'appuie sur un jeu de données disponibles sur le lien suivant : [Tuto_hydra_assainissement.zip](https://hydra-software.net/tutoriel/assainissement/Tuto_hydra_assainissement.zip) : 

- Un fichier tiff permettant de localiser les regards et collecteurs : *localisation_georef.tif*
- Fichiers shape à importer (reagrds, collecteurs).

>:hourglass: Ce jeu de données comprend également le modèle construit pouvant être importé directement *tuto_network.sql*. **Pour importer le modèle, voir le chapitre dédié** [Importer un modèle](#import_project)

Ce modèle comprend les objets de mdélisation suivants:
- Réseau unitaire secondaire : bassins versants d’apport
- Réseau principal : éléments collecteurs
- Maillages vers le station d’épuration et le milieu naturel : station de gestion
- Exutoires aval : conditions aux limites aval



![presentation_tuto.svg](/tutorials/network/presentation_tuto.svg)

Ce petit tutoriel vise à :
- construire pas à pas ce modèle de réseau alimenté par un modèle pluie / débit (en important certains éléments de fichiers shape),
- paramétrer et lancer un calcul,
- présenter brièvement quelques fonctionnalités d'analyse des résultats.

Des liens sont proposés vers la documentation pour plus de précisions sur certains fonctionnalités.

>:warning: **L'exemple présenté ici est totalement fictif et ne s'appuie sur aucun ouvrage réel.**

# Création d'un projet et d'un modèle
:book:[Documentation](https://wiki.hydra-software.net/fr/project_management)

## Nouveau projet


Ouvrir le gestionnaire de projet via le menu Hydra\Manage project
![manage_project.png](/tutorials/network/manage_project.png)

Créer un nouveau projet en sélectionnant le bouton `New`
- `Project name` : nom du projet
- `EPSG` : système de projection dans lequel sera créé le modèle (le bouton Select EPDG permet d’ouvrir la liste des systèmes de projection disponibles dans QGIS)
- `Working directory` : répertoire dans lequel seront créés les résultats de calcul et où peuvent être stockés les fichiers externes (MNT, données hydrologiques, …). Par défaut dans le répertoire user.

![manage_project_ui_new.svg](/tutorials/network/manage_project_ui_new.svg)

Valider, le projet est créé. Le gestionnaire de projet récapitule tous les projets présents dans la base. Il précise la version de la base de données associée au projet (Data version).
![manage_project_ui_after_creation.png](/tutorials/network/manage_project_ui_after_creation.png)

Sélectionner le projet qui vient d’être créé, puis cliquer sur `Open` pour l’ouvrir :
- Les tables associées au projet sont chargées dans le gestionnaire de couches dans un groupe nommé Projet,
- Les différentes barres d’outils d’Hydra sont chargées dans QGIS. Ci-dessous la barre d’outils :
 ![hydra_button_tools.png](/tutorials/network/hydra_button_tools.png =500x)
- Un répertoire portant le nom du projet est créé dans le Working directory :
  ![folder_project.png](/tutorials/network/folder_project.png)

Le fichier qgs comprend l’appel de l’ensemble des couches du projet et du modèle (cf. ci-après) issues de la base de données, mais également toutes les couches complémentaires qui pourront être appelées par l’utilisateur (fond de plans, ….).

## Nouveau modèle
Un ou plusieurs modèles peuvent être associés à un projet ; ces modèles pourront ensuite être assemblés lors du calcul (sous modèles géographiques interconnectés par exemple). Nous ne travaillerons ici que sur un seul modèle.

Créer un nouveau modèle via le menu `Hydra\Models\Add model`


![menu_add_model.png](/tutorials/network/menu_add_model.png) ![menu_add_model_ui.png](/tutorials/network/menu_add_model_ui.png)

Les tables associées au modèle sont chargées dans le gestionnaire de couche, dans un groupe portant le nom du modèle.

# Construction du modèle
## Principes

Les objets de modélisation sont créés via les utilitaires disponibles dans le panneau de gauche Model dock ou par import de fichiers shape; ces deux méthodes seront utilisées ici.

![hydra_model_dock.png](/hydra_model_dock.png)

Commencer par importer le fond de plan localisant les regards et collecteurs du projet.

## Regards et collecteurs
Les collecteurs sont matérialisés par des polylignes accrohées à leurs extrémités à des regards. Une section géométrique et des cotes de radier amont et aval sont associées à chaque collecteur. Ces objets peuvent être créés manuellement via le panneau d'outils d'hydra ou importés de fichiers SIG (shape, gpkg, ...).

### Création manuelle
**Créer les regards**
Sélectionner l’outil `manhole`
![hydra_model_dock_manhole.png.svg](/tutorials/network/hydra_model_dock_manhole.png.svg)

Puis créer les regards NODA1 et NODA2


>:warning: Sélectionner l’outil pour chaque objet à créer


![manhole_ui.png](/tutorials/network/manhole_ui.png)

Renseigner la cote de terrain naturel `z_ground`: 22.50 pour les deux regards

Valider 

![window_manhole_manual.png](/tutorials/network/window_manhole_manual.png){.align-center}

>- L’outil ![hydra_button_edit_object.png](/tutorials/network/hydra_button_edit_object.png) permet d’éditer les caractéristiques de l’objet
>- Possibilité de passer par l’éditeur de couches de QGIS pour paramétrer les objets en lots
>- L’outil ![hydra_button_delete_object.png](/tutorials/network/hydra_button_delete_object.png) permet de supprimer un objet


> Les sympboles ![warning_icon.png](/tutorials/network/warning_icon.png) indiquent que les objets présentant des invalidités topologiques. Ils sont associés à la table `Validity`(groupe `Général` du gestionnaire de couches)
> La liste et le détail des invalidités peut être consultée via le bouton ![button_list_invalid_objects.png](/tutorials/network/button_list_invalid_objects.png).
> ![validity_manager_ui.png](/tutorials/network/validity_manager_ui.png)

________________________________
**Créer les collecteurs**
 Sélectionner l’outil `pipe`
![hydra_model_dock_pipe.svg](/tutorials/network/hydra_model_dock_pipe.svg)

Créer le pipe TRC1 en cliquant sur les regards NODDA1 puis NODA2, renseigner les champs mentionnés et valider.


![pipe_ui.svg](/tutorials/network/pipe_ui.svg)

 
> Hydra permet de créer différents types de sections :
>- circulaires,
>- ovoïdes,
>- paramétriques fermées (*pipe*),
>- paramétriques ouverts (*channel*).

![window_pipe_manual.png](/tutorials/network/window_pipe_manual.png){.align-center}

### Import depuis des fichiers shape
Nous allons à présent importer les regards et collecteurs complémentaires à partir des fichiers shape. Ces fichiers sont importés en deux étapes décrites ci-après:
- importer le fichier shape dans la base de données (dans un répertoire de travail dénommé *work*)
- importer cette table dans la table correspondante du modèle

La **première étape** permet d'importer le fichier shape dans la base de données.

Sélectionner `Manage external tables` dans le menu hydra
![menu_manage_external_table.png](/tutorials/river/menu_manage_external_table.png)

Sélectionner le fichier *data\regards.shp* via le bouton   ![button_add.png](/tutorials/river/button_add.png). Cette opération permet d’importer le fichier shape dans un répertoire de travail de la base de données.

![menu_manage_external_table_ui_name_table.png](/tutorials/network/menu_manage_external_table_ui_name_table.png)

La **seconde étape** permet d’importer les données dans la table `manhole` d'hydra.
- Sélectionner la table **regards** dans le tableau de gauche (les champs de la table sont rappelés dans le tableau central Details)
- Dans le tableau de droite `Import table to project` :
    -	Target schema : sélectionner **modele1**
    -	Target table : sélectionner **manhole**
    -	Préciser l’association à faire entre les champs de la table *regards* et ceux de la table *manhole* du projet
- Cliquer sur `Import`
![menu_manage_external_table_ui_import_manhole.png](/tutorials/network/menu_manage_external_table_ui_import_manhole.png)


Après validation (OK), les regards sont chargés dans le gestionnaire de couche (rafraichir l’affichage avec ![button_refresh.png](/tutorials/river/button_refresh.png) ).

![window_manhole_import.png](/tutorials/network/window_manhole_import.png){.align-center}

**Effectuer la même opération avec le fichier *data\collecteurs.shp***

:warning: Bien préciser la correspondance entre *Z_invert_up* et *z_up*, et *Z_invert_down* et *z_down*; pour les autres champs, l'affectation se fait automatiquement (noms identiques).

![menu_manage_exyernal_table_import_ui_pipe.svg](/tutorials/network/menu_manage_exyernal_table_import_ui_pipe.svg)

Le modèle comprend à présent l'ensemble des regards et des collecteurs, sans invalidités.

![window_pipe_importpng.png](/tutorials/network/window_pipe_import.png){.align-center}

## Modèle pluie - débit

Le modèle pluie débit va permettre de défniir les hydrogrammes injectés dans le réseau à partir de pluies réeele ou de projet; ces dernières seront appelées depuis le gestionnaire de scénario.

>Seuls les centroïdes des bassins versants sont créés ici *(leurs contours peuvent également être créés, nécessaires pour utiliser des pluies radar ou des pluies définies au droit de pluviomètres pour lesquels une interpolation est effectuée)*.
>
>Ils sont créés manuellement mais pourraient tout comme les regards et les collecteurs être importés depuis un shape file.


 Sélectionner l’outil `catchment node` du menu Hydrology.
![hydra_model_dock_catchment_node.svg](/tutorials/network/hydra_model_dock_catchment_node.svg)

Puis créer les bassins versants BV_1 et BV_2 (respectivement en amont des regards NODA5 et NODA8, ce. illustration ci-après).

Renseigner les paramètres ci-dessous, puis valider.

![catchment_node_ui.svg](/tutorials/network/catchment_node_ui.svg)


Sélectionner l’outil `routing` du menu Hydrology pour créer les routage entre le centroïde des bassins  versants et leur exutoire *(conserver les valeurs par défaut dans l'interface)*.
![hydra_model_dock_hydrology_routing.svg](/tutorials/network/hydra_model_dock_hydrology_routing.svg)

:high_brightness: L’élément **routing** permet de paramétrer un temps de transfert entre le bassin versant et l’exutoire dans le réseau

![hydrology_routing_ui.png](/tutorials/network/hydrology_routing_ui.png)

Le modèle hydrologique est construit.
![window_hydrology.png](/tutorials/network/window_hydrology.png){.align-center}



>Lors de la création de la liaison `routing`, hydra a créé automatiquement un objet `hydrograph boundary condition` ![hydra_model_dock_hydrograph_bc.png](/tutorials/network/hydra_model_dock_hydrograph_bc.png)sur les regards auxquels elles sont attachées.
>Cet objet est indispensable pour relier un objet du domaine hydrologique à un autre du domaine hydraulique.
>
>:high_brightness: L’élément **hydrograph** permet d’intégrer d’autres sources d’apport que celle du ruissellement issu du bassin versant :
>- Hydrogramme externe
>- Apports de temps sec


## Ouvrages de répartition
Hydra propose toute une gamme d'ouvrages hydrauliques pouvant être posés sur les regards (singularités) ou relier deux noeuds (liaisons).

Dans cet exemple, l'ouvrage de répartition entre la conduite d'alimentation de la STEP et le déversoir d'orage est modélisé par une `station de gestion`qui contient:
- une bâche de pompage,
- une pompe de relevage vers la STEP,
- un déversoir vers la conduite exutoire vers le milieu naturel (déversoir d'orage).


>:high_brightness: La **station de gestion** sert à modéliser des ouvrages complexes ou successifs
>- C’est un domaine « transparent » d’un point de vue surfacique et peut être superposé aux autres
>- Une station de gestion ne peut contenir que des « station node », des conditions aux limites et des liaisons binodales

### Station de gestion
- Sélectionner l’outil `station contour` du menu Station pour créer le contour de la station de gestion (polygone, clic droit pour fermer).
- Sélectionner l’outil `station node` du menu Station pour créer le noeud de station dans la sation créée précédemment

![hydra_model_dock_station.svg](/tutorials/network/hydra_model_dock_station.svg)![window_station.png](/tutorials/network/window_station.png)

### Bâche de pompage
La bâche de pompage est modélisée par un objet tank.
- Sélectionner l’outil `tank bc` du menu Boundary conditions, et poser l'objet sur le noeud de station de gestion
![hydra_model_dock_bc_tank.svg](/tutorials/network/hydra_model_dock_bc_tank.svg)
- Paramétrer la loi cote / surface de l'objet tank comme suit
- Valider

![tank_bc_ui.png](/tutorials/network/tank_bc_ui.png)

### Liaisons
- Créer les différentes liaisons en sélectionnant l'outil correspondant dans le menu `links`puis en cliquant sur le noeud amont et le noeud aval.
- Paramétrer les objets comme indiqué ci-dessous.
![window_station_final.svg](/tutorials/network/window_station_final.svg)

## Conditions limites aval
Les conditions limite aval permettent de modéliser les exutoires du modèle: aval du déversoir d’orage et niveau d’eau moyen constant de la station d’épuration.

- Sélectionner l’outil `z(t)` du menu Boundary conditions, et poser l'objet sur les noeuds avaln
![hydra_model_dock_bc_tz.svg](/tutorials/network/hydra_model_dock_bc_tz.svg)
- Paramétrer les lois des objets z(t) comme suit
- Valider
![bc_tz_ui.png](/tutorials/network/bc_tz_ui.png)![window_bc_tz.svg](/tutorials/network/window_bc_tz.svg)

# Création des données de pluie
Hydra permet d'utiliser différentes pluies, réelles (pluviographes, radar) ou de projet. Nous allons ici créer une **pluie simple triangle** :
- Ouvrir la fenêtre de paramétrage des pluies depuis le menu Hydra\hydrology\Rain scénario
![menu_hydrology_rain_scenario.png](/tutorials/network/menu_hydrology_rain_scenario.png =200x)
- Choisir l’onglet `Hyetograph` pour construire un hyétogramme homogène sur l’ensemble du modèle
- Utiliser le bouton ![button_add.png](/tutorials/network/button_add.png) pour ajouter une pluie
- Rentrer les valeurs de Temps / intensités 
- Utiliser le bouton ![button_add.png](/tutorials/network/button_add.png) ou cliquer sous la dernière ligne pour ajouter des lignes dans le tableau de données
- Le graphique s’affiche automatiquement lors de la saisie des données
- Valider
![rain_scenario_hyetograph_ui.png](/tutorials/network/rain_scenario_hyetograph_ui.png)


<a name="import_project"></a>

# Importer directement le modèle
Pour une première découverte d'hydra, vous pouvez tout simplement importer le modèle exemple fourni *tuto_network.sql*. Pour ce faire:

- Ouvrir le gestionnaire de projet via le menu `Hydra\Manage project`
![manage_project.png](/tutorials/network/manage_project.png) ![manage_project_ui_import.png](/tutorials/network/manage_project_ui_import.png)
- Sélectionner `Import from file` et aller chercher le fichier *tuto_network.sql*

- Renseigner les menus :
![manage_project_ui_new.svg](/tutorials/network/manage_project_ui_new.svg)


- Valider, le projet est créé. Le gestionnaire de projet récapitule tous les projets présents dans la base. Il précise la version de la base de données associée au projet (Data version).

![manage_project_ui_after_creation.png](/tutorials/river/manage_project_ui_after_creation.png)

- Sélectionner le projet qui vient d’être créé, puis cliquer sur `Open` pour l’ouvrir

# Paramétrage du scénario de calcul et lancement des calculs

https://wiki.hydra-software.net/fr/scenarios

:warning: **Si le modèle a été importé**, utiliser le scénario pré-paramétré.

## Interface de paramétrage des scénarios

Le paramétrage des scénarios se fait via le menu Hydra\Scenarios\Settings
![menu_scenario_settings.png](/tutorials/network/menu_scenario_settings.png)![menu_scenario_settings_computation_ui.png](/tutorials/network/menu_scenario_settings_computation_ui.png)

Créer un scénario avec le bouton  ![button_add.png](/tutorials/network/button_add.png) :
- Donner un nom
- Ajouter un commentaire
- Sélectionner ce scenario 

## Computation settings
Conserver les paramètres de calcul définis par défaut dans l’onglet computation settings
- Computation duration : 12
- Output time step : 0h 5m
- T max : 12h

## Hydrology settings
Définir les paramètres relatifs à la définition des apports hydrologiques; ici la pluie de projet.
- Rainfall scenario : sélectionner la pluie créée précéedmment
- Valider et fermer l’interface de paramétrage de scénario
![menu_scenario_settings_hydrology_ui.png](/tutorials/network/menu_scenario_settings_hydrology_ui.png)


# Lancement des calculs
Le lancement des calculs se fait via la barre d’outils :
- Sélectionner le scénario à lancer dans le menu déroulant
![hydra_toolbar_run.svg](/tutorials/network/hydra_toolbar_run.svg)
- Cliquer sur run
- La fenêtre de suivi du déroulement du calcul apparait
- Le calcul est terminé lorsque le message whydram24 ok apparait dans la fenêtre de gauche
![whydram24.png](/tutorials/network/whydram24.png)

- Fermer la fenêtre.


# Exploitation des résultats
Nous présentons ici les principaux outils d’exploitation des résultats de calcul.
https://wiki.hydra-software.net/fr/results

## Courbes X(t)
L’outil ![hydra_toolbar_xt.png](/tutorials/river/hydra_toolbar_xt.png)  permet de visualiser les variations temporelles des différentes variables associées aux objets de modélisation (nœuds, liaisons, …)
-	Sélectionner le bouton
-	Cliquer sur l’objet souhaité
-	Paramétrer l’affichage :
    -	Choix des grandeurs à afficher et axe associé, 
    -	Axe, 
    -	…
![xt_ui.png](/tutorials/network/xt_ui.png)

## Synthèse des grandeurs maximales
Le bouton ![hydra_toolbar_minmax.png](/tutorials/river/hydra_toolbar_minmax.png)  permet d’accéder à la synthèse des grandeurs maximales associées à un objet de modélisation : cliquer sur l’outil, puis sélectionner l’élément choisi.

![minmax_ui.png](/tutorials/network/minmax_ui.png)

## Cartographie des hauteurs et vitesses maximales d’écoulement sur les élément surfaciques (mailles 2D, casiers et rues) ![hydra_toolbar_synthetic_res.png](/tutorials/river/hydra_toolbar_synthetic_res.png) 
Actualiser la géométrie des objets via le menu Hydra\Settings puis panneau Synthetic settings

![menu_settings.png](/tutorials/river/menu_settings.png)  ![menu_settings_synthetic_res_ui.png](/tutorials/network/menu_settings_synthetic_res_ui.png)

Activer le bouton ![hydra_toolbar_synthetic_res.png](/tutorials/river/hydra_toolbar_synthetic_res.png)  : les résultats pour le scénario actif (i.e. sélectionné dans le menu déroulant également utilisé pour lancer les calculs) sont chargés dans le gestionnaire de couches.

Les résultats sont placés par défaut en dessous de toutes les couches déjà chargées ; les remonter dans le gestionnaire de couches au dessus des fonds de plan le cas échéant, et masquer la couche pipe pour visualiser les analyses thématiques de mise en charge.

![window_synthetic_res.png](/tutorials/network/window_synthetic_res.png)!


## Profil en long des reach
- Afficher le panneau de contrôle Longitudinal profil dock à gauche
- Sélectionner le reach avec le bouton ![button_add.png](/tutorials/river/button_add.png)  (reach to display)
- Sélectionner le scénario avec le bouton  ![button_add.png](/tutorials/river/button_add.png) (Scénarios to display)
- Sélectionner les grandeurs à afficher (géométrie et résultats de calcul)
- Possibilité d’afficher 
     -	les résultats de calcul : profils en long des lignes d'eau, des débits, des vitesses, ... le long des biefs de cours d'eau (reach) ou des branches de collecteurs (branch),
    -	la géométrie des biefs de cours d'eau ou/et des tronçons d'assainissement, ainsi que les ouvrages et liaisons associés.
    -	les valeurs max en activant le bouton  ![longitudinal_profile_max.png](/tutorials/network/longitudinal_profile_max.png)
    -	ou l’animation dynamique des grandeurs hydraulique en cliquant sur la flèche de défilement ![longitudinal_profile_play.png](/tutorials/network/longitudinal_profile_play.png) 

![longitudinal_profile.png](/tutorials/network/longitudinal_profile.png)
