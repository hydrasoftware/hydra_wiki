---
title: Premiers pas avec hydra
description: 
published: true
date: 2021-11-19T16:56:18.745Z
tags: tutoriel
editor: markdown
dateCreated: 2021-05-19T14:53:22.936Z
---

# Installation et configuration

Voir la [procédure d'installation](/fr/installation_v3)


# Création du modèle

Les actions élémentaires présentées ci-après permettent de créer un fichier modèle test, de créer des branches de réseaux modélisées par des nœuds et liaisons, de modéliser une pluie de projet, et faire tourner un scénario de temps de pluie. Le modèle test permet également de tester un modèle 2D, constitué d’un canal maillé en 2D et d’un casier en aval. 

Le modèle test proposé contient 2 branches : 

*	La première branche est composée de 2 canalisations de 100 m en D500, une vanne permet de relier ce réseau à un réservoir. Un bassin versant est modélisé en amont de ce réseau. Le scénario créé est un scénario qui modélise le ruissellement d’une pluie décennale sur ce bassin versant, jusque dans le réservoir. 

*	La seconde branche est composée de 2 canalisations de 100 m en D500, une vanne permet de relier ce réseau à un réservoir. Un hydrographe est modélisé pour injecter 30 m3 en 2h dans le réseau. 
 
Le modèle test permet de vérifier le ruissellement correct du bassin versant dans le réservoir, et le bon écoulement des 30 m3 injectés dans l’hydrographe. 

![modele_1.png](/tutorials/modele_1.png)

Pour la modélisation de la partie 2D, un seuil de déversement est ajouté dans le réservoir. L’eau ruisselée dans le réservoir est ainsi en partie surversée vers un canal avec maillage 2D. Ce canal se rejette dans un casier en aval. Le modèle test permet de vérifier le correct débordement du réservoir par le seuil, et le bon écoulement de l’eau dans le canal 2D. 

![modele_2.png](/tutorials/modele_2.png)

## Création du modèle

Depuis le menu hydra ![menu_hydra.png](/tutorials/menu_hydra.png) ouvrir l’onglet `Manage Project`, puis cliquer sur `New` et remplir les caractéristiques `Project Name` et `EPSG`.
Ici, l’EPSG choisit est 2154.

![1_new_model.png](/tutorials/1_new_model.png)

Cliquer sur ok puis sur open. Un projet vierge s’ouvre.

![2_projet_vide.png](/tutorials/2_projet_vide.png)

Pour ajouter le modèle test, depuis le menu déroulant, choisir models puis add model.

![3_ajout_model.png](/tutorials/3_ajout_model.png)

## Ajouter des nœuds et canalisations

Ajouter un nœud dans les items « Network »

![4_ajout_noeud.png](/tutorials/4_ajout_noeud.png)

Remplir les caractéristiques et cliquer sur OK. Le nœud n’étant pas lié, un point d’exclamation apparait. 

![5_caracteristique_noeud.png](/tutorials/5_caracteristique_noeud.png)

Répéter l’opération et faire un deuxième et troisième nœud N2 et N3 identiques.

![6_ajout_autres_noeuds.png](/tutorials/6_ajout_autres_noeuds.png)

Depuis les items « Network », cliquer sur le bouton pipe pour créer un objet pipe, de N1 vers N2.
Remplir les caractéristiques.

> **Remarque** : une canalisation se trace toujours dans le sens d’écoulement amont vers aval, même > en cas de contrepente.

![7_ajout_canalisation.png](/tutorials/7_ajout_canalisation.png)

Ajouter un objet pipe entre N2 et N3 et cliquer sur ok.

![8_ajout_canalisation.png](/tutorials/8_ajout_canalisation.png)

## Ajouter un exutoire à la branche

Ici, l’exutoire proposé est un réservoir de profondeur 6m et de surface 8x8m. Ce réservoir est relié à la branche via une vanne manuelle. 

Créer une station de gestion via l’outil station contour. La station de gestion n’est pas à proprement parler un objet de modélisation, c’est une unité fonctionnelle qui sert à regrouper visuellement un certain nombre d’objets de modélisation. Elle permet de placer à l’intérieur un nœud de gestion.

![9_ajout_station.png](/tutorials/9_ajout_station.png)

Placer à l’intérieur un station node. Il permettra de fixer le réservoir. 

![10_ajout_station.png](/tutorials/10_ajout_station.png)

Ajouter un Tank, et le placer sur le station node créé. Les caractéristiques du réservoir à remplir sont les suivantes. Une erreur apparait si le Zinit n’est pas rempli. 

![11_ajout_reservoir.png](/tutorials/11_ajout_reservoir.png)

Dans les items Links, choisir l’objet gate. Placer la gate entre le dernier nœud N3 et le tank. La liaison se fait de l’amont vers l’aval.

![12_ajout_vanne.png](/tutorials/12_ajout_vanne.png)

Remplir les caractéristiques et valider.
Ici, la vanne est ouverte.
La largeur de la vanne est une conversion du diamètre circulaire de la canalisation amont, en largueur rectangulaire.

> **Remarque** : Z invert doit être plus bas que Z ceiling, et au moins aussi bas que le fil d’eau aval de la canalisation sur laquelle est raccordée la vanne.

![13_caracteristique_vanne.png](/tutorials/13_caracteristique_vanne.png)

Répéter les étapes de création de nœuds, de collecteur et de vannes pour créer une seconde ligne parallèle à la première et connectée au bassin (voir schéma au §2).

![14_modele_final.png](/tutorials/14_modele_final.png)

## Création d'un scénario

Pour créer un scénario, dans le menu déroulant sur l’item hydra, choisir scénario puis settings.
La fenêtre scenario manager s’ouvre. Cliquer sur le bouton + pour ajouter un scénario.

![15_ajout_scenario.png](/tutorials/15_ajout_scenario.png)

Le scénario est préconfiguré. Dans le premier onglet, modifier la ligne computation duration pour mettre 6h. 

![16_config_scenario.png](/tutorials/16_config_scenario.png)

Comme il n’y a pas de pluie, dans l’onglet hydrologie settings, le visuel doit être le suivant.

![17_config_pluie.png](/tutorials/17_config_pluie.png)

Pour faire tourner le scénario, sélectionner le scénario voulu et cliquer sur le bouton run.

![18_lancer_calcul.png](/tutorials/18_lancer_calcul.png)

Le scénario se lance. Attendre la fin d’exécution du calcul et fermer la fenêtre de calcul.

Pour créer un profil en long, aller dans le menu longitudinal profil.

![18_le_calcul_tourne.png](/tutorials/18_le_calcul_tourne.png)

## Créer un profil en long

Pour créer un profil en long, aller dans le menu longitudinal profil.

![19_profil_en_long.png](/tutorials/19_profil_en_long.png)

Dans la case scenarios to display, cliquer sur le bouton + et ajouter un scénario qui a tourné, ici le scénario 1.

![20_scn_profil_en_long.png](/tutorials/20_scn_profil_en_long.png)

Puis, pour visualiser le profil en long de la branche souhaitée, cliquer sur le bouton + dans Reach to display, puis cliquer sur la branche. Ainsi, la branche dont le profil en long sera visualisé et affiché en rouge.

![21_bief_a_visualiser.png](/tutorials/21_bief_a_visualiser.png)

Cliquer sur min ou max, et links pour afficher les nœuds, puis display graph.

![22_config_affichage.png](/tutorials/22_config_affichage.png)

Le profil en long est le suivant. Il permet de vérifier les caractéristiques des canalisations modélisées.

![23_profil_en_long.png](/tutorials/23_profil_en_long.png)


# Hydraulique 1D

Les bases de création du modèle test dorénavant exposées, nous proposons de vérifier les items suivants dans le modèle 1D : 

*	Création d’un hydrographe.
*	Création d’un bassin versants
*	Modélisation d’une pluie, par exemple double triangle. 

## Créer un hydrographe

Pour créer un hydrographe, cliquer sur le bouton hydrographe depuis les items Boundary conditions.

![24_hydrographe.png](/tutorials/24_hydrographe.png)

Pour placer l’hydrographe, il faut l’accrocher sur un nœud. Placer deux hydrograph : un sur le nœud 1 et un sur le nœud 4. Pour le nœud 4 uniquement, compléter les caractéristiques comme indiqué ci-contre. Cliquer sur ok. 

![25_hydrographe.png](/tutorials/25_hydrographe.png)

## Modéliser un bassin versant

Pour créer un bassin versant, ajouter cliquer sur le bouton catchment depuis les items Hydrology. Placer le catchment dans un espace vide du modèle. Les coordonnées spatiales du catchment n’ont pas d’impact sur ses valeurs. 

![26_bassin_versant.png](/tutorials/26_bassin_versant.png)

Remplir les valeurs du catchment, choisir pour ce modèle test Linéar réservoir et constant runoff dans le menu déroulent de Net flow production fonction. 

![27_config_bassin_versant.png](/tutorials/27_config_bassin_versant.png)

![28_config_bassin_versant.png](/tutorials/28_config_bassin_versant.png)

Pour relier le catchment a l’hydrographe du nœud 1, utiliser le bouton routing dans les items hydrology et cliquer sur ok. 

![29_routage.png](/tutorials/29_routage.png)

![30_routage.png](/tutorials/30_routage.png)

## Modéliser une pluie test

Pour le modèle test, nous choisissons de modéliser une pluie test double triangle, de période de retour 10 ans, de durée 4 heures avec un pic d’intensité d’une heure. Les coefficients de montana utilisés sont ceux de la région Ile-de-France, près de Mantes-la-Jolie. 

La pluie test tombera sur le ou les bassins versants créés lors du déroulement d’un scénario avec une pluie associée.


Pour modéliser une pluie, dans le menu déroulant sur l’item hydra, choisir hydrology puis rain scenario. La fenêtre rains s’ouvre.
Cliquer sur le bouton + pour ajouter des coefficients de montana et remplir les caractéristiques. 

![31_pluie.png](/tutorials/31_pluie.png)

Puis, pour remplir une pluie double triangle, cliquer sur double triangle et remplir les caractéristiques.
La pluie test aura la forme ci-contre.
Cliquer sur ok pour valider. 


![32_pluie_test.png](/tutorials/32_pluie_test.png)

![33_pluie_test.png](/tutorials/33_pluie_test.png)

Pour créer un scenario avec une pluie associée, aller dans le menu déroulant et cliquer sur scenario puis settings. 
Cliquer sur + pour ajouter un scénario qu’on nommera temps_pluie, et dans hydrology settings, rainfall scenario, ajouter la pluie_test et cliquer sur valider.


![34_scenario_pluie.png](/tutorials/34_scenario_pluie.png)


## Vérification des fonctionnalités du modèle test

Le modèle test créé comporte deux branches en parallèles, reliées chacune indépendamment au bassin grâce à une vanne. 

Le modèle test comporte également un scénario temps de pluie, associée à une pluie de retour 10 ans choisie. 

Dans un premier temps, nous allons vérifier le bon écoulement des 30 m3 dans l’hydrographe vers le réservoir. Pour cela, la configuration sera : 

*	Vanne ouverte sur la branche où se situe l’hydrographe seul.
*	Vanne fermée sur la branche sur laquelle un bassin versant est accroché.

Puis, on fait tourner le scénario temps de pluie. On attend 30 m3 dans le bassin.

Pour vérifier l’ouverture de la vanne, cliquer sur le bouton Edit dans la barre d’outils hydra, et cliquer sur la vanne associée à la branche avec un hydrographe de 30 m3.
Si Zgate=Zceiling, la vanne est ouverte.

![35_ouvrir_vanne.png](/tutorials/35_ouvrir_vanne.png)

Pour fermer une vanne, modifier la valeur de Zgate, pour que Zgate=Zinvert. Ici, on fera cette opération sur la vanne associée à la branche à laquelle un bassin versant est raccroché. 

![36_fermer_vanne.png](/tutorials/36_fermer_vanne.png)

Faire tourner le scénario temps_pluie en cliquant sur run.

Pour vérifier la présence d’eau dans le réservoir, cliquer sur le bouton show graph.

![40_lancer_calcul.png](/tutorials/40_lancer_calcul.png)

Puis, cliquer sur l’objet modélisé tank BC qui correspond au réservoir.

Une fenêtre s’ouvre qui indique la hauteur d’eau dans le réservoir. Avec la souris, on peut lire la côte de l’eau la plus haute dans le réservoir. 

![41_resultat_reservoir.png](/tutorials/41_resultat_reservoir.png)

L’icone loupe permet de zoomer sur une partie du graphique. Plus on zoome, plus la cote de hauteur d’eau dans le réservoir est indiquée précisément. 

Ici, la hauteur d’eau dans le réservoir est entre 0,46 et 0,47 m (0,465 m avec le zoom). La surface du réservoir étant 64 m2, on obtient un volume d’eau de 29.8 m3 dans le réservoir (calcul avec la valeur 0.465), soit près des 30 m3 attendus. On peut constater que le volume final retrouvé dans le réservoir varie en fonction de l’arrondi choisit pour le calcul.

![42_zoom_resultat.png](/tutorials/42_zoom_resultat.png)

![43_zoom_resultat.png](/tutorials/43_zoom_resultat.png)


Dans un second temps, nous allons vérifier le bon écoulement de la pluie sur la surface du bassin versant test, vers le réservoir. Pour cela, la configuration sera : 

*	Vanne fermée sur la branche où se situe l’hydrographe.
*	Vanne ouverte sur la branche sur laquelle un bassin versant est accroché.

Puis, on fait tourner le scénario temps de pluie. La pluie modélisée fait tomber une hauteur d’eau de 37,7 mm. La surface du bassin versant test est de 0,5 hectares, et le coefficient de ruissellement est égale à 1, ce qui rend ce bassin versant imperméable : toute l’eau est attendue dans le réseau. On attend donc 188 m3 dans le réservoir à l’exutoire. 

Modifier la fermeture des vannes depuis le bouton edit, pour ce test. Refaire tourner le scenario temps_pluie. Afficher la courbe de remplissage du réservoir avec l’outil show graph. 

La hauteur d’eau dans le réservoir est de 2.94 m. Le volume d’eau est donc de 188 m3.
Le volume peut varier selon la précision choisie de la valeur de la hauteur d’eau dans le réservoir.

![44_resultat_autre_vanne.png](/tutorials/44_resultat_autre_vanne.png)

On peut également utiliser l’outil show graph sur la vanne associée à la branche où se situe le BV. On constate que le débit reçu par la vanne a le même type de profil que la pluie double triangle modélisée.

![45_debit.png](/tutorials/45_debit.png)

Modifier la fermeture des vannes depuis le bouton edit, pour ce test. Refaire tourner le scenario temps_pluie. Afficher la courbe de remplissage du réservoir avec l’outil show graph. 

On constate que la hauteur d’eau dans le réservoir n’a pas augmentée.


![46_debit_ferme.png](/tutorials/46_debit_ferme.png)

On peut également utiliser l’outil show graph pour vérifier qu’il n’y a pas de débit qui soit passé par les vannes. 

![47_debit_ferme.png](/tutorials/47_debit_ferme.png)


En revanche, on peut constater un faisant un profil en long des branches de réseau, que l’eau s’est accumulée en amont des vannes. Ici, le profil en long de la vanne associée à la branche avec bassin versant. 

![48_profil_en_long.png](/tutorials/48_profil_en_long.png)

Ici, le profil en long avec la branche associée à l’hydrographe de 30 m3. 

![49_profil_en_long.png](/tutorials/49_profil_en_long.png)


