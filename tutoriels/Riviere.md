---
title: Tutoriel rivière
description: 
published: true
date: 2025-02-17T16:52:31.925Z
tags: 
editor: markdown
dateCreated: 2021-12-23T16:39:11.198Z
---

# Introduction
Pour l'installation d'hydra, se reporter à la section dédiée : [installation hydra](/fr/installation_v3)

Les actions présentées ci-après permettent de créer un petit modèle couplant des domaines 1D, casiers et 2D, de faire tourner un scénario de calcul et d'exploiter les résultats. Ce tutoriel s'appuie sur un jeu de données disponibles sur le lien suivant : [Tuto_hydra_riviere.zip](https://hydra-software.net/tutoriel/riviere/Tuto_hydra_riviere.zip) : 

- Données topographiques :
    - *Modèle Numérique de terrain (MNT),*
    - *Bathymétrie,*
    - *Ouvrages,*
- Conditions limites amont et aval,
- Fichiers shape à importer (lignes de contrainte),

>:hourglass: Ce jeu de données comprend également le modèle construit pouvant être importé directement *tuto_riviere.sql*. **Pour importer le modèle, voir le chapitre dédié** [Importer un modèle](#import_project)

![modele_final.png](/tutorials/river/modele_final.png)


# Création d'un projet et d'un modèle
:book:[Documentation](https://wiki.hydra-software.net/fr/project_management)
## Nouveau projet


Ouvrir le gestionnaire de projet via le menu Hydra\Manage project
![manage_project.png](/tutorials/river/manage_project.png)

Créer un nouveau projet en sélectionnant le bouton `New`
- `Project name` : nom du projet
- `EPSG` : système de projection dans lequel sera créé le modèle (le bouton Select EPDG permet d’ouvrir la liste des systèmes de projection disponibles dans QGIS)
- `Working directory` : répertoire dans lequel seront créés les résultats de calcul et où peuvent être stockés les fichiers externes (MNT, données hydrologiques, …). Par défaut dans le répertoire user.

![manage_project_ui_new.png](/tutorials/river/manage_project_ui_new.png)

Valider, le projet est créé. Le gestionnaire de projet récapitule tous les projets présents dans la base. Il précise la version de la base de données associée au projet (Data version).

![manage_project_ui_after_creation.png](/tutorials/river/manage_project_ui_after_creation.png)

Sélectionner le projet qui vient d’être créé, puis cliquer sur `Open` pour l’ouvrir :
- Les tables associées au projet sont chargées dans le gestionnaire de couches dans un groupe nommé Projet,
- Les différentes barres d’outils d’Hydra sont chargées dans QGIS. Ci-dessous la barre d’outils :
 ![hydra_button_tools.png](/tutorials/river/hydra_button_tools.png =500x)
- Un répertoire portant le nom du projet est créé dans le Working directory :
  ![folder_project.png](/tutorials/river/folder_project.png)

Le fichier qgs comprend l’appel de l’ensemble des couches du projet et du modèle (cf. ci-après) issues de la base de données, mais également toutes les couches complémentaires qui pourront être appelées par l’utilisateur (fond de plans, ….).

## Nouveau modèle
Un ou plusieurs modèles peuvent être associés à un projet ; ces modèles pourront ensuite être assemblés lors du calcul (sous modèles géographiques interconnectés par exemple). Nous ne travaillerons ici que sur un seul modèle.

Créer un nouveau modèle via le menu `Hydra\Models\Add model`


![menu_add_model.png](/tutorials/river/menu_add_model.png) ![menu_add_model_ui.png](/tutorials/river/menu_add_model_ui.png)

Les tables associées au modèle sont chargées dans le gestionnaire de couche, dans un groupe portant le nom du modèle.


# Importer les données topographiques
Avant de démarrer la construction du modèle, il nous faut importer les données topographiques qui vont être utilisées pour paramétrer les objets de modélisation.
https://wiki.hydra-software.net/fr/topographic_bathymetric_data_management

## Le Modèle Numérique de Terrain (MNT)
Importer un MNT via le menu `Hydra\Terrain`

 ![menu_terrain.png](/tutorials/river/menu_terrain.png) ![menu_terrain_ui.png](/tutorials/river/menu_terrain_ui.png)

Sélectionner via le bouton ![menu_terrain_ui_vrt_button.png](/tutorials/river/menu_terrain_ui_vrt_button.png)  les fichiers fournis dans le répertoire data\MNT.

![menu_terrain_ui_new_dem.png](/tutorials/river/menu_terrain_ui_new_dem.png)

Hydra va importer ces données dans le répertoire `Workspace\terrain` et créer un fichier vrt (raster virtuel). 

Indiquer un nom générique pour le fichier vrt, et l’EPSG des données sources si différent de celui du projet.

![menu_terrain_ui_new_dem_result.png](/tutorials/river/menu_terrain_ui_new_dem_result.png)

Valider, le MNT est chargé dans le gestionnaire de couches.

![dem_hillshade.png](/tutorials/river/dem_hillshade.png){.align-center}


## La bathymétrie
Le MNT étant issu d’un levé LIDAR, il doit être complété par des levés bathymétriques des cours d’eau, fournis dans le répertoire *data\Bathymetrie*. Ces levés sont importés en deux étapes décrites ci-après:
- importer le fichier shape dans la base de données (dans un répertoire de travail dénommé *work*)
- importer cette table dans la table de semis de points du projet


La **première étape** permet d'importer le fichier shape dans la base de données.

- Sélectionner `Manage external tables` dans le menu hydra
![menu_manage_external_table.png](/tutorials/river/menu_manage_external_table.png)

- Sélectionner le fichier *data\Bathymetrie\Bathy.shp* via le bouton   ![button_add.png](/tutorials/river/button_add.png). Cette opération permet d’importer le fichier shape dans un répertoire de travail de la base de données.
- Renseigner le nom de la table créée dans le menu *Importer une couche vecteur*.
La table apparait dans la colonne *Loaded tables*.

![menu_manage_external_table_ui_import_bathy_work.png](/tutorials/river/menu_manage_external_table_ui_import_bathy_work.png)

La **seconde étape** permet d’importer les données dans la table de semis de points vectoriels qui sera utilisée par Hydra pour la construction des objets de modélisation (table Points xyz associée au Projet).
- Sélectionner la table bathy dans le tableau de gauche (les champs de la table sont rappelés dans le tableau central Details)
- Dans le tableau de droite `Import table to project` :
    -	Target schema : sélectionner project
    -	Target table : sélectionner points_xyz
    -	Préciser l’association à faire entre les champs de la table bathy et ceux de la table points_xyz du projet
- Cliquer sur `Import`, puis `ÒK`pour fermer la fenêtre.


![menu_manage_external_table_ui_import_bathy.png](/tutorials/river/menu_manage_external_table_ui_import_bathy.png)


Après validation (OK), les points sont chargés dans le gestionnaire de couche (rafraichir l’affichage avec ![button_refresh.png](/tutorials/river/button_refresh.png) ).

![dem_hillshade_pointsxyz.png](/tutorials/river/dem_hillshade_pointsxyz.png){.align-center}

Une **dernière étape** permet d'affecter ces points dans un groupe dédié. Ceci est nécessaire pour pouvoir sélectionner certains types de points dans certaines applications (par exemple dissocier des points situation actuelle et situation projet pour géénrer des profils en travers ou des liaisons sur des digues).

- **Créer un groupe** via le menu hydra\Terrain et le bouton ![button_add.png](/tutorials/river/button_add.png)dans l'onglet *Terrain points*. Le groupe créé peut être renommé en sélectionnant le texte dans la colonne *Name*
![menu_terrain_ui_terrain_points.png](/tutorials/river/menu_terrain_ui_terrain_points.png) 

- **Sélectionner les points** importés (se positionner sur la couche *Terrain points* et ctrl + A), puis dans l'onglet *Model dock*, sélectionner le bouton ![hydra_model_dock_terrain_points.png](/tutorials/river/hydra_model_dock_terrain_points.png){.align-right}

- **Affecter à ces points le groupe précédemment créé** et valider ![terrain_group_selection_ui.png](/tutorials/river/terrain_group_selection_ui.png =30%x){.align-right}



# Construction du modèle
## Principes

Les objets de modélisation sont créés via les utilitaires disponibles dans le panneau de gauche **Model dock** ou par import de fichiers shape (sur le même principe que pour les points bathymétriques).

![hydra_model_dock.png](/tutorials/river/hydra_model_dock.png)

## Création du modèle 1D

### Reach : L'axe du cours d'eau qui portera les objets 1D (profils en travers, ouvrages, ...)
Nous allons ici le créer manuellement.

Sélectionner l’outil `reach` puis le dessiner de l'amont (nord-ouest) vers l'aval (sud-est), au centre du lit mineur (l'objet est de type *polyligne*).
![hydra_model_dock_reach.png](/tutorials/river/hydra_model_dock_reach.png) |


Donner un nom au Reach, puis valider. Hydra crée deux `river node` aux extrémités du Reach.
![reach_ui.png](/tutorials/river/reach_ui.png) |

![dem_hillshade_reach.png](/tutorials/river/dem_hillshade_reach.png){.align-center}

>:bulb: La géométrie du reach peut être modifiée après création avec le soutils d'édition topologique de QGIS (rendre la couche Reach modifiable, et enregistrer la couche après modification).

### River node : Les noeuds sur le reach qui porteront les objets 1D (profils en travers, ouvrages, ...)

 Sélectionner l’outil `river node`
 ![hydra_model_dock_river_node.png](/tutorials/river/hydra_model_dock_river_node.png)

Puis créer des points sur le Reach
- au droit de chaque profil bathymétr:ique intermédiaires (les river node sont déjà créés aux extrémités amont et aval)
- au droit du pont

:warning: Sélectionner l’outil pour chaque objet à créer

![river_node_ui.png](/tutorials/river/river_node_ui.png) Valider 


![dem_hillshade_reach.png](/tutorials/river/dem_hillshade_river_node.png){.align-center}

### Flood Plain Transect : polylignes support des profils en travers

Les profils en travers seront générés par projection des cotes bathymétriques et du MNT sur des lignes support : des lignes de contrainte de type Flood plain transect.
Ces lignes peuvent être créées via l’outil  ; ce sont des polylignes dessinées de la rive gauche vers la rive droite, les sommets encadrant le point d’intersection avec le reach définissant les limites du lit mineur.
Nous allons ici les **importer à partir d’un fichier shape** sur le même principe que ce qui a été fait pour les points bathymétriques.

![menu_manage_external_table_ui_import_constrain.png](/tutorials/river/menu_manage_external_table_ui_import_constrain.png)

Rafraichir l’affichage avec ![button_refresh.png](/tutorials/river/button_refresh.png) .

![dem_hillshade_flood_plain_transect.png](/tutorials/river/dem_hillshade_flood_plain_transect.png){.align-center}

> :warning: Vérifier que le reach coupe bien les lignes de contraintes extrémités amont et aval. Si ce n’est pas le cas, rendre la couche reach modifiable et déplacer le sommet de la polyligne avec l’outil de QGIS. Faire de même avec le river node pour le recaler sur l’extrémité du reach ; après enregistrement, la cross section se recalera automatiquement sur le river node.

**Associer les flood plain transect aux points bathymétriques** précédement importés : 
- Editer les flood plain transect avec le bouton ![hydra_button_edit_object.png](/tutorials/network/hydra_button_edit_object.png)
- Sélectionner le groupe de point créé précédemment dans *Points type* (pour les contraintes localisées à proximité des points). Ces points seront exploités pour contruire les profis en travers associés à la ligne de contrainte.

![constrain_flood_plain_ui.png](/tutorials/river/constrain_flood_plain_ui.png =80%x)

> hydra offre des fonctionnalités avancées permettant d'automatiser ces différentes étapes. L'objectif est ici de proposer une contruction pas à pas pour bien appréhender les différents concepts et étapes de contruction.

### Cross section
Sélectionner l’outil `cross section` ![hydra_model_dock_cross_section.png](/tutorials/river/hydra_model_dock_cross_section.png) 

Puis créer des cross section comme indiqué ci-après, en commençant par l’amont.

:warning: Sélectionner l’outil pour chaque objet à créer

- **Section amont**
   - Poser l’objet cross section sur le river node amont
   - Sélectionner le type de section *Valley* pour downstream **(1)**. A chaque objet cross section peuvent être associées 2 sections : une amont et une aval. En amont du reach, la section doit être affectée à l’aval (downstream)
   - La Flood plain transect la plus proche *Constr1* **(2)**  est associée à cette section 
   - La geométrie hydra (en bleu) est générée automatiquement à partir des données bathymétriques et du MNT associées à la ligne de contrainte *Constr1*.
   - Définir les paramètres de rugosité et de sinuosité **(6)**
   - Valider

>    - Les sommets de la flood plain transect délimitent les entités de la section :
>       - les deux sommets encadrant le point d'intersection avec le reach marquent les limites du lit mineur;  ces limites sont reportées sur le profil en travers :
>       				- lignes **(3)** : rouge (berge rive gauche) ou verte (berge rive droite)
>       				- la ligne bleue marque l'intersection avec le reach
>      	- les sommets extrémités délimitent l'emprise de la section
>      	- la présence d'un seul sommet sur un côté du flood plain transect indique qu'il n'y a pas de lit majeur sur ce côté
>   - Il est possible d'ajuster la position des limites du lit mineur en sélectionnant les li et en la déplaçant en maintenant le clic gauche de la souris appuyé. La section *hydra* est regénérée automatiquement (fonction autofit activée). La même opération est possible pour les extrémités des sections avec les lignes **(4)**
>   L'encart **(5)** permet de visualiser la position des sommets de la flood plain transect, qui est ajustée en conséquence.
> La molette de la souris permet de zoomer / dézoomer :
>	- souris au centre : action sur les deux axes,
>  		- souris sur l'axe des abscisse : action sur l'échelle horizontale,
>  		- souris sur l'axe des ordonnées : action sur l'échelle verticale.

![cross_section_ui_v2.svg](/tutorials/river/cross_section_ui_v2.svg =750x){.align-center}

Faire ceci pour toutes les cross section :
- Au droit de la Flood plain transect Constrain2 :
    - la section peut être affectée indifféremment à l’amont ou l’aval de la cross section
    - la section ne comporte pas de lit majeur rive gauche
- Pour la constrain3 : positionner la corss section au droit du pont, et appeler la Flood plain transect Constrain3 si elle n'est pas identifiée automatiquement. Nous faisons ici l'approcximation que la section a été levée au droit du pont; il est toujours préférable de positionner une section au droit des ouvrages. Eventuellement cliquer sur *Reset* pour réinitialiser la section.
- Section aval (Constrain5): affecter la section à l’amont de la cross section (upstream).


### Ouvrages hydrauliques

Deux ouvrages sont présents sur le linéaire modélisé :

**Seuil** : poser un objet `Weir` (singularity) sur le nœud situé à proximité le Constrain2

![hydra_model_dock_weir.png](/tutorials/river/hydra_model_dock_weir.png)  <img src="/tutorials/river/weir_ui.png" height="400px"/>

**Pont** : poser un objet `Bridge` (singularity) sur le nœud déjà positionné

![hydra_model_dock_bridge.png](/tutorials/river/hydra_model_dock_bridge.png) <img src="/tutorials/river/bridge_ui.png" height="400px"/> 


## Maillage 2D et casiers

La construction de mailles 2D et des casiers s’appuie également sur des lignes de contraintes, qui définissent :
- Les limites des domaines,
- Le pas de discrétisation du maillage (paramètre element length)
- Le type de liaison qui sera généré à la frontière de deux domaines.

### Dessiner les lignes de contrainte contours des différents domaines

Importer les lignes de contrainte sur le même principe que les Flood plain transect précédemment.

![menu_manage_external_table_ui_import_constrain2.png](/tutorials/river/menu_manage_external_table_ui_import_constrain2.png)


![dem_hillshade_constrains.png](/tutorials/river/dem_hillshade_constrains.png){.align-center}

Deux types de lignes de contraintes sont importées :
- Overflow (en vert ci-dessous) sur les lignes de surverse (berges limite 1D/2D, remblais)
- Standard (en noir ci-dessous) sur les contours extérieurs

> :bulb: Les lignes de contraintes peuvent être créées manuellement avec l’outil ![hydra_model_dock_constrain.png](/tutorials/river/hydra_model_dock_constrain.png) .

> :warning: Les extrémités des lignes de contraintes doivent impérativement être fusionnées :
>- Lorsqu’elles sont créées avec l’outil Hydra, un accroche objet est automatiquement activé,
>- Si la géométrie de ces lignes de contraintes devait être modifiée via les outils d’édition d’hydra, activer l’accroche objet.
>Exemple en **rive gauche amont du reach** : les deux lignes de contraintes ne sont pas fusionnées; un symbole ![unclosed_constrain_symbol.png](/tutorials/river/unclosed_constrain_symbol.png)permet d'identifier les sommets non fusionnés ou très proches.
<img src="/tutorials/river/constrain_not_snapped.png" height="150px"/> 
>**--> les accrocher avec les outils de QGIS** :
>			- rendre la couche Constrain éditable![qgis_edit_layer.png](/tutorials/qgis_edit_layer.png)
>  		- activer l'accroche objet de QGIS ![qgis_snap_tool.png](/tutorials/qgis_snap_tool.png)
>			- fusionner les sommets (modifier de préférence la ligne noire, disjointe des deux autres)




Cliquer sur le bouton `Update coverage` du model dock (si il n'est pas activé par défaut) : le programme identifie les polygones formés par les lignes de contrainte, délimitant les différents domaines. Ceux intersectant le reach sont définis de type Reach et affichés en bleu. Les autres sont pour le moment de type 2D, en gris.

![update_coverages.png](/tutorials/river/update_coverages.png)

> L’activation d’Auto regeneration permet d’actualiser les coverage à chaque enregistrement de la couche de lignes de contraintes (il peut être nécessaire de rafraicir l'affichage après une action ![button_refresh.png](/tutorials/river/button_refresh.png).

![dem_hillshade_coverages.png](/tutorials/river/dem_hillshade_coverages.png){.align-center}

### Les casiers
Les casiers sont des zones de stockage sans vitesses ; nous allons en créer sur les deux coverage au nord, isolés de l’écoulement principal par de hauts remblais. 
- Sélectionner le bouton ![hydra_model_dock_storage.png](/tutorials/river/hydra_model_dock_storage.png)  du model dock
- Cliquer sur le centre du coverage
- Cliquer sur `Generate filling curve from terrain` (définit la loi de remplissage cote / volume du casier)

![storage_ui.png](/tutorials/river/storage_ui.png)

- Regénérer les coverage : ceux contenant le marqueur de casier apparaissent alors en vert.

![dem_hillshade_storages.png](/tutorials/river/dem_hillshade_storages.png)


### Le maillage 2D
Sélectionner l’outil ![hydra_model_dock_mesh.png](/tutorials/river/hydra_model_dock_mesh.png)  et cliquer sur le coverage gris pour mailler.

![dem_hillshade_mesh1.png](/tutorials/river/dem_hillshade_mesh1.png)

Il est possible à tout moment d’ajuster le maillage en ajoutant des lignes de contraintes ou en modifiant le type et le paramètre element length de celles déjà créées. Les géométries des lignes de contrainte peuvent également être modifiées via les outils topologiques de QGIS (rendre la couche constrain modifiable).
- Démailler avec l’outil ![hydra_model_dock_unmesh.png](/tutorials/river/hydra_model_dock_unmesh.png) 
- Ajouter / modifier les lignes de contraintes puis enregistrer la couche si modifiée manuellement
- Regénérer les coverage
- Remailler

![dem_hillshade_mesh2.png](/tutorials/river/dem_hillshade_mesh2.png)

De la même façon, un casier peut être supprimé puis maillé 

- Supprimer le marqueur de casier avec l’outil ![button_delete.png](/tutorials/river/button_delete.png)  du menu principal
- Regénérer les coverage
- Mailler le coverage

> les **liaisons associées aux mailles 2D** sont **automatiquement générées** lors du maillage :
> - Liaisons inter-mailles 2D (*mesh link*)
> - Liaisons 1D/2D (*overflow*). Leur orientation est définie par l’orientation des flood plain transect qui les encadrent
> - Liaisons 2D/casiers (*overflow*)

## Liaisons complémentaires
### Liaisons en lot
Les liaisons entre les domaines autres que 2D ne sont pas générées automatiquement. Pour les générer :
- Sélectionner l’outil  ![hydra_model_dock_auto-links.png](/tutorials/river/hydra_model_dock_auto-links.png) du model dock
- Sélectionner successivement les deux coverage à relier
- Les liaisons sont générées au pas défini par le paramètre element length de la ligne de contrainte frontière entre les deux domaines.

![dem_hillshade_auto-links.png](/tutorials/river/dem_hillshade_auto-links.png)


### Liaisons individuelles
Créer des liaisons pour représenter les buses sous le remblai rive gauche :
- Sélectionner l’outil `Gate` du model dock
![hydra_model_dock_gate_link.png](/tutorials/river/hydra_model_dock_gate_link.png)
- Sélectionner les éléments amont et aval de chacune des liaisons et définir leurs paramètres
![gate_link_ui.png](/tutorials/river/gate_link_ui.png)


| **Buse 1**          | **Buse 2**          |
|----                 |-------               |
| *Z gate : 404.40**  | *Z gate : 401.80*    |
| *Zceiling : 404.40* | *Zceiling : 401.80*  |
|*Z invert : 403.40*  | *Z invert : 400.80*  |
| *Width : 1*         | *Width : 2*          |


**au minimum la cote de la maille. Il est possible de forcer la cote de la maille pour l’abaisser.*

![dem_hillshade_gate_link.png](/tutorials/river/dem_hillshade_gate_link.png)

## Conditions aux limites
#### Amont
Une condition à la limite aval de type injection d’hydrogramme est posée en amont.
- Sélectionner le bouton `hydrograph`  ![hydra_model_dock_hydrograph_bc.png](/tutorials/river/hydra_model_dock_hydrograph_bc.png) du model dock
- Poser l’objet sur le nœud amont du reach
- Le paramétrer comme suit
    -	Renommer *amont*,
    -	Passer `External file` data sur Yes
![hydrograph_bc_ui.png](/tutorials/river/hydrograph_bc_ui.png)

### Aval
Une condition à la limite aval de type courbe de tarage est posée en aval.
-	Sélectionner le bouton `zq` ![hydra_model_dock_zq_bc.png](/tutorials/river/hydra_model_dock_zq_bc.png) du model dock
-	Poser l’objet sur le nœud aval du reach
-	Le paramétrer comme suit :
    -	Renommer,
    -	Renseigner la courbe de tarage (cf. fichier fourni *data\Condition limite aval\ q_z_downstream.csv* et copier-coller du fichier)

<a name="import_project"></a>

# Importer directement le modèle
Pour une première découverte d'hydra, vous pouvez tout simplement importer le modèle exemple fourni *tuto_riviere.sql*. Pour ce faire:

- Ouvrir le gestionnaire de projet via le menu `Hydra\Manage project`
![manage_project.png](/tutorials/river/manage_project.png) ![manage_project_ui_import2.png](/tutorials/river/manage_project_ui_import.png)
- Sélectionner `Import from file` 
![manage_project_ui_import2.png](/tutorials/river/manage_project_ui_import2.png)
- Renseigner les menus :
    - `Project name` : nom du projet
    - `EPSG` : système de projection dans lequel sera créé le modèle (le bouton Select EPDG permet d’ouvrir la liste des systèmes de projection disponibles dans QGIS)
    - `Working directory` : répertoire dans lequel seront créés les résultats de calcul et où peuvent être stockés les fichiers externes (MNT, données hydrologiques, …). Par défaut dans le répertoire user.

- Valider, le projet est créé. Le gestionnaire de projet récapitule tous les projets présents dans la base. Il précise la version de la base de données associée au projet (Data version).

![manage_project_ui_after_creation.png](/tutorials/river/manage_project_ui_after_creation.png)

- Sélectionner le projet qui vient d’être créé, puis cliquer sur `Open` pour l’ouvrir

:warning: **Le MNT n'est pas importé avec le projet**. Reportez vous à la rubrique *Importer les données topographiques* pour le charger.

# Paramétrage du scénario de calcul et lancement des calculs

https://wiki.hydra-software.net/fr/scenarios

:warning: **Si le modèle a été importé**, utiliser le scénario pré-paramétré et définir le chemin vers le fichier d'hydrogramme (cf. *Hydrology settings* ci-après).

## Interface de paramétrage des scénarios

Le paramétrage des scénarios se fait via le menu Hydra\Scenarios\Settings
![menu_scenario_settings.png](/tutorials/river/menu_scenario_settings.png)

![menu_scenario_settings_computation_ui.png](/tutorials/river/menu_scenario_settings_computation_ui.png)

Créer un scénario avec le bouton  ![button_add.png](/tutorials/river/button_add.png) :
- Donner un nom
- Ajouter un commentaire
- Sélectionner ce scenario 

## Computation settings
Définir les paramètres de calcul dans l’onglet computation settings
- Computation duration : 48
- Output time step : 00h 10m
- T max : 48h

## Hydrology settings
Définir les paramètres relatifs à la définition des apports hydrologiques; ici un hydrogramme défini dans un fichier externe.
- External files hydrographs :
    -	Ajouter un fichier via le bouton  
    -	chercher le fichier q_t_upstream.hyg fourni dans *data\Hydrogramme amont\q_t_upstream.hyg* préalablement copié dans le répertoire data du workspace
- Valider et fermer l’interface de paramétrage de scénario
![menu_scenario_settings_hydrology_ui.png](/tutorials/river/menu_scenario_settings_hydrology_ui.png)


# Lancement des calculs
Le lancement des calculs se fait via la barre d’outils :
- Sélectionner le scénario à lancer dans le menu déroulant
 ![hydra_toolbar_run.png](/tutorials/river/hydra_toolbar_run.png)
- Cliquer sur run
- La fenêtre de suivi du déroulement du calcul apparait
- Le calcul est terminé lorsque le message whydram24 ok apparait dans la fenêtre de gauche

![whydram24.png](/tutorials/river/whydram24.png)

Fermer la fenêtre.


# Exploitation des résultats
Nous présentons ici les principaux outils d’exploitation des résultats de calcul.
https://wiki.hydra-software.net/fr/results

## Courbes X(t)
L’outil ![hydra_toolbar_xt.png](/tutorials/river/hydra_toolbar_xt.png)  permet de visualiser les variations temporelles des différentes variables associées aux objets de modélisation (nœuds, liaisons, …)
-	Sélectionner le bouton
-	Cliquer sur l’objet souhaité
-	Paramétrer l’affichage :
    -	Choix des grandeurs à afficher et axe associé, 
    -	Axe, 
    -	…

![xt_ui.png](/tutorials/river/xt_ui.png)

## Synthèse des grandeurs maximales
Le bouton ![hydra_toolbar_minmax.png](/tutorials/river/hydra_toolbar_minmax.png)  permet d’accéder à la synthèse des grandeurs maximales associées à un objet de modélisation : cliquer sur l’outil, puis sélectionner l’élément choisi.

![minmax_ui.png](/tutorials/river/minmax_ui.png)

## Cartographie des hauteurs et vitesses maximales d’écoulement sur les élément surfaciques (mailles 2D, casiers et rues) ![hydra_toolbar_synthetic_res.png](/tutorials/river/hydra_toolbar_synthetic_res.png) 
Actualiser la géométrie des objets via le menu Hydra\Settings puis panneau Synthetic settings

![menu_settings.png](/tutorials/river/menu_settings.png)  ![menu_settings_synthetic_res_ui.png](/tutorials/river/menu_settings_synthetic_res_ui.png)

Activer le bouton ![hydra_toolbar_synthetic_res.png](/tutorials/river/hydra_toolbar_synthetic_res.png)  : les résultats pour le scénario actif (i.e. sélectionné dans le menu déroulant également utilisé pour lancer les calculs) sont chargés dans le gestionnaire de couches.

Les résultats sont placés par défaut sous le MNT ; les remonter dans le gestionnaire de couches.  ![synthetic_res_layers.png](/tutorials/river/synthetic_res_layers.png)

![dem_hillshade_synthetic_res_surface_depth_velocity.png](/tutorials/river/dem_hillshade_synthetic_res_surface_depth_velocity.png)


## Profil en long des reach
- Afficher le panneau de contrôle Longitudinal profil dock à gauche
- Sélectionner le reach avec le bouton ![button_add.png](/tutorials/river/button_add.png)  (reach to display)
- Sélectionner le scénario avec le bouton  ![button_add.png](/tutorials/river/button_add.png) (Scénarios to display)
- Sélectionner les grandeurs à afficher (géométrie et résultats de calcul)
- Possibilité d’afficher 
     -	les résultats de calcul : profils en long des lignes d'eau, des débits, des vitesses, ... le long des biefs de cours d'eau (reach) ou des branches de collecteurs (branch),
    -	la géométrie des biefs de cours d'eau ou/et des tronçons d'assainissement, ainsi que les ouvrages et liaisons associés.
    -	les valeurs max en activant le bouton  ![longitudinal_profile_max.png](/tutorials/river/longitudinal_profile_max.png)
    -	ou l’animation dynamique des grandeurs hydraulique en cliquant sur la flèche de défilement ![longitudinal_profile_play.png](/tutorials/river/longitudinal_profile_play.png) 

![longitudinal_profile.png](/tutorials/river/longitudinal_profile.png)