---
title: Bienvenue dans la documentation hydra
description: 
published: true
date: 2025-02-04T23:33:26.132Z
tags: 
editor: markdown
dateCreated: 2024-10-22T10:19:19.492Z
---

# ![0_hydra_icone.png](/0_hydra_icone.png =50x) Bienvenue dans la documentation hydra

Bienvenue dans la documentation d'hydra, le logiciel de modélisation hydrologique et hydraulique des  réseaux urbains et des écoulements de surface immergé dans QGIS.

Si vous êtes un nouvel utilisateur de cette documentation, la `table des matières`  dans la `barre latérale`  vous permet d’accéder facilement à la documentation pour le sujet qui vous intéresse.
Des **[tutoriels](/fr/tutoriels/home-page)** permettent une **prise en main rapide** d'hydra en balayant sur des cas simples l'ensemble des étapes de construction d'un modèle, de paramétrage et de lancement des calculs et d'exploitation des résultats.

Outre la documentation en ligne, des `documents techniques` présentent plus en détail les équations mises en oeuvre. Ils sont accessibles à partir du menu *Documentation technique* ci-dessous. Des documentations complémentaires relatives au paramétrage avancé de certaines fonctionnalités sont également dispoibles dans le menu ![0_hydra_icone.png](/0_hydra_icone.png =15x) *Documentation*

> L’équipe de documentation peut à tout moment bénéficier de vos retours pour améliorer les tutoriels et manuels d’utilisation. Si vous ne comprenez pas quelque chose, ou estimez qu’il manque des informations, n’hésitez pas à nous le faire savoir :
>- via les espaces de commentaires sur ce site
>- via notre support : support@hydra-software.net


---



---


# Tabs {.tabset}


## Actualités
### 04/02/2025 : La **version 3.10.11** d'hydra vient de sortir.
Cette version apporte de nombreuses petites évolutions détaillées ci-dessous.

Vous pouvez mettre à jour votre plugin depuis le gestinnaire d'extensions d'hydra.

Le Changelog liste les évolutions par rapport à la version précédente.

### Corrections :
       
:gear: **moteur de calcul**
- réparation de l'attribut restart manquant pour le second scénario d'une chronique lorsque la chronique contient exactement deux scénarios
- réparation d'un bug whydram dans les sections avec PT doublelm/lmme et param heps
- modification de la routine hydram esing2_q2_aux pour éliminer les ralentissement de calcul
- modification de l'algo de régulation des seuils mobiles pour une meilleure stabilité pour les faibles débits
- réparation d'un bug pour le mot clef correct_hydrau en mode hotstart
- réparation d'un bug dans calcul traitement sédimentaire
- réparation d'un petit bug dans le module de controle


:desktop_computer: **interface**
- ajout de la fonction "create_polygonz" manquante pour la création de stations de gestion dans le module "map_point_tool module" 
- réparation des listes de simulation (algo simulate) qui ne pouvaient pas être chargées à cause d'une fonction setValue manquante dans le widget de choix de scénario
- réparation export_calcul avec les chroniques
- correction affichage des temps dans le grapheur
- outils de post-traitement cartographique :
    - slf_to_tif (sorties raster lignes d'eau, vitesses et hauteurs d'eau) : répare une erreur de calcul des enveloppes pour le traitement des maxima
    - modifications mineures : renommage des outils, suppression CRGENG (toujours accessible dans advanced tools)
- corrections export_calcul.py sur ZQsplit et L_Rgate (la loi Q(h) était mal exportée)


### Améliorations
:gear: **moteur de calcul**
- rationalisation du module de gestion des formats des dates dans whydram_24 et hydrol
>Les programmes hydrol.exe et Whydram.exe et extrat.exe gèrent désormais en lecture les formats dates « standard » suivants en plus de ceux déjà acceptés dans les fichiers externes de données :
2020-12-31 18:00: 00
2020-12-31T18:00:00
31/12/2020 18:00:00
En écritures les dates sont désormais produites au format « presque iso » : 2020-12-31 18:00:00, sans la lettre séparatrice « T » entre le jour et l’heure qui n’est pas interprétée par Excel.
{.is-info}

- Enrichissement des fichiers de sorties W13, W14, W15 
>Ces fichiers sont exploités par l’outil « visu rapide ». Cet outil nécessite de lire certaines informations dans le fichier de paramétrage scenario.nom et  .cmd du scénario. 
Pour s’affranchir de cette contrainte et exploiter le grapheur avec les seules informations contenues dans les fichiers Wxx on a enrichi ces derniers aves les informations manquantes.
Les modifications introduites sont décrites dans la note NT88. Elles permettent désormais de récupérer les fichiers Wxx générés par une application externe et de les importer dans hydra/Qgis  pour visualiser les courbes via le grapheur « visu rapide » sans avoir recours à des fichiers autres que les fichiers Wxx.
Au passage le plug in a été modifié pour afficher les temps relatifs tels qu’ils sont définis dans les fichiers Wxx .
{.is-info}

- Amélioration des modules BO et CAS 
>Pour éviter les problèmes de grattage numérique le code n’impose plus la condition : z>zf. C’est à l’utilisateur de régler correctement les cotes de liaisons adjacentes pour assurer le respect de cette condition. 
De plus en démarrage à froid : le code impose z=zini jusqu’au temps t=0 .
Pour l’objet BO : la command *CORRECTB a été améliorée pour assurer le respect de la cote du bassin au démarrage des calculs avec une tolérance plus faible sur l’écart entre la cote imposée et la cote calculés au premier pas de temps.
{.is-info}

- Gestion des mots clé d’options
>Un gros travail a été effectué pour rationaliser la gestion des mots clés d’options. Tous les mots clés sont exportés désormais dans le fichier de calcul X\travail\X_optsor.dat à l’exception de mots clés réserves qui sont exportés dans les fichiers X\scenario.nom et X\travail\X.cmd.
{.is-info}

- Emulation du pilotage d’une chaîne de calcul externe  avec  hydra/Qgis
>Les applications « temps réel » occupent une place croissante dans l'utilisation d'hydra ![0_hydra_icone.png](/0_hydra_icone.png =10x). Dans ces applications la chaine de calcul est exécutée par un superviseur externe à partir d’un fichier de paramétrage décrivant le scénario. Ceci nécessite de formaliser un protocole d’échange standardisé entre hydra/Qgis et le superviseur externe de l’application temps réel. Les principes sous-jacents sont décrits dans la note NT79.
Afin d’émuler la chaîne de calcul en mode externe on a développé une procédure permettant de lancer les calculs  avec hydra/Qgis en utilisant les mêmes fichiers de paramétrage de scénarios que ceux générés par l’application externe. Pour plus de précisions se référer à la note NT79.
Ce développement est motivé par le besoin de rejouer dans hydra/Qgis des scénarios qui ont tourné dans l’application externe avec les mêmes fichier de paramétrage, afin d’analyser plus en détails les résultats et diagnostiquer les anomalies constatées.
De plus si une option est déclarée à la fois dans un formulaire du gestionnaire de scénario et dans un fichier externe d’options c’est désormais l’option du formulaire qui est prise en compte.
Pour plus de précisions se référer à la note NT25 et la note NT87.
{.is-info}

- Optimisation de la taille mémoire occupée par les modèles 
>Le pilotage de certains modèles peut provoquer une saturation de la taille mémoire disponible, réglée dans la version V3.10.11 4x100 Millions d’octets et correspondant à 200 000 nœuds de calcul. On ne souhaite pas augmenter cette allocation mémoire pour ne pas pénaliser les applications qui tournent en parallèle. L'option de calcul en cascade (ou en mode mixte) permet par ailleurs de contourner cette difficulté.
Pour permettre à ces gros modèles de tourner, on a limité la taille mémoire occupée par les tableaux de sortie en convertissant les variables correspondantes en mots de 4 octets (ce qui n’a aucune incidence sur la précision des résultats). La place mémoire tombe alors à moins de 4x97 Millions d’octets.
Une réduction supplémentaire de la place mémoire est obtenue en introduisant le mot clé *NOSORCSV : ce mot clé désactive la production des fichiers de sortie .csv produits dans les routine de calcul des fichiers W14 et W15. La place mémoire tombe alors à 4x76 Millions d’octets. Cette action a de plus comme conséquence de diminuer ls temps de simulation liés à la production de ces fichiers.
{.is-info}

- Nouveau mot clé *MARAIS
>Ce mot clé a été introduit pour modéliser le ressuyage des sols dans les zones marécageuses des Wateringues. Se référer à la note NT89 pour plus de précisions.
{.is-info}

- ajustement de la gestion des résultats dans le cas de l'aéraulique


:desktop_computer: **interface**
- amélioration du message d'avertissement lors de l'importation de conduites depuis une table externe

- Traitement des fichiers de données codés en ANSI
>Les fichiers de données lus par les programmes de la chaîne hydra doivent être encodés en UTF-8. Il arrive que des fichiers soient édités avec le  code ANSI ce qui génère des problèmes de fonctionnent  souvent incompréhensibles pour l’utilisateur dan lorsque les  caractères sont accentués.
Pour résoudre cette difficulté une fonction a été développes développée pour traduire en encodage UTF-8 les fichiers externes lus. Elle est opérationnelle pour le programme Extract.exe et reste à implémenter pour les autres exe.
{.is-info}

---

---

### 21/11/2024 : La **version 3.10.10** d'hydra vient de sortir.
Cette version apporte des corrections mineures à la version 3.10.9.

Vous pouvez mettre à jour votre plugin depuis le gestinnaire d'extensions d'hydra.

Le Changelog liste les évolutions par rapport à la version précédente.

### Corrections :
:gear: **moteur de calcul**
- correction d'un bug sur la pluie PRADP (radar temps réel)
- augmentation de 2000 à 10 000 du nombre de géométries valley dans un modèle
- correction du test de cohérence des configurations entre les scénarios de hotstart et de référence

:desktop_computer: **interface**
- correction de l'installeur (réduction des dépendances python)
- correction de l'accrochage hydra qui ralentissait les opérations de création d'objets depuis 3.10.8
- correction d'une fonction utilisée pour la cartographie des zones inondables en lit mineur (selafin rendering mesh)
- correction du démaillage/remaillage automatique

---

---
      
### 28/10/2024 : La **version 3.10.9** d'hydra vient de sortir.
Cette version apporte des corrections mineures à la version 3.10.8.

Vous pouvez mettre à jour votre plugin depuis le gestinnaire d'extensions d'hydra.

Le Changelog liste les évolutions par rapport à la version précédente.

### Corrections :
:gear: **moteur de calcul**
- correction d'un bug sur les conditions aux limites aval z(t) en mode AFFIN introduit en 3.10.7
- correction d'un bug dans correct_hydrau pour le temps réel

:desktop_computer: **interface**
- import SQL sans échec: les erreurs d'import sur la création de types déjà existants dans l'extension hydra (e.g. soil_infiltration_type) ou sur un utilisateur inconnu (cas de modèles hydra 2 qui n'avaient pas l'utilisateur hydra) sont ignorées comme c'était le cas dans des versions antérieures d'hydra.
- correction d'un bug lors de l'update du projet (fix bug 3.10.8)

        

---

---


### 07/10/224 : La **version 3.10.8** d'hydra vient de sortir.
Vous pouvez mettre à jour votre plugin depuis le gestinnaire d'extensions d'hydra.

Le Changelog liste les évolutions par rapport à la version précédente.

### Corrections :
:gear: **moteur de calcul**
- Correction d’un bug sur le traitement des pluies lorsque la première valeur de la courbe d’intensité n’est pas nulle.
- Contrôle et corrige les formulations de toutes les lois dans l'objet borda
- Corrige la formulation des liaisons Strickler à géométrie trapézoïdale lorsque la liaison est surélevées par rapport au TN. Possibilité de conserver l’ancienne formulation via le mot clé VERSION_KERNEL
- Corrections et enrichissement méthode affin

:desktop_computer: **interface**
- Anim-Eau : corrige la sauvegarde/rappel de la configuration de la visualisation du profil en long

### Améliorations
:desktop_computer: **interface**
- Répare l'affectation des domaines 2D au maillage lors des opérations de dé/re/maillage
`L'affectation des domaines 2d aux mailles du coverage considéré se faisait lors de la pose de l'objet domaine 2D. Cette affectation est à présent actualisée automatiquement lorsqu'un remaille un coverage.`
- Rend paramétrable le coefficient d'unité des pluies radar
- Amélioration des frontières pour le rendu cartographique
- Permet la visualisation des profils en long sur des scénarios issus de chroniques de pluie
- Optimisation du traitement des fichiers de sorties pour limiter les temps CPU en fin de calcul

:gear: **moteur de calcul**
- Explicite le message d'erreur en cas d'incohérence de paramétrage entre modèle de redémarrage à chaud et modèle de référence
- Introduit une coupure hydraulique sur les objets borda et DH en cas de discontinuité de géométrie entre le nœud amont et le nœud aval.
- Modifie le comportement des objets pertes de charge paramétriques sur les débits hors intervalles de la courbe de définition dh(Q)
- Consolide le terme cinétique amont (largeur au miroir bornée inférieurement à 0,05m) sur les liaisons gate et weir.
- Cycle sur les CLZT : le cycle s'applique aussi dans les temps précédents la définition du motif

### Nouveautés
:gear: **moteur de calcul**
- Ajouts de mots clefs spécifiques Orge pour assurer la compatibilité des résultats entre les versions 3.5/3.6 et la version 3.10.8 d’Hydra

:desktop_computer: **interface**
- Fichier de commande : implémentation de l'option de lecture d'une consigne de position de vanne via la commande SET


## Prise en main rapide

Les tutoriels pas à pas permettent une prise en main rapide d'hydra sur des petits modèles simples. Retrouvez nos deux tutoriels ainsi que les fichiers nécessaires ici :
- [Tutoriel assainissement](/fr/tutoriels/Assainissement)
- [Tutoriel riviere](/fr/tutoriels/Riviere)


Ces tutoriels sont également disponibles en **vidéo** :


[![tuto_riviere.jpg](/home/tuto_riviere.jpg =250x)](/fr/https://www.youtube.com/embed/TLEpA9BsaiE)
[↓![tuto_assinissement.jpg](/home/tuto_assinissement.jpg =250x)](https://youtu.be/oFx421WttBE)

## Dernières astuces

### Vous pouvez continuer à travailler pendant que le moteur calcule.
Il y a un algo "simulate" dans la boîte à outils de traitement, testez-le pour lancer un scenario... ou plusieurs 
 
Lancer l'algo "simultate" de la boite à outil traitements c'est comme appuyer sur le bouton run... mais mieux.
 
Normalement, vous devriez trouver l'algo dans le groupe de traitement "hydra" de la boite à outil, ou en tapant "simu" dans la barre de recherche de la boite à outil, ou dans les algos récemment utilisés (pratique, c'est tout en haut, faut juste l'avoir récemment utilisé).

![processing_simulate.png](/computation/processing_simulate.png =250x)
 
Voilà à quoi ça ressemble...

 ![processing_simulate_ui.png](/computation/processing_simulate_ui.png =600x)
 
et là vous en voyant que vous pouvez choisir un autre projet que le projet courant, et une deuxième fois vous en voyant qu'une fois le calcul lancé vous pouvez faire autre chose dans QGIS que regarder le calcul défiler... 
 
Vous le faisiez déjà, me direz-vous, en lançant un autre QGIS, mais QGIS est assez gourmand en mémoire, donc c'est mieux comme ça non ? #sobrietenumerique.
 
Et pour le même prix: la boîte à outil vous offre la possibilité de lancer plusieurs calculs (le bouton en bas de la fenêtre "run as batch" ou un truc du genre en français). 

 ![processing_simulate_ui_batch.png](/computation/processing_simulate_ui_batch.png =600x)

> Plus d'astuces ? Consultez la rubrique [faq](/fr/faq)
{.is-info}


## Documentation technique

Les documentations techniques détaillés présentant les équations mises en oeuvre dans les différents modules sont disponibles au format pdf sur les liens ci-dessous.

[Manuel d'analyse hydraulique](/fr/https://hydra-software.net/docs/manuels/analyse_hydraulique.pdf) 
[Manuel d'analyse hydrrologique](https://hydra-software.net/docs/manuels/analyse_hydrologie.pdf)
[Manuel d'analyse pollution et qualité](https://hydra-software.net/docs/manuels/analyse_qualite.pdf)
[Manuel d'analyse transport hydro-sédimentaire](https://hydra-software.net/docs/manuels/analyse_transport_sedimentaire.pdf)

## [Accès rapide éléments de modélisation](/fr/model_building/model_elements)


## Compatibilité versions QGIS![0_qgis_icone.jpg](/0_qgis_icone.jpg =18x) et hydra ![0_hydra_icone.png](/0_hydra_icone.png =15x)

Les évolutions des package QGIS pour windows et notamment des librairies Python associées nécessitent de faire évoluer en conséquence hydra. Les versions d'hydra sont donc pas compatibles avec toutes les versions de QGIS.
 
Hydra 1.21 à 1.71 | Qgis 2.16
hydra 1.71 à 2.6.0 | Qgis 2.18
 
hydra 3.0.0 à 3.6.0 |  Qgis 3.16 à Qgis 3.18
hydra 3.7.0 à hydra 3.10.2 |  Qgis 3.22 -  Qgis 3.28
hydra 3.10.2 et supérieures | Qgis 3.30 et supérieures

N'hésitez pas à nous faire remonter d'éventuelles incompatibilités que nous n'aurions pas identifiées.

