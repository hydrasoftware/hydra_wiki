---
title: Outil chroniques (Time Series)
description: 
published: true
date: 2021-05-11T08:56:03.831Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:45.869Z
---


# Définition

L’outil Chroniques automatise la simulation en continu d’un réseau sur une longue période de temps ainsi que la production d’un bilan de fonctionnement sur cette période en un nombre limité de points : apports, déversements, traitement aux usines etc.
L’exploitation de cet outil se décline en trois étapes :

-    paramétrage d’une série chronologique,
-    exécution des calculs,
-    exploitation des résultats.

Les règles d’utilisation sont illustrées ci-après sur la base de l’exemple ci-dessous :

<img src="/chroniques/ts_illustration.png" width=900/>


Dans cet exemple on souhaite calculer un bilan de fonctionnement sur 8 jours, du 01/01/2018 au 08/01/201, aux trois stations cerclées en couleur rouge : 

-    Un hydrogramme d’apport,
-    Sortie de step,
-    Rejet EP.

Les précipitations sont mesurées par le pluviographe cerclé en couleur verte et sont définies par le fichier externe suivant (voir [Hydrologie](/hydrology_data_management/#pluviometrie))

<img src="/chroniques/ts_fich_plu.png" width=200/>


# Menu Etape 1 : paramétrage de la chronique

## Menu général

Il faut activer le menu «Scenarios/Time Series» : 

<img src="/chroniques/ts_menu.png" width=250/>


La fenêtre «Serie Manager» s’affiche :

<img src="/chroniques/ts_serie_manager.png" width=800/>


La fenêtre `Time Series` permet d’ajouter/supprimer une nouvelle chronique. Une chronique est décomposée en un ou plusieurs blocs temporels, un bloc définit une unité homogène de paramétrage. On peut par exemple distinguer dans la série la saison été/hiver pour la modulation des apports de temps sec ou des périodes de fonctionnement dégradées du réseau, décrites par des fichiers de contrôle spécifiques.

Les différents menus de la fenêtre `Time Series settings` se rapportent soit aux paramètres de la série, communs à tous les blocs, soit aux paramètres d’un bloc.

Les paramètres se rapportant à un bloc sont grisés si la ligne `Série` est active dans la fenêtre `Time Series`. Seuls les paramètres se rapportant à la série sont modifiables.

De même, les paramètres se rapportant à la série sont grisés si une ligne `Bloc` est active dans la fenêtre `Time Series`. Seuls les paramètres se rapportant au bloc sélectionné sont modifiables.
Dans l’exemple ci-dessus. La chronique *TP* est constituée de deux blocs : *bloc1* et *bloc2* :


<img src="/chroniques/ts_bloc.png" width=200/>


La première opération consiste à ajouter une nouvelle série : *TP* dans notre exemple et à renseigner les champs suivants dans l’onglet `computational settings` de la fenêtre `Chronicle Settings` :
- Le champ `Computation starting date`
- Le champ `Computation duration (days)`
- Les champs `Days per scenario`

Les autres champs non grisés dans cette fenêtre sont définis par défaut, ils peuvent être modifiés au besoin.


<img src="/chroniques/ts_bloc_settings.png" width=400/>


Le programme découpe alors automatiquement chaque cloc en scénarios en fonction de la durée du bloc et du nombre de jours définis pour chaque scénario :      


<img src="/chroniques/ts_bloc_decoup.png" width=300/>


## Autres menus associés à la fenêtre *Time Serie settings*

On retrouve les menus du gestionnaire de scénarios :

### Hydrology settings

L’onglet `Hydrology settings` se rapporte à un bloc. On définit généralement dans cet onglet uniquement le scénario de temps sec. Les données pluviométriques et les hydrogrammes externes se rapportent plutôt à la série chronologique et sont renseignés dans l’onglet computational option. Dans l’exemple traité, les paramètres de cet onglet sont renseignés comme suit pour chaque bloc :

<img src="/chroniques/ts_hydrology_settings.png" width=600/>

### Model ordering

Cet onglet se rapporte à la chronique : le mode de pilotage des réseaux du modèle est le même pour tous les scénarios.

### Regulation

On peut définir un ou plusieurs fichiers de régulation différents pour chaque bloc. Les temps définis dans chaque fichier sont définis relativement à la date de démarrage de chaque scénario.

### Configuration

Les données de configurations se rapportent à la chronique : ils sont communs à tous les blocs.


### Computation options

Les données d’options se rapportent à la chronique : ils sont communs à tous les blocs.
L’option couramment utilisée est le fichier externe de données : 

<img src="/chroniques/ts_comput_options.png" width=400/>

Elle permet en effet de renseigner le fichier de données pluviométriques commun à toute période de la chronique. Dans notre exemple le fichier paramétrage.dat est défini comme suit :

<img src="/chroniques/ts_pini.png" width=500/>

Le contenu du fichier *pluvio.plu* est défini plus haut.

On peut également renseigner des hydrogrammes d’apports externes sur toute la durée de la chronique via le mot-clé *F_HY. La liste des mots-clés reconnus est consultable en ligne via le bouton « ? » ci-dessus.

L’option `Ouput option` est également très couramment renseignée pour l’application *Chronicles*. Elle permet en effet de définir les stations pour lesquelles un bilan de fonctionnement est souhaité. Dans l’exemple ci-dessus trois stations sont renseignées comme suit :

<img src="/chroniques/ts_output_option.png" width=500/>


Il est important de cocher l’option `2 files - averaged` pour activer le calcul de bilan. La fenêtre de temps ci-dessus précise l’intervalle de temps sur lequel chaque bilan doit être calculé en heures et minutes : *00000 :00*. On souhaite ici partitionner le bilan par tranche de 12 heures.

### Transport

Les données de `Transport` se rapportent à la chronique : elles sont communes à tous les blocs.

Lorsque cet onglet est renseigné, les paramètres de transport (flux et concentration) sont automatiquement intégrés dans les bilans calculés.

# Etape 2 : exécution des calculs

Il suffit de taper sur le bouton `Run` en bas à gauche de l’écran *Série manager* :

<img src="/chroniques/ts_run.png" width=150/>

Ne pas oublier d’activer auparavant la série « TP ».
La fenêtre suivante apparait :

<img src="/chroniques/ts_run_ts.png" width=400/>

Pour lancer les calculs il suffit d’appuyer sur le bouton « Run chronicle ». Le programme va alors exécuter tous les scénarios en séquence. Le premier scénario de la série est exécuté en démarrage à froid. Les autres scénarios sont enchaînés avec pour chacun un démarrage à chaud : les résultats en fin de simulation du scénario précédent sont lus dans le scénario actif en début de la simulation. Les calculs sont donc réalisés en mode continu sur toute la période considérée. Tout se passe comme ci une seule simulation était effectuée.

En pratique le nombre de jours affectés à un scénario ne doit pas dépasser 7 afin de limiter la taille des fichiers de sorties calculés par chaque scénario.

Au cours de la simulation le programme déroule la progression des calculs comme suit :

<img src="/chroniques/ts_execute.png" width=800/>

La colonne « Status » renseigne les scénarios calculés. En cas d’anomalie de calcul sur un scénario l’exécution s’arrête et redonne la main à l’IHM.

# Exploitation des résultats

## Bilans

Deux fichiers bilans sont générés dans le répertoire racine du projet : 

<img src="/chroniques/ts_resultats.png" width=700/>

Note : les débits des hydrogrammes sont comptés négativement car ils entrent dans le réseau. Les débits sortants sont comptés positivement.

Le fichier `deb` fournit des bilans de volumes et de charge. Le fichiers `lim` fournit des côtes et des concentrations moyennées sur le pas de temps de sortie du bilan.

## Visu des courbes X(t)

Le bouton cerclé en vert ci-dessous permet de visualiser les courbes produites par chaque scénario de la série :

<img src="/chroniques/ts_visu_tool.png" width=250/>

Les paramètres consultables pour chaque objet sont exactement les même que pour les scénarios simples, mais il faut à chaque fois préciser le scénario pour lequel on souhaite tracer une courbe.

<img src="/chroniques/ts_graph.png" width=600/>












