---
title: Procédure d'installation et de mise à jour - Version 3
description: 
published: true
date: 2024-11-05T10:41:14.988Z
tags: 
editor: markdown
dateCreated: 2021-10-20T10:16:42.189Z
---

# Première installation

## En un clin d'oeil
:earth_africa: Installer QGIS3 (minimum version 3.22.10)

:globe_with_meridians: Faire une demande de licence sur https://licence.hydra-software.net, le téléchargement du set-up se déclenche automatiquement
    
:point_up_2:Lancer le set-up téléchargé lors de la demande de licence (aussi accessible [ici](https://hydra-software.net/telechargement/setup_hydra.exe))
      
:book: Ouvrir QGIS

:gear: Ouvrir le gestionnaire d'extensions et charger **Hydra** *(le set-up a paramétré le dépôt)*

:fire: **Hydra** est disponible dans le menu principal et vous demande de saisir la clé de licence reçue par courriel.
                  
>C'est prêt!
{.is-success}

> Si le set-up est lancé par un autre utilisateur (e.g. un administrateur), le dépôt `https://hydra-software.net/telechargement/hydra.xml` n'est pas ajouté au gestionnaire d'extension QGIS, il faut donc que l'utilisateur l'ajoute manuellement pour pouvoir installer le plugin hydra.
> Ce dernier doit aussi **vérifier sa configuration dans *hydra\Settings\Connction settings*.** Cette étape permet, lors de la validation, de renseigner le fichier pg_service.conf nécessaire à l'établissement dela connexion à la base de donnée lors de l'ouverture d'hydra.
{.is-warning}


## Demande de licence

Faire une demande de licence et télécharger le plugin en remplisant le [formulaire de demande de licence starter](https://licence.hydra-software.net) ou le [formulaire spécifique pour les membres d'hydratec](https://licence.hydra-software.net/hydratec.html)

Après validation du formulaire, un message donne accès au téléchargement du set-up d'Hydra. Une clé de licence est adressée dans les 48h par mail sur l'adresse renseignée dans le formulaire.

Pour les renouvellements, la procédure est la même, mais il est inutile de re-lancer le set-up.

<br/>

## Modes d'installation : légère ou standard

Les modèles hydra sont des bases de données relationnelles. Pour construire des modèle, il est donc nécessaire de disposer d'un serveur de base de données PostgreSQL/PostGIS. Certaines extensions (postgis, plython3u), modules pythons (ogr) et programmes (gmsh) sont également nécessaires au fonctionnement du serveur. Le set-up téléchargé après la demande de licence permet l'installation de l'ensemble de ces composants.

Pour faciliter la découverte du logiciel, un **serveur de démo** est également mis à disposition des utilisateurs. Ce mode **léger** permet un accès simple et rapide au logiciel en se limitant à l'installation du plugin et à l'activation de la licence; l'exécution du set-up (installation de la base de données et des modules complémentaires) n'est pas nécessaire.

Les procédures d'installation associées aux modes **serveur de démo** et **standard** sont décrites ci-après.

>Le schéma ci-dessous précise les différents modes de connexion du plugin Hydra à la base de données :
>
>![bd_plugin.svg](/installation/bd_plugin.svg)

<br/>

## Installation standard

La procédure d'installation est décrite ci après :
- :earth_africa: Installer QGIS 3 (minimum version 3.6, https://www.qgis.org/fr/site/forusers/download.html) 
- :point_up_2: Lancer le SetUp Hydra téléchargé lors de la demande de licence  (aussi accessible [ici](https://hydra-software.net/telechargement/setup_hydra.exe))
- :book: Démarrer QGIS 3
- :gear: Ouvrir le gestionnaire d'extensions et charger **Hydra** *(le set-up a paramétré le dépôt)*
- :fire: **Hydra** est disponible dans le menu principal

>C'est prêt!
{.is-success}


### Activation de la clé de licence

L'activation de la clé de licence se fait via le menu `Hydra\Licence activation`. Renseigner le numéro de licence reçu et valider.

![licence_activation.jpg](/installation/licence_activation.jpg)

Si pour des raison d'accessibilité du réseau (pare-feu etc.), l'activation via QGIS échoue, une activation manuelle est possible:
- dans QGIS, ouvrir `About Hydra` dans le menu hydra
- noter le `Serial number` affiché en bas de la fenêtre
- aller sur https://licence.hydra-software.net/activation.html et remplir le formulaire
- le fichier obtenu doit être renommé `hydra_licence.key` et mis dans le répertoire .hydra de l'utilisateur.


### Configuration

Les paramètres de connexion au **serveur local** (`Hydra`) sont récupérés automatiquement lors de l'installation.

Pour utiliser le **serveur de démo**s, dans le menu hydra ![menu_hydra.png](/tutorials/menu_hydra.png) choisir `Settings` puis `Connection settings`, et sélectionner le service `hydra_remote`. Remplir le formulaire avec les informations suivantes:

* `user`: l'adresse courriel que vous avez saisi dans le [formulaire de demande de licence starter](https://licence.hydra-software.net)
* `password`: la clef reçu par courriel en réponse à cette demande
* `host`: database.hydra-software.net
* `port`: 5444

Valider en cliquant `OK`.

>Si il existe déja une installation d'hydra utilisant un service de base de données plus ancien (version 9.6), le plugin va écrire les paramètres de connexion à cet ancien service dans le fichier de service PostgreSQL (service nommé `hydra_legacy`) afin de permettre de se reconnecter au besoin à cet ancien service pour en exporter certains projets et les réimporter ensuite dans le nouveau serveur de bases de données. 

<br/>

### Installation légère (mode serveur de démo)

#### Installation

- :earth_africa: Installer QGIS 3 (https://www.qgis.org/fr/site/forusers/download.html) 
- :book: Démarrer QGIS 3
- :pencil2: renseigner le dépôt hydra https://hydra-software.net/telechargement/hydra.xml dans le gestionnaire d'extensions :
    - Dans le menu `Extensions` choisir `Installer/Gérer les extensions`
    - Choisir l'onglet `Paramètres`
    - Renseigner le nom et l'adresse du dpôt (https://hydra-software.net/telechargement/hydra.xml)
    - Valider
![param_depot_hydra.jpg](/installation/param_depot_hydra.jpg)
- :fire: Dans le menu `Toutes`sélectionner **Hydra** et installer le plugin

> Le menu hydra ![menu_hydra.png](/tutorials/menu_hydra.png) se trouve désormais dans la barre de menus de QGIS.
{.is-success}

#### Configuration

Pour utiliser le serveur de démo, dans le menu hydra ![menu_hydra.png](/tutorials/menu_hydra.png) choisir `Settings` puis `Connection settings`, et sélectionner le service `hydra_remote`. Remplir le formulaire avec les informations suivantes:

* `user`: l'adresse courriel que vous avez saisie dans le [formulaire de demande de licence starter](https://licence.hydra-software.net)
* `password`: la clef reçu par courriel en réponse à cette demande
* `host`: database.hydra-software.net
* `port`: 5444

Valider en cliquant `OK`.

>Un modèle créé sur le serveur peut être à tout moment récupéré (`Project Manager` dans le menu hydra ![menu_hydra.png](/tutorials/menu_hydra.png)) et rechargé dans une base PostgreSQL locale installée en suivant les instructions d'installation ci-après.



<a name="update_plugin"></a>

# Mise à jour du plugin

## Procédure de mise à jour

Si le dépôt du plugin hydra a été paramétré et est accessible (cf. Première installation d'hydra), la mise à jour du plugin est réalisée via le menu extensions > installer/gérer les extensions > Tout mettre à jour

<img src="/installation/instal_hydra2.png" width="500px" align="center"/>


## Compatibilité projet / version plugin

Lors du travail avec le plugin HYDRA, la gestion des versions est effectuée à deux niveaux : on distingue la version du **plugin HYDRA**, qui trace la version du code de l’interface, et le **numéro de version du projet**, qui trace le format dans lequel se trouve la base de données liée au projet.

### Versions du plugin

Le numéro de version du plugin est accessible via le gestionnaire d’extension de QGIS. 

Que le plugin soit installé à partir d’un dépôt en ligne ou via une archive .zip, le **numéro de version** est écrit **en tête de la page de présentation du plugin** :

<img src="/installation/version_hydra.png" width="500px" align="center"/>

A chaque itération sur le code du plugin HYDRA, ce numéro de version est incrémenté de façon à refléter l’importance des évolutions apportées.

A chaque nouvelle version, un *changelog* résume également les principaux changements : nouvelles fonctionnalités et corrections de bugs.

### Versions du projet

Une mention est également présente dans le *changelog* en cas d’évolution de la structure du modèle de données (la base PostgreSQL) : *"Database version increased to … "*.

Ainsi, chaque version du plugin est conçue pour fonctionner avec des projets d’une structure précise. 

Le **numéro de version de la structure de données** (des projets) est mentionné en tête du **Gestionnaire de Projets** (Menu *Manage projects*) :


