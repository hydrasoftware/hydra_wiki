---
title: Execution d'un calcul de simulation
description: 
published: true
date: 2024-10-22T21:31:09.448Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:59.210Z
---


# Lancement d'un scénario de calcul

Un scénario de calcul est lançé via le bouton `run` de la barre d'outils après avoir sélectionné le scénario actif via le menu déroulant :

<img src="/computation/run_scenario.png" width=250/>

# Groupage de scénarios

Les scénarios peuvent être également exécutés par groupe : le lancement d'un groupe   active l'exécution séquentielle de tous les scénarios du groupe sans que l'utilisateur n'ait besoin d'intervenir.

Pour paramétrer un groupe activer le menu `scenarios/Grouping`. La fenêtrre suivante s'affiche :

<img src="/computation/grouping_vide.png" width=350/>

Les  différents boutons de la fenêtre permettent  de créer un groupe et sélectionner les scénarios appartenant au groupe :

<img src="/computation/grouping_renseigne.png" width=350/>

Pour initier un calcul il faut sélectionner le groupe et appuyer sur le bouton `Run`. La fenêtre suivante apparait :

<img src="/computation/grouping_run.png" width=550/>

Taper sur le bouton **Run_all** pour lancer la simulation. Le programme exécute les calculs en séquence et affiche le statut d'exécution pour chaque scénario. L'utilisateur peut ainsi suivre l'état de progression des calculs.

> :bulb: Vous pouvez également lancer les calculs via l'outil [simulate](/fr/faq#processing_simulate) de la boîte à outils de traitement hydra. Cet outil permet également de lancer les calculs en groupe, tout en pouvant continuer à travailler dans l'interface QGIS.
![processing_simulate.png](/computation/processing_simulate.png =250x)
{.is-info}


# Etapes de calcul

Les opérations suivantes sont réalisées en séquence :

1. Création du sous répertoire xxx dans le répertoire du projet, xxx désignant le nom du scénario ( 24 caractères au maximum).

2. Création de trois sous répertoires :  /travail ,  /hydrol et / hydraulique  sous le répertoire xxx.

3. Extraction de la base du projet de toutes les données nécessaires à l’exécution des calculs. Ces données sont converties en fichiers et stockées dans le répertoire « travail », à l’exception du fichier *scenario.nom* qui est copié dans le répertoire du scénario. Ce fichier contient le nom des modèles à exécuter ainsi que les modes de connexion entre modèles. 

4. Exécution de  l’étape de calcul hydrologique à l’aide du programme exécutable **hydrol.exe**. Les fichiers produits sont stockés dans le répertoire /hydrol. La simulation s’arrête là si le mode de calcul sélectionné est `hydrology`.

5. Exécution du programme utilitaire **hydragen.exe**. Cet utilitaire met en forme les fichiers générés et les complètent pour assurer une compatibilité parfaite avec les fichiers traités par le programme de calcul hydraulique. C’est notamment dans cet étape que sont ajoutés tous les nœuds de calcul intermédiaires le long des branches filaires, ainsi que les éléments de modélisation visant à assurer les continuités des écoulements au droit des ruptures géométriques. Les fichiers générés par cet utilitaire sont copiés dans le répertoire /hydraulique.

6. Exécution du programme de calcul **whydram24_dos.exe**. les fichiers de résultats produits par cet exe sont stockés dans le répertoire /hydraulique.

> **Note**
>
>L’exécution des calculs ne pourra démarrer que si toutes les invalidités de données détectées par l’IHM ont été résolues. 
>
>Autrement dit aucun symbole <img src="/computation/erreur.png"/> ne doit subsister dans le plan de travail du modèle pour permettre le démarrage des calculs.
>


# Répertorisation

L’arbre suivant récapitule l’organisation des fichiers créés dans l’étape d’exécution des calculs :

<img src="/computation/repertorisation.png" width="350px"/>

# Contrôle du bon déroulement des opérations

Deux fichiers sont générés à chaque étape de calcul :

-    le fichier d’extension .err  retourne la valeur **0** si le traitement s’est déroulé normalement et si aucune anomalie bloquante n’a été détectée. Il retourne la valeur **1** dans le cas contraire.
-    le fichier d’extension .log contient la nature du message d’erreur détecté par le programme 

L'écran suivant s'affiche durant la simulation :

<img src="/computation/suivi_simulation.png" width="500px"/>

La fenêtre de gauche trace le suivi d'enchaînement des calculs, ainsi le statut d'exécution des différents modules :
- si un module est exécuté correctement le mot clé **OK** est afficihe en fin d'exécution
- dans le cas contraire l'exécution s'arrête et un message d'erreur est écrit au bas de la fenêtre : c'est le message écrit dans le ficiher Log du module de calcul.

La fenêtre de droite permet de contrôler les calculs  hydrauliques en cours ; une ligne est écrite à chaque pas de temps de simulation, elle affiche les paramètres de contrôle suivants :
- th  : temps relatif en heures,
- dth : pas de temps de laimulation
- dz  : variation maximum de cote entre deux pas de temps, tous noeuds de calculs confondus.
- model, branche, noeud : identificateur du noeud correspondant à dz, nom du modèle et de la branche contenant ce noeud.

<img src="/computation/computation.png" width="400px"/>

Le pas de temps th est variable, il est  constamment adapté par le programme pour maintenir dz à tous les noeuds entre 5cm et 10 cm. Ce pas de temps ne dépasse jamais le pas de temps maximum spécifié dans la fenêtre de paramétrage *Computation* du gestionnaire de scénario.

En cas (peu fréquent) de plantage des calculs déclenchant une exception du compilateur un message est affiché dans cette fenêtre avec le nom du module source et de la ligne de calcul concernés. Cette information doit être relayée par l'utilisateur à l'équipe de développement  pour lui permettre d'identifier la cause du plantage et y remédier.


# Suivi des calculs hydrauliques

Les calculs hydrauliques sont gérés par des algorithmes de résolution numérique des équations et nécessitent des moyens de suivi et de contrôle spécifiques. Les précisions données ci-après ne sont pas nécessaires à la bonne exécution des calculs mais elles facilitent la compréhension de la séquence de déroulement des calculs, ainsi que la recherche des solutions en cas d’anomalie de calcul.

## Génération des conditions initiales

En mode de calcul avec **démarrage à froid**, le calcul démarre avec toutes les liaisons fermées. Les hauteurs d’eau affectées à chaque nœud sont par défaut nulles. 

En pratique une hauteur minimum de 20cm a été introduite le long des branches filaires pour accélérer l’établissement du régime permanent dans les temps négatifs. Les cotes d’eau initiales sont également recalculées pour tenir compte des effets de rehaussement engendrés par des singularités telles que des déversoirs le long d’une branche. Aucun débit fictif n’est introduit. 

Les débits d’apports physiques sont ensuite progressivement injectés dans le modèle pour mettre en eau le modèle. Si une branche n’est alimentée par aucun débit le niveau d’eau initial va rapidement décroitre et la hauteur d’eau sera nulle partout.

Cette méthode d’établissement des conditions de régime établi explique la raison pour laquelle le régime transitoire doit démarrer à un temps négatif, afin d’éviter de polluer les résultats au temps positifs tant qu’on n’a pas atteint un régime établi.


## Suivi du déroulement des calculs à l’écran

Le suivi des calculs de simulation hydraulique est contrôlé par des messages écrits dans la fenêtre décrite plus haut.


## Le fichier .out

Un certain nombre d’informations sont stockées dans le fichier **hydraulique/xxx.out**. Ce fichier contient des messages d’avertissements (warnings) sur des anomalies résiduelles non bloquantes détectées sur le jeu de données et sur la façon dont elles ont été résolues. Il contient également :

-    La liste des nœuds de calculs et des singularités rangées par branche, ainsi que les cotes initiales affectées au nœud de calcul avant le démarrage du calcul transitoire,
-    la copie des informations de contrôle affichées sur la fenêtre DOS , plus des informations complémentaires sur l’évolution des pas de temps.

La consultation de ce fichier après calcul permet d’identifier certaines anomalies traduites en particulier par des pas de temps anormalement faibles et imputables à des nœuds que l’on peut parfaitement identifier en inspectant ce fichier. 

A noter que les numéros de nœuds de calcul principaux utilisés par le programme **whydram_dos** sont très exactement les id des noeux consultables dans les éditeurs généraux des couches de type *node*. On peut donc très facilement accéder dans l’IHM à l’endroit du modèle qui présente une difficulté de calcul afin d’identifier la nature du problème et faciliter sa résolution.

> :bulb: utilisez l'outil  [slowdown](/fr/faq#slowdown) de la boite à outil hydra pour identifier rapidement les points de ralentissement des calculs.
{.is-info}


## Suivi graphique des calculs

Le programme de simulation hydraulique dispose d’un module d’imagerie dynamique de suivi des calculs pendant leur exécution. 

Ce module est activé avec l’option *graphic control during computation* dans le gestionnaire de scénarios. Il permet de visualiser à l’écran l’évolution des lignes d’eau et des débits le long des branches filaires et donc de contrôler graphiquement le bon déroulement des calculs.

<img src="/computation/visu.png" width="800px"/>

Cet utilitaire comporte un menu permettant de sélectionner les branches, de régler les échelles, d’ajuster les intervalles de rafraichissement entre autres. 

On peut à tout moment interrompre le calcul et accéder au menu en tapant sur la touche Echap n’importe où sur le graphe.

Pour sélectionner les branches, il faut cliquer sur le menu branche ; la fenêtre se saisie suivante apparait :

<img src="/computation/select_branche.png" width="400px"/>


Il faut sélectionner le modèle, puis la liste des branches à afficher, séparées entre elles par un caractère blanc. Une branche est définie par son nom précédé du signe « * ».

Cet utilitaire se révèle très utile en phase de mise au point du modèle. A noter que les temps de  calculs sont plus longs dans ce mode qu’en mode DOS en raison des temps d’affichage graphiques.


> **warning**
>
>  pour accéder au menu lorsque le calcul est en cours, ne pas utiliser la souris, mas taper sur la touche **Echap**, faute de quoi le programme plantera. La souris peut être utilisée pour naviguer dans le menu après suspension du réroulement des calculs et pour réactiver les calculs.
> 

# Fichiers résultats

Les fichiers résultats sont regroupés par modèle, même dans le cas d’un calcul multi-modèles en mode global. 

Le tableau suivant récapitule les fichiers résultats produits par défaut pour chaque modèle simulé :

<img src="/computation/tableau.png" width="470px"/>

Tous ces fichiers sont exploités par les utilitaires d’exploitation des résultats, qu’ils soient intégrés à l’IHM ou externes. Plus spécifiquement :

- Les fichiers .csv sont exploités par l’utilitaire d’analyse thématique. Ils sont également consultables pour des traitements spécifiques sous Excel.
- Les fichiers binaires .W13, .W14 et .W15 sont lus par les utilitaires de visualisation des courbes x(t), le fichier .W15 est exploité en plus par les utilitaires de traitement cartographique.
- Les fichiers .W16 servent à tracer des profils en long détaillés le long des branches filaires en prenant en compte l’ensemble des points de calcul et pas seulement les nœuds utilisateurs définis dans l’IHM.

