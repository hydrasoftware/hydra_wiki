---
title: Gestion des données hydrologiques et hydro climatiques
description: 
published: true
date: 2024-02-28T12:12:41.915Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:50.004Z
---


# Injections de débit

Les  apports de débit  sont définis  directement dans les objets <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point), posés sur un nœud de calcul (manhole, river node, elem 2D node, storage node, station node).

Ces hydrogrammes peuvent être constitués de la superposition de trois sources distinctes :

- Les apports ponctuels définis explicitement par un **hydrogramme** associé à l'objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) :
    - dans le tableau associé à l'objet `(External file data = No)`; cet hydrogramme est alors identique pour l'ensemble des scénarios de calcul,
    - dans un fichier externe déclaré dans le gestionnaire de scénario `(External file data = Yes)`; cette possibilité permet de modifier les hydrogrammes d'apport pour chacun des scénarios de calcul.
  
<img src="/hydrology_data_management/hydrograph_ui.png" width=500px/>
  
- Le **ruissellement sur les bassins versants**  généré par les précipitations. L'objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) peut être alimenté par un hydrogramme issu d'un [réseau hydrologique](/model_building/hydrology). Ce réseau hydrologique est lui même alimenté par des données pluviométriques (cf. § [Pluviometrie](#pluviometrie) appelées dans le gestionnaire de scénarios de calcul.

- Les **apports de temps sec** résultant de l’activité anthropique ou d’infiltration d’eau claire en provenance des nappes (cf. § [Temps sec](#temps_sec)).


On dispose ainsi d’une grande flexibilité pour définir des hydrogrammes injectés dans le réseau hydraulique, illustrée par l’exemple suivant :

<img src="/hydrology_data_management/_hydrologic_input_ui.png" width=900px/>

- L’hydrogramme **HY1** injecté dans le réseau hydraulique est formé par la superposition des hydrogramme ruisselés  en provenance des 2 bassins versants et d’un hydrogramme de temps sec généré à partir des données du secteurs de temps sec.

- L’hydrogramme **HY2** est formé par la superposition de l’hydrogramme défini dans l’élément et  d’un hydrogramme de temps sec généré à partir des données du secteurs de temps sec.

- L’hydrogramme **HY3** est défini par un hydrogramme externe appelé dans le gestionnaire de scénario.


> **note**
>
>  L'objet `Hydrograph`permet également de **calculer les flux polluants** associés aux points d'apport.
> 
> 

<a name="temps_sec"></a>

# Apports de temps sec

## Principe

Les apports de temps sec sont définis par secteurs, <img src="/hydrology_data_management/dry_inflow_sector_polygon.png"/> *Dry inflow sector* (matérialisés par des polygones), puis répartis sur les <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/constant_inflow_bc_point) associés à chacun d'entre eux.


## Le secteur d’apport

Le secteur d’apport est délimité par un polygone à l’aide du bouton <img src="/hydrology_data_management/dry_inflow_sector_polygon.png"/> *Dry inflow sector*.C’est une entité définie au niveau « projet ».

Les données du secteur sont accessibles via le menu principal <img src="/0_hydra_icone.png" height="25px"/> *Hydrology*

<img src="/hydrology_data_management/_dry_inflow_sector_ui.png" width=900px/>


Les apports de temps sec peuvent être constants ou variables sur une journée, via une **courbe de modulation** (modulation curve). La courbe de modulation définit la part du volume de temps sec journalier apporté à chaque pas de temps; elle commence à (0;0) et finit à (24;1). 

L’application liste automatiquement tous les objets <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/constant_inflow_bc_point) trouvés à l’intérieur du contour du secteur. Pour chaque objet *hydrograph*  associé à un secteur, sont renseignés :

- `Contribution`: 
- `Distribution coefficient` : ratio de volume total de temps sec du secteur affecté à l’élément,
- `Constant flow` : débit constant,
- `Lag-time` : temps de transfert entre l’hydrogramme injecté dans l’élément et l’hydrogramme global défini à l’exutoire aval du secteur . En général lag-time =0 sauf pour les réseaux de grandes étendues.




## Les scénarios d’apport de temps sec    

Un scénario d’apport de temps sec est défini via le menu principal <img src="/0_hydra_icone.png" height="25px"/> *Hydrology*

<img src="/hydrology_data_management/_dry_inflow_scenario_ui.png" width=900px/>


Un scénario définit pour chaque secteur d'apport de temps sec les paramètres suivants :

- Le volume Veu (VS m3/d) de référence des apports d’eau usées strictes en m3/j,
- Un coefficient d’ajustement des apports EU,
- Le volume Vecpp (Vcw m3/d) de référence d’eaux claires en m3/j,
- Un coefficient d’ajustement des apports EU.


## Calcul des apports de temps sec    

Les scénarios sont appelés dans le paramétrage des scénarios de calcul.

L’hydrogramme d’apport de temps sec en chaque point d’injection est construit à partir des courbes volumétriques adimensionnelles comme suit (en m3/s):

$$
Q(t)=V_{eu}*\frac{df(t)}{dt}+V_{Ecpp}*\frac{1}{86400}
$$

*Avec f(t) la courbe de modulation journalière des apports de temps sec*

<a name="pluviometrie"></a>

# Pluviométrie

Hydra propose  un choix de 4 familles de pluies. Le mode de calcul de chaque pluie et les conseils d’utilisation sont  détaillés dans le manuel d’analyse consacré à l’hydrologie.

- Des pluies réparties de façon homogène sur l'ensemble des bassins versants :
  - Les pluies de projet synthétiques (simple triangle, double triangle et Caquot), basées sur une bibliothèque de coefficients de Montana,
  - Les pluies définies par des hyétogrammes,
- Des pluies spatialisées :
  - Les pluies définies aux pluviographes,
  - Les pluies radar (2 formats proposés, un standard et un spécifique à l'application propriétaire MAGES).
  
Les données pluviométriques sont définies via le gestionnaire de pluies, accessible depuis le menu principal <img src="/0_hydra_icone.png" height="25px"/> *Hydrology*, puis appelées dans le gestionnaire de scénarios de calcul.


<img src="/hydrology_data_management/rain_ui.png" width=700px/>

> Les données de pluie peuvent également être définies dans des fichiers externe (ASCII) appelés via le gestionnaire de scénarios.


![rainfall_settings_synthesis.svg](/hydrology_data_management/rainfall_settings_synthesis.svg)

>:warning: **Pour les pluies définies sous forme de hyétogrammes (en intensités ou en cumuls), il est important de définir un pas de temps de calcul hydrologique (dans le paramétrage du scénario de clacul) au moins égal au pas de discrétisation des pluies. Dans le cas contraire, la pluie sera ré-échantillonnée, et les pointes d'intensités moyennées.**

## Pluies de projet synthétiques

Les pluies de projet sont construites à partir des **coefficients de Montana** permettant de reconstituer des courbes intensité - durée - fréquence. Ces coefficients sont définis dans une bibliothèque commune à toutes les pluies de projet.

<img src="/hydrology_data_management/synthetic_rainfall_ui.png" width=700px/>

Trois types de pluie de projet peuvent être définies à partir de cette bibliothèque de coefficients de Montana.

### Pluie simple triangle

Cette pluie, uniforme sur l’ensemble de la zone d’étude, est définie par un hyétogramme en forme de triangle simple, construit à partir des paramètres suivants fournis par l’utilisateur :

- Les coefficients de Montana associés à l’épisode pluvieux, sélectionnés dans la bibliothèque,
- `Total duration`: durée de la pluie (D en min),
- `Peak time`: instant du pic de pluie (tP en min).

<img src="/hydrology_data_management/simple_triangular_rain_ui.png" width=700px/>


> **note**
>
>  le graphique apparaît lorsque la pluie concernée est sélectionnée, après avoir renseigné ses paramètres
> 

### Pluie double triangle

Cette pluie, uniforme sur l’ensemble de la zone d’étude, est définie par un hyétogramme en forme de double triangle, construit à partir des paramètres suivants fournis par l’utilisateur :

- `Total event duration` : durée totale de l’épisode pluvieux (D1 en min),
- `Total event Montana coefficient` : coefficients de Montana associés à l’épisode pluvieux, sélectionnés dans la bibliothèque,
- `Peak duration` : durée de l’épisode pluvieux intense (D2 en min),
- `Peak Montana coefficient` : coefficients de Montana associés à l’épisode pluvieux intense, sélectionnés dans la bibliothèque,
- `Peak time`: instant du pic de pluie (tP en min).

Il faut respecter la double inégalité :

- D2 $\leq$ 2 heures
- D1 $\geq$ 2 heures (en général D1 peut être fixé à 4 heures)

<img src="/hydrology_data_management/double_triangular_rain_ui.png" width=700px/>


> **note**
>
>  le graphique apparaît lorsque la pluie concernée est sélectionnée, après avoir renseigné ses paramètres
> 

### Pluie de caquot

Cette pluie, qui n’a rien de physique, est caractérisée par un hyétogramme de forme doublement triangulaire de durée différente pour chaque bassin versant élémentaire. Cette durée est ajustée pour satisfaire le débit de pointe donné par la méthode Caquot à l’exutoire de chaque bassin versant élémentaire : la durée de la pluie est égale au temps de concentration du bassin versant.
Cette méthode a l’avantage sur la méthode classique de Caquot, de fournir des hydrogrammes et des débits de pointe, et donc de permettre de dimensionner des réseaux étendus et de nature très hétérogène.

<img src="/hydrology_data_management/caquot_rain_ui.png" width=700px/>



## Pluies définies par des hyétogrammes synthétiques 

La pluie est définie par un tableau temps (min) / intensité (mm/h). Elle est répartie de façon uniforme sur l'ensemble des sous bassins versants du modèle.

<img src="/hydrology_data_management/hyetograph_ui.png" width=700px/>



> **note**
>
>  les données peuvent être copiées / collées depuis et vers un tableur.     
> 
>  

<a name="gage_rainfall"></a>

## Pluies définies à partir de données pluviographiques 

Les pluies sont associées à des objets pluviographes <img src="/hydrology_data_management/gage_rainfall_button.png"/> qui doivent être préalablement créés dans le projet.

Les données à définir pour chaque pluviographe consistent une courbe de hauteur cumulée en mm en fonction du temps en minutes.
Les lames d’eau  moyennes de chaque bassin versant (`Catchment`) sont calculées par interpolation des données des pluviographes, selon deux options :
- Pluviographe le plus proche (`shortest_distance`)
- Méthode ce pondération par les distances (`distance_ponderation`).
- Méthode Thiessen (`thiessen`): permet de calculer une lame d’eau attachée à un *catchment* par interpolation selon l’algorithme de Thiessen. Les coefficients de Thiessen sont calculés par le programme via le bouton *Create Thiessen coefficient files*. Les fichiers générés sont exploités automatiquement par le programme lorsque le mode d’interpolation pat le méthode Thiessen est sélectionné.

> Les données peuvent être **saisies directement dans l'interface**, ou définies dans des **fichiers externes** (ASCII); le format des fichiers est spécifié dans la note technique NT53 disponible via le menu *hydra\documentation*.


<img src="/hydrology_data_management/gaged_rain_ui.png" width=900px/>

### Dans le cas où les **données sont renseignées dans l'interface**:
- **créer une pluie** dans le tableau de gauche `Gage rainfall list` avec le bouton +
- renommer la pluie si besoin
- définir le **mode d'interpolation**
- **sélectionner** la ligne de la pluie à définir dans ce tableau
- **paramétrer la pluie** dans l'onglet `Gage rainfall data` :
    - **cocher la case « data »** de chaque pluviographe pour lequel des données sont disponibles (si la case n'est pas cochée, les données ne seront pas prises en compte),
    - **renseigner le hyétogramme**. :bulb:  les données peuvent être copiées / collées depuis et vers un tableur.


>:warning: **Si les premiers et derniers points de la courbe sont respectivement postérieur et antérieur aux temps de début et de fin de calcul, les valeurs manquantes sur ces intervalles sont nulles.**

### Dans le cas où les données sont **renseignées dans des fichiers externes**:
- **créer une pluie** dans le tableau de droite `Gage rainfall list` avec le bouton +; l'explorateur de fichiers permet de sélectionner le fichier à appeler
- renommer la pluie si besoin
- définir le **mode d'interpolation**

<a name="radar_rainfall"></a>

## Pluies radar 

L’intensité de pluie affectée à chaque bassin versant j hbv(j) est déduite de celle des mailles radar H(i) par pondération à la surface élémentaire issue de l’intersection entre les contours de bassins versants <img src="/model_building/hydrology/catchment_contour_polygon.png"/> `Catchment contour` et les mailles radar Sbv(ij).

<img src="/hydrology_data_management/radar_rain_scheme.png" width=650px/>


### Format standard

Hydra exploite des données de pluies radar définies par **un fichier raster au format `TIF` ou `asc` par pas de temps**:

- Les fichiers doivent être dans le même système de projection que le projet,
- Le pas de temps de chaque fichier doit pouvoir être lu dans son nom,
- Chaque pixel comporte le cumul de pluie sur ce pas de temps en mm.

___

Sélectionner les fichiers source (format TIF ou asc) via le bouton <img src="/hydrology_data_management/radar_vrt_multi_layer_button.png"/>, puis préciser le **template** permettant d'identifier le pas de temps associé à chaque fichier.


Dans l'exemple ci-dessous, les fichiers source sont nommés `LAME_EAU.NIZI.YYYYMMDDHHMM`, avec
- YYYY année
- MM mois
- DD jour
- HH heure
- MM minutes

<img src="/hydrology_data_management/radar_template.png" width=400px/>

La pluie est enregistrée, et ses caractéristiques précisées dans l'interface.

<img src="/hydrology_data_management/radar_ui.png" width=700px/>


Hydra crée dans le répertoire **Working directory/Project/Rain** :

- un **raster virtuel multibandes nom_pluie.vrt** à partir des fichiers source (une bande par pas de temps); il recopie également les fichiers source appelés par le raster virtuel dans ce répertoire,
- un **fichier ASCII nom_pluie.time** précisant la correspondance entre les pas de temps de calcul et les dates associées à chaque fichier.



> **note**
>
> Le bouton <img src="/0_hydra_-.png" height="25px"/> permet de supprimer la pluie sélectionnée de la bibliothèque.
> 
> Le bouton <img src="/0_hydra_+.png" height="25px"/> permet d'ajouter une pluie à la bibliothèque, à partir d'un fichier vrt multibandes (et d'un fichier .time) préalablement généré par hydra. Cette opération peut être nécessaire si une pluie a déjà été mise en forme dans un autre projet.
> 

____

Le bouton <img src="/hydrology_data_management/radar_hyetograph_button.png"/> permet d'affecter les cumuls de pluie issus des images radar à chacun des sous bassins versants; les hyétogrammes associés peuvent être visualisés pour chaque contour de bassin versant ( [catchment](/model_building/hydrology/catchment_contour)) via l'éditeur <img src="/general_overview/hydra_edit_button.png" height="25px"/>.



> **warning**
>
>  Cette étape de génération des hyétogrammes par sous bassins versants doit être effectuée avant de lancer les calculs. Après ajout, suppression ou modification d'un sous bassin versant, cette étape doit être relancée. 
> 
> 

### Format MAGES

Les pluies sont définies par les deux blocs suivants :

- une grille
- un fichier ASCII des données pluviométriques donnant au pas de temps de 5 min l’intensité de la pluie en mm/h sur chaque maille de la grille radar où la lame d’eau est non nulle :
  - Chaque ligne définit la hauteur de pluie tombée dans une maille depuis le pas de temps précédent en dixième de millimètres,
  - Une maille est repérée par son numéro d’ordre dans la grille dans la direction horizontale de gauche à droite et dans la direction verticale de haut en bas.
  
La grille radar peut être renseignée dans le menu <img src="/0_hydra_icone.png" height="25px"/> hydrology.




<a name="vent"></a>

# Vent

Les conditions de vent sont définies dans la fenêtre de paramétrage d’un scénario, sous forme de courbes temporelles de vitesse et de direction de vent à chacun des anémomètres définis dans le modèle.  Les caractéristiques du vent sont appliquées en tout point  et à tout instant par interpolation des valeurs définies aux stations anémométriques. 
L'action du vent est appliquée aux mailles d’un domaine 2D et le long d’un domaine filaire.

Dans la version actuelle d'HYDRA les données de vent ne peuvent être définies qu'en mode externe comme suit : 

L'action du vent est activée en déclarant l'option *VENT dans le fichier de paramètrage à déclarer dans le menu *computation option - external file* du gestionnaire de scénario :

<img src="/hydrology_data_management/wind_parameter_file.png" width=500px/>

Le bouton **?** permet d'accéder à la documentation décrivant les données à renseigner sous le mot clé *VENT. Ces données sont définies dans le tableau suivant :


<img src="/hydrology_data_management/wind_data.png" width=600px/>

Les  courbes  temporelles de vent (vitesse et direction) doivent être définies dans le fichier : Fich_data_vent. La structure de ce fichier est de type  HYDRA .

- Densa : densite de l’air  (sélectionner densa =  1.2)
- Cfa : coefficient de frottement de peau de l’eau sur la surface de l’eau ; sélectionner : Cfa = 0.008
- Cidst_vit1, Cidst_vdir1 : identificateurs des courbes de données de vitesse (en m/s) et de direction de vent (en degrés) ;

Attention : la direction du vent est la direction de la provenance du vent ; elle doit être comprise entre 0  et 360 degrés. 

Exemple : 
- 90.0  : vent en provenance de la direction est
- 180.0 : vent en provenance de la direction sud
- 270.0 : vent en provenance de la direction ouest
- 360.0 : vent en provenance de la direction nord

X, Y : coordonnées géographiques de la station


# Compléments

Les informations  des sections précédentes fournissent une base conséquente pour la construction d’un modèle classique. Cependant certaines applications nécessitent de maîtriser des notions additionnelles pour tirer pleinement profit des fonctionnalités proposées par HYDRA.

Ces compléments sont décrits ci-après.

### Paramétrage des données hydrologiques en mode externe

Les données hydrologiques sont définies par saisie manuelle dans des écrans de saisie via le menu hydrology de l’IHM (Interface Homme-Machine), puis stockées dans la base du projet.
HYDRA offre une autre possibilité de définir ces données via des fichiers externes déclarés par le bouton output files de l’onglet computation option du gestionnaire de scenarios :


<img src="/hydrology_data_management/comp_options.png" width=500px/>

La documentation associée est consultable en activant le bouton « ? » ci-dessus. Elle décrit le format des fichiers externes à renseigner au chapitre 6 de cette documentation. Les données concernées sont :

-    Les données pluviométriques,
-    Les données de scénarios d’apports de temps sec,
-    Les données de déroutage,
-    Les hydrogrammes d’apports,
-    Les fichiers de régulation.

Chacune de ces rubriques est déclarée par un mot clé. Lorsque que ce mot clé est détecté, HYDRA ignore les données correspondantes renseignées dans le gestionnaire de scénarios et sélectionne les données définies sous le mot clé du fichier externe.

Cette alternative s’avère très intéressante dans les cas suivants :

-    Pour profiter de la plus grande richesse de format de données offerte par les fichiers externes. Si on considère par exemple les données pluviométriques, les temps peuvent être exprimés en date calendaire, alors que dans l’IHM ils sont exprimés en minutes écoulées depuis la date origine du scénario.
-    Pour éviter de transposer manuellement dans les écrans de saisie de l’IHM des données déjà définies et mises en forme dans des fichiers externes (à noter que la plupart de ces fichiers sont définis au format .csv).
-    Pour les applications temps réel, dans lesquels les calculs sont pilotés par un superviseur dédié, et non par l’IHM d’HYDRA.
-    Pour le pilotage des séries chronologiques : les données pluviométriques sont généralement définies dans un seul fichier couvrant la totalité de la période avec le temps exprimé en date calendaire. L’importation d’un tel fichier dans l’IHM représenterait un travail très lourd dont on s’affranchit avec cette procédure de lecture en mode externe.

### Définiton et paramétrage des lois de production des apports de ruissellement

Les lois disponibles sont renseignées dans l’objet catchment, elles sont décrites dans le manuel d’analyse (chapitres 3 et 4). Quelques précisions utiles complémentaires sont détaillées ci-après.

**Bassin versants urbains**

Un paramètre intrinsèque important est le coefficient d’imperméabilisation : il est défini comme le rapport entre les surfaces imperméabilisées et la surface totale du bassin versant. La surface imperméabilisée englobe les voiries et leurs bas-côtés, les toitures et terrasses, ainsi que les parkings et surfaces aménagées des zones d’activité.
Deux modèles sont proposés pour modéliser le ruissellement sur ces surfaces :

-    le modèle Horner qui tient compte des pertes initiales,
-    le modèle à coefficient de ruissellement Cr constant.

Le modèle d’Horner est réputé plus précis, mais le calage des coefficients nécessite de disposer d’un nombre conséquent de données de mesures, qui ne sont pas toujours disponibles.

Le modèle à coefficient Cr constant est plus simple à mettre en œuvre, mais il tombe en défaut pour les petites pluies (inférieures à 5mm) et les gros épisodes (supérieurs à 25 mm).

Entre ces deux bornes l’expérience montre que ce coefficient est relativement constant et peut être pris égal à 0.7 fois le coefficient d’imperméabilisation.

A l’extérieur de ces bornes des ajustements doivent être apportés à cette formulation. Des lois d’ajustement sont proposées dans HYDRA via trois mots clés déclarés dans le fichier d’options :


<img src="/hydrology_data_management/comp_options.png" width=500px/>

Ces mots clé sont : PINI_CR, PINI2_CR, PINI3_CR.

La loi d’ajustement la plus complète correspond au mot clé : PINI3_CR. Le coefficient Cr est modulé comme suit :

<img src="/hydrology_data_management/comp_pini.png" width=400px/>

 Cette loi a été calée/testée sur des épisodes orageux s’étalant sur plusieurs jours en région parisienne lors du mois de juillet 2018. Elle est intéressante car elle tient compte de l’assèchement des sols en période non pluvieuse et peut donc s’appliquer à une chronique continue.


**Bassins versants ruraux**

On s’intéresse pour ce type de bassins à des épisodes de crues sur plusieurs jours, voire des chroniques de plusieurs semaines ou plusieurs mois. La réaction de sols non urbanisés joue un rôle important et les modèles associés sont des modèles à réservoirs plus ou moins élaborés. HYDRA propose 4 modèles de production de ruissellement :

- Le modèle Holtan,
- le modèle SCS adapté pour une exploitation en continu par des chroniques longue durée,
- le modèle GR4J développé par l’IRSTEA,
- le modèle HYDRA qui constitue une variante du modèle GR4J.

Le modèle Holtan, tel qu’il est paramétré, peut être utilisé pour modéliser des événements pluvieux simples, il ne dispose pas de fonction de vidange du réservoir par évapotranspiration (ETP) ou autre mécanisme.

Les trois autres modèles permettent de modéliser des chroniques longue durée :

- le modèle **SCS modifié** reprend la formulation classique SCS et la complète par une fonction de vidange du réservoir de sol. Son utilisation est recommandée pour les épisodes pluvieux simples, les résultats fournis sont comparables à ceux du modèle Holtan après calage. Les tests réalisés sur des chroniques longue durée montrent que les résultats ont tendance à se dégrader sur la durée.
- Les modèles **GR4J et HYDRA** sont recommandés pour des simulations de chroniques longue durée. Ce sont tous deux des modèles à deux réservoirs et à 4 paramètres de calage.

   - Le modèle **GR4J** a été appliqué à des typologies de bassins versants très contrastées et a prouvé ses qualités de précision et de robustesse. Les paramètres de calage n’ont cependant pas d’interprétation physique évidente et sont difficiles à ajuster sans outil d’aide spécifique. L’IRSTEA a développé un outil Open source en langage Python pour caler automatiquement les paramètres du modèle à partir de données de chroniques sur une, voire plusieurs années hydrologiques, comprenant des données pluviométriques, l’enregistrement des débits journaliers à l’exutoire du BV et les données mensuelles d’ETP.

   - Le modèle **HYDRA** est basé sur un modèle conceptuel à réservoirs avec une architecture légèrement différente, qui utilise notamment pour la RFU la formulation SCS. Les paramètres de ce modèle présentent l’avantage de pouvoir être interprétés physiquement et être estimés pour certains par lecture directe des enregistrements. Il est applicable pour des pas de temps de calcul quelconques, contrairement au modèle GR4J pour lequel un pas fixe de 24 heures ou de une heure est prescrit. Ce modèle dispose également d’un outil spécifique de calage d’une chronique longue durée, écrit en langage Python. Cet outil n’est pas intégré à la version actuelle d’HYDRA et doit être exploité en externe. Son intégration dans l’IHM d’HYDRA est prévue à terme.

        Des tests de comparaison ont été effectués entre ces deux modèles sur des bassins versants contrastés (cf. documents de validation). Les ajustements obtenus à l’aide des outils de calage précités sont de qualité comparable.


#### Ajustement des conditions initiales des sols

Deux types d’ajustement des conditions initiales de sols sont proposés :

**Ajustement imposé par l’utilisateur**

L’utilisateur a la possibilité d’ajuster les paramètres de sol au début d’un événement de crue en affectant un coefficient initial de saturation via les champs suivants du gestionnaire de scénarios :

<img src="/hydrology_data_management/comp_ci_sols.png" width=300/>

**Runoff coefficient variation « Rcv »**

Ce coefficient s’applique uniquement à la loi de production Cr constant : le coefficient Cr de chaque BV concerné est multiplié par le factor Rcv.

**Soil moisture coefficient « Smc »**

Ce coefficient s’applique aux réservoirs superficiels des modèle Holtan, SCS et HYDRA : le niveau d’humidité de chaque réservoir est calculé en multipliant la capacité du réservoir par le coefficient Smc. Par défaut ce coefficient est égal à 0 : les réservoirs sont à sec en début de simulation.

Cette fonctionnalité ne concerne que les modèles à réservoirs : Holtan, SCS et HYDRA. Elle est activée dans le cas de la modélisation hydraulique d’une crue démarrant à une date donnée. Les conditions d’écoulement sont calmes au démarrage de la crue, mais l’humidité des sols est généralement affectée par les événements pluvieux antérieurs à la crue. Pour quantifier les conditions d’humidité du sol en début de crue on précède en deux temps :

*Première étape** : scénario préparatoire Scen1*

Simulation de la période préparatoire à l’aide du module hydrologique uniquement. Le gestionnaire du scénario préparatoire « Scen1 » est positionné comme suit :

<img src="/hydrology_data_management/comp_periode_preparatoire.png" width=450/>

Les niveaux de remplissage au temps Tsave=2880 heures sont stockés dans le fichier `Scen1_R_hydrol.ini` du répertoire `Scen1\hydrol`.
Les niveaux calculés sont également stockés dans le fichier `Scen1_R_soil_ini.csv` pour consultation.

*Deuxième étape** : scénario Scen2*

Simulation de la crue avec les conditions initiales de sol contenues dans le fichier Scen1_R_hydrol.ini.

Il est impératif que la scénario Scen2 soit réglé en démarrage à froid. Il suffit de positionner les champs suivants comme suit :

<img src="/hydrology_data_management/comp_recup.png" width=700/>

Dans la phase de calcul hydrologique le programme va récupérer les niveaux d’humidité des réservoirs dans le fichier Scen1_R_hydrol .ini généré dans la simulation précédente.       



