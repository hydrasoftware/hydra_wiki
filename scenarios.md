---
title: Paramétrage d'un scénario 
description: 
published: true
date: 2024-10-18T16:37:32.202Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:36.029Z
---


# Gestionnaire de scenario

Le menu principal <img src="/0_hydra_icone.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> **scenarios/Settings**  permet de créer de nouveaux scénarios  et définit tous les paramètres  du scénario nécessaires au bon déroulement d’une simulation :


<img src="/scenarios/_scenario_settings_ui.png" width=900/>

L'ajout et la suppression de scénarios se font via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.



Le bouton *clone scénario* permet de créer un nouveau scénario en dupliquant tous les paramètres d’un scénario déjà créé.

Le bouton *Import from file* permet  de sélectionner un fichier d'échange au format .csv  et d'importer les scénarios renseignés dans ce fichier. Le bouton **?** donne accès à la documentation sur le format des données à respecter. Cette fonctionnalité est surtout utile pour importer des scénarios d'études réalisées avec les logiciels Hydranet et HydraCity.

Le gestionnaire de scénario comprend six onglets détaillés ci-après.

<a name="scenario_computation_settings"></a>

# Menu **Computation settings** (*Paramètres de calcul*)

<img src="/scenarios/_computation_settings_ui.png" width=900px/>

Les valeurs par défaut dont indiquées ci-dessus. A noter que les temps sont renseignés en format date. Les différentes rubriques sont  définies comme suit :



## Computation mode

Par défaut une simulation enchaîne les deux étapes de calcul hydrologique et de calcul hydraulique (option *Hydrologic and hydraulic*). Deux options complémentaires sont disponibles :

- Le **calcul hydrologique** (*Hydrology*) mobilise uniquement le domaine hydrologique d’un modèle et se conclue par le calcul des hydrogrammes d’apport dans le réseau hydraulique au niveau de chaque objet *hydrograph*. Il intègre donc le calcul des apports de temps sec associés aux secteurs d’apports.
- Le **calcul hydraulique** (*Hydraulic*) mobilise uniquement le réseau hydraulique ; les hydrogrammes d’injection au niveau de chaque objet *hydrograph*  sont lus dans un scénario antérieur qui a déjà tourné. Ce scénario est déclaré dans le sous menu *hydrology  settings* (Hydrographs from other scenario).


## Computation  starting date

Date calendaire de démarrage effectif des calcul au temps relatif t=0. En réalité le calcul en régime transitoire et en démarrage à froid démarre à un temps relatif négatif suffisant pour atteindre les conditions de régime établi au temps t=0. Ce temps est défini dans l'onglet *Initial conditions settings*.

## Computation duration

Durée de la simulation : à compter de la date *Computation starting date*


## Hydrologic computation time step

Pas de temps de calcul  dth dans l’étape de  calcul hydrologique. Le pas de temps est exprimé en **hr:mn:sec**.

Il doit être réglé pour satisfaire la condition $60*t_{fin}/dth<5000$


## Hydraulic computation time step

Les 2 valeurs min et max contrôlent  la fourchette de variation du pas de temps de calcul. Le pas de temps min est réglé automatiquement par le programme durant l'exécution des calculs. Le pas de temps max doit en revanche être ajusté à la nature de la simulation :

- pour les simulations de **réseau d’assainissement** ou les **études courantologiques locales**, il conviendra de se limiter à 0.1h au maximum, la valeur plus faible de 0.02 heures étant recommandée pour minimiser les risques de diffusion numérique dans le cas de réseaux à fortes pentes.
- Pour les simulations **fluviales** de propagation de crues le pas de temps maximum peut être porté à 0.5h ou plus.


## Water surface variation between two time steps

Ces paramètres règlent l’algorithme de pas de temps adaptatif appliqué par le programme de calcul :

- min : variation de hauteur d'eau minimale sur l'ensemble des noeuds de calcul permettant d'augmenter le pas de temps de calcul,
- max : variation de hauteur d'eau maximale sur l'ensemble des noeuds de calcul entrainant une réduction du pas de temps de calcul.

En pratique, les valeurs  par défaut proposées sont  correctement  ajustées et conviennent pour la quasi-totalité des simulations. Elles ne doivent donc pas être modifiées, saut cas très exceptionnel.


> **Note**
>
>  les paramètres : `Min computation_time_step`, `Min water_surce_variation` et `Max water_surface_variation` sont désormais automatiquement réglés par le programme : l'utilisateur n'a plus à s'en préoccuper et n'y a donc plus accès.
> 

<a name="scenario_computation_settings_output_hot_start"></a>

## Output time for hot start scenario

Cette option permet de stocker l’état hydraulique d’un système à un instant  donné, pour une reprise des calculs à cet instant par un scénario ultérieur (*Initial conditions settings/Hot start scenario*).

Cette option est très utile dans le cas de simulations de longue durée, elle permet de scinder un calcul en plusieurs scénarios successifs tout en assurant une continuité parfaite des calculs entre 2 scénarios successifs.

## Initial conditions settings

Deux options mutuellement exclusives :

- dans l’option **start from rest**, le démarrage d’un calcul se fait à froid, avec des conditions de débit nul dans toutes les branches et à travers toutes les liaisons. Il faut alors définir un temps tini (`Duration of initial conditions settings simulation (hr:mn)`), nécessaire à l’établissement du régime établi. Ce temps est calé sur le temps de propagation le long du système hydraulique modélisé.
- Si l’option `hot start scénario` est  activée, il faut préciser le scénario pour lequel une sauvegarde a été faite et indiquer le temps relatif de reprise dans le repère de temps du nouveau scénario.


## Computation output time interval

Définition des **pas de temps de stockage des résultats** (*Output time step (hr:mn)*)  pour tous les types de sorties et de l’intervalle de temps pendant lequel les résultats sont stockés. 

Par défaut tmin = 0. et tmax= 12h.


> **warning**
>
>  Penser à ajuster le temps max des sorties à la durée effective du calcul. 
> 

## Graphic control during computation

Cette option permet de contrôler graphiquement la bonne exécution des calculs en visualisant l’évolution temporelle des lignes et des débits le long des branches filaires.

Cet outil est intéressant en phase de réglage du modèle; il permet notamment de mettre en évidence certaines anomalies de cotes de fond ou de cotes de débordement, ou d’identifier les zones responsables d’un ralentissement des calculs et nécessitant généralement des corrections locales à apporter au modèle.

## Reference scenario

Cette option évite de générer les fichiers de données structurelles exploitées par le moteur de calcul d'HYDRA si celles ci ont été  générées dans un scénario antérieur appelé **Reference scenario**. Elle se traduit par des gains de temps très appréciables pour les simulations de gros modèles.


> **warning**
>
>  Cette option doit  être utilisée à bon escient : uniquement dans le cas où les données structurelles du modèles n'on pas été modifiées depuis la date de calcul du scénario de référence. Il faut notamment veiller à ce que les paramètres de configuration soient les mêmes pour les deux scénarios. En revanche  toutes les autres données de paramétrage définies dans les différents onglets du gestionnaire de scénario  peuvent être différentes.
> 

<a name="menu_hydrology_settings"></a>

# Menu **Hydrology settings** (*Paramètres hydrologiques*)

<img src="/scenarios/_hydrology_settings_ui.png" width=900/>


## Rainfall scenario

Sélection du scénario de pluie dans la bibliothèque définie dans le menu principal <img src="/0_hydra_icone.png" height="25px"/> **hydrology/Rainfall**. 

Par défaut, aucun scénario de pluie n'est défini.

## Dry inflow scénario

Sélection du scénario d’apport de temps sec dans la bibliothèque définie dans le menu principal <img src="/0_hydra_icone.png" height="25px"/> **hydrology/Dry flow scenario**. 

Par défaut, aucun scénario de temps sec n'est défini.


## Hydrographs from other scenario

Cette option permet de s'affranchir des calculs hydrologiques (modèle pluie débit de grande ampleur par exemple) pour simuler des scénarios d'aménagement n'intégrant que des modifications structurelles du modèle via des configurations ou des modes de régulations distincts reprenant les entrants hydrologiques d'un scénario de référence.


## Hydrologic initial conditions

- **Initial soil conditions**

Permet de 'forcer' les conditions initiales de sol. Ces paramètres sont définis pour chacune des lois de production disponibles dans Hydra.

  - Lois de production de type Hydra, GR4, SCS et Holtan : coefficient initial de saturation des sols en début de simulation **compris entre 0 et 1**. Un coefficient est associé à chacun des réservoirs de sol associés à ces modèles de production
  - Lois de production de type  Coefficient de ruissellement constant ou Horner : coefficient multiplicateur des coefficients définis par défaut 
  
<img src="/scenarios/_hydrologic_initial_conditions_ui.png" width=550/>
  
  
- **From previous scenario** : les conditions initiales de sols sont lues dans un fichier généré en fin de simulation d'un scénario ayant déjà tourné. Cette option est utile dans les applications suivantes:
  - chroniques longues scindées en plusieurs scénarios,
  - prise en compte d'une période préparatoire sur une longue durée permettant de prendre en compte l'état de saturation des sols à l'arrivée d'un événement pluviométrique intense, ne nécessitant que des calculs hydrologiques.
  
<a name="network_auto-dimensionning"></a>

## Hydrology network auto-dimensionning

Cette application permet, pour une pluie donnée, de dimensionner automatiquement le diamètre équivalent de canalisations et le volume de bassins de rétention d’un réseau hydrologique, les autres caractéristiques du réseau étant rentrées par l’utilisateur, à savoir :

- la description topologique du réseau,
- les données altimétriques des collecteurs et notamment les pentes des fils d’eau,
- les débits de fuite des bassins.

Cette application s’avère utile pour les projets de dimensionnement de réseaux pluviaux neufs de ZAC ou des diagnostics de réseaux secondaires pluviaux.

### Paramétrage

Considérons le réseau test suivant :

<img src="/scenarios/_hydrology_network_auto-dimensionning_example.png" width=350/>

Ce réseau hydrologique comprend 2 BV, 3 tronçons de canalisations et un bassin. On souhaite calculer les diamètres de collecteurs et le volume du bassin nécessaires pour faire passer une pluie de type Caquot sans excéder la capacité du réseau.

Il faut dans tous les cas renseigner les objets, mais on peut donner des valeurs quelconques aux diamètres de canalisations et au volume du bassin. Les autres paramètres doivent être définis précisément.
Pour activer le calcul de dimensionnement automatique il suffit de cocher la case hydrology_network_auto-dimensionning dans l’écran ci-dessous :

<img src="/scenarios/_hydrology_network_auto-dimensionning_ui.png" width=350/>

### Résultats

Après calcul deux fichiers sont générés dans le sous répertoire *[scenario]*\\hydrology :

- le fichier *[scenario]_[model]*\_pipe_redim_hydrol.csv : fournit les nouveaux diamètres de canalisations si les diamètres initialement rentrés sont insuffisants

- le fichier *[scenario]_[model]*\_reservoir_redim_hydrol.csv : fournit les nouveaux volumes des bassins si les volumes initialement rentrés sont insuffisants

<img src="/scenarios/_hydrology_network_auto-dimensionning_res_pipes.png" width=400/>


<img src="/scenarios/_hydrology_network_auto-dimensionning_res_reservoirs.png" width=350/>


## External file hydrographs

Liste des fichiers d’hydrogrammes externes lus par le programme de calcul pour les objets *hydrographs* lorsque l’option *external file data* est activée (cf. <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point)). Ces fichiers peuvent également contenir des courbes de marée et des courbes de vent.


Le bouton **?** donne accès à la documentation les différents formats disponibles.


## Hydrograph derouting

Cette option permet de modifier les hydrogrammes d'apports calculés dans les objets de type  [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) à la fin de la phase de calcul hydrologique via des opérations de déroutages d'un hydrogramme *from* vers un hydrogramme *to*.

Les déroutages entre hydrogrammes de sous modèles différents sont acceptés. Duex modes de déroutages sont proposés:
- *fraction* :  *l'hydrogramme_from* est amputé d'une partie de son volume, cette fraction est ajouté à *l'hydrogramme_to*,
- *qlimit*   :  un débit limit est appliqué à *l'hydrogramme_from*. Le débit excédentaite est ajouté à *l'hydrogramme_to*.

Cette option a été introduite pour tenir compte de la réalité des transferts de débit opérés par l'exploitant dans des parties  amont du réseau non modélisé, dans le cadre d'opérations de chomage notamment.   


<a name="scenario_settings_model_ordering"></a>

# Menu **Model ordering** (*pilotage des modèles*)

Ce menu concerne un projet composé de plusieurs modèles. Hydra propose **trois modes de pilotage** des modèles en phase calcul :

- **1.    Mode global :** Les modèles sont assemblés dans un fichier de calcul unique, ce qui permet de calculer exactement les interactions aval-amont entre les modèles : tout se passe  pour les calculs comme si le projet était composé d’un seul modèle.
<img src="/scenarios/_model_ordering_global_ui.png" width=900/>

- **2.    Mode cascade :** Les modèles sont exécutés en séquence les uns après les autres, dans l’ordre affiché dans le tableau.
<img src="/scenarios/_model_ordering_cascade_ui.png" width=700/>

Dans ce mode, les modèles doivent être connectés entre eux via la condition limite <img src="/model_building/boundary_conditions/connect_model_bc_point.png"/> [Model connection](/model_building/boundary_conditions/connect_model_bc_point). L'ordre des modèles doit être cohérent avec ces connections (possibilité de modifier l'ordre des modèles avec les flèches).



> **warning**
>
>  Les simulations  des différents sous modèles sont effectuées dans l'ordre affiché dans la fenêtre ci dessus. Il est prévu de pouvoir modifier cet ordre dans une version ultérieure. Si cet ordre ne convient pas l'utilisateur a la possibilité de le modifier en paramétrant le mode *Mixte* ci après, beaucoup plus flexible. 
> 

- **3.    Mode mixte :** Les deux précédents modes sont panachés. On peut très bien dans ce mode ne simuler qu’une partie des modèles et les assembler comme on le souhaite par groupe, à condition bien sûr de respecter des règles topologiques de cohérence amont-aval entre les groupes.
<img src="/scenarios/_model_ordering_mixed_ui.png" width=700/>

  - Ajouter un groupe avec le bouton Add
  - Sélectionner le groupe, puis sélectionner un modèle et l'affecter au groupe avec le bouton avec la flèche `Models` vers `Groups`
  
  Le calcul sera conduit groupe par groupe en mode cascade, les modèles définis dans un groupe étant assemblés en mode global.
  
 
 <a name="scenario_settings_regulation_configuration"></a>
 
# Menu **Regulation and configurations** (*régulation et configurations*)
  
  
<img src="/configurations_regulation/configurations_scenario.png" width=900/>
  
## Régulation

Hydra dispose d’un module de pilotage d’un certain nombre d’objets de modélisation hydraulique via un langage de programmation formelle. Les instructions sont définies dans un fichier ASCII, déclaré comme un fichier externe. Les règles détaillées de pilotage et la syntaxe associée sont décrites dans le document consultable via le bouton **?**.

Ce module complète et étend considérablement la gamme des fonctionnalités de régulation locale intégrées aux actionneurs mobiles.

On peut déclarer plusieurs fichiers de contrôle dans un même scénario.

L'ajout ou la suppression de fichiers se fait via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.

Pour plus de précisions sur les fichiers de régulation, se reporter à la [note technique dédiée](https://hydra-software.net/docs/notes_techniques/NT22.pdf).
> **warning**
>
>   Il faut faire attention à l'ordre de déclaration de ces fichiers lorsque la même instruction apparaîtrait dans deux fichiers différents : la priorité est dans ce cas donnée à la dernière instruction lue. Les fichiers doivent donc être déclarés dans un ordre de priorité croissante.
> 

## Configurations

La **Configuration** permet de générer et de tracer des scénarios impliquant des modifications de paramètres géométriques, topographiques et hydrauliques attachés aux objets de modélisation, dans un même projet. Cette fonctionnalité permet de créer dans une base de données `projet` définissant un état de référence ainsi que des scénarios d'aménagement ou d'évolution n’impliquant pas de modifications de structure topologique : création ou effacement de digues, modification de coefficients de ruissellements, recalibrage de réseaux, ...

La création de configurations passe par les étapes suivantes (pour plus de précisions, se référer au chapitre [Configurations](/configurations_regulation)):

- Création d'un nouveau groupe de configurations via le menu <img src="/0_hydra_icone.png" height="25px"/> *Configuration*,
- Activation d'un groupe de configuration dans la barre d'outils,
- Édition des objets à configurer : les modifications apportées via l'éditeur de l'objet sélectionné sont enregistrées dans la configuration active,
- Appel du groupe de configuration dans le gestionnaire de scénarios de calcul.


L'ajout ou la suppression d'un groupe de configuration dans le scénario actif se fait via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.

Les flèches permettent de modifier l'ordre des groupes de configurations appelés; si un objet est configuré dans plusieurs groupes, c'est la configuration définie dans le groupe positionné en première position qui sera prise en compte dans le calcul.


<a name="scenario_computation_output_options"></a>

# Menu **Computation and output options** (*options de calcul et de sorties*)

<a name="scenario_computation_output_options_upgrade_computation"></a>


## Option de calcul **Upgrade computation**

Par défaut les calculs sont basés sur une résolution simplifiée  des équations de Barré de St Venant, c’est-à-dire ne tenant pas compte des termes convectifs et d'inertie dans les équations de bilan de quantité de mouvement. Cette simplification permet de limiter les temps de calculs, et elle est parfaitement justifiée pour de nombreuses classes d’application. 

Elle s’avère néanmoins erronée dans quelques cas, tels que la courantologie 2D, les ondes de rupture de barrage ou les écoulements torrentiels impliquant des ressauts hydrauliques. Il est alors nécessaire d’affiner la formulation en activant les options de calcul affinées dans les domaines 2D ou les branches filaires qui le nécessitent. Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

<img src="/scenarios/_update_computation_option_ui.png" width=650/>

Cet écran permet de déclarer les *Branch*, les *Reach* ou les *Domain 2D* que l’on souhaite traiter en mode affiné.

Trois options de calcul sont disponibles :
1. **Affin** : calcul avec tous les termes de l’équation de Barré de Saint Venant
2. **Affin with dry land** :  mise à zéro des termes convectifs dès qu’une maille 2D est mise à sec (principalement pour les modèles estuariens avec bancs découvrants)
3. **Inertia terms only** : Activation des termes d’accélération, mais désactivation des termes convectifs.

Les termes activés dans ces différentes options sont explicités ci -après :

<img src="/scenarios/_update_computation_option_scheme.png" width=650/>


Il faut également définir le temps à partir duquel le calcul affiné est déclenché.

On peut également spécifier une surface minimum à affecter aux mailles pour éviter des pas de temps de calculs trop faibles.

Une discussion détaillée de cette option est présentée dans le chapitre [Avanced - Mode de calcul Affin](/advanced/affin).


## Option **Strickler coefficient adjustment** (*ajustement du coefficient de Strickler*)

Cette option est généralement utilisée dans la phase de calage des lignes d’eau d’un modèle. Elle permet de modifier les coefficients de frottement dans un domaine 2D ou le long d’un bief de rivière sans devoir rééditer les données de chaque objet. Ces données sont lues dans la phase de calculs et se substituent aux coefficients définis dans la base de données. On peut ainsi tester plusieurs réglages afin de converger vers un calage satisfaisant sans affecter les paramètres de référence du modèle.

Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

<img src="/scenarios/_strickler_option_ui.png" width=400/>

Cet écran permet de déclarer les **biefs 1D** (*Reach* de rivière) ou les **domaines 2D** sur lesquels on souhaite modifier les coefficients de frottement. Les paramètres à définir dépendent du sous domaine :

- Sous domaine 2D : le coefficient de  Strickler Kmin  s’applique à toutes les mailles du domaine.
- Reach : on définit les coefficients de strickler Kmin et Kmaj des lits mineurs et majeurs. Si les champs PK1 et PK2 ne sont pas renseignés ces coefficients s’appliquent à la totalité du bief. Dans le cas contraire ils s’appliquent au tronçon compris entre les pk PK1 et PK2.


<a name="scenario_computation_runoff" ></a>

## Domain 2D runoff

<img src="/model_building/runoff/scenario_settings_runoff.png" width=400/>

Cette option permet d’identifier les **domaines 2D** pour lesquels le calcul de ruissellement sur le maillage 2D sera appliqué dans l’étape hydraulique d’exécution (cf. [Runoff](/model_building/#runoff)).


> **note**
>
>Un même modèle peut contenir à la fois des bassins versants et des domaines 2D intégrant du ruissellement :
>- Les hydrogrammes d’apports à l’exutoire des chaque BV sont calculés dans la phase de calcul hydrologique,
>- Les apports de ruissellement dans un domaine 2D déclaré dans le formulaire ci-dessus sont calculés dans la phase de calcul hydraulique.


Les données pluviométriques sont appelées dans l'onglet *Hydrology* du gestionnaire de scénario. Le module de calcul de ruissellement reconnaît tous les types de pluies à l’exception des pluies radars.


## Additionnal output : z(t) et q(t) (Options de sortie complémentaires : *z(t) et q(t)*)

Cette option permet de générer automatiquement en sortie de simulation des fichiers d’hydrogrammes et de limnigrammes en des points particuliers d’un modèle. Ces fichiers sont générés au format .csv.

Le paramétrage de cette option est réalisé via l’écran de saisie suivant :

<img src="/scenarios/_zt_qt_output_option_ui.png" width=500/>

Une sortie de courbe peut être demandée au niveau d’une singularité, d’une condition à la limite ou d’un link.

Dans le cas du link ou de la singularité  la cote d’eau stockée est la cote au nœud amont.

<a name="scenario_computation_option" ></a>

## Options diverses via un **fichier externe**

Les trois options ci-dessus ont été isolées car elles sont utilisées relativement fréquemment. D’autres options sont disponibles et peuvent être définies dans le fichier déclaré dans la fenêtre suivante :

<img src="/scenarios/_additionnal_output_options_ui.png" width=500/>

Une option déclarée dans ce ficher se compose d’une première ligne avec un mot clé précédé du signe « * ». les lignes suivantes précisent les paramètres attachés à l’option.
Dans ce fichier deux blocs d’options consécutives doivent être séparés par au moins une ligne blanche.


<a name="scenario_settings_transport_pollution_quality" ></a>

# Menu **Transport** (*transport solide et qualité*)

Le transport de substances peut être simulé dans Hydra via 5 options mutuellement exclusives :

<img src="/scenarios/_transport_options_ui.png" width=300/>

- Option **Pollution** : Pollution generated and network transport (*génération de la pollution et transport dans les réseaux d'assainissement*)
- Option **Quality 1** : Physico chemical, 4 inter related parameters DBO, NH4, NO3, O2 (*Physico chimie, 4 paramètres interragissant entre eux DBO, NH4, NO3, O2*)
- Option **Quality 2** : Bacteriological, no chemical intercation between parameters (*Bactériologie, pas d'interraction entre les paramètres*)
- Option **Quality 3** : Suspended sediment transport (*transport de matières en suspension, MES*)
- Option **Hydro morphology** : Bedload (*transport sédimentaire*)


## Option **Pollution**

Cette option permet de simuler la génération de pollution par ruissellement et sa propagation dans un réseau d’assainissement. 4 paramètres peuvent être simulés :

<img src="/scenarios/_pollution_option_ui.png" width=600/>

Ces paramètres n’interagissent pas entre eux.

Les stocks de pollution entrainés dans les réseaux par ruissellement sont définis dans la table *land pollution accumulation* du menu principal <img src="/0_hydra_icone.png" height="25px"/> **scenarios/hydrology**.


## Option qualité 1 : **physico chemical**

Cette option permet de simuler l’évolution de la qualité physico-chimique d’un milieu récepteur : lac ou cours d’eau à l’aide de 4 paramètres : O2, DBO, HH4 et NO3. Ces paramètres sont liés par des réactions d’échanges chimiques. Les constantes de réactions sont défies par défaut dans la grille de saisie suivante :

<img src="/scenarios/_physico-chemical_option_ui.png" width=650/>

On peut également simuler les effets de diffusion  turbulente et de dispersion qui se superposent aux phénomènes convectifs de transport et ont pour effet d’étirer le nuage de pollution.


## Option quality 2 : **bactériological**

Cette option permet de simuler l’évolution de la qualité bactériologique d’un milieu récepteur : lac ou cours d’eau à l’aide de 4 paramètres  de contamination fécale dont les Eschéricia Ecoli et les Entéroques intestinaux :

<img src="/scenarios/_bacteriological_option_ui.png" width=550/>


 A chaque paramètre on associe un taux de mortalité en $jour^{-1}$.

Les paramètres n’interagissent pas entre eux.

On peut prendre en compte  les effets de dispersion comme pour l’option *quality 1*.  


## Option quality 3 : **Suspended sediment transport**

Le transport de matières en suspension peut être simulé dans Hydra via 5 options mutuellement exclusives :

<img src="/scenarios/_suspended_sediment_transport_ui.png" width=550/>

Ce module a été développé pour analyser le devenir  des rejets polluants chargés de MES dans un milieu récepteur , afin de dresser la carte des matériaux polluants déposés après un événement pluvieux. 


## Option **Hydro morphology** (*transport sédimentaire*)

Cette option active le module de calcul de transport hydro sédimentaire par charriage. Les paramètres de transport sont définis dans le fichier déclaré dans la fenêtre de saisie :

<img src="/scenarios/_bedload_transport_ui.png" width=800/>

Le format des données est détaillé dans le document consultable avec le bouton **?** 

les données de l'option *Hydro morphologie* sont totalement contenues dans le fichier déclaré ci dessus. 


> **Note**
>
>Les régles d'application des options 1 à 4 sont décrite dans le chapitre  [Pollution et Qualité](/pollution_qualite).
>
>
>La fomulation retenue pour les simulations hydro-sédimentaire est décrite dans le document d'analyse de transport sédimentaire.
>

