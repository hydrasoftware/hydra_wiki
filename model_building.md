---
title: Les domaines de modélisation
description: 
published: true
date: 2025-02-04T10:30:14.346Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:45:07.890Z
---

# Présentation générale

Le développement urbain rend de plus en plus complexe la gestion de l’eau et des risques associés. Les métropoles littorales peuvent ainsi être soumises à l’interaction de différents aléas : submersion marine, débordement de cours d’eau, saturation des réseaux d’assainissement pluvial en cas d’événement pluvieux intense entrainant des débordements de surface.

Hydra permet de représenter ces différents phénomènes en un même modèle afin de mettre à disposition de l’ingénieur des outils de compréhension du fonctionnement du système et de lui permettre de proposer et dimensionner des solutions techniques adaptées.

La modélisation numérique est la représentation d'un système physique par une schématisation permettant sa description, sur laquelle sont résolues les équations propres aux phénomènes étudiés. La modélisation revêt ainsi nécessairement un caractère simplificateur de la complexité du système : simplification des caractéristiques géométriques, et simplification des lois physiques associées.

Se pose alors la question des méthodes de schématisation de ces différents systèmes hydrauliques. Alors que les systèmes maritimes peuvent être modélisés par un maillage représentant la bathymétrie (dont la résolution est dépendante de l’échelle de travail), la modélisation des systèmes fluviaux et d’assainissement nécessite de prendre en compte le fonctionnement des singularités que constituent les ouvrages hydrauliques ; l’échelle 
de travail et les enjeux associés à la modélisation mise en œuvre peuvent par ailleurs amener le modélisateur à simplifier ou affiner la schématisation du système.

Hydra propose ainsi différents schémas de modélisation, adaptés à l’échelle de travail et aux systèmes étudiés, tout en permettant leur couplage :

- **filaire**, ou unidimensionnel (1D) :
  - réseaux d'assainissement, composés de tronçons auqxquels sont affectés une géométrie, connectés à des regards,
  - cours d’eau (lits mineur et / ou majeur), représentés par des sections géométriques agencées le long d’un axe préférentiel d’écoulement ; les vitesses d’écoulement sont imposées le long de cet axe,
- **bidimensionnel** (2D) : le terrain naturel et / ou la bathymétrie sont schématisés par un maillage triangulaire ou quadrangulaire, chaque maille étant caractérisée par sa surface et la cote moyenne du terrain naturel,
- **casiers** : zones de stockage présentant de faibles vitesses d’écoulement et de faibles variations de hauteurs d’eau, caractérisés par une loi de remplissage cote / volume,
- **rues** : axes potentiels privilégiés des écoulements dans un environnement urbain dense et/ou régulier, assimilées à un schéma filaire,
- **stations de gestion** : unité fonctionnelle composée de chambres interconnectées, permettant une description fine des ouvrages hydrauliques complexes,
- **hydrologie** : bassins versants et réseaux de routage simplifiés, permettant le calcul des apports de ruissellement.

La connexion de ces différents domaines de modélisation (et plus généralement des différents objets de modélisation) est assurée par des liaisons, de natures différentes selon la topographie ou le type d’ouvrage le cas échéant.

Le schéma ci-dessous présente l’ensemble des domaines de schématisation disponibles ainsi que les liaisons hydrauliques permettant leur connexion.

<img src="/model_building/modelisation_domains.png" width="650px"/>

*Schéma de synthèse des domaines de schématisation disponibles et des liaisons hydrauliques permettant leur connexion*

Une **arborescence hydrologique** se distingue des autres blocs par l’ordre dans lesquels les calculs sont réalisés : les hydrogrammes sont tout d’abord calculés de proche en proche le long de chaque arborescence hydrologique dans l’étape de génération des fichiers de calcul hydraulique. Ils sont ensuite stockés aux exutoires avals, puis injectés à chaque pas de temps de  calcul dans le modèle hydraulique formé par tous les autres blocs fonctionnels.

Le **réseau hydraulique d’assainissement** et l’**arborescence hydrologique** partagent des objets communs de modélisation notamment les tronçons, mais ils diffèrent par les singularités qui sont spécifiques à chaque type de réseau. Les deux types de réseaux ne sont donc pas interchangeables, des adaptations sont nécessaires pour convertir une arborescence hydrologique en un réseau hydraulique et vice versa.

Dans le cas de schématisations 2D, la représentation des ouvrages et discontinuités par des lois d’écoulement précises permettent de conserver un maillage relativement lâche et d’optimiser les temps de calcul.

Hydra propose **deux modes de résolution des équations de Barré de St Venant**, l’un complet et l’autre simplifié, pouvant être affecté à tout ou partie des différents domaines constituant un modèle donné :

- le **mode complet** prend en compte la totalité des termes des équations de St Venant. Celles-ci mettent en œuvre trois variables indépendantes pour chaque nœud de calcul  d’un domaine 2D ou deux dans un domaine 1D du fait du couplage des équations via les termes convectifs : cote d’eau et vitesses d’écoulement. 
- le **mode simplifié** néglige les termes convectifs, ce qui revient à ne considérer qu’une variable indépendantes à chaque nœud de calcul, à savoir la cote d’eau. Il en résulte des gains appréciables en temps de calcul.

La souplesse d’assemblage de ces différents schémas de modélisation permet de définir différents niveaux de précision suivant les objectifs attendus pour un modèle donné.

Ces schémas de modélisation permettent de couvrir l’ensemble des problématiques associées à la gestion des eaux de surface :

- Hydrologie via des modules de transformation pluie – débit et de routage simplifié,
- Fluviaux,
- Assainissement urbain,
- Maritime et estuariens.

# Conceptualisation

Les briques décrivant chaque bloc fonctionnel et les interactions entre blocs sont classées formellement en trois grandes entités :

- Le « node » (ou containeur),  un objet de stockage correspondant à un nœud de calcul. Huit types de containeurs sont reconnus dans Hydra :
  - Le bassin versant.
  - Le regard d’une arborescence hydrologique
  - Le regard d’un réseau d’assainissement,
  - Le nœud d’un bief fluvial,,
  - La chambre dans une station de gestion,
  - Le casier,
  - La maille 2D (ou le pavé),
  - Le carrefour,
- La « singularité », entité nodale « posée » sur un containeur. Elle assure des fonctions différentes selon le type :
  - Perte de charge singulière le long d’une branche filaire,
  - Conditions à la limite aval,
  - Liaison latérale uninodale de type Q=f(z).
- La liaison latérale (ou « link »), connectant deux containeurs ; il véhicule les flux d’échanges entre les containeurs.

Certaines de ces entités peuvent être regroupées en collections. On distingue ainsi :
- La branche de collecteur,
- Le bief de vallée (ou reach),
- La station de gestion,
- Les domaines 2D.

Toutes ces entités servent à définir un modèle hydraulique. Plusieurs modèles peuvent co-exister au sein d’un projet. Les connexions entre modèle sont assurées par des singularités particulières nommées « model connexion ».  
Les principes d’agencement entre ces différentes entités seront gérés dans l’IHM par des règles de cohérence (ou règles d’intégrité), dont la plupart sont intégrées dans l’architecture du modèle conceptuel de données ci-après.

Un modèle hydraulique est décrit dans *PostGre* par un schéma composé de :
- 73 tables
- 82 vues

Les tables sont regroupées en trois grandes entités : les « nodes », les « singularities » et les « links ».
Chaque entité se compose d’une table abstraite décrivant les caractéristiques topologiques de l’objet de modélisation et de tables dérivées contenant chacune les attributs spécifiques à chaque objet.
Le diagramme suivant décrit l’ossature du MCD :

<img src="/model_building/6_mcd.png" width="480px"/>

A chaque entité correspond une table abstraite et autant de tables dérivées que d’objets de modélisation appartenant à cette entité.
De nombreuses autres tables viennent compléter ce MCD. Les caractéristiques de chacune peuvent être consultées via le gestionnaire pgAdmin.

Les contraintes d’intégrité reliant les tables entre elles, ainsi que les fonctions « triggers »qui sont déclenchées lors d’opérations de création ou de mises à jour de données, contrôlent la validité des différentes opération de création et de mise à jour et  assurent  ainsi la cohérence topologique du modèle à chaque étape de son évolution.Les objets de modélisation

La figure suivante présente l’ensemble des objets disponibles pour construire un modèle, ces objets sont regroupés par famille « métier » :

<img src="/model_building/6_model_objects.png" width="750px"/>

Chaque objet de modélisation fait l’objet d’une fiche précisant :
- la fonction de l’objet
- les équations mises en œuvre,
- les règles  et les restrictions éventuelles d’utilisation

Les chapitres suivants précisent les règles générales de construction d’un modèle en raisonnant par domaine.


**Hydrologie**

<img src="/model_building/hydrology/catchment_node_point.png"/> [Catchment](/model_building/hydrology/catchment_node_point) : **bassin versant**

<img src="/model_building/hydrology/catchment_contour_polygon.png"/> [Catchment contour](/model_building/hydrology/catchment_contour) : *contour de bassin versant**

<img src="/model_building/hydrology/hydrology_node_point.png"/> [Hydrology manhole](/model_building/hydrology/hydrology_node_point) : **regard hydrologique**

<img src="/model_building/hydrology/connector_hydrology_line.png"/> [Connector](/model_building/hydrology/connector_hydrology_line) : **connecteur**

<img src="/model_building/hydrology/routing_line.png"/> [Hydrology routing](/model_building/hydrology/routing_line) : **routage hydrologique**

<img src="/model_building/hydrology/qq_split_hydrology_point.png"/> [Split QQ](/model_building/hydrology/qq_split_hydrology_point) : **dérivation QQ**

<img src="/model_building/hydrology/zq_split_hydrology_point.png"/> [Split ZQ](/model_building/hydrology/zq_split_hydrology_point) : **dérivation ZQ** 

<img src="/model_building/hydrology/reservoir_rs_point.png"/> [Reservoir](/model_building/hydrology/reservoir_rs_point) : **bassin de stockage**

<img src="/model_building/hydrology/reservoir_rsp_point.png"/> [Parametric reservoir](/model_building/hydrology/reservoir_rsp_point) : **bassin de stockage paramétrique**

<img src="/model_building/hydrology/hydrology_bc_point.png"/> [Hydrology boundary condition](/model_building/hydrology/hydrology_bc_point) : **condition limite hydrologique**


**Réseaux d'assainissement**   

<img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line) : **tronçon de collecteur**

<img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point) : **regard**


**Rivières et écoulements à surface libre**

<img src="/model_building/river_free_surface_flow/reach_line.png"/> [Reach](/model_building/river_free_surface_flow/reach_line) : **bief**

<img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point) : **noeud de rivière**

<img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point) : **profil en travers**

<img src="/model_building/river_free_surface_flow/pave_polygon.png"/> [Mesh element](/model_building/river_free_surface_flow/mesh_element) : **maille 2d**

<img src="/model_building/river_free_surface_flow/storage_marker.png"/> [Storage](/model_building/river_free_surface_flow/storage) : **casier**

<img src="/model_building/river_free_surface_flow/crossroad_point.png"/> [Crossroad](/model_building/river_free_surface_flow/crossroad_street) : **carrefour**

<img src="/model_building/river_free_surface_flow/street_line.png"/> [Street](/model_building/river_free_surface_flow/crossroad_street) : **rue**


**Stations de gestion**

<img src="/model_building/station/station_node_point.png"/> [Station node](/model_building/station/station_node_point) : **noeud de station de gestion**

<img src="/model_building/station/station_polygon.png"/> [Station contour](/model_building/station/station_polygon) : **contour de station de gestion**


**Ouvrages**

<img src="/model_building/structures/gate_point.png"/> [Gate](/model_building/structures/gate_point) : **vanne**

<img src="/model_building/structures/zregul_weir_point.png"/> [Weir](/model_building/structures/zregul_weir_point) : **déversoir frontal** (seuil)

<img src="/model_building/structures/borda_headloss_point.png"/> [Borda](/model_building/structures/borda_headloss_point) : **perte de charge de type Borda** 

<img src="/model_building/structures/param_headloss_point.png"/> [Parametric headloss](/model_building/structures/param_headloss_point) : **perte de charge paramétrique**

<img src="/model_building/structures/bradley_headloss_point.png"/> [Bradley](/model_building/structures/bradley_headloss_point) : **ouvrage de franchissement**

<img src="/model_building/structures/bridge_headloss_point.png"/> [Bridge](/model_building/structures/bridge_headloss_point) : **ouvrage de franchissement**

<img src="/model_building/structures/regul_sluice_gate_point.png"/> [Regulated gate](/model_building/structures/regul_sluice_gate_point) : **vannage régulé**

<img src="/model_building/structures/hydraulic_cut_point.png"/> [Hydraulic cut](/model_building/structures/hydraulic_cut_point) : **coupure hydraulique**

<img src="/model_building/structures/marker_point_point.png"/> [Point marker](/model_building/structures/marker_point_point) : **marqueur de point**



**Liaisons**

<img src="/model_building/links/gate_line.png"/> [Gate link](/model_building/links/gate_line) : **vanne manuelle**

<img src="/model_building/links/regul_gate_line.png"/> [Regulated gate link](/model_building/links/regul_gate_line) : **vanne régulée**

<img src="/model_building/links/gate_line.png"/> [Weir link](/model_building/links/weir_line) : **seuil déversant**

<img src="/model_building/links/pump_line.png"/> [Pump link](/model_building/links/pump_line) : **maillage par pompage**

<img src="/model_building/links/deriv_pump_line.png"/> [Deriv pump link](/model_building/links/deriv_pump_line) : **dérivation par pompage**

<img src="/model_building/links/borda_headloss_line.png"/> [Borda link](/model_building/links/borda_headloss_line) : **perte de charge de type Borda** 

<img src="/model_building/links/connector_line.png"/> [Connector link](/model_building/links/connector_line) : **connecteur**

<img src="/model_building/links/strickler_line.png"/> [Strickler link](/model_building/links/strickler_line) : **liaison frottement**

<img src="/model_building/links/porous_line.png"/> [Porous link](/model_building/links/porous_line) : **liaison poreuse**

<img src="/model_building/links/overflow_line.png"/> [Overflow link](/model_building/links/overflow_line) : **surverse**

<img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line) : **surverse depuis un réseau**

<img src="/model_building/links/interlink_line.png"/> [Interlink](/model_building/links/interlink_line) : **surverse depuis un réseau**, connexion entre deux modèles   

<img src="/model_building/links/mesh_2d_line.png"/> [Mesh 2d link](/model_building/links/mesh_2d_line) : **liaison 2d**



**Conditions aux limites**

<img src="/model_building/boundary_conditions/constant_inflow_bc_point.png"/> [Constant inflow](/model_building/boundary_conditions/constant_inflow_bc_point) : **débit constant**

<img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) : **hydrogramme**

<img src="/model_building/boundary_conditions/froude_bc_point.png"/> [Froude](/model_building/boundary_conditions/froude_bc_point) : condition aval de type **chute (Froude)**

<img src="/model_building/boundary_conditions/strickler_bc_point.png"/> [Strickler](/model_building/boundary_conditions/strickler_bc_point) : condition aval de type **strickler**

<img src="/model_building/boundary_conditions/zq_bc_point.png"/> [Z(Q)](/model_building/boundary_conditions/zq_bc_point) : **loi z(q)**

<img src="/model_building/boundary_conditions/tz_bc_point.png"/> [Z(t)](/model_building/boundary_conditions/tz_bc_point) : **loi z(t)**

<img src="/model_building/boundary_conditions/weir_bc_point.png"/> [Weir](/model_building/boundary_conditions/weir_bc_point) : **seuil**

<img src="/model_building/boundary_conditions/tank_bc_point.png"/> [Tank](/model_building/boundary_conditions/tank_bc_point) : **bassin**

<img src="/model_building/boundary_conditions/connect_model_bc_point.png"/> [Model connection](/model_building/boundary_conditions/connect_model_bc_point) : **connexion de modèles**


# Domaine hydrologique

## Introduction

Dans le cas des réseaux des grandes agglomérations ou dans les secteurs amont des vallées alluviales, il est souvent inutile de modéliser finement la propagation des hydrogrammes des réseaux amont à l'aide de lois de type Barré de Saint Venant. Des logiciels de calculs simplifiés, basés sur la méthode de l’onde diffusive, suffisent généralement. 

D’où l’intérêt, pour favoriser la rapidité des calculs et faciliter l’exploitation de grands modèles, de limiter la modélisation hydraulique aux secteurs aval, caractérisés généralement par de faibles pentes, de nombreuses singularités et d’effets aval, et d’alimenter ces réseaux par des modèles hydrologiques simplifiés.

Le **domaine hydrologique** répond à ces besoins, il assure une double fonction de:
- génération d'hydrogrammes de ruissellement à partir de données pluviométriques,
- propagation de ces hydrogrammes  le long d’un réseau secondaire composé de tronçons de routage et de singularités de type dérivation ou bassins.

Un sous réseau hydrologique génère des hydrogrammes à partir de la pluie, les propage à l’aide de lois de routage simplifiées, et modélise des ouvrages particuliers tels que des dérivations ou des bassins. Les hydrogrammes calculés aux exutoires aval d’un réseau hydrologique sont automatiquement injectés dans le réseau hydraulique ou rejetés vers l’extérieur.

Lors d’une simulation les calculs hydrologiques peuvent être exécutés indépendamment de la simulation hydraulique : les résultats de cette modélisation peuvent être donc exploités sans devoir exécuter la simulation  des sous modèles hydrauliques. 

Sur un plan algorithmique les calculs des réseaux hydrologiques  sont exécutés suivant une table d’opérations en procédant de l’amont vers l’aval dans chaque sous réseau. A chaque étape de calcul  un hydrogramme attaché à un objet de modélisation est calculé. Il résulte de cette procédure que les calculs hydrologiques sont très rapides mais la validité des résultats est conditionnée par l’absence d’influence hydraulique aval.

## Schématisation d’un sous réseau hydrologique

La modélisation d’un sous réseau hydrologique met un œuvre un ensemble d’objets de modélisation hydrologique, distinct des objets de modélisation hydraulique, pour  simuler :
- Les apports de ruissellement à partir de la pluie,
- Les apports de temps sec,
- Les apports définis par des hydrogrammes externes,
- Les routages le long de tronçons de collecteurs,
- Les dérivations,
- Les stockages dans les bassins.  

Le domaine hydrologique se compose d’arborescences  distinctes. Une arborescence se compose en amont de  nœuds de bassin versants (catchment) , connectés chacun à un regard( hydrology node) par l’intermédiaire d’un élément de routage (hydrology routing).

<img src="/model_building/hydrology/_hydrologic_domain.png" width="550px"/>

Les **objets de modélisation** associés au domaine hydrologique sont les suivants :    

<img src="/model_building/hydrology/_hydrology_button_ui.png" width="80px"/>



Le <img src="/model_building/hydrology/catchment_contour_polygon.png"/> [catchment contour](/model_building/hydrology/catchment_contour) est une entité polygonale servant à **délimiter le contour du bassin versant**. Cet objet, rattaché au [Catchment node](/model_building/hydrology/catchment_node_point) qu'il contient, n’a pas d’attribut particulier et sert  au calcul des lames d’eaux moyennes précipitées sur chaque bassin versant en cas de pluie spatialement hétérogène.

Le <img src="/model_building/hydrology/catchment_node_point.png"/> [Catchment node](/model_building/hydrology/catchment_node_point) permet la **transformation pluie - débit**. Il correspond à un noeud auquel sont attachées les caractéristiques géométriques, les lois de production de pluie nette et de ruissellement et les paramètres associés au bassin versant considéré. Certaines de ces caractéristiques peuvent être calculées [automatiquement](/model_building/hydrology/catchment_node_point#auto_fill_hydrologic_parameters) (surface, longueur, pente, coefficient d'imperméabilisation et paramètres associés aux fonction de production de pluie nette).

Le **routage simplifié** des apports hydrologiques se fait via un réseau maillé pouvant contenir les éléments suivants :

-  des <img src="/model_building/hydrology/hydrology_node_point.png"/> [regards](/model_building/hydrology/hydrology_node_point)
-  des **liaisons binodales**, connectées à chaque extrémité à un regard :
  - <img src="/model_building/hydrology/connector_hydrology_line.png"/> [connecteurs](/model_building/hydrology/routing_line), permettant de propager un hydrogramme entre deux noeuds sans déphasage et sans déformation
  - **liaisons de dérivation**, posées sur des nœuds hydrologiques; elles doivent obligatoirement figurer à tous les points comportant des diffluences 
    - <img src="/model_building/hydrology/qq_split_hydrology_point.png"/> [split QQ](/model_building/hydrology/qq_split_hydrology_point)
    - <img src="/model_building/hydrology/zq_split_hydrology_point.png"/> [split ZQ](/model_building/hydrology/zq_split_hydrology_point)
  - des **colllecteurs**; ce sont les mêmes objets que ceux du réseaux hydraulique d’assainissement : <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line)
- des <img src="/model_building/hydrology/reservoir_rs_point.png"/> [bassins de stockage](/model_building/hydrology/reservoir_rs_point) et des <img src="/model_building/hydrology/reservoir_rsp_point.png"/> [bassins de stockage paramétriques](/model_building/hydrology/reservoir_rsp_point)
- des <img src="/model_building/hydrology/hydrology_bc_point.png"/> [conditions limites](/model_building/hydrology/hydrology_bc_point)

Les **exutoires aval** d’un sous réseau hydrologique sont obligatoirement matérialisés par un des objets suivants :

- Une **condition limite aval** (<img src="/model_building/hydrology/hydrology_bc_point.png"/> [hydrology BC](/model_building/hydrology/hydrology_bc_point)) qui doit être posé sur le regard aval : l’hydrogramme aval sort du réseau modélisé, dans le cas par exemple d’un rejet dans le milieu récepteur,
- Un élément de **routage hydrologique** (<img src="/model_building/hydrology/routing_line.png"/> [hydrology routing](/model_building/hydrology/routing_line)) permettant de **raccorder un réseau hydrologique à un réseau hydraulique**. Le nœud hydraulique peut être de type quelconque ([manhole](/model_building/network/manhole_point), [river node](/model_building/river_free_surface_flow/river_node_point), [station node](/model_building/station/station_node_point), [storage](/model_building/river_free_surface_flow/storage), [element 2D](/model_building/river_free_surface_flow/mesh_element), [crossroad](/model_building/river_free_surface_flow/crossroad_street)) mais **doit porter un objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [hydrograph](/model_building/boundary_conditions/hydrograph_bc_point)**.



## Règles d'agencement des objets

Le schéma ci-après présente des exemples licites d’agencement d’arborescences hydrologiques

<img src="/model_building/hydrology/_hydrology_connecting_rules.png" Width="600px"/>

## Principe de calcul

L’arborescence hydrologique constitue un domaine à part : les calculs sont en effets réalisés dans une étape préliminaire selon une logique amont-aval : les hydrogrammes sont calculés de proche en proche en descendant le long de l’arborescence. 

Les hydrogrammes obtenus aux exutoires aval qui sont connectés au réseau hydraulique sont stockés temporairement. 

Ils sont ensuite utilisés comme input hydrologiques dans la phase suivante de calcul hydraulique qui met en œuvre tous les autres domaines.


## Résultats fournis par un calcul hydrologique

Les résultats de calcul sont disponibles via les outils d'exploitations communs à l'ensemble des nœuds de calcul (hydrogrammes notamment).

Les **résultats de synthèse** sont également stockés dans un fichier au format .csv situé dans le répertoire hydrol du working directory. Ce dernier fournit des bilans à chaque point de calcul.

Un outil appelé via le gestionnaire de scénarios permet le **[dimensionnement automatique](#network_auto-dimensionning)** des réseaux pour un scénario hydrologique donné.


## Conversion d'une arborescence hydrologique en sous réseau hydraulique

### Principes

Le sous domaine « arborescence hydrologique » permet de propager des hydrogrammes le long d’un réseau secondaire vis des lois hydrauliques simplifiées basées sur la méthode de l’onde cinématique. Un tel réseau est structuré par des nœuds hydrologiques et des tronçons de collecteurs identiques à ceux générés pour décrire un sous réseau hydraulique.
Dans certaines applications il s’avère utile de convertir un réseau secondaire (ou arborescence hydrologique) en sous partie du réseau hydraulique, pour prendre en compte par exemple des effets de remous.
util développé pour faire cette conversion est décrit ci-après.

Considérons le modèle suivant :

<img src="/model_building/hydrology/hydrology_network-hydraulic_network_example.png" width="400px"/>

Le sous réseau encadré vert est une arborescence hydrologique composée d’une dérivation, d’un exutoire aval et d’un raccordement au réseau hydraulique. On souhaite convertir cette arborescence en sous réseau hydraulique. Cette conversion nécessite de :
- remplacer les nœuds hydrologiques <img src="/model_building/hydrology/hydrology_node_point.png"/> [Regards](/model_building/hydrology/hydrology_node_point) en nœuds hydrauliques <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point),
- connecter les tronçons de collecteur aux nœuds hydrauliques <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line),
- remplacer les connecteurs hydrologiques <img src="/model_building/hydrology/connector_hydrology_line.png"/> [Hydrology connector](/model_building/hydrology/connector_hydrology_line) en <img src="/model_building/links/connector_line.png"/> [Connector](/model_building/links/connector_line) hydraulique,
- remplacer la condition aval hydrologique <img src="/model_building/hydrology/hydrology_bc_point.png"/> [Conditions limites](/model_building/hydrology/hydrology_bc_point) par une condition à la limite de type <img src="/model_building/boundary_conditions/froude_bc_point.png"/> [Froude](/model_building/boundary_conditions/froude_bc_point),
- ajouter un objet <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) sur le nœud connecté aux objets <img src="/model_building/hydrology/routing_line.png"/> [Hydrology routing](/model_building/hydrology/routing_line),
- supprimer l’objet dérivation.


L’outil **conversion** permet d’automatiser toutes ces opérations en un seul clic.

### Mise en œuvre

L’opération de conversion nécessite deux étapes :
**Etape 1** : sélection des nœuds de l’arborescence que l’on souhaite convertir 
Il suffit pour cela de sélectionner les nœuds hydrologiques avec l’outil sélection de Qgis (les nœuds sélectionnés sont de couleur jaune ci-dessous) :

<img src="/model_building/hydrology/hydrology_network-hydraulic_network_example1.png" width="400px"/>

**Etape 2** : calcul de conversion 
Cliquer sur le bouton : pour lancer la conversion. Après traitement le réseau est transformé comme suit :

<img src="/model_building/hydrology/hydrology_network-hydraulic_network_example2.png" width="300px"/>


> **note**
>
> Cette opération de conversion n’est effectuée que si tous les tests de cohérence sont trouvés positifs. En particulier on ne peut pas convertir une arborescence hydrologique connectée à l‘aval avec un nœud hydrologique.
> 
> 

<a name="network"></a>

# Réseaux d'assainissement

## Introduction

Le système d’assainissement urbain ne présente pas de difficulté de représentation topologique car il est parfaitement structuré par le réseau de collecteurs qui le composent. La difficulté principale vient de l’existence de nombreuses singularités interagissant au sein d’une même entité fonctionnelle de gestion : usine de pompage, ouvrage de stockage, station de maillage des flux…

Pour tenir compte de cette réalité, la schématisation d’un réseau d’assainissement distingue trois groupes d’entités, illustrés dans la figure ci-dessous :
- Le domaine filaire, détaillé dans le présent chapitre,
- Les [stations de gestion](/model_building#station_de_gestion),
- Les [liaisons latérales](/model_building#liaisons).

## Principes de modélisation

Les objets de modélisation associés au domaine assainissement sont les suivants :    

<img src="/model_building/network/_network_button_ui.png"/>

Le domaine d’assainissement est structuré par des tronçons de collecteurs ( <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line)) connectés à chaque extrémité  à un regard ( <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point)) :


<img src="/model_building/network/_network_pipes_nodes.png" width="400px"/>

Un enchaînement de collecteurs sans défluence ou confluence intermédiaire constitue une **branche de collecteurs**. Ces branches sont découpées automatiquement et de façon dynamique au fur et à mesure de l’enrichissement du modèle. Les branches ainsi générées et leur délimitation peuvent être consultées dans la couche *branch* du gestionnaire de couches.

Il est possible d'identifier une branche avec un marqueur  <img src="/model_building/network/marker_branch_point.png"/> [Marker](/model_building/network/marker_branch_point), permettant de lui donner un identifiant spécifique et de **redéfinir le pas de discrétisation des calculs** (par défaut ce pas est égal à 50m).



Dans l’exemple ci-dessous l’agencement du réseau génère 5 branches délimitées comme suit :

<img src="/model_building/network/_network_branches.png" width="450px"/>

Les regards peuvent porter des [ouvrages](/model_building#ouvrages) et être connectés entre eux par des [liaisons](/model_building#liaisons).   


## Les objets de modélisation

<a name="manhole_type"></a>

### Les regards

Un **regard** est défini par sa section et la cote du terrain naturel. Il est créé avec le bouton <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point); une cote lui est affectée automatiquement à partir du MNT chargé le cas échéant.

Deux types de regards peuvent être définis, conditionnant les **modalités de débordement** associées (cf. [network overflow link](/model_building/links/network_overflow_line)):
- **Regard fermé** (`Manhole cover`), caractérisé par :
  - `Cover diameter (m)` : Diamètre de couverture,
  - `Cover critical pressure (m)` : Pression critique à partir de laquelle la bouche sera soulevée.
- **Bouche d'avaloir** (`Drainage inlet`), caractérisé par :
  - `Inlet width (m)` : largeur de la grille d'avaloir,
  - `Height width (m)` : hauteur de la grille d'avaloir.
  
<img src="/model_building/network/manhole.png" width="500px"/>
  
  
  
### Les collecteurs

Un **tronçon** de collecteur est défini par les cotes radier amont et aval et sa géométrie. Il est créé via le bouton <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line) puis sélection des regards amont et aval.

La géométrie d’un collecteur peut être de 4 types différents :
- Circulaire (`Circular`),
- Ovoïde (`Ovoïd`),
- Paramétrique fermé (`Pipe`),
- Paramértique ouvert (`Channel`).


> **note**
>
> Très souvent les modèles de réseaux d’assainissement intègrent des tronçons de collecteurs très courts qui représentent en fait des galeries de liaison. Afin d’alléger les modèles et limiter le nombre de branches, hydra offre la possibilité de convertir un tronçon en simple liaison de type link (raccordée à un nœud de type manhole à chaque extrémité) via l'option *Exclude_from_branches* de l'éditeur de collecteurs <img src="/model_building/network/pipe_line.png"/> ([Pipe](/model_building/network/pipe_line)).
> 
> 
> 


> **note**
>
> Des outils permettent d'insérer et de supprimer des regards sur un collecteur :
> - <img src="/model_building/network/insert_node_button.png"/> : insertion d'un regard sur un collecteur existant. Le collecteur est coupé au droit du regard inséré; la géométrie est reprise du collecteur initial et les cotes radier interpolées linéairement.
> - <img src="/model_building/network/delete_node_button.png"/> : suppression d'un regard sur un collecteur existant. Les deux collecteurs associés au regard supprimé sont fusionnés; la géométrie du collecteur amont est automatiquement affectée au nouveau collecteur.






<a name="network_overflow"></a>

## Gestion des débordements

Les débordements sont générés lorsque le niveau d’eau dans un regard atteint la chaussée (cote TN). Le débit de débordement est calculé selon une loi d’orifice définie par une section d’orifice paramétrable. Le volume débordé est considéré comme perdu par le modèle, sauf si le regard est connecté à un réseau de surface par une liaison de type <img src="/model_building/links/network_overflow_line.png"/> [Network overflow](/model_building/links/network_overflow_line). Ce point est détaillé au § [Liaisons](#liaisons).

Les liaisons entre les regards et les mailles de surface sont générées via le bouton <img src="/model_building/network/network_overflow_button.png"/>





<a name="free_surface_flow"></a>

# Rivières et écoulements à surface libre

## Introduction

Les objets de modélisation et outils de construction associés au domaine rivières et écoulements à surface libre sont les suivants :    

<img src="/model_building/river_free_surface_flow/_river_button_ui.png" width="350px"/>

Hydra propose différents schémas de modélisation des écoulements de rivière, et de façon plus générale à surface libre (lit majeur, secteurs urbains soumis aux débordements de réseaux, ...), adaptés à l’échelle de travail et aux systèmes étudiés, tout en permettant leur couplage :
- **filaire**, ou unidimensionnel (1D) : cours d’eau (lits mineur et / ou majeur, canaux, tronçons couverts), représentés par des sections géométriques agencées le long d’un axe préférentiel d’écoulement ; les vitesses d’écoulement sont imposées le long de cet axe,
- **bidimensionnel (2D)** : le terrain naturel et / ou la bathymétrie sont schématisés par un maillage triangulaire ou quadrangulaire, chaque maille étant caractérisée par sa surface et la cote moyenne du terrain naturel,
- **casiers** : zones de stockage présentant de faibles vitesses d’écoulement et de faibles variations de hauteurs d’eau, caractérisés par une loi de remplissage cote / volume,
- **rues** : axes potentiels privilégiés des écoulements dans un environnement urbain dense et/ou régulier, assimilées à un schéma filaire,    

Ces différents schémas et les objets de modélisation associés sont présentés dans les chapitres suivants. Ils peuvent être connectés entre eux par des [liaisons](#liaisons).


Dans le cas de modèles surfaciques comportant plusieurs milliers de mailles et de liaisons il est illusoire de tenter de construire et de renseigner manuellement chaque objet de modélisation. 
Hydra dispose pour cela d’un **outil de génération automatique** très performant permettant, à partir du tracé par l’utilisateur de simples polylignes, des **lignes de contrainte** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> `Constrain`, de générer automatiquement les maillages 2D ainsi que les liaisons connectant ces différents domaines.

Ces outils sont présentés dans le chapitre suivant.


<a name="conceptualisation"></a>

## Conceptualisation du territoire à modéliser pour la génération du maillage 2D, de liaisons en lot et la cartographie avancée des zones inondables

### Objectifs

La construction des modèles, notamment pour les écoulements de surface, s’appuie sur des méthodes et moyens de conceptualisation permettant de passer d’un schéma de modélisation très général défini par l’utilisateur à la génération automatisée des objets de modélisation associés à cette représentation.

Cette méthodologie permet au modélisateur de se focaliser sur la conceptualisation du schéma de modélisation, découlant directement de sa compréhension du fonctionnement physique du système, et d’automatiser la génération des objets de modélisation associés.

Les écoulements de surface sont avant tout contraints par des marqueurs physiques du terrain naturel, que le modélisateur se doit d’identifier afin de définir les schémas de modélisation les plus adaptés au contexte géographique et aux attendus associés à la modélisation mise en œuvre :
- structures du système alluvial au sens hydro géomorphologique : lit mineur, délimité par ses berges, lit moyen accueillant les crues fréquentes, lit majeur qui accueille les crues rares à exceptionnelles
- Obstacles aux écoulements ( remblais, digues, …) et ruptures de pente,  
- Axes préférentiels d’écoulement,  
- Fossés de drainage et de ressuyage des plaines alluviales,  
- Occupation du sol : végétation, bâti, …

La construction des modèles passe ainsi par une première étape préalable de délimitation de l’ensemble de ces espaces ou marqueurs, via des **lignes de contraintes** <img src="/model_building/river_free_surface_flow/constrain_line.png"/>.

<a name="constrain"></a>

### Lignes de contraintes et coverages

Les **lignes de contraintes** sont matérialisées par des polylignes créées par l'utilisateur, via le bouton <img src="/model_building/river_free_surface_flow/constrain_line.png"/>; leurs fonctions sont multiples :
- délimitation des **frontières extérieures des différents domaines de modélisation**. La juxtaposition de lignes de contraintes contiguës se refermant sur elles-mêmes permet de délimiter des **coverage**, surfaces polygonales auxquelles sont affectés des schémas de modélisation donnés. Ces polygones sont créés dynamiquement par l'application lors de l'enregistrement des lignes de contraintes. Les modalités de création de ces domaines et des entités associées sont détaillées dans les chapitres suivants :
  - `reach` : domaine 1D (§ [Domaine filaire](#river_and_surface_1d_domain)),
  - `mesh` : domaine 2d et génération du maillage 2d (§ [Domaine 2D](#2d_domain)),
  - `storage` : domaine casiers (§ [Casiers](#storage)),
  - `street` : domaine rues (§ [Rues](#streets)),
- **automatisation de la génération de liaisons** entre domaines (§ [Liaisons](#liaisons)),
- génération des **analyses cartographiques** des résultats de calcul.





Pour chacune de ces lignes de contraintes, l’utilisateur définit :
- une **fonction** (type) :
  - **boundary** permettant de générer des liaisons d'un domaine 2D vers une condition limite aval,
  - **flood plain transect**  permettant de définir l’**orientation de la vallée** et de **générer des sections de vallée** pour les modèles filaires,
  - le **type de liaisons** hydrauliques associées le cas échéant, et leurs paramètres non géométriques : 2D, overflow,  porous.   
- une **taille objectif d’éléments de modélisation** (mailles 2D pour les domaines 2D, largeurs de liaisons d’échange entre deux domaines filaires, …).

<img src="/model_building/river_free_surface_flow/_constrain_ui.png" width="500px"/>


> **note**
>
> Une étoile jaune ( <img src="/model_building/river_free_surface_flow/unclosed_constrain.png"/> `Unclosed constrain`) matérialise les sommets des lignes de contraintes proches mais non fusionnées afin d'alerter l'utilisateur sur :
>   - la présence de sommets de deux lignes de contraintes très proches mais non fusionnées, ne permettant pas la création d'un coverage,
>   - la proximité de deux sommets d'une même ligne de contrainte susceptibles d'entraîner la création de mailles de très petite dimension.
>   
>   
>   **L'outil** <img src="/model_building/river_free_surface_flow/split_constrain.png"/> **permet de couper une ligne de contrainte.**
>   
>   
>   

<a name="river_and_surface_1d_domain"></a>

## Domaine filaire (ou 1D)

### Les objets de modélisation

Les objets de modélisation associés au domaine 1D sont les suivants :    

<img src="/model_building/river_free_surface_flow/_river_1d_button_ui.png"/>

### Principes de modélisation

Le bief  ( <img src="/model_building/river_free_surface_flow/reach_line.png"/> [Reach](/model_building/river_free_surface_flow/reach_line)) constitue l’ossature de ce domaine : c’est une polyligne représentant l’axe du cours d’eau. Cet objet doit être tracé avant tous les autres. Deux nœuds extrémités ( <img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point)) sont automatiquement générés  lors de la création du bief.  

Après sélection du bouton <img src="/model_building/river_free_surface_flow/reach_line.png"/> [Reach](/model_building/river_free_surface_flow/reach_line), l'utilisateur dessine la polyligne matérialisant le bief de l'amont vers l'aval.

Les données attributaires d’un bief sont :
- son nom
- le PK origine du bief (0.0 par défaut)
- le pas de discrétisation (50m par défaut)

On peut ensuite poser des nœuds ( <img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point)) intermédiaires  le long du bief; ces nœuds servent de support aux profils de sections, aux singularités et aux liaisons latérales.


> **note**
>
>  les outils <img src="/model_building/river_free_surface_flow/merge_reach_button.png"/> et <img src="/model_building/river_free_surface_flow/split_reach_button.png"/> permettent respectivement d'assembler deux reach ou d'en séparer un en deux.
> 
> 

La géométrie d’un bief est définie par les objets <img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point) posés sur les nœuds.


Les extrémités des biefs :
- portent des [conditions aux limites](/model_building/#boundaries),
- ou sont connectées à un ou plusieurs autres biefs par des [liaisons](#liaisons).


<img src="/model_building/river_free_surface_flow/_1d_general.png" width="900px"/>


> **warning**
>
>
>Du fait de la définition de la géométrie, une cross section doit être définie aux 2 nœuds extrémités d’un reach. Par ailleurs les extrémités de deux biefs différents ne peuvent pas être fusionnés , ils doivent être connectés par un link; Il y a donc là une importante différence avec les règles d’agencement des tronçons d’assainissement.
>

### Géométries de sections et règles d'agencement

La géométrie d’une branche filaire est renseignée au niveau des objets <img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point), posés sur des nœuds de rivière <img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point). Pour chacun de ces objets on peut définir une section amont et une section aval, afin de prendre en compte les discontinuités de géométries.

Après création de l'objet  <img src="/model_building/river_free_surface_flow/river_cross_section_point.png"/> [Cross section](/model_building/river_free_surface_flow/river_cross_section_point), **l'éditeur** de création et de paramétrage des **géométries de section** s'ouvre :

![_cross_section_ui.svg](/model_building/river_free_surface_flow/_cross_section_ui.svg)

> Cette interface permet de :
> - définir les **paramètres associés à l'objet cross section** :
		- choix d'un type de géométrie de section et position de cette dernière (mont ou aval du noeud)
    - strickler,
    - cote de radier.
> - **créer des géométries de section** qui sont appelés par l'objet cros section. Les géométries de section peuvent également être créées depuis la **bibliotèque de sections** : <img src="/tool_bar/geom.png"/> `Geometries library` de la barre d'outils hydra

**Cinq types différents de sections géométriques sont disponibles :**
- les **4 sections de collecteurs** également disponibles pour les branches d’assainissement, <img src="/model_building/network/pipe_line.png"/> [tronçons de collecteur](/model_building/network/pipe_line) (la bibliothèque de sections est commune aux branches de rivière et d'assainissement)
  - **Circular** : collecteur circulaire, caractérisé par son diamètre
  - **Ovoïd** : collecteur ovoïde, caratérisé par ses diamètres bas et haut et par sa hauteur
  - **sections paramétriques**, définies par une courbe tabulée hauteur / largeur :
    - **Pipe** : collecteur paramétrique fermé
    - **Channel** : canal paramétrique ouvert   
    Une **cote radier** (*zf* upstream elevation et *zf* downstream elevation) et un **coefficient de strickler** sont affectés à chaque section.
    
    Pour les **sections paramétriques**, l'onglet **geométry** permet de sélectionner une géométrie existante ou d'en créer une nouvelle qui sera enregistrée dans la bibliothèque de sections.
- la section **valley** à lits composés (section de cours d'eau comprenant le lit mineur et les lits majeurs rives gauche et droite). Elle peut être construite manuellement, ou **générée à partir du MNT et/ou de semis de points bathymétriques**. Pour plus de précisions sur ces sections, se reporter à la section **[Valley cross section](/model_building/river_free_surface_flow/cross_section/valley_cross_section)**.


**Un bief peut ainsi être défini géométriquement comme suit :**

<img src="/model_building/river_free_surface_flow/_reach_sections_building_rules.png"  width="700px"/>


> **warning**
>
> - Une *geometry downstream* doit être nécessairement définie sur le **nœud amont** du bief
> - Une *geometry upstream* doit être nécessairement définie sur le **nœud aval** du bief
> - Si un seul profil est défini sur un point courant, il peut être posé indifféremment en amont (*upstream*) ou en aval (downstream).
> 

Le **positionnement des sections** (amont ou aval) et le choix du type de section se fait via les sélectionneurs en haut à droite de l'éditeur de *cross sections*.

<img src="/model_building/river_free_surface_flow/_cross_section_navigation_ui.png"/>

Les **flèches < >** permettent de **naviguer le long des cross section** d'une branche. Cet outil permet de créer et d'ajuster les sections d'une même branche sans sortir de l'éditeur (évitant ainsi l'édition systématique de chacun des objets).

La position de la section sur le reach ainsi que le profil en long du fond du lit sont présentés en bas de léditeur. L'encart en bas à droit présente une vue en plan locale au droit de la cross section.
<img src="/model_building/river_free_surface_flow/_cross_section_longitudinal_profile_navigation_ui.png"/>

<a name="1d_domain"></a>

### Délimitation du domaine surfacique 1D

La délimitation du domaine surfacique 1D (coverage de type `reach`) est nécessaire pour la génération automatique de liaisons vers d'autres domaines et les traitements cartographiques des résultats de calcul. Elle se fait via les lignes de contrainte <img src="/model_building/river_free_surface_flow/constrain_line.png"/> [Constrain](#constrain).

Ce domaine correspond à l'emprise du territoire couverte par une modélisation de type 1D à surface libre, par le biais de sections de type *vallée*. Il est délimité par :
- deux lignes de contrainte de type flood plain transect définissant les limites amont et aval du domaine,
- des lignes de contrainte délimitant les frontières latérales du domaine. L'utilisateur précise le type de liaisons associées à ces frontières le cas échéant (cf. [liaisons](#liaisons)); par défaut, des liaisons <img src="/model_building/links/overflow_line.png"/> [Overflow](/model_building/links/overflow_line) seront générées.


Un coverage est créé lorsque plusieurs lignes de contraintes juxtaposées délimitent un polygone. Ce coverage est de type **Reach** (1D) si il intersecte une portion de bief de type **vallée** (i.e. les sections amont et aval de cette portion de bief sont de type *Valley*).


> **Note**
>
>  les couches Valley et Channel peuvent être visualisées dans le gestionnaire de couche (groupe `River and free surface flow` du modèle actif). Elles matérialisent les tronçons de reach de type Valley ou Channel.
> 

Des flood plain transect intermédiaires peuvent être ajoutés :
- pour générer des géométries de sections (cf. [Valley cross section](/model_building/river_free_surface_flow/cross_section/valley_cross_section)),
- pour préciser l'orientation de la vallée. Ces lignes de contraintes complémentaires permettent de définir l'axe d'interpolation des cotes calculées sur le lit mineur en lit majeur pour le traitement cartographique des résultats de calcul ainsi que l'orientation des liaisons vers les domaines connexes le cas échéant. 

<img src="/model_building/river_free_surface_flow/_1d_coverage_building.png"  width="800px"/>



> **Warning**
>
> - Les lignes de contraintes doivent être fusionnées les unes aux autres sur leurs extrémités (notamment les flood plain transect et les lignes de contraintes latérales).
> - Les flood **plain transect** doivent être **orientées de la rive gauche vers la rive droite**.
> - Un coverage 1D ne doit intersecter qu'un seul reach.
> - Une attention particulière doit être apportée à la définition des Flood plain transect aux **extrémités des biefs et des tronçons de type vallée**. Sur l'exemple ci-dessous, à la transition entre un tronçon de type vallée et un tronçon de type collecteur circulaire, les contraintes devant délimiter le domaine 1D doivent être positionnées à l'extérieur des biefs de type vallée : en aval de la dernière section vallée et en amont de la dernière section couverte.
>
>
> 
> <img src="/model_building/river_free_surface_flow/_1d_coverage_rules.png"  width="600px"/>
> 

<a name="storage"></a>

## Casiers

Le casier est une zone d’expansion du lit majeur caractérisée par des vitesses d’écoulement généralement faibles et dont le contour s’appuie sur la topographie naturelle ou sur des obstacles artificiels à l’écoulement des eaux. Le casier est délimité par un contour polygonal de forme quelconque et caractérisé par une courbe de remplissage cote / surface.

Le **contour** du casier est défini par une ou plusieurs **lignes de contrainte** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> [Constrain](#constrain). Un **coverage** est créé, de type 2D par défaut. L'insertion d'un marqueur de casier <img src="/model_building/river_free_surface_flow/storage_marker.png"/> affecte un type casier au coverage.

<img src="/model_building/river_free_surface_flow/_storage_building.png" width="200px"/>

La loi de remplissage cote / surface du casier est calculée à partir du MNT à l'aide du bouton <img src="/model_building/river_free_surface_flow/_generate_storage_filling_curve_button_ui.png"/>.

<img src="/model_building/river_free_surface_flow/_storage_ui.png" width="400px"/>


<a name="2d_domain"></a>

## Domaine 2D

<a name="mesh"></a>

### Création du maillage

L’ossature d'un sous domaine 2D est la maille à 3 ou 4 nœuds (mesh element), caractérisé par :
- sa surface,
- une cote moyenne de fond,
- une cote basse de fond, calculée à partir des données fournies par les liaisons connectées à la maille.

La génération du maillage s'appuie sur les **lignes de contrainte** <img src="/model_building/river_free_surface_flow/constrain_line.png"/> [Constrain](#constrain). La juxtaposition de lignes de contrainte définit le contour d'un **coverage** qui est de type 2D s'il n'intersecte pas de *bief 1D* de type vallée, de *rue* (street) ou de *marqueurs casiers* ou *null*.

Les paramètres associés à la ligne de contrainte sont :
- La **largeur d'élément** (`element length`), qui correspond à la **largeur de la maille** attendue le long de la ligne de contrainte,
- Le **type de liaison** associé à la ligne de contrainte, pour la génération automatique des liaisons entre les éléments situés de part et d'autre de cette ligne de contrainte.


> **warning**
>
>  Le maillage s'appuie également sur les **sommets des lignes de contrainte**; leur espacement doit donc être cohérent avec la taille de mailles souhaitée et la longueur d'élément affectée à la ligne de contrainte pour ne pas créer de trop fortes distorsions dans le maillage. 
> 

Le maillage est généré via le bouton <img src="/model_building/river_free_surface_flow/mesh.png"/>, puis un clic dans le coverage considéré. Lors de cette étape, sont également créées :
- les liaisons entre mailles 2d situées à l'intérieur du coverage,
- les liaisons vers les autres domaines s'appuyant sur des lignes de contrainte communes (cf. [Liaisons](#liaisons)).


> **note**
>
>  Des lignes de contraintes peuvent être ajoutées à l'intérieur d'un coverage; le maillage s'appuiera alors sur ces lignes suivant le même principe que pour les frontières extérieures du coverage. Les liaisons entre les éléments situés de part et d'autre de ces lignes de contrainte seront également générées lors du maillage.
> 

Le maillage peut être ajusté, par modification des lignes de contrainte ou de leurs caractéristiques. Le maillage existant doit être préalablement supprimé via le bouton <img src="/model_building/river_free_surface_flow/delete_mesh.png"/> puis sélection du coverage à démailler. Toutes les liaisons générées automatiquement avec mailles supprimées sont également supprimées.

<img src="/model_building/river_free_surface_flow/_mesh_2d.png" width="900px"/>

Les outils suivants permettent de générer le maillage en lot :
- <img src="/model_building/river_free_surface_flow/mesh_unmeshed.png"/>  maillage des coverage non maillés,
- <img src="/model_building/river_free_surface_flow/mesh_regen.png"/>  maillage de l'ensemble des coverage 2d, y compris ceux déjà maillés le cas échéant.




### Affectation d'un nom de domaine

Un nom de domaine 2D (*2d domain*) peut être affecté aux mailles via le bouton <img src="/model_building/river_free_surface_flow/2d_domain_marker_point.png"/> , permettant de préciser les modalités d’algorithmes de calculs dans ce domaine.


<a name="streets"></a>

## Rues

Les objets de modélisation associés au domaine rue sont les suivants :    

<img src="/model_building/river_free_surface_flow/_street_button_ui.png"/>

- Les **carrefours** : <img src="/model_building/river_free_surface_flow/crossroad_point.png"/> [Crossroad](/model_building/river_free_surface_flow/crossroad_street) : carrefours, définis par leur surface d'emprise,
- Les **rues** : <img src="/model_building/river_free_surface_flow/street_line.png"/> [Street](/model_building/river_free_surface_flow/crossroad_street) : rues, définies par une largeur moyenne, un décaissé et un coefficient de strickler.

Les objets <img src="/model_building/river_free_surface_flow/street_line.png"/> **street** sont utilisés pour **automatiser** la construction des éléments associés au domaine *street*, et ne sont pas exploités pour le calcul.    

Ce domaine se présente comme un réseau maillé de tronçons de rues connectés à un carrefour à chaque extrémité, permettant de modéliser des débordements le long de rues. Des objets *Tronçons de rues*, reliant chacun des *Crossroad* le long des objets *Street* sont automatiquement générés  entre 2 carrefours par l’application pour le calcul, reprenant les caractéristiques de l'objet *Street*. 


> **note**
>
>  Une modification de géométrie sur une rue doit donc conduire à la séparer en deux objets *street* distincts.
> 

### Création des objets carrefour et rue, délimitation du domaine rue

L'objet <img src="/model_building/river_free_surface_flow/street_line.png"/> *rue* permet d'**automatiser la création des carrefours et des tronçons de rue** le long d'une voirie de largeur homogène. Après sélection du bouton <img src="/model_building/river_free_surface_flow/street_line.png"/>, l'utilisateur positionne la polyligne matérialisant la rue puis lui affecte une largeur et un coefficient de strickler.

Après validation, l'application crée :

- des **carrefours** <img src="/model_building/river_free_surface_flow/crossroad_point.png"/> [Crossroad](/model_building/river_free_surface_flow/crossroad_street) aux sommets de la polyligne *street*,
- des **tronçons de rue** reliant les *Crossroad*; ces objets, générés lors du calcul, ne sont pas visibles via l'interface.
- une **ligne de contrainte** autour de la rue, délimitant un coverage; un type *street* est affecté au coverage, qui contient au moins un objet *street*.

<img src="/model_building/river_free_surface_flow/_street_coverage_generation.png" width="500px"/>


Lorsque plusieurs street s'intersectant sont créées, l'application effectue un découpage des lignes de contraintes créées autour de chaque street. Une reprise manuelle des lignes de contrainte ainsi générées peut être ponctuellement nécessaire pour ajuster les limites de coverages.    

<img src="/model_building/river_free_surface_flow/_street_coverage_generation_2.png" width="800px"/>



### Création manuelle des carrefours

Les carrefours peuvent être créés manuellement via le bouton <img src="/model_building/river_free_surface_flow/crossroad_point.png"/> [Crossroad](/model_building/river_free_surface_flow/crossroad_street) en les posant le long d’un objet *street*.



<a name="runoff"></a>

# Ruissellement

## Introduction

Cette application permet d'affiner la connaissance des mécanismes de ruissellement, en calculant la pluie nette précipitée dans chaque maille d’un domaine 2D et en propageant la lame d’eau nette à travers les mailles du domaine 2D selon des lois hydrauliques adaptées. Ce calcul est réalisé dans la phase d’exécution hydraulique et non hydrologique : à chaque pas de temps de simulation le programme :

- calcule la lame d’eau brute précipitée à partir des données pluviométriques définies dans le gestionnaire de scénarios,
- calcule la lame d’eau ruisselée en exploitant un champ complémentaire de la table  *land occupation*.
- convertit la lame d’eau ruisselée en débit d’injection dans la maille,
- propage l'hydrogramme ainsi généré dans le modèle hydraulique. 

<img src="/model_building/runoff/runoff_results.png" width="450px"/>


Ce mode de représentation est particulièrement adapté aux secteurs urbains, et permet d'affiner le couplage réseau / surface en supprimant les points d'apports de débits concentrés à l'exutoire de sous bassins versants susceptible de sur-estimer ponctuellement les zones inondables.

## Principes de mise en œuvre


### Construction

Le calcul du ruissellement ne s'appuie que sur des mailles 2d (cf. [Maillage](#mesh)). Cependant il peut être nécessaire de coupler ce maillage 2d avec des éléments 1d:

- **Réseau d'assainissement** pour modéliser finement les interactions réseau / surface. Les liaisons *Regards - Mailles 2d* sont générées via l'outil <img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line).

- **Fossés** sur des secteurs plus ruraux; ces derniers peuvent être immergés dans le maillage (reach avec sections de type *Channel*), ou définir une limite au maillage (reach de type *Valley*).

<img src="/model_building/runoff/runoff_model_structure.png" width="450px"/>


> **note**
>
>  
> Lorsque les fossés ont une forme géométrique simple (rectangulaire dans la version actuelle) il est possible de les schématiser par des éléments « street »; cette schématisation présente en effet deux avantages :
> 
>   - elle permet de simplement régler le maillage autour des fossés, et donc de calculer plus finement les champs de vitesses au voisinage des fossés (la mise en œuvre plus simple qu'avec le reach et les sections valley),
>   - elle permet de visualiser les vitesses d’écoulement le long de l’axe des fossés.
>   
>   

Un nom de domaine doit ensuite être affecté à chaque coverage 2d via le bouton <img src="/model_building/river_free_surface_flow/2d_marker.png"/>.


L'affectation de la loi de production de pluie nette et des paramètres associés se fait via la table *land_type* et une jointure avec la la table *land_coccupation* qui contient les polygones d'occupation du sol correspondnt à chaque *land_type*.


**Etape 1** : **Importer une table d'occupation des sols** (*land_occupation*) via l’outil external_table_manager dans le menu <img src="/0_hydra_icone.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> **Advanced tools / Manage external tables** (cf. [Import_shape](/advanced/import_shape)).


<img src="/advanced/import_shape/import_shape_menu.png" style="transform: scale(0.8);"/>

Importer la table .shp d’occupation des sols dans le schéma work à l’aide du bouton <img src="/0_hydra_+.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/>



<img src="/model_building/hydrology/import_shape_land_occupation_ui.png" style="transform: scale(0.8);"/>



**Etape 2** : renseigner la table *land_type* en s'assurant de reprendre tous les types définis dans la table *land_occupation*

<img src="/model_building/hydrology/land_types_ui.png" width="600px"/>

Trois lois peuvent être implémentées :
    1. Coefficient de ruissellement constant
    2. SCS : il faut renseigner le paramètre J (RFU)
    3. Holtan : il faut renseigner les paramètres f0, fc et L
 
>Deux tables de land_type sont pré-paramétrées dans l'application, accessibles via le bouton *Load preset land type* : *mos_paris* et *Corine land cover*. 

### Paramétrage du scénario

L'appel du module ruissellement se fait via le formulaire suivant du menu <img src="/0_hydra_icone.png" height="25px"/> **scénarios / Settings / computation options**.

<img src="/model_building/runoff/scenario_settings_runoff.png" width="400px"/>

Il permet d’identifier les **domaines 2D** pour lesquels le calcul de ruissellement sera appliqué dans l’étape hydraulique d’exécution.


> **note**
>
>Un même modèle peut contenir à la fois des bassins versants et des domaines 2D intégrant du ruissellement :
>- Les hydrogrammes d’apports à l’exutoire des chaque BV sont calculés dans la phase de calcul hydrologique,
>- Les apports de ruissellement dans un domaine 2D déclaré dans le formulaire ci-dessus sont calculés dans la phase de calcul hydraulique.


Les données pluviométriques sont appelées dans l'onglet *Hydrology* du gestionnaire de scénario. Le module de calcul de ruissellement reconnaît tous les types de pluies à l’exception des pluies radars.


<a name="stations"></a>

# Stations de gestion

La station de gestion est utilisée pour modéliser des usines ou des ouvrages électro mécaniques; elle est matérialisée par un contour polygonal <img src="/model_building/station/station_polygon.png"/> entourant des chambres <img src="/model_building/station/station_node_point.png"/> [Station node](/model_building/station/station_node_point) connectées entre elles par des liaisons latérales ([links](#liaisons)).
- le contour est créé via le bouton <img src="/model_building/station/station_polygon.png"/> [Station gestion](/model_building/station/station_polygon),
- les chambres sont crées dans l'emprise d'un contour de station de gestion via le bouton <img src="/model_building/station/station_node_point.png"/> [Station node](/model_building/station/station_node_point)

Un contour polygonal de station ne peut communiquer avec le réseau externe que par l’intermédiaire de liaisons latérales : aucun collecteur ne peut traverser un contour de station.

<img src="/model_building/station/_station_example.png" width="800px"/>


<a name="ouvrages"></a>

# Ouvrages

Les ouvrages (ou singularités) sont posés sur des nœuds : <img src="/model_building/river_free_surface_flow/river_node_point.png"/> [River node](/model_building/river_free_surface_flow/river_node_point) et <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point), via les boutons associés. Ce sont en pratique des ouvrages ou des singularités géométriques qui créent une perte de charge le long d’une branche ou d’un bief.



> **warning**
>
>  
> - On ne peut poser qu’une seule singularité sur un nœud,
> - Il est interdit de poser une singularité sur un nœud extrémité de branche.
> 

Les ouvrages disponibles sont les suivants : 

<img src="/model_building/structures/_strucutres_button_ui.png"/>

- 1. <img src="/model_building/structures/gate_point.png"/> [Gate](/model_building/structures/gate_point) : **vanne**
- 2. <img src="/model_building/structures/zregul_weir_point.png"/> [Weir](/model_building/structures/zregul_weir_point) : **déversoir frontal** (seuil), intègre une loi de régulation exprimée par la tenue d’une cote d’eau constante en amont immédiat du déversoir tant que celui-ci dispose d’une pelle suffisante.
- 3. <img src="/model_building/structures/borda_headloss_point.png"/> [Borda](/model_building/structures/borda_headloss_point) : **perte de charge de type Borda** $KV^2/2g$, offre en réalité une dizaine de lois différentes de pertes de charge. C’est donc un objet multifonctions, les lois de pertes de charge reposant pour beaucoup sur les abaques expérimentales tirées du memento d’Idelsik.
- 4. <img src="/model_building/structures/param_headloss_point.png"/> [parametric headloss](/model_building/structures/param_headloss_point) : perte de charge paramétrique, définie par une loi tabulée dz(Q).
- 5. <img src="/model_building/structures/bradley_headloss_point.png"/> [Bradley](/model_building/structures/bradley_headloss_point) : **ouvrage de franchissement**, ne s'appliquant qu'aux nœuds de rivière. Il reprend la formulation Bradley et s’applique à un ouvrage de franchissement de cours d’eau en prenant en compte l’effet d’obstruction des piles et des culées; une loi d'orifice est appliquée lorsque l'ouvrage se met en charge. 
- 6. <img src="/model_building/structures/bridge_headloss_point.png"/> [Bridge](/model_building/structures/bridge_headloss_point) : **ouvrage de franchissement**, ne s'appliquant qu'aux nœuds de rivière. Il décrit la courbe d’ouverture largeur / cote d’un ouvrage de franchissement, et permet de calculer précisément les pertes de charge tenant compte de la contraction créée par l'ouvrage.
- 7. <img src="/model_building/structures/regul_sluice_gate_point.png"/> [Regulated gate](/model_building/structures/regul_sluice_gate_point) : **vannage régulé**
La vanne régulée et la vanne simple (<img src="/model_building/structures/gate_point.png"/>) ont des caractéristiques géométriques identiques. La vanne régulée autorise une régulation en cote ou en débit, on peut également lui imposer un fonctionnement manuel ; on se retrouve alors dans le cas de la vanne manuelle. On aurait très bien pu supprimer la vanne manuelle de la liste des objets puisque son fonctionnement est couvert par celui de la vanne régulée. Les deux objets sont néanmoins conservés pour bien distinguer un ouvrages statique d’un organe actif régulable.  
- 8. <img src="/model_building/structures/hydraulic_cut_point.png"/> [Hydraulic cut](/model_building/structures/hydraulic_cut_point) : **coupure hydraulique**, permet de décrite sous forme tabulée z(q) une loi de déversement d’un ouvrage en régime dénoyé. Cet objet a été conservé dans cette version Hydra pour des raisons de compatibilité avec des modèles antérieurs  mais son intérêt  pratique demeure limité désormais.
En effet Hydra génère automatiquement une coupure à chaque profil pour lequel deux sections géométriques sont définies. Cette coupure  n’est  toutefois pas générée si une singularité est posée sur le nœud portant le profil. 
- 9. <img src="/model_building/structures/marker_point_point.png"/> [Point marker](/model_building/structures/marker_point_point) : **marqueur de point**, permet de « tagger » un nœud pour générer des sorties ponctuelles et pour couvrir également des fonctionnalités optionnelles intéressantes, comme celle d’imposer un débit à un nœud de calcul quelconque.



> **note**
>
>   - Pour la plupart de ces objets, les lois perte de charge dE(Q) sont exprimées par rapport à la charge totale ($z+v^2/2g$) et non par rapport à la cote : les termes cinétiques sont bien intégrés dans les formulations.
>   - Pour les calculs, ces singularités sont transformées en interne en liaisons binodales alignées le long du bief ou de la branche :
> 
> <img src="/model_building/structures/_structures_computation_decomposition.png" width="600px"/>
>  

<a name="liaisons"></a>

# Liaisons


### Rôle des liaisons

  Les liaisons permettent de connecter les différents objets de modélisation entre eux, soit n’importe quel nœud de n’importe quel domaine à l’exception des nœuds hydrologiques :
  - les manhole <img src="/model_building/network/manhole_point.png"/>,
  - les river node <img src="/model_building/river_free_surface_flow/river_node_point.png"/>,
  - Les station node <img src="/model_building/station/station_node_point.png"/>,
  - Les  mesh element 
  - Les storage <img src="/model_building/river_free_surface_flow/storage_marker.png"/>,
  - les crossroads <img src="/model_building/river_free_surface_flow/crossroad_point.png"/>.
  
  
Des restrictions sont appliquées sur les règles de connexions de certaines liaisons, détaillées ci-après. 


> **note**
>
> Les liaisons peuvent être créées avant la création des nœuds qui les porteront; elles devront cependant toutes être raccordées à un nœud lors de la génération du calcul.
> 
> Cette relaxation de la contrainte de connectivité des liaisons lors de la phase de construction du modèle permet :
> - de positionner des liaisons et de renseigner leurs caractéristiques avant que le schéma de modélisation soit totalement défini (lors du recensement des ouvrages sur le terrain ou de la préparation des levés topographiques par exemple),
> - de conserver leurs caractéristiques si le nœud qui les porte devait être supprimé (actualisation du maillage 2D, modification de l'agencement de carrefours, ...).
> 



### Liaisons disponibles

Les liaisons disponibles sont les suivantes

- 1. <img src="/model_building/links/gate_line.png"/> [Gate link](/model_building/links/gate_line) : **vanne**
- 2. <img src="/model_building/links/regul_gate_line.png"/> [Regulated gate link](/model_building/links/regul_gate_line) : **vannage régulé**
- 3. <img src="/model_building/links/gate_line.png"/> [Weir link](/model_building/links/weir_line) : **seuil déversant**
Les vannes et seuils déversants (1,2 et 3) ont les même caractéristiques que les singularités correspondantes, à l'exception des seuils : la liaison correspond à un seuil fixe alors que la singularité est régulée.
- 4. <img src="/model_building/links/pump_line.png"/> [Pump link](/model_building/links/pump_line) : **maillage par pompage**, permet de modéliser de façon très complète le fonctionnement d’une station de pompage, avec plusieurs pompes en parallèle et une régulation de chaque pompe de type marche-arrêt en fonction d’un niveau bas et d’un niveau haut.
- 5. <img src="/model_building/links/deriv_pump_line.png"/> [Deriv pump link](/model_building/links/deriv_pump_line) : **dérivation par pompage**, loi de pompage simplifiée sous la forme d’un courbe tabulée qpomp(z)
- 6. <img src="/model_building/links/borda_headloss_line.png"/> [Borda link](/model_building/links/borda_headloss_line) : **perte de charge de type Borda** $KV^2/2g$, similaire à la singularité Borda (elle intègre plus de lois de pertes de charge, 20 au total).
- 7. <img src="/model_building/links/connector_line.png"/> [Connector link](/model_building/links/connector_line) : **connecteur**, liaison de raccord très utilisée car elle permet de **propager des flux sans perte de charge** ni déphasage.Elle sert par exemple à raccorder un réseau à des chambres de station de gestion, ou des branches filaires entre elles.
- 8. <img src="/model_building/links/strickler_line.png"/> [Strickler link](/model_building/links/strickler_line) : **liaison frottement**, modélise une perte de charge linéaire; son usage tend à se limiter compte des fonctions similaires assurées par d’autres liaisons, mais elle s’avère utile dans certains cas de raccordement latéral entre deux domaines distincts.
- 9. <img src="/model_building/links/porous_line.png"/> [Porous link](/model_building/links/porous_line) : **liaison poreuse**, sert à modéliser des fuites à travers une digue perméable.
- 10. <img src="/model_building/links/overflow_line.png"/> [Overflow link](/model_building/links/overflow_line) : **surverse**, composée de deux seuils parallèles matérialisant deux niveaux différents de surverse le long d’une même frontière. Elle est notamment utilisée pour connecter :
    - un cours d’eau à un domaine 2D,
    - une rue à un domaine 2D,
    - les mailles d’un domaine 2D au franchissement d’une discontinuité topographique.
  Cet objet dispose par ailleurs d’un **module de rupture** paramétrable, permettant de modéliser des ruptures de berges ou de digues.
  
- 11. <img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line) : **surverse depuis un réseau**, obligatoirement connectée à un nœud de type manhole à l’extrémité amont et à un carrefour ou une maille 2D à son extrémité aval. Elle est utilisée pour simuler les échanges de débit entre un regard d’assainissement et un réseau de surface en cas d’inondation de la surface ou de débordement par le regard. Les différents cas de fonctionnement sont illustrés ci-dessous.

<img src="/model_building/links/_network_overflow_scheme.png" width="400px"/>

L'activation de la liaisons est conditionnée par le type de regard (tampon ou avaloir; cf. [manhole_type]).

- 12. <img src="/model_building/links/interlink_line.png"/> [Inter link](/model_building/links/interlink_line) : **Inter link**, connecte les éléments d'un modèle de réseau aux éléments d'un modèle d'écoulements de surface sur le même principe que la liaison <img src="/model_building/links/network_overflow_line.png"/> [Network overflow link](/model_building/links/network_overflow_line), mais entre deux modèles distincts (respectivement un modèle réseau et un modèle surface).

- 13. <img src="/model_building/links/mesh_2d_line.png"/> [Mesh 2d link](/model_building/links/mesh_2d_line) : **liaison 2d**, connecte les mailles d’un domaine 2D entre elles. 


### Deux types de liaisons peuvent être distingués

- Les liaisons 1 à 6, qui modélisent des ouvrages ponctuels dont les caractéristiques ne peuvent être déduites de la seule topographie : ouvrages de décharge, stations de relevage, ... et doivent être **créées manuellement**,
- Les liaisons 7 à 13, qui peuvent être **générées automatiquement** : liaisons dont les caractéristiques sont uniquement liées à la topographie et à la géométrie des objets auxquels elles sont rattachées : liaisons de déversement sur des digues, berges ou infrastructures en remblai de grande longueur (cf. [Création des liaisons en lot](#links_multi)); ces liaisons peuvent également être créées manuellement.


## Création de liaisons individuelles

Les liaisons sont créées par sélection de l'outil associé à la liaison considérée, puis sélection des objets amont et aval.

<img src="/model_building/links/_links_button_ui.png"/>

Des sommets complémentaires peuvent être ajoutés sur la polyligne matérialisant la liaison, via les outils d'édition topologique de QGIS après avoir rendu la couche correspondante modifiable dans le gestionnaire de couches.


<a name="links_multi"></a>

## Création des liaisons en lot

### Principes

La génération des liaisons en lot passe par la définition de **coverage**, polygones délimitant les frontières des différents domaines (et sous domaines) de modélisation entre eux.

Les coverage sont définis par juxtaposition de lignes de contrainte contiguës et fusionnées à leurs extrémités (cf. [conceptualisation]). Deux domaines frontaliers s'appuient sur une ou plusieur ligne(s) de contrainte commune(s).

Les méthodes de construction des covrages sont décrites dans les chapitre spécifiques à chacun d'entre eux : [domaine 1D](#1d_domain), [casiers](#storage), [domaine 2D](#2d_domain), [rues](#streets)

<img src="/model_building/links/_demo_links_1.png" width="650px"/>

### Données topographiques utilisées pour le calcul des paramètres des liaisons

Lors de la génération de liaisons en lot, l'application calcule automatiquement leurs paramètres géométriques :

- largeur (fonction du pas de discrétisation de la ligne de contrainte frontière),
- cotes caractéristiques (fonction du type de liaison).

Les données topographiques utilisées pour le calcul des cotes caractéristiques sont :

- Par défaut celles fournies par le **MNT** préalablement chargé,
- Des **semis de points topographiques** vectoriels si ces derniers ont préalablement été associés à la ligne de contrainte support. Cette possibilité est utile dans le cas :
  - d’obstacles de faible largeur pour lesquels le MNT ne peut pas fournir l’information requise (murettes par exemple),
  - de simulations de projets d'ouvrages caractérisés par des points vectoriels (murettes ou endiguements par exemple).
  
  L'utilisation de semis de points vectoriels nécessite 2 étapes préalables :
  
  - **Etape 1**: importer le semis de points topographiques dans la table *terrain points* du schéma Project et leur affecter un nom de groupe (cf.[Données topographiques](/topographic_bathymetric_data_management)).

  - **Etape 2**: associer le groupe de points défini ci-dessus à la ligne de contrainte support des liaisons:
    - éditer l’objet ligne de contrainte avec le bouton d’édition d’Hydra,
    - se placer dans le sous menu **3D terrain points**,
    - affecter le groupe souhaité à la ligne de contrainte ; cette action génère une polyligne 3D par projection sur la ligne de contrainte des points les plus hauts situés dans un buffer autour de la ligne de contrainte (longueur définie par le paramètre *Points proximity (m)*), 
    
    Lors de la génération des liaisons (associées au maillage ou à l'aide de l'outil <img src="/model_building/river_free_surface_flow/constrain_links.png"/>, opérations décrites ci-après) les points topographiques seront utilisées en priorité sur le MNT.
    
<img src="/model_building/river_free_surface_flow/_constrain_3d_terrain_points_ui.png" width="450px"/>
    
    

> **note**
>
> Il est possible de mettre à jour les cotes des liaisons générées le long d'une ligne de contrainte en cliquant sur le bouton **Update links** du menu *3D terrain points*.
>     
>     


### Liaisons associées à un maillage 2D

Les liaisons internes à un coverage 2d sont générées avec le maillage <img src="/model_building/river_free_surface_flow/mesh.png"/>.


Les lignes de contrainte peuvent être communes à deux domaines de même type ou de type distinct. Lors du maillage d'un coverage, les liaisons vers les éléments des domaines frontaliers sont également créées automatiquement.

### Autres liaisons entre coverage

L'outil <img src="/model_building/river_free_surface_flow/constrain_links.png"/> permet de générer des liaisons entre les coverage autres que mesh (2d).

Après sélection de l'outil <img src="/model_building/river_free_surface_flow/constrain_links.png"/>, sélectionner le coverage amont puis le coverage aval; les liaisons sont créées par l'application (si un des coverage est de type reach, le sélectionner en premier).



<a name="network_overflow_links"></a>

### Liaisons de **débordement de réseaux** vers les rues et les mailles 2d

Dans les applications urbaines les modèles sont très souvent constitués par des réseaux souterrains filaires interconnectés à un modèle de surface occupé par des mailles 2D et des rues.

Hydra permet de connecter un réseau à un modèle de surface appartenant à un même modèle ou à deux modèles distincts (un de réseau et un de surface), pour plus de lisibilité et de flexibilité d’exploitation.


-  **Liaisons entre des objets d'un même modèle**

    L'outil <img src="/model_building/network/network_overflow_button.png"/> [Network overflow link](/model_building/links/network_overflow_line) permet de générer les liaisons Network overflow, entre les regards des réseaux d'assainissement et les domaines de surface mesh (2d) et street.

    L'activation de l'outil régénère l'ensemble des liaisons du modèle :
    - vers les **mailles 2d** contenant les regards : une liaison  de type network overflow est créée entre le manhole et la maille correspondante,
    - vers les **coverage street** contenant les regards : une liaison  de type network overflow est créée entre le manhole et le carrefour (crossroad) le plus proche.

<a name="interlink"></a>

- **Liaisons inter-modèles** : liaisons entre les objets d'un modèle réseau et d'un modèle de surface distincts

    Considérons un projet deux modèles différents comme illustré ci-après :
    - un modèle de surface
    - un modèle de réseau sous-jacent.

    <img src="/model_building/links/interlinks_scheme.png" width="550px"/>

    Pour créer des liaisons inter-modèles cliquer sur le bouton « inter links » puis cliquer à l’intérieur du coverage, après avoir sélectionné le modèle « surface » comme le modèle actif :

    <img src="/model_building/links/interlink_button.png" width="300px"/>

    Cette opération crée des liaisons inter-modèle <img src="/model_building/links/interlink_line.png"/> [Interlink](/model_building/links/interlink_line). Ces liaisons sont stockées dans la table interlinks du schéma Projet. Les règles de génération sont les mêmes que dans le cas d’un seul réseau. Elles sont applicables à des coverage 2D, des coverage streets et des coverage storage.


    <img src="/model_building/links/interlink_layer.png" width="550px"/>


    La liaison interlinks ne possède aucun attribut physique. Elle est traitée en interne comme une liaison de type connector. La table interlink contient les enregistrements suivants :

    <img src="/model_building/links/interlink_database.png" width="500px"/>



    > **note**
    >
    >
    >Cette procédure s’avère très utile dans le cas de gros modèles comprenant plusieurs réseaux >différents :
    >- elle permet de construire le modèle simultanément avec des équipes différentes, chaque équipe prenant en charge un sous modèle réseau ou un sous modèle de surface. L’assemblage des sous modèles n’est effectué qu’après calage et validation de chaque sous modèle,
    >- lors de l’étape de calage les sous modèles peuvent être testés séparément, ce qui simplifie considérablement ce travail de calage.
    >

### Liaisons entre un **maillage 2d** et un **reach de type channel**

Cette fonctionnalité permet d'immerger un bief 1d dans le maillage 2D sans que celui-ci s'appuie sur les berges du cours d'eau. Elle est notamment utilisée pour modéliser des fossés de faible largeur au regard de la taille des mailles 2d afin de représenter précisément le ressuyage du lit majeur.
    Ces liaisons sont générées lors du maillage. Une liaison est générée depuis le reach portant des sections de type *channel* vers chacune des mailles 2d qu'il intersecte.


> **note**
>
>  la couche Channel peut être visualisée dans le gestionnaire de couche (groupe *River and free surface flow* du modèle actif). Elle matérialise les tronçons de reach de type *channel*.   
> 
> 
> <img src="/model_building/links/_channel_mesh_links.png" width="500px"/>
> 

### Liaisons entre un **maillage 2d** et une condition limite

Ces liaisons, de type [connector](/model_building/links/connector_line), sont générées via l'outil <img src="/model_building/river_free_surface_flow/constrain_links.png"/>. Cliquer **d'abord** sur la ligne de contrainte de frontière (type **Boundary**) ou sur une rue (type **Street**), puis sélectionner un nœud de station de gestion ( <img src="/model_building/station/station_node_point.png"/> [station node](/model_building/station/station_node_point)).

<img src="/model_building/links/_station_node_meh_link.png" width="300px"/>

> **note**
>
>  Attention à la distance d'accrochage en particulier sur les ecrans haute résolution.  Elle peut être configurée dans le menu hydra/settings.
>  
> 

### Synthèse

L'image ci-dessous présente les éléments générés à partir des différents outils disponibles pour l'exemple présenté en début de chapitre.

<img src="/model_building/links/_demo_links_2.png" width="650px"/>

Le tableau ci-dessous synthétise les liaisons inter-domaines pouvant être générées ainsi que l'outil associé.


    

|  | Mesh | 1d | Street | Storage | Network | Channel | Station |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Mesh | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/network/network_overflow_button.png"/> | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/mesh.png"/>|
| 1d | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | - | - | - |
| Street | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/network/network_overflow_button.png"/> | -  | - |
| Storage  | <img src="/model_building/river_free_surface_flow/mesh.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | -  | -  | - |
| Network | <img src="/model_building/network/network_overflow_button.png"/> | -  | <img src="/model_building/network/network_overflow_button.png"/> | -  | - | -  | - |
| Channel  | <img src="/model_building/river_free_surface_flow/constrain_links.png"/> | - | - | - | - | - | - |
| Station | <img src="/model_building/river_free_surface_flow/mesh.png"/> | - | - | - | - | - | - |

<a name="boundaries"></a>

# Conditions aux limites

## Définition

Les conditions aux limites (*Boundary conditions*) sont des liaisons uninodales, d’une utilisation très générale comme pour les links. Ce  sont des liaisons connectées à n’importe quel noeud à leur extrémité amont et à un nœud  fictif externe à leur extrémité aval :

- Si le flux est positif, il sort du modèle et il est donc perdu.
- S’il est négatif , c’est un apport qui vient enrichir le modèle.

Chacune de ces liisons est attachée à un noeud, et est donc posée sur celui-ci comme pour le cas  d’une singularité .

Si une liaison uninodale est posée sur le nœud aval d’un branche d'assainissement (*branch*) ou d’un bief de rivière (*reach*), elle est assimilée à une **condition limite aval**. Mais cette notion de liaison uninodale recouvre une signification et un emploi beaucoup plus général puisqu’elle s’étend à l’ensemble des noeuds (containeurs) du modèle, à l’exception toutefois des nœuds hydrologiques.


> **warning**
>
>  Un nœud ne peut accueillir au plus qu’une seule liaison uninodale ou une seule singularité.
> 

## Objets de modélisation

Les conditions limites disponibles sont les suivantes : 

<img src="/model_building/boundary_conditions/_boundaries_button_ui.png"/>

- 1. <img src="/model_building/boundary_conditions/constant_inflow_bc_point.png"/> [Constant inflow](/model_building/boundary_conditions/constant_inflow_bc_point) : **débit constant**, permet d’injecter un débit constant en tout point du modèle.
- 2. <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph](/model_building/boundary_conditions/hydrograph_bc_point) : **hydrogramme**, élément de modélisation composite qui fournit tous les **apports au modèle hydraulique**:
  - Hydrogrammes :
    - Hydrogramme imposé
    - Hydrogramme externe:
      - défini par une **courbe Q(t)** appelée dans le gestionnaire de scénario
      - issu des apports d'un **réseau hydrologique** (via une ou plusieurs liaisons *Hydrolgy routing* connectées au nœud portant l'objet Hydrogramme),
  - Apports variables de **temps sec** en relation avec les concepts de secteurs d’apports.
> **note**
>
>  Pour plus de précisions, se reporter au chapitre [Gestion des données hydrologiques et hydro climatiques](/hydrology_data_management)
>   

- 3. <img src="/model_building/boundary_conditions/froude_bc_point.png"/> [Froude](/model_building/boundary_conditions/froude_bc_point) : condition aval de type **chute (Froude)**, modélise les déversements libre en aval de branche ou de bief.
- 4. <img src="/model_building/boundary_conditions/strickler_bc_point.png"/> [Strickler](/model_building/boundary_conditions/strickler_bc_point) : condition aval de type **strickler**, modélise un écoulement uniforme  en aval de branche ou de bief.
- 5. <img src="/model_building/boundary_conditions/zq_bc_point.png"/> [Z(Q)](/model_building/boundary_conditions/zq_bc_point) : **loi z(q)**,  modélise un écoulement  de type courbe de tarage en aval de branche ou de bief.
- 6. <img src="/model_building/boundary_conditions/tz_bc_point.png"/> [Z(t)](/model_building/boundary_conditions/tz_bc_point) : **loi z(t)**, modélise un niveau d’eau fixé à un nœud ; ce peut être une courbe de marée par exemple.

<img src="/model_building/boundary_conditions/_sea_level_ui.png" width="450px"/>

Cette courbe peut être définie dans la fenêtre de l’objet ou via un fichier externe. Si l’option *cyclic* est cochée, la courbe est répétée cycliquement.

- 7. <img src="/model_building/boundary_conditions/weir_bc_point.png"/> [Weir](/model_building/boundary_conditions/weir_bc_point) : **seuil**, modélise une loi de surverse de type seuil.
- 8. <img src="/model_building/boundary_conditions/tank_bc_point.png"/> [Tank](/model_building/boundary_conditions/tank_bc_point) : **bassin**, généralement utilisée pour modéliser des bassins de retenue dans des [stations]. On peut également imposer une cote initiale sur le nœud connecté au bassin.
- 9. <img src="/model_building/boundary_conditions/connect_model_bc_point.png"/> [Model connection](/model_building/boundary_conditions/connect_model_bc_point) : **connexion de modèles**, sert à connecter hydrauliquement deux modèles différents d’un même projet. On distingue deux modes de fonctionnement selon les options sélectionnées dans le gestionnaire de scénario :
  - 1. **Mode global :**
  Les deux modèles sont simulés simultanément et forment donc une seule entité de calcul. Le programme va dans ce cas chercher à appareiller les noms des objets [Model connection](/model_building/boundary_conditions/connect_model_bc_point). S’il trouve deux noms identiques dans deux modèles différents, il va alors générer une liaison de type *connector* entre les deux noms portant ces objets :
  
<img src="/model_building/boundary_conditions/_connect_model_global_scheme.png" width="600px"/>
  
  - 2. **Mode cascade :**
  Dans ce mode, le modèle 1 est exécuté d’abord, puis le modèle 2. L’hydrogramme sortant du modèle 1 est stocké puis réinjecté dans le modèle 2 lors de la seconde simulation.
  
  Il faut pour cela  définir une condition à la limite de type z(q) dans l’élément [Model connection](/model_building/boundary_conditions/connect_model_bc_point) du modèle 1 (la courbe z(q) doit être définie par l’utilisateur) et une condition à la  limite de type hydrograph dans l’élément [Model connection](/model_building/boundary_conditions/connect_model_bc_point) du modèle 2. 
  
  
  
  
  
  