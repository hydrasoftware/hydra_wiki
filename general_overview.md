---
title: Présentation générale
description: 
published: true
date: 2021-02-10T18:40:08.801Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:45:14.543Z
---


Hydra s'intègre totalement dans l'environnement QGIS, dont toutes les fonctionnalités restent disponibles (ainsi que la possibilité de charger d'autres extensions). L'application ajoute ainsi à l'interface QGIS :

- Un menu dédié <img src="/0_hydra_icone.png" height="25px" align="middle" style="transform: scale(1.0);"/>
- Une barre d'outils,
- Différents panneaux latéraux.


<img src="/general_overview/3_gui_overview.png" width="900px"/>

# Barre de menu

Le menu déroulant hydra <img src="/0_hydra_icone.png" height="25px" align="middle"/> donne accès aux principaux outils de gestion des projets, modèles, scénarios de calcul et bibliothèque de données hydrologiques. Il est disponible dès l'ouverture de QGIS si le plugin hydra est activé.


<img src="/general_overview/3_hydra_menu.png" width="150px"/>


# Barre d’outils généraux

Cette barre d’outils, positionnée sous le menu, permet un accès direct aux principales fonctionnalités :

- Construction des modèles et édition des objets de modélisation 
- Lancement des calculs et exploitation des résultats. 

La barre d'outils apparait après ouverture d'un projet hydra.

<img src="/general_overview/3_hydra_tool_bar.png" width="900px"/>

# Panneaux latéraux

3 panneaux latéraux dédiés à hydra viennet compléter le gestionnaire de couches :

- **Couches** : gestionnaire de couches de QGIS dans lequel s'affichent les couches associées à hydra (objets de modélisation, données topographiques, résultats de calcul, ...) ainsi que les couches ouvertes directement via QGIS,
- **Project items** : synthèse de tous les éléments compris dans le projet : modèles, données de pluies, scénarios de calcul,
- **Model elements** : boutons de création des objets de modélisation,
- **Longitudinal profile** : paramétrage des profils en long (géométrie et grandeurs hydrauliques associées au scénario de calcul actif).

<img src="/general_overview/3_hydra_panels.png" width="900px"/>

Les panneaux latéraux apparaissent après ouverture d'un projet hydra (à l'exception du gestionnaire de couche, disponible dès l'ouverture de QGIS).




