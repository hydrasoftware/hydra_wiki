---
title: Gestion des projets
description: 
published: true
date: 2024-07-23T16:55:05.260Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:44:55.143Z
---


# Notions de projets et de modèles

Un modèle correspond à la description, par le biais d'objets de modélisation spécifiques, du territoire à modéliser.
Un projet appelle un ou plusieurs modèles et contient les paramètres des scénarios de calcul et les données hydrologiques associées. Le pilotage des calculs se fait au niveau du projet.


Les données associées à chaque projet et chaque modèle sont structurées en tables dans la base de données sous-jacente. A chaque projet correspond une base de données, structurée en différents schémas :
- un schéma par modèle,
- un schéma project regroupant les tables de gestion du projet, communes à tous les modèles hydrauliques du projet. Ce sont des tables généralement alphanumériques décrivant des données non géographiques telles que :
  - la pluviométrie,
  - le paramétrage des scénarios de calcul,
  - les paramètres d’exploitation de résultats, ...
  
les schémas ci-dessous illustrent les correspondance entre un système hydraulique et la base de données sous-jacente :

<img src="/project_management/4_project_scheme_bd.png" width="550px"/>


Les modèles peuvent être connectés entre eux lors du calcul:
- Couplage de **modèles contigus** par le biais d'objets de connexion <img src="/model_building/boundary_conditions/connect_model_bc_point.png"/> [Model connection](/model_building/boundary_conditions/connect_model_bc_point)
- Couplage de **modèles superposés** (couplage d'un modèle de réseau et d'un modèle de surface) par le biais de liaisons <img src="/model_building/links/interlink_line.png"/> [Interlink](/model_building/links/interlink_line)


# Gestion des projets

La gestion des projets se fait via l'icone hydra du menu Principal <img src="/0_hydra_icone.png" height="25px"/> Project manager.



L'onglet `Project manager` donne accès au gestionnaire de projet, qui offre les fonctionnalités suivantes, détaillées dans les paragraphes suivants :
- `New` : Création d'un nouveau projet,
- `Open`: Ouverture d'un projet existant,
- `Delete`: Suppression d'un projet,
- `Duplicate`: Copie d'un projet,
- `Import from file`: Import d'un projet,
- `Export as file`: export d'un projet,
- `Update project`: Mise à jour d'un projet existant pour être compatible avec la verison de base de données utilisée par le plugin,

<img src="/project_management/4_project_manager.png" width="650px"/>


Différents attributs sont attachés à chacun des projets :
- Projetc name : nom du projet, en minuscule et sans caractères spéciaux,
- SRID : système de projetction du projet,
- Data version : version de base de données utilisée par le plugin,
- Creation date : date de création du projet,
- Workspace : Répertoire dans lequel seront notamment **stockés les résultats de calcul**. Ce chemin est également utilisé comme **chemin relatif** pour localiser les **fichiers externes** appelés lors du lancement des calculs. 


Les fonctionnalités de création et d'ouverture d'un projet existant sont directement accessibles depuis l'icône <img src="/0_hydra_icone.png" height="25px"/> hydra du menu Principal.


## Création d'un projet

<img src="/project_management/4_new_project.png" width="400px"/>

Lors de la création d'un nouveau projet, l'utilisateur défini :
- Un nom,
- Un système de projection, qui sera appliqué à l'ensemble des modèles constituant le projet,
- Un **working directory** (ou workspace) dans lequel seront notamment créé les fichiers de résultats de calcul. Ce chemin est également utilisé comme **chemin relatif pour les fichiers externes** appelés lors du lancement des calculs.
> **note**
>
>  Ce répertoire doit être situé sur le même disque que celui où sont lancés les calculs
> 

L'application crée les tables associées au projet dans la base de données et charge les vues graphiques associées :
- Rain gage,
- Dry inflow sector,
- Anemometer,
Terrain points.

A ce stade, aucun modèle n'est associé au projet. Ils sont créés dans un second temps.

Un répertoire portant le nom du projet est cré dans le **workspace** ainsi qu'un fichier projet.qgs qui sera appelé lors de l'ouverture d'un projet.

Le fichier projet.qgs est mis à jour lors de l'enregistrement du projet sous QGIS; les mises en forme spécifiques ainsi que tables complémentaires appelées par l'utilisateur sont ainsi enregistrées et chargées à la prochaine ouverture du projet.


## Ouverture d'un projet existant

L'ouverture d'un projet entraine le chargement de l'ensemble des tables associées et des outils de création et d'exploitation des modèles dans l'interface.


## Import / export d'un projet

Les fonctionnalités d'import / export permettent de sauvegarder et d'échanger un projet via un fichier sql unique, contenant la structure et le contenu de la base de données associée.

Lors de l'export, l'utilisateur sélectionne le répertoire dans lequel le fichier projet.sql sera créé.

Lors de l'import, l'utilisateur sélectionne le fichier *.sql à importer, et renseigne le nom du projet à créer (qui peut différer de celui du fichier importé) ainsi que le **working directory**.


> **note**
>
>  Si dans le modèle source les fichiers externes appelés par le gestionnaire de scénario étaient situés dans un sous répertoire du working directory, ces fichiers seront recherchés par défaut dans le même sous répertoire du working directory défini lors de l'import.
> 
> 

## Mise à jour d'un projet existant

La mise à jour du plugin peut nécessiter la modification de la structure de la base de données sous-jacente. Les projets présentant une structure de base de données non compatible avec la version du plugin installée sont indiqués en rouge dans le gestionnaire de projet, et ne peuvent être ouverts.

La fonction de mise à jour permet de mettre à jour la structure de la base de données de ces projets afin de les rendre compatibles avec le plugin installé.


> **note**
>
>  La mise à jour de la base de données est irréversible; si besoin, exporter préalablement le modèle pour en conserver une sauvegarde dans la version antérieure.
> 
> 

## Copie d'un projet existant

`Duplicate` permet de dupliquer un projet, sous un autre nom.

# Création d'un modèle

Les fonctions de création de modèles sont disponibles lorsqu'un projet est ouvert:

<img src="/project_management/4_model_manager.png" width="250px"/>


- `Set current` : définition du modèle actif, pour la création des objets de modélisation (également disponible via la barre d'outils),
- `Add model` : création et ajout d'un nouveau modèle au projet actif; l'application crée le schéma correspondant dans la base de données et charge les tables associées dans l'interface,
- `Delete current model` : suppression du modèle actif,
- `Import model (sql)` : import d'un modèle depuis un export sql d'un modèle ou d'un projet,
- `Exort model (sql)` : export d'un modèle au format sql,
- `Import model (sql)` : import d'un modèle au format csv, généré par Hydracity.


> **warning**
>
>   - Un maximum de **20 modèles distincts** peut être créé dans un même projet.
>   - Les modèles ont le même **système de projection** que le projet; il est impossible d'importer un modèle défini dans un autre système de projection que celui du projet.
>   
>   

Les différents outils hydra de construction et d'exploitation des modèles sont chargés lors de la création du premier modèle associé au projet ouvert.

<a name="change_srid"></a>

## Manipulation pour un changement de projection d'un projet hydra

Ouvrir un outil de requête vers la base de données projet hydra (PgAdmin ou psql). 

> :warning: le Gestionnaire base de données Qgis ne fonctionne actuellement pas pour les fonction en plpython dont nous avons besoin ici

>:warning: Fermer le projet cà reprojeter pendant l'exécution des requêtes

**Suppression de l'api**

Créer la fonction drop_api, remplacer les numéro de version (majeur, mineur, patch) dans hydra_MAJEUR_MINEUR_PATCH par les bonne valeur (e.g. hydra_3_10_2, si vous êtes en hydra 3.10.2) puis executer la requête:
```sql
create or replace function project.drop_api() returns void language plpython3u volatile as
$$
    from hydra_MAJEUR_MINEUR_PATCH import drop_model_api, drop_project_api, create_project_api, create_model_api
    r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
    srid, version = r['srid'], r['version']
    for r in plpy.execute('select name from project.model'):
        for statement in drop_model_api(r['name']):
            plpy.notice(statement)
            plpy.execute(statement)
    for statement in drop_project_api():
        plpy.notice(statement)
        plpy.execute(statement)
$$;

select project.drop_api();
```

**Changement de SRID pour les géométries données d'hydra**

Préciser le SRID cible *[SRID_final]* (i.e. changer 2154 pour la valeur souhaitée dan sle requête suivante).
```sql
do 
$$ 
declare 
    stmt varchar; 
begin
    for stmt in select 'ALTER TABLE ' || f_table_schema  || '.' ||  f_table_name 
        || ' ALTER COLUMN ' || f_geometry_column 
        || ' TYPE geometry(' || type || case when coord_dimension=2 then '' else 'Z' end||', 2154)'
        || ' USING ST_Transform(' || f_geometry_column || ', 2154);'from geometry_columns loop
        raise notice '%', stmt;
        execute stmt;
    end loop;
    for stmt in select 'update ' || f_table_schema  || '.' ||  f_table_name 
        || ' set '||f_geometry_column||'=st_snaptogrid('||f_geometry_column||', (select precision from hydra.metadata))' from geometry_columns 
        loop
        raise notice '%', stmt;
        execute stmt;
    end loop;
    update hydra.metadata set srid=2154;
end;
$$;

```

>:warning: il faut absolument snapper les géométries à la grille (comme c'est fait dans la requête ci-dessus) dans le nouveau système de coordonées, sinon les contraintes éditée sont déconnectées des autres contraintes quand elles sont snappées après édition.


**Lancer la regénération de l'API (vues, trigers et fonctions)**

Remplacer la aussi les numéro de version dans hydra_MAJEUR_MINEUR_PATH par les bonnes valeurs et lancer la requête:

```sql
create or replace function project.refresh_api()
returns void language plpython3u security definer volatile as
$$
from hydra_MAJEUR_MINEUR_PATCH import drop_model_api, drop_project_api, create_project_api, create_model_api
r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
srid, version = r['srid'], r['version']
for r in plpy.execute("SELECT tables.table_schema AS name FROM information_schema.tables WHERE tables.table_name::name = '_singularity'::name"):
    for statement in drop_model_api(r['name']):
        plpy.execute(statement)
for statement in drop_project_api():
    plpy.execute(statement)
for statement in create_project_api(srid, version):
    plpy.execute(statement)
for r in plpy.execute("SELECT tables.table_schema AS name FROM information_schema.tables WHERE tables.table_name::name = '_singularity'::name"):
    for statement in create_model_api(r['name'], srid, version):
        plpy.execute(statement)
$$
;

select project.refresh_api();

```

**Supprimer le projet .qgs, puis ré-ouvrir le projet dans qgis**





