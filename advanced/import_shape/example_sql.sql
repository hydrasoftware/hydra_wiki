insert into model.reach(geom, name, pk0_km, dx)
select 
	b.geom, 
	b.nom,
	b.pk0,
	50
from work.bief as b
;

/* Insertion sections paramétriques ouvertes */
with sec as (
    select row_number() over() as id, t.cp_geom_name as cp_geom_name
	from work.TRC as t
	where t.cp_geom_name!='NULL' group by t.cp_geom_name
    )
insert into model.closed_parametric_geometry(id, name, zbmin_array)
select
    s.id,
    case
		when t.dimension = 'CARRE' then replace(t.dim_mod,' ','_')
		when (t.dimension = 'RECTANGULAIRE' and t.dim_mod is not NULL) then replace(t.dim_mod,' ','_')
		when (t.dimension = 'RECTANGULAIRE' and t.dim_mod is NULL and t.sect_tron is not NULL and t.sect_tron2 is not NULL) then 'SEC_' || round(t.sect_tron) || '/' || round(t.sect_tron2)
		when t.dimension = 'OVOIDE' and t.dim_mod is not NULL then replace(t.dim_mod,' ','_')
	end,
    '{{0.0, 0.0}, {999, 999}}'::real[] 
from sec as s;


/* Insertion des collecteurs */
insert into model.pipe_link(geom, name, z_invert_up, z_invert_down, cross_section_type, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom_name)
select 
	t.geom, 
	t.id_arc,
	t.ffil,
	t.tfil,
	case
		when t.dimension = 'CIRCULAIRE' then 'circular'::hydra_cross_section_type /*6*/
		when t.dimension = 'CARRE' then 'channel'::hydra_cross_section_type /*4*/
		when t.dimension = 'RECTANGULAIRE' then 'pipe'::hydra_cross_section_type /*3*/
		when (t.dimension = 'OVOIDE' and t.dim_mod is not NULL) then 'pipe'::hydra_cross_section_type /*3*/
		when (t.dimension = 'OVOIDE' and t.dim_mod is NULL) then 'ovoid'::hydra_cross_section_type /*5*/
		when t.dimension = '' then 'NULL'
	end,
    t.circular_diameter,
    t.ovoid_height/1000,
    t.ovoid_top_diameter/1000,
    t.ovoid_invert_diameter/1000,
    s.id
from work.TRC as t, model.closed_parametric_geometry as s
where s.name=t.cp_geom_name;
;

