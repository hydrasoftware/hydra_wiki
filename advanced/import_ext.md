---
title: Import de fichiers externe dans un modèle
description: 
published: true
date: 2023-01-09T17:30:27.198Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:40:27.088Z
---


L'ensemble des objets de modélisation étant associé à une ou plusieurs tables de la base de données, il est possible de les importer en lot dans un modèle à partir de fichiers shape, afin d'optimiser les temps de construction et de valoriser les données disponibles. Certaines précautions doivent cependant être prises quant à la structure et la cohérence des données à importer et à l'ordre des imports, au regard des contrôles de cohérence effectués par la base de données lors de l'enregistrement : absence de doublons d'identifiants ou de géométrie, cohérence topologique entre les différents objets (les extrémités tronçons de collecteurs doivent être fusionnés à des regards par exemple).

Cette opération se fait en 2 étapes, via le menu <img src="/0_hydra_icone.png" height="25px"/> **Advanced tools / Manage external tables** :

- Import de fichiers externes (shape, gpkg, ...) dans un schéma de travail de la base (dénommé *work*),
- Import de fichiers depuis le schéa *work* de la base vers le modèle.


# Import de fichiers externes dans la base

Hydra dispose d'un outil permettant d'importer des fichiers externes dans un schéma de travail *work* de la base du projet en cours, via le menu <img src="/0_hydra_icone.png" height="25px"/> **Advanced tools / Manage external tables**. Ces tables peuvent ensuite être importées dans les modèles à l'aide d'un utilitaire intégré ou de requêtes sql. Elles peuvent également être pré-traitées via des requêtes sql préalables afin de contrôler et de corriger le cas échéant certaines incohérences.

<img src="/advanced/import_shape/import_shape_menu.png" width=250/>

L'interface suivante apparaît :

<img src="/advanced/import_shape/import_shape_ui.png" width=800/>

Le bouton <img src="/0_hydra_+.png" height="25px"/> permet de charger le fichier externe (ici format shape) choisi dans le schéma *work*. Une fois l'import sélectionné, les champs de la table sont listés.

Le bouton <img src="/advanced/import_shape/load_table_button.png" height="25px"/> permet de charger la table dans le gestionnaire de couches (elle apparaît dans le groupe *work* du gestionnaire de couches).

Le bouton <img src="/0_hydra_-.png" height="25px"/> permet de supprimer la table du schéma *work*.


> **Note**
>
>  Il n’y a pas de restriction dans l’import que vous pouvez réaliser, vous pouvez importer n’importe quel fichier dans le schéma work sans affecter le fonctionnement direct d’hydra. 
> 

Le deuxième encart présente les caractéristiques de la table sélectionnée dans la liste des tables du schéma **work**. On ne parle pas ici du fichier externe source, mais bien de la donnée présente dans le schéma work de la base de données.  Par exemple, ici on voit l’ensemble des colonnes de la table points_terrain_demo  dans le schéma work; Elle a une colonne z_ground de type numeric et une colonne géométrique nommée geom de type POINT. 


Ces tables peuvent ensuite être importées dans le modèle :

- via l'[utilitaire dédié](#tool_import_file_into_model) : Manhole, Pipe, catchment, catchment countour, hydrograph boudary condition et points xyz
- via des [requêtes sql](#sql_import_file_into_model) (outils QGIS ou PGadmin).



# Import de tables du schéma work dans le modèle

<a name="tool_import_file_into_model"></a>

## Outil intégré d'import

Hydra permet d'importer des objets du schéma *work* vers un modèle, via les fonctionnalité de la partie droite de l'interface <img src="/0_hydra_icone.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> **Advanced tools / Manage external tables**. Cette fonctionnalité est disponible pour les objets suivants:

- Vers le projet:
  - Points topographiques ou bathymétriques *points_xyz*
  - Mode d'occupation du sol (cf. [Catchment node](/model_building/hydrology/catchment_node_point))
  
- Vers un modèle:
  - Regards d'assainissement <img src="/model_building/network/manhole_point.png"/> [Manhole](/model_building/network/manhole_point)
  - Collecteurs d'assainissement <img src="/model_building/network/pipe_line.png"/> [Pipe](/model_building/network/pipe_line)
  - Biefs de cours d'eau <img src="/model_building/river_free_surface_flow/reach_line.png"/> [Reach](/model_building/river_free_surface_flow/reach_line)
  - Bassins versants <img src="/model_building/hydrology/catchment_node_point.png"/> [Catchment node](/model_building/hydrology/catchment_node_point)
  - Contours de bassins versants <img src="/model_building/hydrology/catchment_contour_polygon.png"/> [Catchment contour](/model_building/hydrology/catchment_contour)
  - Condition limite de type Hydrogramme <img src="/model_building/boundary_conditions/hydrograph_bc_point.png"/> [Hydrograph bc singularity](/model_building/boundary_conditions/hydrograph_bc_point)
  - Lignes de contrainte <img src="/model_building/river_free_surface_flow/constrain_line.png"/>
  
  
  
  
Dans l'exemple ci-dessous, on importe la table *regards* préalablement importée dans le schéma work dans la table *manhole* du modèle *model1*.

<img src="/advanced/import_shape/import_shape_ui_2.png" width=900/>

Le tableau permet d'affecter une valeur à chaque champs de la table cible *Target column* (ici *Manhole*); deux options sont possibles:

- `Source column` : affecter la valeur d'un champ de la table source (ici *regards*),
- `Default value` : affecter une valeur par défaut pour tous les objets.

Data type est imposé par la structure de la table cible.


> **Note**
>
>  Certaines tables ont des contraintes, notamment de connectivité; c'est notamment le cas 
> 
> - des *pipe_link* qui doivent être connectés à des *Manhole_point*,
> - des *hydrograph_bc_point* qui doivent être posés sur des *node*.
> 
> Si les contraintes ne sont pas vérifiées, l'import ne pourra être effectué. Dans ce cas, les tables doivent être préalablement corrigées, manuellement ou via des requêtes sql dans la tabe work par exemple.
> 
> 


> **Note**
>
> Les fonctions associées à la table de mode d'occupation du sol sont détaillées dans [Catchment node](/model_building/hydrology/catchment_node_point).
> 

<a name="sql_import_file_into_model"></a>

## Requêtes sql

Pour les autres objets de modélisation ou pour accéder à des fonctions plus complexes, les imports doivent se faire via des requêtes sql spécifiques (cf. [sql](/advanced/sql)).

Deux exemples sont présentés ci-dessous. Pour plus de précisions sur la structure des requêtes sql, se reporter au chapitre spécifique de la documentation.

> **Note**
>
>- L'utilitaire <img src="/advanced/sql/pgadmin.png"/> **PgAdmin** installé avec PostGis permet d'explorer la structure des tables hydra.
>- Les requêtes peuvent être lancées dans PgAdmin ou dans l'utilitaire de gestion de bases de données de QGIS.
>

### Import de Reach

L'exemple ci-dessous permet d'importer une table de polylignes *biefs* préalablement importée dans le schéma *work* vers la table *reach* du mdoèle *model*.

<img src="/advanced/import_shape/sql_reach.png" width=350/>


### Import de tronçons de collecteurs 

Les exemples ci-dessous permettent d'importer les **collecteur** d'une table de polylignes *TRC* préalablement importée dans le schéma *work* vers la table *pipe_link* du mdoèle *model*, en intégrant le type de section ainsi que le diamètre (pour les collecteurs circulaires) et le nom de la section (pour les collecteur de type pipe ou chanel.

Les géométries de sections doivent préalablement être importées dans la base (table *model.closed_parametric_geometry*).

**Import des sections paramétriques ouvertes**

<img src="/advanced/import_shape/sql_section.png" width=1200/>

**Import des collecteurs**

<img src="/advanced/import_shape/sql_pipe.png" width=1200/>








