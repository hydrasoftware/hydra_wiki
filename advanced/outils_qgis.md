---
title: Utilisation des outils QGIS pour préparer la modélisation, créer ou modifier des objets de modélisation
description: 
published: true
date: 2023-01-05T17:20:52.138Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:40:18.576Z
---

# Création ou modification des objets de modélisation

## Introduction

Les objets de modélisation créés via les boutons associés sont enregistrés dans la base de données du projet; les tables associées sont chargées dans le gestionnaire de couche et peuvent ainsi être modifiées via les outils QGIS:

- modification de la géométrie des objets,
- renseignement, modification de leurs attributs.

Ces manipulations doivent cependant tenir compte de la structure du modèle conceptuel de données (MCD) dans la mesure où les tables associées aux objets de modélisation peuvent être liées à d'autres tables et comporter des contraintes spécifiques (par exemple une singularité doit être portée par un nœud). La structure du MCD peut être consultée avec le programme **PgAdmin** installé avec PostGre.



> **Note**
>
>  La création d'objets via ces outils n'est pas recommandée, dans la mesure où les boutons disponibles facilitent nettement les manipulations. L'import de données extérieures (depuis des fichiers shape notamment) peut se faire via des outils dédiés (cf. § [Import de fichiers externe](/advanced/import_ext)).
> 
> 

## Modification de la géométrie des objets de modélisation

Pour modifier un objet existant du modèle:

- sélectionner la couche associée dans le gestionnaire de couches,
- la rendre active  <img src="/advanced/outils_qgis/qgis_edit.png"/>,
- procéder aux modifications souhaitées via les outils d'édition topologique de QGIS <img src="/advanced/outils_qgis/qgis_topo.png"/>,
- enregistrer.

Les options d'accroche objet (*Préférences/Options d'accrochage...*) permettent de garantir la fusion des objets d'une même table (extrémité de pipes par exemple) ou de tables différentes lorsque les contraintes topologiques l'imposent (extrémité de pipe fusionnée avec un manhole).

Certaines tables étant liées entre elles par des contraintes topologiques, la modification d'un objet peut entraîner des modifications sur les objets liés; notamment:

- Le déplacement d'un noeud (river node, manhole, station node) entraînera le déplacement des singularités et liaisons qui lui sont liées. Plus particulièrement, le déplacement d'un manhole entraînera le déplacement des  collecteurs qui lui sont liés,
- La modification de l'extrémité d'un reach entraînera le déplacement des *river node* extrémités vers les nouvelles extrémités. Attention, si la nouvelle extrémité porte déjà un *river node* (dans un rayon de 1m), celui-ci doit préalablement être supprimé pour éviter la création d'un doublon (qui renverra une erreur empêchant d'enregistrer la modification).



## Renseignement des champs des tables

Les champs des tables hydra peuvent également être renseignés directement par la calculatrice de champs de QGIS  <img src="/advanced/outils_qgis/qgis_champs.png"/>


<a name="create_3D_polyline"></a>

# Création de polylignes 3D
Les polylignes 3D peuvent être utilisées pour créer des profils en travers bathymétriques pour des modélisations 1D de rivière.

Nous présentons ici les outils QGIS permettant de générer des polylignes 3D de à partir de semis de points fournis au format csv, classés par profils.
-	Ouvrir le fichier csv en tant que couche (Ajouter une couche vecteur\Ajouter une couche de texte délimité)
![couche_texte_delimite.jpg](/advanced/outils_qgis/couche_texte_delimite.jpg)
- Paramétrer le menu pour créer une couche de type **Point 3D** (bien définir la colonne Z).
![couche_texte_delimite_ui.jpg](/advanced/outils_qgis/couche_texte_delimite_ui.jpg)
- Enregistrer la couche temporaire des points créée par Qgis dans un format quelconque (gestionnaire de couches : clic droit sur la couche temporaire, puis exporter)
- Dans la **boîte à outils** de Qgis (menu Traitement\Boite à outils), chercher l’outils *Points vers lignes* et l’ouvrir.
![traitement_points_vers_lignes.jpg](/advanced/outils_qgis/traitement_points_vers_lignes.jpg)
![traitement_points_vers_lignes_ui.jpg](/advanced/outils_qgis/traitement_points_vers_lignes_ui.jpg)
	-	Appeler le fichier enregistré précédemment,
	-	Champs de tri : ordre des points le long de la polylogne (ici *name_id*),
	-	Champs de regroupement : champ indiquant à quel polyligne appartient le point (ici une polyligne par profil en travers identifié par *name*) 
	-	Sauvegarder la couche créée dans un format quelconque


