---
title: Outils additionnels d'exploitation des résultats
description: 
published: true
date: 2021-02-09T11:02:16.994Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:40:22.928Z
---


Les utlitaires présentés ci-après ne font pas partie à propement parler de la plateforme Hydra. Ce sont des programmes exécutables qui ont été développés dans le cadre de projets antérieurs ; ils ont prouvé leur utilité et ont pour vocation d'être retranscrits dans la plateforme Open Source d'Hydra dans une étape de développement future. Ces outils sont pilotés depuis l'IHM et leur exploitation par l'utilisateur ne présente pas de différences notables avec les outils développés en langage Python.

Trois outils additionnels d'exploitation des résultats  de simulation sont disponibles :

1. l'outil EXTRACT : postraitement des courbes x(t) de résultats et paramétrage de la mise en forme des graphes.
2. l'outil ANIMEAU : profils en long des grandeurs calculées le long d'un bief ou d'une branche filaire.
3. l'outil CRGENG : production de cartes détaillées à l'échelle de résolution du pixel du MNT sous jacent au modèle.

Ces outil sont accessibles via le menu *Advanced tools* : 


<img src="/advanced/additional_results/ar_menu.png" style="transform: scale(0.8);"/>

# Outil EXTRACT

### Définition

EXTRACT est un utilitaire général d’extraction de courbes X(t) de fichiers externes, de transformation de ces courbes et de génération de fichiers contenant les courbes X(t) transformées. Les fichiers ainsi produits sont exploités ensuite par le grapheur WEXPDESS.

Les courbes exploitées sont de deux natures :

1. les courbes issues des fichiers de résultats brutes .W14 et .W15 de simulation Hydra,
2. des courbes extérieures de toute nature pouvant être superposées aux courbes calculées.

Le programme accepte en entrée des formats variés de fichiers  et propose plusieurs options de format en sortie. 

Cet outil démontre tout son intérêt lorsqu'il s'agit de produire des résultats graphiques de nombreux scénarios, de superposer les courbes calculées avec des mesures et de définir une mise ne forme personnalisée qui peut être rejouée facilement pour de nouveaux scénarios. Cet outil complète ainsi les fonctionnalités de l'outil de visualisation décrit dans le chapitre 13.


### Paramétrage

EXTRACT est alimenté par un fichier de commande en format texte. 

<img src="/advanced/additional_results/extract_menu.png" style="transform: scale(0.8);"/>

Le fichier de commande se présente sous forme de blocs contigus séparés par une ou plusieurs lignes blanches.
Chaque bloc est structuré comme suit :

motcle

ligne 1

ligne 2
……….

Ligne n

Les blocs sont  séparés entre eux par au moins une ligne blanche. La structure de chaque ligne d’instruction dépend du mot clé.

Le format des instructions est défini dans la note utilisateur accessible via le bouton **?** de l'écran ci-dessus.

### Exécution

Il faut tout d'abord  importer le fichier de commande dans la fenêtre de l'écran ci dessus via le bouton <img src="/0_hydra_+.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/>, sélectionner le fichier que l'on souhaite traiter et cliquer sur *Extract*.

Le fenêtre de droite permet de suivre le déroulement des calculs. En cas d’anomalie le bouton « Open log file » permet d’éditer le fichier .log.

Le post traitement se décline en deux temps :

**1. Activation du bouton Extract**

Cette opération active la lecture du fichier de commande sélectionné  et le traitement des  instructions définies dans ce fichier. Chaque instruction esr recopiée dans une fenêtre DOS de façon à permettre à l'utilisateur de suivre le déroulement du traitement. En cas d'anomalie détectée sur la syntaxe d'une instruction le programme s'arrête sur l'instruction défallante et invite l'utilisateur à éditer le fichier pour procéder aux ajustements nécessaires.

Après traitement deux types de fichiers sont générés :

- des fichiers de synthèse au format .csv, contenant les résultats attributaires spécifiés dans le fichier de commande,
- des fichiers de commande pour la production de graphes.

**2. Activation du bouton Expdess**

Wexpdess est un grapheur permettant de visualiser les pages de graphes spécifiés  par le fichier de commande . Il possède son propre menu pour sélectionner les attributs de courbes et de naviger entre les différentes pages graphiques produites.

<img src="/advanced/additional_results/extract_menu_wexpdess.png" style="transform: scale(1.0);"/>

Note : pour accéder à l'écran pour modifier la palette de style des différentes courbes : cliquer sur *menu/fichier/divers/paltette de styles par défaut*.      

Un exemple de page graphique généré est reproduit ci-dessous :

<img src="/advanced/additional_results/extract_courbes.png" style="transform: scale(0.7);"/>

**3. Production de fichiers graphiques** 

Les pages graphiques consultées à l'écran peuvent être sauvegardées au format image .EMP ou au format .PDF. Dans ce dernier cas l'utilisateur peut générer un fichier PFD contenant l'ensemble des pages graphiques générées par le grapheur.

____________________________________________________________________________________________________

Tous les les fichiers produits par cet outil sont copiés dans le répertoire du fichier de commande.


# Outil ANIMEAU

### Définition

ANMEAU est un utilitaire de dessin animé de profils en long de différents paramètres de calcul le long d'une branche ou d'un bief filaire. Cet outil possède des fonctionnalités voisines de l'utilitaire décrit au chapitre 13, il est d'une conception plus ancienne et la navigation dans les différents menus nécessite un apprentissage minimum.

Cet utilitaire possède néanmoins des fonctionnalités puissantes rodées sur de très nombreux projets et conserve de ce fait toute son attractivité pour analyser  graphiquement les résultats d'une simulation.

### Paramétrage et exécution

<img src="/advanced/additional_results/anim_menu.png" style="transform: scale(0.75);"/>

Il faut spécifier le nom du modèle et le nom du scénario dans la fenêtre ci-dessus. Appuyer sur le bouton **Go!**. le menu suivant apparait :

<img src="/advanced/additional_results/anim_menu2.png" style="transform: scale(0.75);"/>

- le sous menu *vue en plan"  visualise l'évolution  temporelle en plan de grandeurs hydrauliques diverses. l'outil synthétique de "mise en charge-débordement" est spécialement utile pour visualiser rapidement l'évoultion temporelle des états de mise en charge et de débordement du réseau.
- le sous menu profil en long  visualise l'évolution temporelle de plusieurs grandeurs en simultanée le long d'un cheminement constitué de plusieurs branches accolées.Un exemple de visualisation est donné ci dessous à un instant donné :

<img src="/advanced/additional_results/anim_pl.png" style="transform: scale(0.75);"/>

### Sauvegarde des profils et des vues en plan

Animeau dispose d'un ensemble complet de fonctionnalités permettant de sauvegarder les vues d'écran en fichiers au format image.

Il est également possible de sauvegarder la mise en page d'un vue dans un fichier de commande et de rejouer la mise en page en rappelant le fichier dans la fenêtre du menu de l'IHM.


# Outil CRGENG

### Définition

Le programme Crgeng permet de faire différents post-traitements cartographiques sur  les objets de modélisation des écoulements de surface. Huit modes de traitement sont proposés :

1)    Mode « Contour » : production d’une carte d’inondation associée à un scénario.
2)    Mode « Contourz » : production d’une carte iso-cote associée à un scénario.
3)    Mode « Gain » : production d’une carte de différentiel de hauteurs d’eau entre deux scénarios.
4)    Mode « Envelop » : production d’une carte enveloppe d’inondation d’un groupe de scénarios.
5)    Mode « Envelopz » : production d’une carte enveloppe iso-cote d’un groupe de scénarios.
6)    Mode « Alea » : production d’une carte d’aléa associée à un scénario.
7)    Mode « Alea_Env » : production d’une carte enveloppe d’aléa associée à un groupe de scénarios.
8)    Mode « WL_Layer » : production de fichier binaires destinées à un post traitement d’animation de séquences  temporelles d’inondation.

Les données de terrain sont lues au format .TIF. Attention, les fichiers vrt ne sont pas lus. Si le traitement doit prendre en compte différentes sources de données topographiques, un pré-traitement doit être réalisé pour agréger l'ensemble de ces données.

Si le format vectoriel est sélectionné, deux types de traitement sont disponibles :
-    La solution lissée, 
-    Une solution pixélisée (plus légère).

Les traitements  sont effectués par Crgeng à partir des fichiers de données et résultats fournis par Hydra.

Les fichiers carto fournis sont en format raster (*.ASC) ou shape.



### Paramétrage et exécution

La fenêtre suivante apparait après activation du menu *CGRENG* :

<img src="/advanced/additional_results/crgeng_menu.png" style="transform: scale(0.75);"/>

l'application est pilotée par un fichier de contrôle qu'il faut renseigner via un éditeur de texte. Le bouton **?** décrit les règles à respecter pour la localisation des différents fichiers et la définition du fichier de contrôle. Le bouton *Generate file* active un formulaire d’aide à la construction du fichier de contrôle.

Il faut  importer le fichier de contrôle dans la fenêtre ci-dessus via le bouton <img src="/0_hydra_+.png" height="25px" width="25px" align="middle" style="transform: scale(1.0);"/> , activer ce fichier et appuyer sur le bouton  **Go!**. Le traitement démarre, la fenêtre de droite permet de suivre le déroulement des calculs. En cas d’anomalie le bouton « Open log file » permet d’éditer le fichier .log.

Après traitement deux types de fichiers cartographiques sont générés dans le répertoire du fichier de commande :

- des fichiers vectoriels au format .SHP,
- des fichiers image au format .ASC.

Les natures de fichiers produits dépendent du paramétrage défini dans le fichier de commande.

A titre d'illustration de résultats produits la carte ci dessous restitue les hauteurs d'inondation générées par la crue centenale de l'Oise dans la zone de confluence Oise_Aisne :

<img src="/advanced/additional_results/crgeng_carto_oise.png" style="transform: scale(1.0);"/>




