---
title: Configurations et régulation
description: 
published: true
date: 2021-03-16T18:39:56.495Z
tags: 
editor: undefined
dateCreated: 2021-02-08T23:44:31.858Z
---


# Configurations

## Principes

La **Configuration** permet de générer et de tracer des scénarios impliquant des modifications de paramètres géométriques, topographiques et hydrauliques attachés aux objets de modélisation, dans un même projet. Cette fonctionnalité permet de créer dans une base de données **projet** définissant un état de référence ainsi que des scénarios d'aménagement ou d'évolution n’impliquant pas de modifications de structure topologique : création ou effacement de digues, modification de coefficients de ruissellements, recalibrage de réseaux, ...

Hydra permet ainsi de définir différents groupes de configurations, contenant chacun un ou plusieurs objets dont les paramètres structurels sont modifiés, qui peuvent ensuite être appelés et combinés dans un scénario de calcul.

Cette fonctionnalité permet d'assurer la traçabilité des  différents scénarios modélisés, mais également de faciliter l'exploitation des résultats et la comparaison des différents scénarios de calcul entre eux.

Par défaut, les nouveaux objets de modélisation sont créés dans une configuration **Default**. La création de configurations, détaillée ci-dessous, passe par les étapes suivantes :

- Création d'un nouveau groupe de configurations via le menu <img src="/0_hydra_icone.png" height="25px"/> *Configuration*,
- Activation d'un groupe de configuration dans la barre d'outils,
- Édition des objets à configurer : les modifications apportées via l'éditeur de l'objet sélectionné sont enregistrées dans la configuration active,
- Appel du groupe de configuration dans le gestionnaire de scénarios de calcul.

## Configuration des objets de modélisation

Un groupe de configurations est une bibliothèque d’objets modifiés, associé à un modèle. Il est créé via le menu <img src="/0_hydra_icone.png" height="25px"/> *Configuration*.

<img src="/configurations_regulation/configurations_ui.png" width="650px"/>

L'ajout ou la suppression d'un groupe de configuration se fait via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>. Le bouton <img src="/configurations_regulation/duplicate_button.png" height="25px"/> `Duplicate` permet de dupliquer la configuration sélectionnée.




Une fois le groupe de configuration créé, il doit être activé via la barre d'outils :

<img src="/configurations_regulation/configurations_tool_bar.png" width="350px"/>

Les modifications apportées via l'éditeur de l'objet sélectionné sont enregistrées dans la configuration active.

Les objets configurés dans le groupe de configurations actif sont identifiés par une étoile <img src="/configurations_regulation/configuration_marker.png" width="30px"/>.




> **note**
>
>  Lors de la création d'un modèle, les objets sont créés dans une configuration `défault`. Il n'est pas possible de créer un objet qui ne soit appelé que dans une configuration.
> 

L'ensemble des objets configurés ainsi que les paramètres modifiés dans le groupe de configurations actif peuvent être édités via le menu <img src="/configurations_regulation/configured_object_list.png" height="25px"/>.

<img src="/configurations_regulation/configured_object_list_ui.png" width="650px"/>

- `Edit` permet d'ouvrir l'éditeur de l'objet sélectionné (et de modifier ses paramètres),
- `Zoom` permet de zoomer sur l'objet sélectionné
- `Undo configuration` permet de supprimer la configuration sur l'objet sélectionné
- `Copy data` permet de copier le détail des modifications apportées à chaque objet configurées en format texte.



## Appel des configurations dans les scénarios de calcul

L'appel d'un ou plusieurs groupes de configuration dans le gestionnaire de scénario se fait via l'onglet `Regulation and configuration` du `gestionnaire de scénario`.

<img src="/configurations_regulation/configurations_scenario.png" width="900px"/>


L'ajout ou la suppression d'un groupe de configuration dans le scénario actif se fait via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.

Les flèches permettent de modifier l'ordre des groupes de configurations appelés; si un objet est configuré dans plusieurs groupes, c'est la configuration définie dans le groupe positionné en première position qui sera prise en compte dans le calcul.


<a name="regulation"></a>

# Régulation

## Principes

Hydra dispose d’un module de pilotage d’un certain nombre d’objets de modélisation hydraulique via un langage de programmation formelle. Les instructions sont définies dans un fichier ASCII, déclaré comme un fichier externe dans le gestionnaire de scénarios  <img src="/0_hydra_icone.png" height="25px"/> *Scenario/Settings/Regulation and configuration*. Les règles détaillées de pilotage et la syntaxe associée sont décrites dans le document consultable via le bouton **?** dans le gestionnaire de scénarios.

Ce module complète et étend considérablement la gamme des fonctionnalités de régulation locale intégrées aux actionneurs mobiles.


## Appel des fichiers de régulation dans les scénarios de calcul

L'appel d'un ou plusieurs fichiers de régulation dans le gestionnaire de scénario se fait via l'onglet `Regulation and configuration` du `gestionnaire de scénario`.

<img src="/configurations_regulation/regulation_scenario.png" width="900px"/>

On peut déclarer plusieurs fichiers de contrôle dans un même scénario.

L'ajout ou la suppression de fichiers se fait via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.


> **warning**
>
>   Il faut faire attention à l'ordre de déclaration de ces fichiers lorsque la même instruction apparaît dans deux fichiers différents : la priorité est dans ce cas donnée à la dernière instruction lue. Les fichiers doivent donc être déclarés dans un ordre de priorité croissante.
> 

## Consultation des règles de pilotage d'actionneurs à partir de l'IHM

Dans le cas de gros modèles les règles de pilotage par des fichiers externes peuvent concerner des centaines d’actionneurs. Une bonne compréhension du fonctionnement des réseaux passe par une consultation rapide des règles appliquées aux actionneurs pilotés en mode externe.

L'interface permet de repérer très facilement les actionneurs pilotés par des règles externes et d’accéder aux commandes définies dans ces fichiers pour les éditer au besoin.

### Accès aux objets régulés depuis l'interface

Dans la fenêtre du plan de travail tous les objets régulés trouvés dans le scénario actif sont repérés avec le symbole : <img src="/configurations_regulation/regulated_objects_layer_symbol.png"/> . L’édition d’un objet régulé avec le bouton d’édition classique fait apparaître une fenêtre indiquant le nom des fichiers de régulation et les lignes du fichier portant le nom de l’actionneur. 


Le bouton **Open files** active l’ouverture du fichier de régulation contenant la commande avec un éditeur de texte; il suffit ensuite de naviguer vers la ligne indiquée pour lire les instructions de régulation se rapportant à l’actionneur et éventuellement les modifier.

<img src="/configurations_regulation/regulation_scheme.png" width="600px"/>



### Regulated objects manager

Le bouton <img src="/configurations_regulation/regulated_objects_manager_button.png"/> permet d’accéder la liste de tous les objets régulés pour le scénario et le réseau actifs.


<img src="/configurations_regulation/regulated_objects_manager.png" width="550px"/>

Il est possible de sélectionner un objet régulé particulier et d’ouvrir le fichier correspondant pour éditer l’instruction sous-jacente.


> **note**
>
>  Si le modèle comprend un grand nombre d’objets régulés, l’édition d’un objet quelconque peut s’en trouver ralentie. Une option dans le menu « settings » permet de désactiver l’outil « regulated » pour s’affranchir de cette lenteur.
> 
> 
> <img src="/configurations_regulation/regulated_ignore_items.png" width="400px"/>
> 
> 

