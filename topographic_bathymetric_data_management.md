---
title: Gestion des données topographiques et bathymétriques
description: 
published: true
date: 2024-10-22T09:53:35.356Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:40:46.712Z
---

## Tabs{.tabset}


## Types de données utilisées

Hydra utilise des données topographiques et bathymétriques en formats :
- **raster** (modèles numériques de terrain) pour le calcul automatique des caractéristiques géométriques des objets de modélisation,
- **vecteurs** (semis de points x,y,z ou polylignes 3D) pour certaines fonctionnalités nécessitant des données non visibles ou insuffisamment précises sur les MNT:
  - génération de profils en travers de cours d'eau ([Cross sections](/model_building/river_free_surface_flow/cross_section/valley_cross_section)) à partir de levés bathymétriques,
  - calcul des cotes de liaisons ([liaisons en lot](/model_building#links_multi)) à partir de profils en long de berges, de digues ou de murettes, ...
  
La gestion des données topographiques se fait via le menu <img src="/0_hydra_icone.png" height="25px"/> **Terrain**, qui ouvre le gestionnaire de terrain (Terrain manager).


<img src="/topographic_bathymetric_data_management/5_manage_terrain_data_ui.png" width="550px"/>




## Données raster (DEM)

Les données raster sont appelées via l'onglet **DEM** (Digital Elevation Model) du Terrain manager.

Hydra exploite des données raster au format vrt (raster virtuel) appelant des dalles au format GeoTif. Ces fichiers doivent être définis dans le même système de projection que le projet.


> **note**
>
> Le raster virtuel est un fichier qui permet d'appeler plusieurs fichiers image source (appelées *dalles* ou *tuiles* afin de simplifier leur gestion dans l'interface (une seule couche - virtuelle - au lieu d'une couche par image). Ce format de données est imposé car les traitements et l'affichage des données raster sont plus performants avec plusieurs dalles de taille réduite qu'avec une seul dalle couvrant le territoire d'étude.
> 
> 

L'outil <img src="/topographic_bathymetric_data_management/5_vrt.png" height="25px"/> permet de créer un raster virtuel à partir de fichiers source de différents formats:

- Cliquer sur le bouton <img src="/topographic_bathymetric_data_management/5_vrt.png" height="25px"/> et sélectionner des fichiers source au format ARCGIS ASCII *.asc ou geotiff,

- sélectionner le système de projection des fichiers source,

<img src="/topographic_bathymetric_data_management/5_manage_terrain_data_dem_epsg_ui.png" width="400px"/>

- l'application génère des fichiers geotif dallés et un fichier vrt associé (dans le système de projection du projet), dans le sous répertoire Terrain du workspace associé au projet.





> **note**
>
> Les fichiers peuvent être déplacés manuellement et le fichier vrt renommé; il faut alors modifier le chemin d'accès dans le gestionnaire de terrain.
> 

Les raster virtuels peuvent être appelés ou supprimés via les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/>.

**Plusieurs raster peuvent être appelés**. Un ordre de priorité est alors fixé par l'utilisateur à chacun d'entre eux en cas de superposition; il correspond à l'ordre d'affichage des différentes sources dans le gestionnaire de données topographiques qui peut être modifié via les flèches <img src="/0_hydra_down.png" height="25px"/> et <img src="/0_hydra_up.png" height="25px"/>.

Les raster appelés sont chargés dans le gestionnaire de couches, et une analyse thématique avec ombrage est automatiquement appliquée; celle-ci peut être modifiée par l'utilisateur via le gestionnaire de couches.

<img src="/topographic_bathymetric_data_management/5_raster.png" width="620px"/>


<a name="terrain_points"></a>

## Données vectorielles

Ces données peuvent être exploitées pour certaines fonctionnalités spécifiques pour lesquelles les données MNT ne sont pas disponibles au manquent de précision:

- La génération de profils en travers de cours d'eau [Cross sections](/model_building/river_free_surface_flow/cross_section/valley_cross_section),

- Le calcul des cotes de liaisons ([liaisons en lot](/model_building#links_multi)). 

Pour pouvoir être exploitées par hydra, les données topographiques doivent être importées :
- Pour les **semis de points** dans la table *terrain points* (*points_xyz*) du schéma projet

- Pour les **polylignes 3D** dans la table  *terrain lines* (*lines_xyz*) du schéma projet

### Semis de points

Les données vectorielles de type semis de points (x, y, z) sont gérées via l'onglet **Terrain points** du Terrain manager.

<img src="/topographic_bathymetric_data_management/5_manage_terrain_data_vector_ui.png" width="550px"/>

Le bouton <img src="/topographic_bathymetric_data_management/import_terrain_points_button.png"/> permet d'importer des fichiers externes (*shape, gpkg, ...*) de semis de points dans le projet (cf. également [Import fihiers externes](/advanced/import_ext)). Cet import de fichiers externe se fait en 2 étapes:
- Etape 1 : Importer le fichier externe dans le schéma work de la base de données à l'aide du bouton <img src="/0_hydra_+.png" height="25px"/>,
- Etape 2 : Affecter les champs de la table source aux champs de la table projet, puis cliquer sur Import.

<img src="/topographic_bathymetric_data_management/import_terrain_points_ui.png" width="750px"/>


Les boutons <img src="/0_hydra_+.png" height="25px"/> et <img src="/0_hydra_-.png" height="25px"/> permettent respectivement d'ajouter et de supprimer des groupes auxquels pourront être affectés les points importés (exemple : bathymetrie, murettes, digues_existantes, digues_projet, ...).

Pour associer des points à un groupe:

- Sélectionner les points considérés,

- Activer le bouton <img src="/topographic_bathymetric_data_management/group_points_button.png" align="middle"/>, et sélectionner le groupe auquel affecter les points sélectionnés.

<img src="/topographic_bathymetric_data_management/5_manage_terrain_data_vector_group.png" width="550px"/>

> :bulb: Si le champ *Points Type* est renseigné dans la table source (sous forme de chaine de caractère), le groupe est automatiquement créé dans Hydra, et les points lui sont associés.

### Polylignes 3D
Les poylignes 3D peuvent être directement copiées/collées dans la table du projet appelée **Terrain lines** (dans le groupe Project).

> **note**
>
>La fonction de copier/coller ne fonctionne que si les champs de la table source sont identiques à ceux de la table de destination ; si besoin, renommer les champs de la table source.

Pour la créatino depolylignes 3D à partir de semis de points se reporter au chapitre [Création de polylignes 3D](/advanced/outils_qgis#create_3D_polyline)






















