# Vanne (gate)

## Objectif           

Vérification de la loi de débitance d'une vanne fixe frontale, sur un modèle de rivière de géométrie simple et écoulement à surface libre.											
									
## Lois appliquées

Les équations font référence aux points 1 amont de l'écoulement et 2 aval de l'écoulement, avec :
- V1 et V2 vitesses de l'écoulement dans l'axe de la liaison (m/s)
- Z1 et Z2 ligne d'eau (m)
- E1 et E2 charge (m)
- g accélération de la pesanteur $$9.81m/s^-2$$

Les équations supposent $E_1 > E_2$, sinon l'écoulement se fait en sens inverse et $E_1 > z_s$, sinon il n'y pas d'écoulement.

Les différents régimes d'écoulement (dans les deux sens) sont définis sur le ldiagrame ci-dessous.

![](gate_equations.svg)

**Expression de la charge :**

- $E_1 = z_1 + \frac{v_1^2}{2 g}$
- $E_2 = z_2 + \frac{v_2^2}{2 g}$

La **vitesse prise en compte** pour le calcul de la charge dépend du type d'élément et de la position de la liaison:
- Casier, carrefour, noeud 1D hors extrémité de *branch* ou de *reach* (maillage latéral) : v = 0
- Noeud 1D en extrémité de *branch* ou de *reach* : vitesse sur le noeud
- Maille 2D : composante de la vitesse projetée sur l'axe de la liaison

**Condition seuil noyé:** $E_2 \geq z_s$

**Condition de passage seuil -> vanne**  $E_1 \geq z_v$:
- vanne dénoyée $E_2 < z_s$
- vanne noyée-dénoyée $z_s < E_2 < z_v$
- vanne noyée $E_2 > z_v$

<br/>
<br/>

| Régime de fonctionnement   |     Equation                                     |
|:---------------------------|:-------------------------------------------------|
| Déversoir dénoyé | $$QD_d = \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-z_s)^\frac{3}{2}$$  |
| Déversoir noyé | $$QD_n =  \frac{2}{3} C_d b_s \sqrt{2 g} (E_1-E_2)^\frac{3}{2} + C_n b_s \sqrt{2 g} (E_2 - z_s)(E_1 - E_2)^\frac{1}{2}$$  |
| Vanne dénoyée | $$QV_d = \frac{2}{3} C_d b_s \sqrt{2 g}  \left[ (E_1-z_s)^\frac{3}{2} - (E_1 - z_v)^\frac{3}{2} \right]$$  |
| Vanne noyée-dénoyée | $$QV_{nd} = \frac{2}{3} C_d b_s \sqrt{2 g} \left[ (E_1-E_2)^\frac{3}{2} - (E_1 - z_v)^\frac{3}{2} \right] + C_n b_s \sqrt{2 g} (E_2 - z_s)(E_1 -E_2)^\frac{1}{2}$$  |
| Vanne noyée | $$QV_n = C_n b_s \sqrt{2 g} (z_v - z_s)(E_1 - E_2)^\frac{1}{2}$$  |

<br/>

Les équations ci-dessus respectent la continuité des débits à la frontière entre deux régimes d’écoulement. Elles sont dérivables analytiquement à partir des lois générales de l’hydraulique pour les régimes : DD, VD et VN. Les formes des équations mises en œuvre pour les autres régimes sont définies de façon à satisfaire la continuité des débits entre deux régimes.

<br/>

### **Paramètres**
------
Le coefficient **Cd** est le **coefficient de seuil déversant en régime dénoyé**. Il est défini par l’utilisateur, il est généralement égal à sa valeur théorique : 0.6. L’utilisateur peut rentrer une valeur plus faible pour tenir compte d’un coefficient de contraction latérale de la veine fluide lorsque b/b0<1, où b0 est la largeur du canal ou du collecteur en amont de la vanne. On recommande d’appliquer le coefficient de correction « alp » suivant sur CD:

| b/b0   |     alp = Cd/0.6         |
|:-------|:-------------------------|
| 1      |   1                      |
| 0.8    |   0.8                    |
| 0.6    |   0.6                    |
| 0      |   0.6                    |

Le coefficient **Cn** est le **coefficient de seuil déversant ou de vanne en régime noyé**. Il est par défaut égal à 0.6, sauf dans le cas où la liaison relie deux extrémité de reach ou de branch où ce dernier est alors sélectionné automatiquement par le programme qui tient compte du rapport de section s/S (s'il n'est pas forcé par l'utilisateur via l'option *Submerged coef*) où :
- s est la section d’ouverture de la vanne,
- S est la section mouillée dans le collecteur aval de la liaison.

| s/S   |     Cn                    |
|:-------|:-------------------------|
| 0      |   0.6                    |
| 0.3    |   0.73                   |
| 0.6    |   1                      |
| 0.8    |   1.87                   |
| 0.9    |   3.12                   |
| 0.95   |   5                      |

Ce tableau résulte d’une analyse hydraulique spécifique basée sur la formulation de perte de charge à la Borda. L’utilisateur peut également imposer une Valeur de son choix pour le coefficient Cn.

## Modèle test (assainissement)

Lilaison latérale entre deux collecteurs.

![](gate_link_schema_test.svg)

On impose une côte aval.

## Paramètres

| Coefficient   | Attribut    | Valeur   | Description                                                                                       |
|:--------------|:------------|:---------|:--------------------------------------------------------------------------------------------------|
| $C_d$         | `cc`        | 0.55     | coefficient de seuil dénoyé                                                                       |
| $C_n$         |             | 0.5      | coefficient de seuil noyé valeur par défaut $C_n=C_d$ utilisée dans le test avec porte ascendante |
| $b_s$         | `width`     | 1.5      | largeur de la crête                                                                               |
| $z_s$         | `z_invert`  | 21       | cote du seuil                                                                                     |
| $z_v$         | `z_ceiling` | 22       | cote de la vanne                                                                                  |
|               | `z_gate`    |          | `z_invert` pour un porte ascendante, sinon `z_ceiling`                                            |

## Résultats

### Porte ascendante

|   $z_1$ |   $z_2$ | seuil noyé   | vanne noyée   |   $QD_{th}$ |   $QD_{calcul}$ |   Différence relative [%] | Validé (< 2%)   |   $C_n$ |
|--------:|--------:|:-------------|:--------------|------------:|----------------:|--------------------------:|:----------------|--------:|
| 21      | 21      | -            | -             |    0        |     0           |                  0        | oui             |    0.55 |
| 21.4996 | 21.0013 | oui          | non           |    0.86029  |     0.849962    |                 -1.20049  | oui             |    0.55 |
| 21.6793 | 20.8752 | non          | non           |    1.36397  |     1.34754     |                 -1.20453  | oui             |    0.55 |
| 21.4997 | 21.2006 | oui          | non           |    0.799414 |     0.793628    |                 -0.723757 | oui             |    0.55 |
| 21.4999 | 21.4101 | oui          | non           |    0.514646 |     0.512561    |                 -0.405149 | oui             |    0.55 |
| 21.5998 | 21.5002 | oui          | non           |    0.653446 |     0.651249    |                 -0.336214 | oui             |    0.55 |
| 21.9992 | 21.6014 | oui          | non           |    1.99735  |     1.98624     |                 -0.556317 | oui             |    0.55 |
| 22.9985 | 22.0024 | oui          | oui           |    3.64716  |     3.63796     |                 -0.252275 | oui             |    0.55 |
| 23      | 23      | oui          | oui           |   -0        |     1.56327e-10 |                  0        | oui             |    0.55 |
| 22.9992 | 22.5007 | oui          | oui           |    2.5801   |     2.57391     |                 -0.239797 | oui             |    0.55 |
| 22.9974 | 21.0323 | oui          | oui           |    4.44979  |     4.39754     |                 -1.17424  | oui             |    0.55 |
| 21.6    | 22      | oui          | oui           |   -2.00302  |    -1.98624     |                 -0.837793 | oui             |    0.55 |
| 21.5    | 21.6    | oui          | non           |   -0.654834 |    -0.651249    |                 -0.547443 | oui             |    0.55 |
| 21.41   | 21.5    | oui          | non           |   -0.515255 |    -0.512561    |                 -0.522928 | oui             |    0.55 |
| 21.2    | 21.5    | oui          | non           |   -0.800616 |    -0.793628    |                 -0.872783 | oui             |    0.55 |

### Porte descendante

|   $z_1$ |   $z_2$ | seuil noyé   | vanne noyée   |   $QD_{th}$ |   $QD_{calcul}$ |   Différence relative [%] | Validé (< 2%)   |   $C_n$ |
|--------:|--------:|:-------------|:--------------|------------:|----------------:|--------------------------:|:----------------|--------:|
| 21      | 21      | -            | -             |    0        |     0           |                  0        | oui             |     0.5 |
| 21.4996 | 21.0013 | oui          | non           |    0.859985 |     0.849657    |                 -1.20093  | oui             |     0.5 |
| 21.6793 | 20.8752 | non          | non           |    1.36397  |     1.34754     |                 -1.20453  | oui             |     0.5 |
| 21.4997 | 21.2006 | oui          | non           |    0.762968 |     0.757373    |                 -0.733296 | oui             |     0.5 |
| 21.4999 | 21.4101 | oui          | non           |    0.47382  |     0.471968    |                 -0.390847 | oui             |     0.5 |
| 21.5998 | 21.5002 | oui          | non           |    0.601003 |     0.599114    |                 -0.314373 | oui             |     0.5 |
| 21.9992 | 21.6014 | oui          | non           |    1.87134  |     1.86124     |                 -0.539782 | oui             |     0.5 |
| 22.9985 | 22.0024 | oui          | oui           |    3.3156   |     3.30854     |                 -0.212957 | oui             |     0.5 |
| 23      | 23      | oui          | oui           |   -0        |     1.42223e-10 |                  0        | oui             |     0.5 |
| 22.9992 | 22.5007 | oui          | oui           |    2.34554  |     2.3406      |                 -0.210728 | oui             |     0.5 |
| 22.9974 | 21.0323 | oui          | oui           |    4.43475  |     4.38254     |                 -1.17727  | oui             |     0.5 |
| 21.6    | 22      | oui          | oui           |   -1.87696  |    -1.86124     |                 -0.837361 | oui             |     0.5 |
| 21.5    | 21.6    | oui          | non           |   -0.602307 |    -0.599114    |                 -0.530139 | oui             |     0.5 |
| 21.41   | 21.5    | oui          | non           |   -0.474394 |    -0.471968    |                 -0.51134  | oui             |     0.5 |
| 21.2    | 21.5    | oui          | non           |   -0.764224 |    -0.757373    |                 -0.896467 | oui             |     0.5 |

### Porte ascendante avec clapet

|   $z_1$ |   $z_2$ | seuil noyé   | vanne noyée   |   $QD_{th}$ |   $QD_{calcul}$ |   Différence relative [%] | Validé (< 2%)   |   $C_n$ |
|--------:|--------:|:-------------|:--------------|------------:|----------------:|--------------------------:|:----------------|--------:|
| 21      | 21      | -            | -             |    0        |     0           |                  0        | oui             |     0.5 |
| 21.4996 | 21.0013 | oui          | non           |    0.859985 |     0.849657    |                 -1.20093  | oui             |     0.5 |
| 21.6793 | 20.8752 | non          | non           |    1.36397  |     1.34754     |                 -1.20453  | oui             |     0.5 |
| 21.4997 | 21.2006 | oui          | non           |    0.762968 |     0.757373    |                 -0.733296 | oui             |     0.5 |
| 21.4999 | 21.4101 | oui          | non           |    0.47382  |     0.471968    |                 -0.390847 | oui             |     0.5 |
| 21.5998 | 21.5002 | oui          | non           |    0.601003 |     0.599114    |                 -0.314373 | oui             |     0.5 |
| 21.9992 | 21.6014 | oui          | non           |    1.87134  |     1.86124     |                 -0.539782 | oui             |     0.5 |
| 22.9985 | 22.0024 | oui          | oui           |    3.3156   |     3.30854     |                 -0.212957 | oui             |     0.5 |
| 23      | 23      | oui          | oui           |   -0        |     1.42223e-10 |                  0        | oui             |     0.5 |
| 22.9992 | 22.5007 | oui          | oui           |    2.34554  |     2.3406      |                 -0.210728 | oui             |     0.5 |
| 22.9974 | 21.0323 | oui          | oui           |    4.43475  |     4.38254     |                 -1.17727  | oui             |     0.5 |
| 21.6    | 22      | oui          | oui           |   -1.87696  |     0           |                  0        | oui             |     0.5 |
| 21.5    | 21.6    | oui          | non           |   -0.602307 |     0           |                  0        | oui             |     0.5 |
| 21.41   | 21.5    | oui          | non           |   -0.474394 |     0           |                  0        | oui             |     0.5 |
| 21.2    | 21.5    | oui          | non           |   -0.764224 |     0           |                  0        | oui             |     0.5 |
