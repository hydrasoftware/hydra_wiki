---
title: Exploitation des résultats
description: 
published: true
date: 2024-06-11T08:49:56.005Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:40:10.348Z
---

Le présent chapitre présente les outils d'exploitation des résultats intégrés à l'interface :

- Profils en long,
- Visualisation des courbes x(t) aux nœuds de calcul (hydrogrammes, limnigrammes, vitesses, ...),
- Visualisation rapide des grandeurs min et max calculées aux noeuds hydrauliques,
- Visualisation animée des hauteurs et vitesses (rivière),
- Visualisation synthétique en plan des grandeurs maximales (hauteurs et vitesses en rivière, niveau de mise en charge des collecteurs et points de débordement en réseaux d'assainissement).


# Profils en long

Le profil en long est un outil intégré dans Qgis sous la forme d’un panneau de contrôle (**Longitudinal profil dock**) permettant de visualiser :

- les résultats de calcul : profils en long des lignes d'eau, des débits, des vitesses, ... le long des biefs de cours d'eau (reach) ou des branches de collecteurs (branch),
- la géométrie des biefs de cours d'eau ou/et des tronçons d'assainissement, ainsi que les ouvrages et liaisons associés.

<img src="/results/dock_profil_doc.png" width="800px"/>

La sélection des biefs de rivière ou des branches d'assainissement se fait via le bouton <img src="/0_hydra_+.png" height="25px"/>, puis sélection de l'objet considéré. Le bouton <img src="/0_hydra_-.png" height="25px"/> permet de supprimer un des éléments du profil en long.

Les flèches directionnelles permettent de modifier l'ordre des biefs et des branches.


L'outil permet de tracer, via le bouton <img src="/results/btn_profile_display_graph.png" width="300px"/> :


- La **géométrie des objets de modélisation** le long des biefs et des branches :
  - Profils en long :
    - du radier (*invert*, par défaut),
    - des voûtes des ouvrages souterrains (*vault*, par défaut),
    - du terrain naturel au droit des ouvrages souterrains (*ground*, par défaut) : défini à partir des cotes zground des regards (manhole) et des river node portant les profils en travers de rivière (cross sections).
    - des berges rives gauche (*Reach left bank*) et droite (*Reach right bank*) des biefs de type *Valley*
  - Objets de modélisation associés attachées aux biefs et aux branches :
    - *Links* : les cotes de radier des liaisons [Links](/model_building/#liaisons),
    - *Singularities* : les ouvrages [Structures](/model_building/#ouvrages),
    - *Cross sections* : les profils en travers [Cross section](/model_building/river_free_surface_flow/river_cross_section_point) pour les biefs de rivière 
- Les **résultats de calcul** pour le scénario actif :
  - Disponibles quelles que soient les options de calcul :
    - *Water level z* : ligne d'eau,
    - *Water flow Q* : débit,
    - *Water speed v* : vitesse en lit mineur,
    - *Energy E* : ligne de charge,
    - *Critical Zc* : hauteur critique,
  - Disponibles lorsque des options de transport sont activées pour le scénario actif :
    - *Quality* : accessible lorsqu'une option de pollution ou de qualité est activée,
    - *Solid flow Qs* : débit solide lorsque l'option transport solide est activée,
    - *Silting hight hs* : hauteur de dépôt ou d'érosion lorsque l'option transport solide est activée.
    
    
Le menu <img src="/results/btn_profile_time.png" width="350px"/> permet de lancer l'animation dynamique des résultat de calcul ou de se postionner sur un instant donné de la simulation ou le programme se trouve, on peut arrêter la simulation ou la lancer avec le bouton à l’intérieur. 


Le menu <img src="/results/btn_profile_minmax.png" width="150px"/> permet d’afficher les valeurs maximales ou minimales des résultats sur les profils. 



> **warning**
>
>  cet outils affiche les minima / maxima que prennent toutes les grandeurs calculées.
> 

## Navigation sur le graphique

Jusqu'à trois graphiques sont créés suivant les options d'affichage sélectionnées :

- un pour les lignes d'eau, ignes d'énergie et hauteurs critiques,
- un pour les vitesses,
- Un pour les paramètres de transport et de qualité.

Le scroll de la souris lorsque le curseur est positionné sur l'abscisse permet de zoomer ou dézoomer sur les abscisses; les trois graphiques sont synchronisés. 

Le scroll de la souris lorsque le curseur est positionné sur les ordonnées permet de zoomer ou dézoomer sur les ordonnées.

Le bouton <img src="/results/btn_profile_home.png"/> permet de réinitialiser les profils.



Des outils avancés de zoom sont également disponibles:

<img src="/results/btn_profile_zoom_ad.png" width="150px"/>



> **warning**
>
>  ces outils désynchronisent les 3 graphiques. Pour les réinitialiser, utiliser le bouton <img src="/results/btn_profile_home.png"/>.
> 
> 

## Comparaison de scénarios

La dernière boite de dialogue permet d’afficher d’autres scénarios(6) pour les comparer avec le scénario actif.

le bouton <img src="/0_hydra_+.png" height="25px"/> permet d'ajouter un scenario au scénario actif et le le bouton <img src="/0_hydra_-.png" height="25px"/> permet d'en retirer un.


> **warning**
>
>   seuls des scénarios de même durée et de même pas de calcul peuvent être comparés.     
> 

## Exports

Le bouton <img src="/results/btn_profile_copy_text.png"/> permet d’exporter les données du graphique dans le presse – papier (format texte). 


Le bouton export graphique <img src="/results/btn_profile_copy_image.png"/> permet de copier une prise de vue du graphique du profil affiché dans le presse-papier. 

## Enregistrement de paramétrages utilisateur

Le bouton <img src="/results/btn_profile_save.png"/> permet d’enregistrer le paramétrage du profil en long dans un fichier externe. Le bouton <img src="/results/btn_profile_open.png"/> permet de parcourir les fichiers de sauvegarde de paramètres enregistrés pour l'afficher. 




# Visualisation des courbes x(t)

La toolbar propose un outil de visualisation graphique <img src="/results/tb_visu_graph.png"/> permettant de récapituler les données disponibles sur différents nœuds de calcul sous forme de courbes x(t). 


La sélection d'un objet de modélisation permet d'afficher le graphique pour le scénario actif.

<img src="/results/visu_graph.png" width="700px"/>


> **warning**
>
>  pour pouvoir être sélectionné, l'objet doit être dans le modèle actif.
> 

Une fois le graphique affiché, il est possible d'ajouter ou de supprimer des courbes, pouvant être extraites de tous les objets de modélisation du projet pour tous les scénarios disposant de résultats de calcul:

- sélection du modèle auquel appartient l'objet de modélisation,
- sélection du type de l'objet de modélisation,
- sélection du scénario de calcul,
- sélection du paramètre de calcul.

Le graphique dispose de deux axes verticaux. Une fois ces sélections faites, l'ajout au graphique se fait via les boutons <img src="/results/btn_visu_add_graph1.png"/> ou  <img src="/results/btn_visu_add_graph2.png"/>.



Le bouton <img src="/results/btn_visu_reset_graph.png"/> permet de récupérer un graphique vide. 


Les différentes courbes affichées sur le graphique sont rappelées dans la fenêtre suivante:

<img src="/results/visu_list_curves.png" width="350px"/>


Le bouton <img src="/results/btn_visu_remove.png"/> permet d’enlever la courbe sélectionnée dans la fenêtre ci-dessus.  



> **note**
>
>  Lorsque la souris survole une courbe, un petit point rouge apparaît. Les coordonnées de ce point dans le repère de la courbe sont affichées au-dessus du graphique. 
> 

## Affichage de données externes

L'outil permet d'afficher des courbes issues de fichiers externes (fichiers de mesures par exemple).

- le bouton <img src="/results/btn_visu_import_csv.png" style="transform: scale(0.75);"/> permet d'importer des fichiers au format csv,
- le bouton <img src="/results/btn_visu_import_data.png" style="transform: scale(0.75);"/> permet d'importer des fichiers dans des formats spécifiques.



## Enregistrement de paramétrages utilisateur

Le bouton <img src="/results/btn_profile_save.png"/> permet d’enregistrer le paramétrage du graphique en cours dans un fichier externe. Le bouton <img src="/results/btn_profile_open.png"/> permet de parcourir les fichiers de sauvegarde de paramètres enregistrés pour les afficher. 



> **note**
>
>  Le contenu du graphique est stocké dans un fichier au format spécifique *.hgf, dans `.hydra\nom_projet\graphs`. 
> 

Les deux boutons avant/après permettant de naviguer entre les différents graphiques enregistrés dans le répertoire prévu à cet effet. 

## Outils avancés de paramétrage du graphique

Un panneau d’outil divers <img src="/results/btn_visu_graph_advanced.png"/> en haut à gauche de l’interface permet de configurer plus finement le graphique. 

Le bouton <img src="/results/btn_profile_home.png"/> permet de réinitialiser les échelles du graphique.


## Exports

Le bouton <img src="/results/btn_profile_copy_text.png" align="middle" style="transform: scale(1.0);"/> permet d’exporter les données du graphique dans le presse – papier (format texte). 


Le bouton export graphique <img src="/results/btn_profile_copy_image.png" align="middle" style="transform: scale(1.0);"/> permet de copier une prise de vue du graphique affiché dans le presse-papier. 


# Calcul de flux à travers une ligne de contrainte

## Principes

Le bouton <img src="/results/tb_hy_sum.png" style="transform: scale(1.0);"/> permet de sommer les hydrogrammes calculés sur des liaisons, des biefs de cours d'eau (*Reach*) et des rues (*Street*) intersectés par une ligne de contrainte.

Cette application permet notamment de calculer un hydrogramme sur un transect de vallée modélisé avec différents domaines.



Le calcul se fait en considérant un flux positif défini comme suit : 

<img src="/results/hydrograph_sum_scheme.png" width=350/>

<img src="/results/hydrograph_sum_formula.png" width=450/>

> Note
>
> Pour calculer un débit le long d'une frontière quelconque (pouvant "couper" différents domaines), utiliser une ligne de contrainte de type `Non topological`. Ces lignes de contraintes ne sont pas prises en compte dans la définition des coverage.
>

## Mise en oeuvre

Après sélection du bouton <img src="/results/tb_hy_sum.png"/>, sélectionner une ligne de contrainte. L'interface suivante s'affiche :

<img src="/results/hydrograph_sum_graph.png" width=600/>

Le graphique présente le flux total traversant le segment ab. Il est également possible de visualiser les hydrogrammes élémentaires représentant chacun le flux associé à une des liaisons traversant le segment ab.



> **note**
>
>Le calcul du flux peut se faire sur une ligne de contrainte utilisée pour construire le modèle (dite topologique), ou sur une ligne de contrainte sans fonction de construcion; un type **Non topological constrain** doit alors lui être affecté.   
> 
> 
> <img src="/results/constrain_ui_for_hydrograph.png" width=500/>
> 
> 
> 

# Min / Max

Le bouton <img src="/results/tb_minmax.png"/> permet d'afficher de façon concise les valeurs maximales et minimales de différents paramètres calculés pour l’objet sélectionné et le scénario actif. Après sélection d'un objet de modélisation, la fenêtre suivante s'affiche :



<img src="/results/visu_minmax.png" width=400/>

Dans cet exemple, l’objet est un nœud de rivière présentant :

- le débit total maximal et minimal (q),
- la hauteur d’eau (z),
- la vitesse moyenne dans le lit mineur (vit_lm),
- le débit dans le lit majeur (q_lmaj).  



# Rendus cartographiques : visualisation synthétique des grandeurs maximales
<a name="animation_map_results"></a>

Le bouton <img src="/results/tb_display_results.png"/> **Synthetic results** permet de visualiser les grandeurs maximales calculées aux nœuds de calcul pour le scénario actif. Cet utilitaire exploite les sorties de synthèses produites par le moteur de calcul au format csv, disponibles dans les répertoires hydrologie et hydraulique du scénario de calcul actif :

- **Hydrologie** :
  - {scenario}_{modele}_catchment.csv
  - {scenario}_{modele}_pipe_hydrol.csv
- **Hydraulique** :
  - Domaine **assainissement** :
    - {scenario}_{modele}_link.csv
    - {scenario}_{modele}_manhole.csv
    - {scenario}_{modele}_pipe.csv
  - Domaines **rivière** et **écoulements à surface libre**
    - {scenario}_{modele}_river.csv
    - {scenario}_{modele}_river_maj.csv    
    - {scenario}_{modele}_surface.csv    
    
    
Lors de l'activation du bouton <img src="/results/tb_display_results.png"/>, l'application charge les fichiers csv dans l'interface, effectue une jointure avec les objets de modélisation associés (sous forme de vue) et propose des analyses thématiques permettant de visualiser les grandeurs hydrauliques associées.

Cette application s'appuie sur des vues des objets de modélisation. Ces dernières doivent être cohérentes avec la topologie du modèle utilisé lors du dernier calcul. Pour se faire, elles doivent être générées (ou mises à jour) lorsque la structure du modèle est modifiée via l'utilitaire Synthetic results setting disponible dans le menu <img src="/0_hydra_icone.png" height="25px"/> **Settings**

<img src="/results/synthetic_results_settings.png" width=500/>

La génération de ces fichiers pouvant être longue (quelques minutes) pour de gros modèles, cette disposition est mise en place pour éviter de les regénérer systématiquement après le calcul.

> **Note**
>
>  Il est possible d'enregistrer les vues dans un fichier shape, et de leur appliquer les analyses proposées par défaut en leur affectant le fichier de style (qml) préalablement enregistré. Pour de gros modèles, l'exploitation de fichiers shape peut être plus fluide que le travail sur les vues générées par l'application.
> 

<a name="river_map_results"></a>
# Sorties cartographiques spécifiques aux applications rivière et écoulements à surface libre

>:warning:Les outils asociés aux rendus cartographiques pour les applications de rivière et d'écoulements à surface libre sont en cours d'évolution. PLusieurs outils sont ainsi disponibles pour des fonctionnalités similaires, certaines en version Bêta.

## Visualisation animée des hauteurs et vitesses

Le bouton <img src="/results/tb_show_w15.png"/> **Animation map results** permet de visualiser l'évolution temporelle des mailles 2D et rues mises en eau et les vitesses d'écoulement associées pour le scénario actif.

![carto_dynamique.svg](/results/carto_dynamique.svg)


>Pour fluidifier l'affichage sur les gros modèles, il peut être nécessaire de réduire la vitesse de défilement.

>`Stride` permet de 'sauter' des pas de temps pour accéleer le défilement (notamment lorsque les pas de temps de srtie sont resserrés).

## Visualisation animée des hauteurs et vitesses (Bêta)
Un second utilitaire permet de disposer d'une animation des hauteurs de submersionet des vitesses d'écoulement à l'échelle des noeuds et mailles de calcul.

Cet utilitaire est disponible via la boite à outils de QGIS :
![qgis_menu_traitement.png](/results/qgis_menu_traitement.png)
Puis dans la boite à outils *hydra* :
![processing_hydra.png](/results/processing_hydra.png)
- ***selafin Rendering mesh*** : génération d'un fichier de triangulation des noeuds de calcul pour un modèle donné. Cette opération est à réaliser après chaque modification de la structure du modèle (ajout de noeuds de calculs)
![processing_rendering_mesh.png](/results/processing_rendering_mesh.png)
- ***selafin Results to mesh*** : génération d'un fichier de résultats au format selafin (**.slf*) pour un modèle et un scénario, qui comprend trois paramètres :
	- les cotes d'eau
  - les hauteurs d'eau
  - les vitesses d'écoulement
 ![processing_resutlts_to_mesh.png](/results/processing_resutlts_to_mesh.png)
 Ce fichier est créé dans le répertoire *Hydraulique* du scénario de calcul.
- L'**exploitation du fichier selafin** se fait via les **outils de QGIS** :
	- **Charger le fichier selafin** via le menu *Couche\Ajouter une couche\Ajouter une couche maillage*; le fichier se charge dans le gestionnaire de couches
  - **Paramétrer l'affichage de la couche** (clic droit, paramètres puis Symbologie). Les trois paramètres disponibles dans le fichier Selafin peuvent être affichés
  	- La ligne d'eau (alttudes)
    - Les vitesses d'écoulement
    - Les hauteurs d'eau
 Sur l'exemple ci-dessous, les paramètres hauteurs d'eau (rendu surfacique) et vitesses d'écoulement (rendus vectoriels) sont sélectionnés.
![qgis_param_selafin1.png](/results/qgis_param_selafin1.png)
![qgis_param_selafin2.png](/results/qgis_param_selafin2.png)

Le bouton ![qgis_menu_controle_temporel.png](/results/qgis_menu_controle_temporel.png) de la barre d'outils de QGIS permet d'afficher la barre de contrôle temporel associée à la couche sélectionnée :
![qgis_ui_controle_temporel.png](/results/qgis_ui_controle_temporel.png)
Le curseur permet d'afficher l'évolution des paramètres sélectionnés à l'échelle des noeuds de calcul.

![selafin_graphic_output.png](/results/selafin_graphic_output.png)

>:warning: les rendus sont actuellement simplifiés sur les domaines 1D:
>    - les hauteurs d'eau calculées sur les berges sont affectées à toute l'emprise des domaines,
>    - les vitesses correspondent aux vitesses calculées en lit mineur.

## Croisement des lignes d'eau avec le Modèle Numérique de Terrain
Deux outils permettent de faire cette opération :

### L'utilitaire **Crgeng**
Accessible depuis le menu *Hydra\Advanced tools*. Une notice détaillée de cet utilitaire est disponible via le bouton **?**.
- Appeler un **fichier de commande** (format ASCII) tel que défini dns la documentation via le bouton **+** (le fichier peut être édité via le bouton d'édition de l'interface). Plusieurs options sont disponibles:
    - temps de sortie,
    - grandeurs (hauteurs, vitesses, aléa) pour un scénario ou enveloppe maximale de plusieurs scénarios
    - format : vectoriel (shape) ou raster (asc)
    - options avancées : aléa (croisement hauteurs et vitesses, comparaison de scénario (gain)
- Sélectionner le fichier dans *Control file* puis cliquer sur Go : le programme est lancé
- Les fichiers de résultat sont enregistrés dans le répertoire Carto du workspace du projet
![crgeng_ui_hydra.png](/results/crgeng_ui_hydra.png)


> :bulb: le bouton ![crgeng_bouton.png](/results/crgeng_bouton.png) permet de lancer crgeng pour générer les **raster de hauteurs et de vitesses maximales** pour le **scénario actif**. Si plusieurs modèles sont appelés par ce scénario, les sorties sont faites sur tous les modèles.
> Les sorties sont disponibles dans le répertoire carto du workspace du prijet et comprennent :
>    - un fichier **.tif* **compessé** par modèle respectivement pour les hauteurs et les vitesses
>        - {scenario}_{modele}_-999.0_nvz_merged.norm.tif
>        - velocity_{scenario}_{modele}_-999.0.tif
>    - un fichier **.vrt* assembalnt les fichiers **tif*
>        - depth_{scenario}.vrt
>        - velocity_{scenario}.vrt

![crgeng_raster_output.png](/results/crgeng_raster_output.png)

> crgeng peut également être lancé via les **outils de traitement de QGIS** (cf. ci-dessus). Comme le bouton ci-dessus, ces outils opèrent un **post traitement des sorties brutes de crgeng**, en générant un fichier raster unique au format **.tif* compressé par sous modèle beaucoup plus simple à exploiter dans QGIS que les fichiers asc dallés produits nativement par crgeng.
>    - crgeng Depth raster : raster de hauteurs d'eau pour un scénario, avec possibilité de choiri le temps de sortie
>    - crgeng Velocity raster : raster de vitesses d'écoulement pour un scénario, avec possibilité de choiri le temps de sortie
>    - crgeng Depth raster envelope : raster de hauteurs d'eau maximales pour un groupe de scénarios, avec possibilité de choiri le temps de sortie. Les groupe de scénarios sont ceux définis dans le menu *hydra\Scenarios\grouping*.
>    - crgeng Velocity raster envelope : raster de vitesses d'écoulement maximales pour un groupe de scénarios, avec possibilité de choiri le temps de sortie. Les groupe de scénarios sont ceux définis dans le menu *hydra\Scenarios\grouping*.

### Un outil accessible via la boite à outils QGIS
Cet outil exploite les fichiers selafin présentés ci-dessus, et permet de générer un raster de hauteurs d'eau pour le modèle et le scénario sélectionné par croisement entre la ligne d'eau issue de la triangulation (selafin) et le MNT sélectionné :
	*hydra\River and free surface flow mapping\selafin Rasterize mesh results at dem resolution*
![processing_raterize_mesh_results.png](/results/processing_raterize_mesh_results.png)
Le fichier **.tiff* est généré dans le répoertoire hydraulique du scénario de calcul.
