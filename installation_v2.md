---
title: Procédure d'installation et de mise à jour - Version 2
description: 
published: true
date: 2021-10-19T14:18:22.951Z
tags: 
editor: markdown
dateCreated: 2021-02-08T23:40:41.762Z
---


Le présent chapitre vise à décrire pas-à-pas la procédure d’installation sur un poste du plugin **hydra**. Cette procédure fait référence aux fichiers fournis dans le **package d'installation** que vous êtes invité à télécharger sur le site http://hydra-software.net après avoir choisi le type de licence souhaité (STARTER, PRO LOCAL, PRO NETWORK).

Le plugin **hydra** s’ajoute à une installation existante de QGIS et s’appuie sur une base de données relationnelle (hébergée sous PostgreSQL). La procédure complète se déroule en 4 étapes :

ETAPE 1 : Installation de **QGIS**

ETAPE 2 : Installation de **PostgreSQL** et de son extension spatiale **PostGis** via **Stackbuilder**

ETAPE 3 : Paramétrage et installation d'hydra et des différents composants nécessaires à son fonctionnement :

- installation de la bibliothèque **GDAL**
- installation du programme **GMSH**
- Création d'un utilisateur PostgreSQL
- Mise à jour des variables d'environnement
- Chargement du plugin hydra dans QGIS

L'étape 3 est réalisée automatiquement par le setup fourni dans le zip d'installation. 

ETAPE 4 : Une fois l'installation d'hydra finalisée, il vous faudra activer votre clé de licence : cette procédure est décrite au chapitre [Activation et gestion de votre licence](/installation/2_2_activation_licence).


> **warning**
>
> - L'ensemble des composants interface est compatible Linux, mais le moteur de calcul n'est à ce jour compilé que pour Windows. Le setup ne fonctionne que sous Windows.
> - L'installation ne peut se faire que sur un poste **64 bits**.
> - Afin que le setup fonctionne correctement, QGIS et PostGre doivent être installés dans leurs répertoires par défaut. Si ce n'est pas le cas, vous devrez réaliser une installation manuelle en suivant la [procédure d'installation avancée](/installation_manuelle).
> - La procédure d’installation est à exécuter **une seule fois**. La mise à jour ultérieure de l’application passe uniquement par la **mise à jour du plugin HYDRA** dans l’interface de QGIS.
> 
> 
> 
> 

<a name="premiere_installation"></a>

# Première installation

## Installation de QGIS 

Installez **QGIS** sur votre poste :   https://qgis.org/downloads/ ou utilisez une version déjà installée. **Les versions 2.14 « Long term release repository » à 2.18 « Latest release » sont supportées**.


## Installation de PostGreSQL et de son extension spatiale PostGis

**PostgreSQL**

Installez **PostgreSQL 9.6.1-1** :

- Lancez l'exe *postgresql-9.6.1-1-windows-x64.exe* disponible dans le package d'installation Hydra    
- A la demande du mot de passe pour le « superuser PostGre », notez le mot de passe fourni.
- Les autres paramètres sont à laisser par défaut.


> **warning**
>
>  Attention au port d’installation si plusieurs versions cohabitent sur le poste.
> 

____

**Postgis et EDB Language Pack**

A l’issue de l’installation de PostgreSQL, installez **Postgis** :

- Lancez l'exe *postgis_2_3_pg96.exe* disponible dans le package d'installation Hydra
- Validez toutes les étapes jusqu'à finalisation de l'installation


Puis installez **EDB Language Pack** (Add-ons, tools and utilities)

- Lancez l'exe *edb_languagepack_96.exe* disponible dans le package d'installation Hydra : clic droit, **Exécuter en tant qu'administrateur**  
- Validez toutes les étapes jusqu'à finalisation de l'installation


## Installation d'HYDRA 

Exécutez le fichier **setup_hydra.exe** qui permet l’installation du plugin hydra dans QGIS.

Après avoir sélectionné la langue et accepté la licence, le premier écran permet de configurer la connexion au serveur PostgreSQL installé en 3.2. Précisez le port d’installation (par défaut 5432) et le mot de passe choisi pour le « superuser postgres ».

<img src="/installation/config_postgre_1.png" width="400px" align="center"/>

Sur l’écran suivant, choisissez un mot de passe pour l’utilisateur local du serveur PostgreSQL qui sera créé (Cette page ne s’affichera pas si l’utilisateur est déjà existant).

<img src="/installation/config_postgre_2.png" width="400px" align="center"/>

Enfin, laissez cochée par défaut la case « Installer le pilote de protection HASP », qui installe un composant nécessaire lors de l’activation de votre clé de produit.

<img src="/installation/config_postgre_3.png" width="400px" align="center"/>

Validez et laissez le script s'exécuter.

Redémarrez le PC, puis ouvrez QGIS. Activez le plugin hydra via QGIS et le menu extensions > installer/gérer les extensions 

<img src="/installation/instal_hydra2.png" width="500px" align="center"/>


<a name="depot"></a>


> **note**
>
>  Le plugin le plus récent est également disponible en téléchargement sur un **dépôt** pouvant être ajouté aux adresses de dépôt d’extensions de QGIS **Extensions | Paramètres** pour une notification des futures mises à jour.
> 
> Adresse du dépôt : ``http://hydra-software.net/telechargement/hydra.xml``
> 
> 
> <img src="/installation/depot_plugin.png" width="500px" align="center"/>
> 
> 
> <img src="/installation/depot_plugin2.png" width="500px" align="center"/>
> 
> 
> 

## Debugage - Problème d'installation 

En cas d’erreur, l’intégralité des composants utilisés ainsi que les logs de l’installation sont localisés dans `C:\Program Files\Hydra`. Les logs (fichiers txt de ce répertoire) seront demandés pour toute sollicitation d’assistance.


<a name="activation_licence"></a>


# Activation et gestion de votre licence

Le lancement du moteur de calcul est contrôlé par une clé de licence, dont la procédure d'installation et d'activation est décrite ci-après. La construction des modèles peut être effectuée sans que la clé de licence soit activée.


L'email contenant votre clé de licence et l'ensemble des informations vous permettant de l'activer selon la procédure décrite ci-après vous est envoyé sous 24 à 48h ouvrées après votre inscription sur le site.


## Activation de la clé

Il existe deux méthodes pour activer votre clé :

- **ONLINE ACTIVATION** (activation en ligne) :  cette méthode est à utiliser si vous êtes sur l'ordinateur où est installé hydra, et que vous êtes connecté à Internet.

- **OFFLINE ACTIVATION** (activation manuelle / hors ligne) : cette méthode est à utiliser si vous souhaitez générer la licence depuis un autre ordinateur que celui où est installé hydra. Dans ce cas, l'ordinateur qui génère la licence doit être connecté à Internet mais celui qui a hydra n'a pas besoin de l'être.

### Online activation

Rendez-vous sur l'interface de gestion de clé accessible à l'adresse suivante (privilégier le navigateur internet Chrome) :

http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html

<img src="/installation/interface_client_ems.png" width="400px" align="center"/>

Entrez le numéro de clé de licence qui vous a été communiqué par mail et cliquez sur **Login**

Vous obtenez alors l'écran suivant :

<img src="/installation/ecran_ems.png" width="650px" align="center"/>

Cliquez sur **Online Activation**. 

Un message s'affiche alors pour vous avertir que la clé a bien été activée. Félicitations, vous êtes prêt à utiliser **hydra** !


> **warning**
>
>  si l'activation en ligne n'a pas fonctionné, plusieurs actions peuvent être explorées :
> 
> - Relancer l'activation online sur le navigateur Chrome
> 
> - Mettre à jour manuellement l'adresse d'activation de la licence
>   - Fermez l'*interface de gestion de clé* http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html
>   - Rendez-vous sur le *Sentinel Admin Control Center* http://localhost:1947/. 
>   - Dans le menu de gauche, sélectionnez **Configuration**. Allez sur le dernier onglet "Network" 
>   - Vérifiez les adresses URL qui se trouvent dans *EMS URL*. Vous devez y voir l'adresse suivante : http://licensing.hydra-software.setec.fr:8080 . Si ce n'est pas le cas, ajoutez-la.
>   
> 
>
> <img src="/installation/config_ems.png"/>
>   
>   - Ouvrez à nouveau l'*interface de gestion de clé* http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html et réessayez de faire l'activation en ligne.
>   
> - Faites une **activation offline**
> 
> 
> 

### Offline activation

La procédure qui suit fait appel au fichier RUS_DDYTE.exe contenu dans le **package d'installation d'hydra**.


> **note**
>
>  Pour l'activation hors ligne, l'ordinateur qui génère la licence doit être connecté à Internet mais celui qui accueillera hydra n'a pas besoin de l'être.
> 
> 

**Sur l'ordinateur où est installé hydra :**

- Executez **RUS_DDYTE.exe**

<img src="/installation/rus_2.png" width="380px" align="center"/>

- Choisissez "Installation of new protection key" et cliquez sur **Collect Information** (en bas à gauche de la fenêtre) pour générer un fichier c2v. Ce fichier contient l'empreinte de la machine.

**Sur l'ordinateur connecté à Internet :**

- Rendez-vous sur l'interface de gestion de clé accessible à l'adresse suivante :

http://licensing.hydra-software.setec.fr:8080/ems/customerLogin.html

<img src="/installation/interface_client_ems.png" width="400px" align="center"/>

- Entrez le numéro de clé de licence qui vous a été communiqué par mail et cliquez sur **Login**

- Vous obtenez alors l'écran suivant :

<img src="/installation/ecran_ems.png" width="650px" align="center"/>

- Cliquez sur **Offline Activation**

- Dans la fenêtre **Upload C2V**, téléchargez le fichier c2v que vous avez généré sur la machine où est installé hydra.  

<img src="/installation/rus_3.png" width="450px" align="center"/>

- Cliquez sur le bouton **Generate**

- Téléchargez le fichier V2C :

<img src="/installation/rus_4.png" width="600px" align="center"/>

**Sur l'ordinateur où est installé hydra :**

- Exécuez à nouveau **RUS_DDYTE.exe**

- Sélectionnez l'onglet **Apply License File**

- Dans "Update File", téléchargez le fichier V2C que vous avez générée sur l'interface de gestion de clé.

<img src="/installation/rus_5.png" width="450px" align="center"/>

- Cliquez sur **Apply update**. Un message s'affiche alors pour vous avertir que la clé a bien été activée. Félicitations, vous êtes prêt à utiliser **hydra** !


<a name="controle_cle"></a>

## Contrôle de la clé

Afin de vérifier que votre clé est bien activée, vous pouvez vous rendre sur le *Sentinel Admin Control Center* http://localhost:1947/.

Dans le menu de gauche, sélectionnez "Sentinel keys". Vérifiez alors que votre clé apparait bien dans la liste à droite. 


<a name="transfert_licence"></a>

## Transfert de la license

Vous pouvez utiliser transférer une license d'un ordinateur (ordinateur "source") à un autre (ordinateur "receveur"). 

Cette procédure se déroule en 3 étapes et nécessite :

- Une connexion Internet
- RTE installé sur les deux ordinateurs (cf. 2.3.1)
- RUS_DDYTE.exe sur les deux ordinateurs

**1. Collecte des informations sur l'ordinateur "receveur" :** 

- Exécutez RUS_DDYTE.exe sur l'ordinateur "receveur", ,

- Sélectionnez l'onglet **Transfer License**

- Suivez les instructions décrites dans "Step 1" pour collecter les informations de l'ordinateur receveur et enregistrez les dans un fichier. Assurez-vous que ce fichier est accessible depuis l'ordinateur "source" (ou copiez le).

<img src="/installation/transfer_1.png" width="450px" align="center"/>

**2. Génération du fichier de transfert**

- Exécutez RUS_DDYTE.exe sur l'ordinateur "source"

- Sélectionnez l'onglet **Transfer License**

- Suivez les instructions décrites dans "Step 2" pour sélectionner la SL key à transférer, lire le fichier contenant les informations de l'ordinateur "receveur" et générer un fichier de transfert de licence (h2h). Assurez-vous que ce fichier est accessible depuis l'ordinateur "receveur".

- Une fois cette étape réalisée, la clé n'est plus disponible sur l'ordinateur "source".

<img src="/installation/transfer_2.png" width="450px" align="center"/>


> **warning**
>
>  Assurez-vous de garder une copie du fichier de transfert jusqu'à la fin de la procédure de transfert. Si vous perdez ce fichier, vous perdez votre licence !
> 

**3. Transfert de la licence**

- Sur l'ordinateur "receveur", ouvrez l'onglet "Apply License File" sur l'utilitaire RUS

- Dans "Update File", téléchargez le fichier de transfert de licence (h2h)

- Cliquez sur **Apply Update**. La clé est maintenant installée sur l'ordinateur "receveur".

<img src="/installation/transfer_3.png" width="450px" align="center"/>


<a name="mise_a_jour_plugin"></a>


# Mise à jour du plugin

## Procédure de mise à jour

Pour mettre à jour hydra :

- Fermez QGIS,

- Supprimez le répertoire ``C:\Users\name\.qgis2\python\plugins\hydra``

- Dézippez le fichier **hydra.zip** dans ``C:\Users\name\.qgis2\python\plugins``,

Le plugin sera mis à jour à l'ouverture de QGIS.


> **note**
>
>  Si le dépôt du plugin hydra a été paramétré et est accessible (cf. Première installation d'hydra), la mise à jour du plugin est réalisée via le menu extensions > installer/gérer les extensions > Tout mettre à jour
> 
> 
> <img src="/installation/instal_hydra2.png" width="500px" align="center"/>
> 

## Compatibilité projet / version plugin

Lors du travail avec le plugin HYDRA, la gestion des versions est effectuée à deux niveaux : on distingue la version du **plugin HYDRA**, qui trace la version du code de l’interface, et le **numéro de version du projet**, qui trace le format dans lequel se trouve la base de données liée au projet.

### Versions du plugin

Le numéro de version du plugin est accessible via le gestionnaire d’extension de QGIS. 

Que le plugin soit installé à partir d’un dépôt en ligne ou via une archive .zip, le **numéro de version** est écrit **en tête de la page de présentation du plugin** :

<img src="/installation/version_hydra.png" width="500px" align="center"/>

A chaque itération sur le code du plugin HYDRA, ce numéro de version est incrémenté de façon à refléter l’importance des évolutions apportées.

A chaque nouvelle version, un *changelog* résume également les principaux changements : nouvelles fonctionnalités et corrections de bugs.

### Versions du projet

Une mention est également présente dans le *changelog* en cas d’évolution de la structure du modèle de données (la base PostgreSQL) : *"Database version increased to … "*.

Ainsi, chaque version du plugin est conçue pour fonctionner avec des projets d’une structure précise. 

Le **numéro de version de la structure de données** (des projets) est mentionné en tête du **Gestionnaire de Projets** (Menu *Manage projects*) :

# Optimisation de l'installation

La procédure qui suit fait appel aux fichiers contenus dans le dossier intitulé **3-licence** du **package d'installation d'hydra**, et qui contient :



## Paramétrage de QGIS

### Affectation de plusieurs cœurs du processeur au rendu graphique

Afin d’accélérer le rafraîchissement des couches graphiques après déplacement ou zoom de la fenêtre d’affichage, cocher l’option ci-dessous dans le menu Préférences/Options de QGIS :

<img src="/installation/qgis_rendu_graphique.png" width="600px" align="center"/>



## Paramétrage de la carte graphique

Afin d’optimiser le fonctionnement de QGIS et de permettre à certaines applications d’hydra de fonctionner (restitution dynamique des résultats de calcul notamment), l’utilisation d’une carte graphique dédiée est recommandée.
La présente note précise comment forcer QGIS à utiliser la carte graphique la plus puissante de votre ordinateur (i.e. la carte graphique dédiée plutôt que celle intégrée à la carte mère généralement utilisée par défaut).


### PC portables
 

**Cartes graphiques NVIDIA**

Ouvrez le panneau de configuration Nvidia <img src="/installation/cg_icone_nvidia.png" width="25px" align="center"/> , vous pouvez : 

- le trouver dans la zone de notification en bas à droite de votre bureau (faites apparaître l’ensemble des icônes avec la flèche si il n’est pas visible directement <img src="/installation/cg_icone_nvidia2.png" width="100px" align="center"/> ),
- faire un clic droit de la souris sur le bureau et sélectionner le panneau Nvidia dans la liste,
- passer par le panneau de configuration de Windows.



Une fois le panneau Nvidia ouvert, cliquez sur « Gérer les paramètres 3D » : 

<img src="/installation/cg_paneau_nvidia.png" width="250px" align="center"/>

- Cliquez sur l’onglet « Paramètres de programme »,
- Cliquez sur « ajouter » puis  « parcourir »,
- Allez chercher l’exe qgis (ou les différents exe si plusieurs versions utilisées : qgis-bin.exe ou qgis-ltr-bin.exe (dans `C:\Program Files\QGIS 2.14\bin` ou `C:\Program Files\QGIS 2.18\bin`)

<img src="/installation/cg_paneau_nvidia2.png" width="450px" align="center"/>

- Sélectionnez « processeur Nvidia haute performance », 
- Cliquez sur « appliquer ».

<img src="/installation/cg_paneau_nvidia3.png" width="450px" align="center"/>


**Cartes graphiques AMD RADEON**

Ouvrez le panneau de configuration Amd settings <img src="/installation/cg_icone_nvidia.png" align="middle" width="25px" align="center"/>, vous pouvez :



- le trouver dans la zone de notification en bas à droite de votre bureau (faites apparaître l’ensemble des icônes avec la flèche si il n’est pas visible directement <img src="/installation/cg_icone_amd2.png" width="100px" align="center"/> ),
- faire un clic droit de la souris sur le bureau et sélectionner « réglages radeon » dans la liste,
- trouver le logiciel avec la recherche de Windows.

<img src="/installation/cg_paneau_amd.png" width="600px"/>

Cliquez sur « Système »

<img src="/installation/cg_paneau_amd2.png" width="550px"/>

Cliquez sur :

- « Switchable graphics »,
- puis sur « Applications actives »,
- et enfin « Applications disposant de profils installées »,

<img src="/installation/cg_paneau_amd3.png" width="550px"/>

Cliquez sur « parcourir », allez chercher l’exe qgis (ou les différents exe si plusieurs versions utilisées : qgis-bin.exe ou qgis-ltr-bin.exe (dans `C:\Program Files\QGIS 2.14\bin` ou `C:\Program Files\QGIS 2.18\bin`)

<img src="/installation/cg_paneau_amd4.png" width="550px"/>

Cliquez sur l’onglet QGIS qui est ajouté, choisissez « Haute performance ».


### PC FIXES

Pour les PC fixes, bien s’assurer que l’écran est branché sur la carte graphique dédiée le cas échéant, et non sur la sortie graphique de la carte mère.

<img src="/installation/cg_poste_fixe.png" width="550px"/>



# Liens de téléchargement

## Plugin

<!--[hydra.3.1.2.zip](https://hydra-software.net/telechargement/hydra.3.1.2.zip)-->

[hydra.3.1.1.zip](https://hydra-software.net/telechargement/hydra.3.1.1.zip)

[hydra.3.1.0.zip](https://hydra-software.net/telechargement/hydra.3.1.0.zip)

[hydra.3.0.1.zip](https://hydra-software.net/telechargement/hydra.3.0.1.zip)

[hydra.2.6.3.zip](https://hydra-software.net/telechargement/hydra.2.6.3.zip)


## Installeur

<!--[setup_hydra_3.1.2.exe](https://hydra-software.net/telechargement/setup_hydra_3.1.2.exe)-->

[setup_hydra_3.1.1.exe](https://hydra-software.net/telechargement/setup_hydra_3.1.1.exe)

[setup_hydra_3.1.0.exe](https://hydra-software.net/telechargement/setup_hydra_3.1.0.exe)

[setup_hydra_3.0.1.exe](https://hydra-software.net/telechargement/setup_hydra_3.0.1.exe)

[setup_hydra_2.6.3.exe](https://hydra-software.net/telechargement/setup_hydra_2.6.3.exe)

## Lien externes

[PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)



